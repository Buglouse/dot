divert(`-1') #M4
include(`profile.m4') #M4
divert(`0')dnl #M4

[client]
ai_manual_turn_done=TRUE
ask_city_name=TRUE
auto_center_each_turn=TRUE
auto_center_on_combat=FALSE
auto_center_on_unit=TRUE
auto_turn_done=FALSE
center_when_popup_city=TRUE
city_report_angry=FALSE
city_report_attack=FALSE
city_report_build_cost=TRUE
city_report_build_slots=FALSE
city_report_building=TRUE
city_report_cityname=TRUE
city_report_cma=TRUE
city_report_content=FALSE
city_report_corruption=FALSE
city_report_defense=FALSE
city_report_elvis=FALSE
city_report_foodplus=FALSE
city_report_gold=FALSE
city_report_growturns=TRUE
city_report_happy=FALSE
city_report_hstate_concise=FALSE
city_report_hstate_verbose=FALSE
city_report_luxury=FALSE
city_report_nation=FALSE
city_report_output=TRUE
city_report_plague_risk=FALSE
city_report_pollution=FALSE
city_report_present=FALSE
city_report_prodplus=FALSE
city_report_resources=TRUE
city_report_science=FALSE
city_report_scientist=FALSE
city_report_size=TRUE
city_report_specialists=FALSE
city_report_supported=TRUE
city_report_taxman=FALSE
city_report_trade_routes=FALSE
city_report_tradeplus=FALSE
city_report_unhappy=FALSE
city_report_waste=FALSE
city_report_workers=TRUE
concise_city_production=TRUE
default_chat_logfile="freeciv-chat.log"
default_metaserver="http://meta.freeciv.org/metaserver.phtml"
default_server_host="localhost"
default_server_port=5556
default_sound_plugin_name="none"
default_sound_set_name="stdsounds"
default_tileset_name="trident"
default_user_name="M4_USER" #M4
do_combat_animation=FALSE
draw_borders=TRUE
draw_cities=TRUE
draw_city_buycost=FALSE
draw_city_growth=FALSE
draw_city_names=TRUE
draw_city_outlines=TRUE
draw_city_output=TRUE
draw_city_productions=FALSE
draw_city_trade_routes=FALSE
draw_coastline=FALSE
draw_focus_unit=FALSE
draw_fog_of_war=TRUE
draw_fortress_airbase=TRUE
draw_full_citybar=FALSE
draw_irrigation=TRUE
draw_map_grid=FALSE
draw_mines=TRUE
draw_pollution=FALSE
draw_roads_rails=TRUE
draw_specials=TRUE
draw_terrain=TRUE
draw_unit_shields=TRUE
draw_units=TRUE
enable_cursor_changes=FALSE
fullscreen_mode=FALSE
goto_into_unknown=TRUE
gui_gtk2_allied_chat_only=FALSE
gui_gtk2_better_fog=TRUE
gui_gtk2_chatline_autocompletion=TRUE
gui_gtk2_citydlg_xsize=687
gui_gtk2_citydlg_ysize=479
gui_gtk2_default_theme_name="Freeciv"
gui_gtk2_dialogs_on_top=TRUE
gui_gtk2_enable_tabs=TRUE
gui_gtk2_font_beta_label="Sans Italic 10"
gui_gtk2_font_chatline="Monospace 8"
gui_gtk2_font_city_label="Liberation Sans 9"
gui_gtk2_font_city_names="Liberation Sans Bold 9"
gui_gtk2_font_city_productions="Liberation Sans 10"
gui_gtk2_font_comment_label="Sans Italic 9"
gui_gtk2_font_help_label="Sans Bold 10"
gui_gtk2_font_help_link="Sans 9"
gui_gtk2_font_help_text="Monospace 8"
gui_gtk2_font_notify_label="Monospace Bold 9"
gui_gtk2_font_reqtree_text="Liberation Sans 9"
gui_gtk2_font_small="Sans 9"
gui_gtk2_font_spaceship_label="Monospace 8"
gui_gtk2_map_scrollbars=FALSE
gui_gtk2_message_chat_location="MERGED"
gui_gtk2_metaserver_tab_first=FALSE
gui_gtk2_mouse_over_map_focus=TRUE
gui_gtk2_new_messages_go_to_top=FALSE
gui_gtk2_show_chat_message_time=TRUE
gui_gtk2_show_message_window_buttons=FALSE
gui_gtk2_show_task_icons=TRUE
gui_gtk2_small_display_layout=TRUE
gui_gtk3_allied_chat_only=FALSE
gui_gtk3_better_fog=TRUE
gui_gtk3_chatline_autocompletion=TRUE
gui_gtk3_citydlg_xsize=687
gui_gtk3_citydlg_ysize=479
gui_gtk3_default_theme_name="Freeciv"
gui_gtk3_dialogs_on_top=TRUE
gui_gtk3_enable_tabs=TRUE
gui_gtk3_font_beta_label="Sans Italic 10"
gui_gtk3_font_chatline="Monospace 8"
gui_gtk3_font_city_label="Liberation Sans 9"
gui_gtk3_font_city_names="Liberation Sans Bold 9"
gui_gtk3_font_city_productions="Liberation Sans 10"
gui_gtk3_font_comment_label="Sans Italic 9"
gui_gtk3_font_help_label="Sans Bold 10"
gui_gtk3_font_help_link="Sans 9"
gui_gtk3_font_help_text="Monospace 8"
gui_gtk3_font_notify_label="Monospace Bold 9"
gui_gtk3_font_reqtree_text="Liberation Sans 9"
gui_gtk3_font_small="Sans 9"
gui_gtk3_font_spaceship_label="Monospace 8"
gui_gtk3_map_scrollbars=FALSE
gui_gtk3_message_chat_location="MERGED"
gui_gtk3_metaserver_tab_first=FALSE
gui_gtk3_mouse_over_map_focus=TRUE
gui_gtk3_new_messages_go_to_top=FALSE
gui_gtk3_show_chat_message_time=TRUE
gui_gtk3_show_message_window_buttons=FALSE
gui_gtk3_show_task_icons=TRUE
gui_gtk3_small_display_layout=TRUE
gui_sdl_default_theme_name="human"
gui_sdl_do_cursor_animation=FALSE
gui_sdl_fullscreen=FALSE
gui_sdl_screen.height=600
gui_sdl_screen.width=1024
gui_sdl_use_color_cursors=TRUE
gui_win32_better_fog=TRUE
gui_win32_enable_alpha=TRUE
highlight_our_names.background="#ffff00"
highlight_our_names.foreground="#000000"
keyboardless_goto=TRUE
mapimg_filename="freeciv"
mapimg_format="ppm|ppm"
mapimg_layer[MAPIMG_LAYER_AREA]=FALSE
mapimg_layer[MAPIMG_LAYER_BORDERS]=TRUE
mapimg_layer[MAPIMG_LAYER_CITIES]=TRUE
mapimg_layer[MAPIMG_LAYER_FOGOFWAR]=TRUE
mapimg_layer[MAPIMG_LAYER_TERRAIN]=TRUE
mapimg_layer[MAPIMG_LAYER_UNITS]=TRUE
mapimg_zoom=2
migration_gtk3_from_gtk2=FALSE
overview.fog=TRUE
overview.layers[OLAYER_BACKGROUND]=TRUE
overview.layers[OLAYER_BORDERS]=FALSE
overview.layers[OLAYER_BORDERS_ON_OCEAN]=TRUE
overview.layers[OLAYER_CITIES]=TRUE
overview.layers[OLAYER_RELIEF]=FALSE
overview.layers[OLAYER_UNITS]=TRUE
player_dlg_ai=TRUE
player_dlg_attitude=TRUE
player_dlg_border=TRUE
player_dlg_diplstate=TRUE
player_dlg_embassy=TRUE
player_dlg_flag=TRUE
player_dlg_host=FALSE
player_dlg_idle=FALSE
player_dlg_nation=TRUE
player_dlg_ping=FALSE
player_dlg_score=TRUE
player_dlg_show_dead_players=TRUE
player_dlg_show_dead_players=TRUE
player_dlg_state=TRUE
player_dlg_team=TRUE
player_dlg_username=FALSE
player_dlg_vision=TRUE
popup_caravan_arrival=TRUE
popup_new_cities=FALSE
reqtree_curved_lines=TRUE
reqtree_show_icons=FALSE
save_options_on_exit=FALSE
separate_unit_selection=FALSE
smooth_center_slide_msec=200
smooth_combat_step_msec=10
smooth_move_unit_msec=30
solid_color_behind_units=FALSE
sound_bell_at_new_turn=FALSE
unit_selection_clears_orders=TRUE
voteinfo_bar_always_show=FALSE
voteinfo_bar_hide_when_not_player=FALSE
voteinfo_bar_new_at_front=FALSE
voteinfo_bar_use=TRUE
wakeup_focus=TRUE

[messages]
event={"name","where"
"E_AI_DEBUG",0
"E_ANARCHY",2
"E_BAD_COMMAND",2
"E_BROADCAST_REPORT",2
"E_CARAVAN_ACTION",2
"E_CHAT_ERROR",1
"E_CHAT_MSG",1
"E_CITY_AQ_BUILDING",2
"E_CITY_AQUEDUCT",2
"E_CITY_BUILD",0
"E_CITY_CANTBUILD",2
"E_CITY_CMA_RELEASE",2
"E_CITY_DISORDER",2
"E_CITY_FAMINE",2
"E_CITY_FAMINE_FEARED",2
"E_CITY_GRAN_THROTTLE",2
"E_CITY_GROWTH",2
"E_CITY_LOST",2
"E_CITY_LOVE",2
"E_CITY_MAY_SOON_GROW",0
"E_CITY_NORMAL",2
"E_CITY_NUKED",2
"E_CITY_PLAGUE",2
"E_CITY_PRODUCTION_CHANGED",0
"E_CITY_RADIUS_SQ",2
"E_CITY_TRANSFER",2
"E_CIVIL_WAR",2
"E_CONNECTION",1
"E_DESTROYED",2
"E_DIPLOMACY",2
"E_DIPLOMATIC_INCIDENT",2
"E_ENEMY_DIPLOMAT_BRIBE",2
"E_ENEMY_DIPLOMAT_EMBASSY",2
"E_ENEMY_DIPLOMAT_FAILED",2
"E_ENEMY_DIPLOMAT_INCITE",2
"E_ENEMY_DIPLOMAT_POISON",2
"E_ENEMY_DIPLOMAT_SABOTAGE",2
"E_ENEMY_DIPLOMAT_THEFT",2
"E_FIRST_CONTACT",2
"E_GAME_END",2
"E_GAME_START",0
"E_GLOBAL_ECO",2
"E_HUT_BARB",2
"E_HUT_BARB_CITY_NEAR",2
"E_HUT_BARB_KILLED",2
"E_HUT_CITY",2
"E_HUT_GOLD",2
"E_HUT_MERC",2
"E_HUT_SETTLER",2
"E_HUT_TECH",2
"E_IMP_AUCTIONED",2
"E_IMP_AUTO",2
"E_IMP_BUILD",2
"E_IMP_BUY",0
"E_IMP_SOLD",0
"E_LOG_ERROR",1
"E_LOG_FATAL",6
"E_LOW_ON_FUNDS",2
"E_MESSAGE_WALL",2
"E_MY_DIPLOMAT_BRIBE",2
"E_MY_DIPLOMAT_EMBASSY",2
"E_MY_DIPLOMAT_ESCAPE",2
"E_MY_DIPLOMAT_FAILED",2
"E_MY_DIPLOMAT_INCITE",2
"E_MY_DIPLOMAT_POISON",2
"E_MY_DIPLOMAT_SABOTAGE",2
"E_MY_DIPLOMAT_THEFT",2
"E_NATION_SELECTED",0
"E_NEW_GOVERNMENT",2
"E_NEXT_YEAR",0
"E_NUKE",2
"E_POLLUTION",2
"E_REPORT",2
"E_REVOLT_DONE",2
"E_REVOLT_START",2
"E_SCRIPT",6
"E_SETTING",1
"E_SPACESHIP",2
"E_TECH_GAIN",2
"E_TECH_GOAL",2
"E_TECH_LEARNED",2
"E_TREATY_ALLIANCE",2
"E_TREATY_BROKEN",2
"E_TREATY_CEASEFIRE",2
"E_TREATY_EMBASSY",2
"E_TREATY_PEACE",2
"E_TREATY_SHARED_VISION",2
"E_TURN_BELL",2
"E_UNIT_BECAME_VET",2
"E_UNIT_BUILT",2
"E_UNIT_BUILT_POP_COST",2
"E_UNIT_BUY",0
"E_UNIT_LOST_ATT",0
"E_UNIT_LOST_DEF",2
"E_UNIT_LOST_MISC",2
"E_UNIT_ORDERS",2
"E_UNIT_RELOCATED",2
"E_UNIT_UPGRADED",2
"E_UNIT_WIN",2
"E_UNIT_WIN_ATT",0
"E_UPRISING",2
"E_VOTE_ABORTED",1
"E_VOTE_NEW",1
"E_VOTE_RESOLVED",1
"E_WONDER_BUILD",2
"E_WONDER_OBSOLETE",2
"E_WONDER_STARTED",2
"E_WONDER_STOPPED",2
"E_WONDER_WILL_BE_BUILT",2
"E_WORKLIST",0
}
count=110

[cma]
number_of_presets=5#If you add a preset by hand, also update "number_of_presets"
preset={"name","minsurp0","factor0","minsurp1","factor1","minsurp2","factor2","minsurp3","factor3","minsurp4","factor4","minsurp5","factor5","reqhappy","happyfactor"
"Very happy",0,10,0,5,0,0,-20,4,0,0,0,4,FALSE,25
"Max food",-20,25,0,5,0,0,-20,4,0,0,0,4,FALSE,0
"Max production",0,10,-20,25,0,0,-20,4,0,0,0,4,FALSE,0
"Max gold",0,10,0,5,0,0,-20,25,0,0,0,4,FALSE,0
"Max science",0,10,0,5,0,0,-20,4,0,0,0,25,FALSE,0
}

[server]
topology="WRAPX|ISO|HEX"
ysize="72"
xsize="54"
generator="ISLAND"
