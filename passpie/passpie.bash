ID_DB="${HOME}/.auth/identity"
if which passpie >/dev/null; then
    eval "$(passpie complete bash)"
fi
identity_() {
    passpie --config "${HOME}/.passpie/passpie.conf" --database "${ID_DB}" "${@}"
}
