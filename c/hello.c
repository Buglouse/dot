#include <stdio.h>

void foo()
{
  int i;
  char *a[] = {"foo", "Bar"};

  for (i=1; i<sizeof(a); i++)
    { printf("%d %s\n", i, a[i]); }
}

void print_null()
{
  char nul = '\0';

  printf("%d", (10 * nul));

}

int main(int argc, char *argv[])
{
  
  foo();
  
  return 0;
}
