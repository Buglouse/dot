export CONF_WPAS="${HOME}/.wpa_supplicant/wpa_supplicant.conf"
export SOCK_WPA="${DIR_SOCK}/wpa_supplicant.conf"

wpa_supplicant() {
    local dev="${1}"; shift
    sudo $(type -P wpa_supplicant) -Bt \
                              -f "${DIR_TMP_LOG}/wpa_supplicant.log" \
                              -i "${dev}" \
                              -c "${CONF_WPAS}" "${@}"
}
wpa_cli() {
    $(type -P wpa_cli) -p"${SOCK_WPA}" "${@}"
}
wpa_gui() {
    $(type -P wpa_gui) -p"${DIR_SOCK}" "${@}"
}
