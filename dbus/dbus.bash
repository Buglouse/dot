# Spotify
SPOTIFY_DEST="org.mpris.MediaPlayer2.spotify"
MPRIS="/org/mpris/MediaPlayer2"
dbus() {
    dbus-send --print-reply --dest="${SPOTIFY_DEST}" "${MPRIS}" "${@}"
}
spotify_play() {
    dbus org.mpris.MediaPlayer2.Player.PlayPause
}
spotify_next() {
    dbus org.mpris.MediaPlayer2.Player.Next
}
spotify_prev() {
    dbus org.mpris.MediaPlayer2.Player.Previous
}
spotify_stop() {
    dbus org.mpris.MediaPlayer2.Player.Stop
}
