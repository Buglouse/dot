#!/usr/bin/env perl -w
#
# XResources
# *.perl-ext-common: clipboard
# *.keysym.: perl:clipboard:copy
# *.keysym.: perl:clipboard:paste
# *.keysym.: perl:clipboard:paste_escaped
# *.copyCommand:
# *.pasteCommand:
# *.perl-lib: /path

use strict;

sub on_start{
    my ($self) = @_;

    $self->{cmd_yank} = $self->x_resource('cmd_yank') || 'false';
    $self->{cmd_paste} = $self->x_resource('cmd_paste') || 'false';
    ()
}

sub yank{
    my ($self) = @_;

    if (open(CLIPBOARD, "| $self->{cmd_yank}")) {
            my $contents = $self->selection();
            utf8::encode($contents);
            print CLIPBOARD $contents;
            close(CLIPBOARD);
    } else {
            print STDERR "Invalid: '$self->{cmd_yank}'\n$!\n";
    }
    ()
}

sub paste{
    my ($self) = @_;

    my $selection = `$self->{cmd_paste}`;
    if ($? == 0) {
            $self->tt_paste($selection);
    } else {
            print STDERR "Invalid: '$self->{cmd_paste}'\n$!\n";
    }
    ()
}

sub paste_escaped{
    my ($self) = @_;

    my $selection = `$self->{cmd_paste}`;
    if ($? == 0) {
            $selection =~ s/([!#\$%&\*\(\) ='"\\\|\[\]`~,<>\?])/\\\1/g;
            $self->tt_paste($selection);
    } else {
            print STDERR "Invalid: '$self->{cmd_paste}'\n$!\n";
    }
    ()
}

sub on_user_command{
    my ($self, $cmd) = @_;

    if ($cmd eq "clipboard:yank") {
        $self->yank;
    } elsif ($cmd eq "clipboard:paste") {
        $self->paste;
    } elsif ($cmd eq "clipboard:paste_escaped") {
        $self->paste_escaped;
    }
    ()
}
