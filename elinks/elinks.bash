export CONF_ELINKS="${HOME}/.elinks/elinks.conf"
export BROWSER_CSS="${HOME}/.elinks/elinks.css"
export CMD_YANK="echo -n %c |xsel -i"
elinks() {
local cmd=$(type -o elinks)
${cmd} -config-dir ${HOME]/.elinks -config-file elinks.conf ${@}
}

