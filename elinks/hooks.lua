enable_systems_functions ()

 function lua_console_hook (expr)
 	return "eval", expr
 end

  function url_encode(text)
       text = gsub(text, " ", "%%20")
       text = gsub(text, "%:", "%%3a")
       text = gsub(text, "%/", "%%2f")
       text = gsub(text, "%||", "%%7c")
       text = gsub(text, "%!", "%%21")
       text = gsub(text, "%**", "%%aa")
       text = gsub(text, "%&", "%%26")
       return gsub(text, "%#", "%%23")
  end
  
  function follow_url_hook(url)
       -- Detecting ed2k links
       if strfind(url,'ed2k.*%||file%||') then
         text = url_encode(gsub(url, "(.-)ed2k.'''file%||(~[[^]]''')%||(%d''')%||(%x''').*", "dllink ed2k://||file||%2%||%3%||%4%||"))
         return "~http://127.0.0.1:4080/submit?q="..text
       end
  
       -- Detecting ~BitTorrent links
       if strfind(url,'.*%.torrent$') then
         return "~http://127.0.0.1:4080/submit?q=dllink%20"..url_encode(url)
       end
  
       -- Return url without changes.
       return url
  end

--    function to_xsel (url)
--    	execute("echo -n \""..url.."\" | xsel -i |echo "true" >>/tmp/elinks2.log")
--    end
--    
--  bind_key ('main', 'V', function () to_xsel(current_url ()) end)
--  bind_key ('main', 'L', function () to_xsel(current_link ()) end)
