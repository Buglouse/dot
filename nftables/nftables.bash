
export NFT_CONF="${HOME}/.nftables/policy.nft"

nft_flush() {
    sudo nft flush ruleset
}
nft_load() {
    sudo nft -f "${NFT_CONF}"
}
nft_save() {
    sudo nft list ruleset >policy.nft
}
