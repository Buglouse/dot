# Spotify
spotify() {
    $(type -P spotify) --username="${AUTH_SPOTIFY_USER}" --password="${AUTH_SPOTIFY_PASS}" "${@}"
    }
