#!/bin/env bash
shopt -s extglob

# Device Input
## Touchpad
lockX() {
    #xl a
    slock echo mem|sudo tee /sys/power/state 2>/dev/null
}

# Suspend
power_suspend() {
    # https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-power
    # state: mem (suspend), standby (power-on-suspend), freeze (suspend-to-idle), disk (hibernation).
    # pm_print_times: [0|1] Time of devices suspend|resumed.
    local dir="${DIR_SYSFS}/power/state"
    local types=( $(<"${dir}") )
    local action='mem'

    #+([[:digit:]:])) time=('-t' "$(date -ud "${1}" '+%s')") ;; # Epoch
    #+([[:digit:]])) time=('-s' "${1}") ;;
    ##rtcwake -u -m "${action}" ${time[@]} || return
    echo "${action}" |sudo tee "${dir}" 2>/dev/null
}

# Internet
select_() {
    # Fix \b in select
    # Return: REPLY
    # Offset 1
    local count items=( ${@} ) prompt="${PS3:-?: }"

    for (( count=0; count != ${#items[@]}; count++ )); do
        printf "%s) %s\n" $(( ${count} + 1 )) "${items[count]}"
    done
    read -e -p "${prompt}"
    (( REPLY-- ))
    case ${REPLY} in
        ''|*[!0-${#items[@]}]*) return 1 ;;
        *) REPLY="${items[${REPLY}]}" ;;
    esac
}
net() {
    local items=( $(devices_class_net) )
    #select item in "${items[@]}"; do break; done
    (( ${#items[@]} > 1 )) && { select_ "${items[@]}" || return; } || REPLY="${items}"
    if [[ ${REPLY} = wl* ]]; then
        ps aux |grep wpa_supplicant &>/dev/null && sudo pkill -SIGHUP wpa_supplicant || wpa_supplicant "${REPLY}"
    fi
    dhcpcd "${REPLY}"
    ntp_update
}
cert() {
    # address:port
    openssl s_client -connect "${1}" -showcerts </dev/null 2>/dev/null |
        openssl x509 -outform PEM
}
nat() {
    # https://wiki.nftables.org/wiki-nftables/index.php/Performing_Network_Address_Translation_(NAT)
    # https://wiki.gentoo.org/wiki/Nftables/Examples
    # https://blog.cryptomilk.org/2007/07/12/qemu-kvm-internal-network-setup/
    # http://blog.elastocloud.org/2015/07/qemukvm-bridged-network-with-tap.html
    # Stateful NAT
    #10.0.0.0/24 dev <dev> proto kernel src 10.0.0.7 metric 202
    local route=( $(ip route list scope link) )
    local dev="${route[2]}"
    local src="${route[0]}"
    local dst="${route[6]}"

    sudo nft add table nat
    sudo nft add chain nat prerouting { type nat hook prerouting priority 0 \;}
    sudo nft add chain nat postrouting { type nat hook postrouting priority 100 \; }
    # Source
    #sudo nft add rule nat postrouting ip saddr "${dst}" oif "${dev}" snat "${src}"
    #sudo nft add rule nat postrouting ip saddr "${src}" oif "${dev}" masquerade
    #sudo nft add rule nat oifname {IFS=$',' ${devs[*]} } postrouting masquerade
    sudo nft add rule nat postrouting oifname "${dev}" masquerade
    # Destination
    # sudo nft add rule nat prerouting iif "${dport { port, port } dnat "${dst}"
    # sudo nft add rule nat prerouting dport { port, port } dnat "${dst}"

}
ix() {
    curl -F 'f:1=<-' -F 'read:1=3' ix.io
}
ip_ext() {
    url="https://ident.me"
    ip4=$(curl -4s "${url}")
    ip6=$(curl -6s "${url}")
    printf "%s\n%s\n" "${ip4}" "${ip6}"
    #nc 4.ifcfg.me 23| grep IPv4| cut -d' ' -f4
    #dig +short @resolver1.opendns.com ANY myip.opendns.com
}
dropbox() {
    dbxfs "${HOME}/mnt/dropbox"
}
# Browser
elinks() {
    type -P elinks 1>/dev/null &&
        $(type -P elinks) -config-dir "${CONF_ELINKS%/*}" \
                          -config-file "${CONF_ELINKS##*/}" "${@}"
}
browser_editable() {
    ${BROWSER_X} 'data:text/html, <html contenteditable>'
}
browser_reload() {
    local cur="$(xdotool getwindowfocus)"
    local oth="$(xdotool search --onlyvisible --class ${BROWSER_X}|head -1)"
    xdotool windowactivate ${oth}
    xdotool key 'ctrl+r'
    xdotool windowactivate ${cur}
}
# Time
ntp_update() {
    #sudo ntpd -s && sudo hwclock -w
    type -P sntp &>/dev/null && sntp pool.ntp.org &>/dev/null && sudo sntp -sS pool.ntp.org &>/dev/null
    sudo hwclock --systz
    sudo hwclock --systohc
}

# Helper
colors() {
    local item
    for item in {0..16}; do
        printf '\x1b[38;5;%smcolor%s  \t' "${item}" "${item}"
        (( item == 7 )) && printf '\n'
    done
    printf '\n'
    for item in {16..255}; do
        printf '\x1b[38;5;%smcolor%s \t' "${item}" "${item}"
        (( item%8 )) || printf '\n'
    done
    printf '%b\n' "${NORMAL}"
}
pdf_font() {
    for f in "${@}"; do
        strings "${f}" |
            grep "FontName"
    done
}
is_running() {
    # [n]otify
    ps aux| grep -qsi -- "${1}"
}
calc() {
    awk "BEGIN{ print $* }"
}
# Initrd|Kernel
image-mount() {
    [[ -e ${1} ]] || return
    sudo mount -oloop,ro "${1}" "${2:-/mnt/iso}"
}
image-pack() {
    local out="${2:-${DIR_TMP_DATA}/image.iso}"
    [[ ${1} ]] || return
    #ISO
    mkisofs -l -J -r -V Version -no-emul-boot -boot-load-size 4 -boot-info-table \
            -b boot/isolinux/isolinux.bin \
            -c boot/isolinux/boot.cat \
            -o "${1}" "${out}"
}
initrd-unpack() {
    local out="${2:-initrd}"
    local file="${1}"
    [[ ${file} = /* ]] || file="${PWD}/${1}"
    [[ -e ${file} ]] || return
    mkdir -p "${out}"
    pushd "${out}" >/dev/null
    zcat "${file}" |sudo cpio -i -H newc -d
    popd >/dev/null
}
initrd-pack() {
    local out="${2:-${DIR_TMP_DATA}/initrd.cpio.gz}"
    [[ ${1} ]] || return
    pushd "${1}" >/dev/null
    sudo find . | sudo cpio -o -H newc | gzip -2 > "${out}"
    #advdef -z4 "${out}"
    popd >/dev/null
}
qemu-kernel() {
    qemu -nographic -kernel "${1}" -initrd "${2}" ${3:+-hda ${3}} -append console=ttyS0
}

#SysFS
fan_find() {
    local item items
    for item in /sys/class/hwmon/hwmon*; do
        [[ -e ${item}/pwm1 && -e ${item}/fan1_input && -e ${item}/pwm1_enable ]] || continue
        items=( ${items[@]} ${item} )
        #/sys/class/hwmon/hwmon#
    done
    #(( ! ${#items[@]} ))
    echo ${items[@]}
}
fan_set() {
    local fan max
    for fan in $(fan_find); do
        [[ -e ${fan}/pwm1_enable ]] || continue
        echo 1| sudo tee "${fan}/pwm1_enable" >/dev/null
        if [[ ${1} ]]; then
            echo "${1}" |sudo tee "${fan}/pwm1"
        else
            max="5000" # poll from what without testing setting
            read -e -p "Divisor: "
            echo "$(( max / REPLY ))" |sudo tee "${fan}/pwm1" >/dev/null
            printf '%s\n' "$(<${fan}/fan1_input)"
        fi
    done
}

#SSH
ssh-fingerprint() { ssh-keygen -lf "${1}" -E md5; }
#PMS
##Pacman
pacman_list() {
    local e
    (
        for e in /var/lib/pacman/local/*/desc; do
            egrep -A1 '%(NAME|SIZE)' $e |
                gawk '/^[a-z]/ { printf "%s ", $1 }; /^[0-9]/ { printf "%.0f\n", $1/1024 }'
        done |sort -nrk2
    ) |column -t
}

####

guile() {
    local guile="$(type -P guile)"
    XDG_CACHE_HOME="${HOME}/.cache" ${guile} -q -l "${HOME}/.guile_user/init.scm" "${@}"
}

# Editor
## PDF
extract_pdf_image_pages() {
    convert -verbose -density 400 -depth 16 \
            "${1}[${2}-${3}]" -set filename:page "%[fx:t+i]" \
            -quality 100 -sharpen 0x1.0 output-%[filename:page].png
    #-trim
}
extract_pdf_image() {
    local item count

    for item in "${@}"; do
        convert -verbose -density 400 -depth 16 \
                -quality 100 -sharpen 0x1.0 "${item}" "output_${count}.png"
        (( count++ ))
    done
}
pdf_merge_crop() {
    gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -dQUIET -sOutputFile=print.pdf "${@}"
    gs -sDEVICE=bbox -dBATCH -dNOPAUSE -dQUIET print.pdf
}

# Devices
## USB
device_usb_suspend() {
    local item="/dev/${1}"
    [[ -e ${item} ]] || return
    udisks --unmount "${item}" && udisks --detach "${item}"
}
## Temp
temp_find_() {
    local item
    local pathThermal='/sys/class/thermal'
    for item in ${pathThermal}/thermal_zone*; do
        [[ -e ${item}/temp ]] || continue
        pathThermal="${item}"
    done
    local pathTemp="${pathThermal}/temp"
    [[ -e ${pathTemp} ]] || return
    export THERMAL_TEMP="${pathTemp}"
}
fan_set_() {
    local item status speed
    fan_find

    if [[ ${1} == 0 ]]; then
        status=2
        speed=0
    else
        status=1
    fi
    printf '%s' "${status}" |sudo tee "${PWM_STATUS}" >/dev/null

    if [[ ! ${speed} ]]; then
        read -p 'PWM Value (0-255)? ' speed
        [[ ${speed} == *([0-9]) ]] || return
    fi
    echo "${speed}" |sudo tee "${FAN_PWM}" >/dev/null
    fan_status
}
fan_status_() {
    fan_find
    printf '%s:%s\n' "${FAN_PWM}" "$(<"${FAN_PWM}")"
    printf '%s:%s\n' "${FAN_FAN}" "$(<"${FAN_FAN}")"
}
temp_status_() {
    temp_find
    printf '%s:%s\n' "${THERMAL_TEMP}" "$(<"${THERMAL_TEMP}")"
}
fan_temp_info() {
    while :; do
        fan_status
        temp_status
        sleep 5
    done
}
## SysFS
### class/net
devices_class_net() {
    # Find available sysfs network devices
    local item devs items=( ${DIR_SYSFS}/class/net/!(lo) )
    #select item in $(realpath "${items[@]}/device"); do
    (( ${#items[@]} )) || return
    for item in "${items[@]}"; do
        #[[ $(realpath "${item}") == +(*/virtual/*) ]] && continue
        devs+=( "${item}" )
    done
    printf '%s ' "${devs[@]##*/}"
}

## Microphone
mic() {
    ffmpeg -threads -y \
           -f alsa -ac 2 -i hw:0,0 \
           -acodec pcm_s16le \
           -acodec flac -aq 499 \
           mic.flac
}

# Media
youtube_writeout() {
    local item itemS args

    for item in "${@}"; do
        unset sub itemS

        [[ -e ${item} ]] || continue
        [[ -d ${item} || ${item} == *+(.srt) ]] && continue
        (( $(stat -c '%s' "${item}") > 1 )) || continue

        itemS=( "${item%.*}"*+(.srt) )
        [[ -e ${itemS} ]] && sub="${itemS}"

        printf '%s\n' "${item}"
        mplayer -osdlevel 3 -speed 1.33 \
                ${sub+-sub "${sub}"} "${item}" &>/dev/null || continue
        { set +C
          read -t 3 -N 1 -p "Writeout? "
          [[ ${REPLY} == 'y' ]] && {
              for item2 in flv webm mp4; do >"${item%.*}.${item2}"; done
              [[ -e ${itemsS} ]] && >"${itemS}"; }
          set -C; }
        printf '\n'
    done
}
## Audio
mp() {
    mplayer -cache 2048 -playlist "${@}"
}
convert_ogg-192() {
    local item path file

    for item in "${@}"; do
        [[ -d ${item} ]] && continue
        if [[ ${item} == +(*/*) ]]; then
            a="${item%/*}"
            path="${a##*/}/ogg"
        else
            path="./ogg"
        fi
        file="${item##*/}"

        mkdir -p "${path}" 2>/dev/null
        ffmpeg -n -i "${item}" -acodec libvorbis \
               -af volume=1.175 -vn -ac 2 -sample_rate 41000 -b:a 192k \
               "${path}/${file%.*}.ogg"
    done
}
convert_ogg-192-2() {
    local item path

    for item in "${@}"; do
        path="${item%/*}"

        mkdir -p "${path}"
        oggenc --resample 41000 --bitrate 192 \
               --output="./ogg/${item%.*}.ogg" "${item}"
    done
}
convert_mp3-192() {
    local item

    mkdir './mp3'
    for item in "${@}"; do
        ffmpeg -n -i "${item}" -acodec libmp3lame \
               -vn -ac 2 -sample_rate 41000 -b 192k \
               "./mp3/${item%.*}.mp3"
    done
}
extract_audio() {
    local item codecType codecName

    for item in "${@}"; do
        [[ -f ${item} ]] || continue
        while read; do
            [[ ${REPLY} == +(codec_type=*) ]] && codecType="${REPLY#*=}"
            [[ ${REPLY} == +(codec_name=*) ]] && codecName="${REPLY#*=}"
            [[ ${codecType} == +(*audio) && ${codecName} ]] && break
        done <<<"$(ffprobe -v quiet -show_streams "${item}")"
        [[ ${codecType} == +(audio*) ]] || continue

        case "${codecName}" in
            aac) codecName='m4a' ;;
            wmva2) codecName='wma' ;;
            vorbis) codecName='ogg' ;;
        esac

        ffmpeg -i "${item}" -vn -acodec copy "${item%.*}.${codecName}"
    done
}

# Game
freeciv() {
    freeciv-gtk2 \
        --log "${LOG_FREECIV}" \
        "${@}"
}
spring() {
    mkdir -p "${DIR_TMP_CACHE}/spring" >/dev/null
    $(type -P spring) \
        --config "${CONF_SPRING}" \
        "${@}"
}
springlobby() {
    $(type -P springlobby) \
        --config-file="${CONF_SPRINGLOBBY}" "${@}"
}

# Internet
## Authentication
vnstat() {
    local cmd="$(type -P vnstat)"

    ${cmd} --config "${CONF_VNSTAT}" "${@}"
}
## Browser
## EMail
## Get
get_magnet() {
    local item item2

    for item in "${@}"; do
        [[ ${item} == *+(xt=urn:btih:)* ]] || continue
        [[ ${item} == +(http?(s)://)* ]] && item="${item##*/}"
        item2="${item%%&*}"
        item2="${item2##*:}"
        printf 'd10:magnet-uri%s:%se' "${#item}" "${item}" \
               >"${DIR_TMP_DATA}/meta-${item2^^}.torrent"
    done
}
get_links() {
    local opts item item2 items path="." gz

    for item in "${@}"; do
        if [[ ${item} == -* ]]; then
            opts+=( ${item} )
        elif [[ -d ${item} ]]; then
            path="${item}"
        elif [[ -f ${item} ]]; then
            items+=( $(<"${item}") )
        else
            #true
            items+=( "${item}" )
        fi
    done

    for item in "${items[@]}"; do
        [[ ${item} == http?(s)://* ]] || continue
        item2="${path}/${item#http?(s)://}"
        if [[ ${item} == */ ]]; then
            wget -t3 --waitretry=3 -kxcrl1 \
                 ${opts[@]} -P "${path}" -k "${item%/}"
        else
            [[ ${FORCE} ]] ||
                case "${item}" in
                    *.gz|*.bz2) [[ -e ${item2} ]] && continue ;;
                    *) [[ -e ${item2}.gz ]] && continue ;;
                esac

            wget -t3 --waitretry=3 -Lkxc \
                 ${opts[@]} -P "${path}" -k "${item}" &&
                [[ ${item2} == *.html ]] && gzip "${item2}"
        fi
    done
}
get_stream() {
    local stream="${DIR_TMP_DATA}/stream"
    wget -O - "${1}" |
        tee "${stream}" |
        mplayer -cache 8192 -cache-min 1 -
}
youtube() {
    youtube-dl --ignore-errors --retries 4 --continue --no-overwrites \
               --prefer-free-formats --write-sub --sub-lang en \
               --sub-format srt --sub-lang en --write-sub --write-auto-sub \
               "${@}"
    #--output "${PWD}/youtube/${FORMAT_YOUTUBE}" \
        #--username "${YOUTUBE_COM_USER}" --password "${YOUTUBE_COM_PASS}" \
        }
rtorrent() {
    for item in "${DIR_TMP_CACHE}/rtorrent/session" "${DIR_TMP_LOG}/rtorrent"; do
        [[ -d ${item} ]] || mkdir -p "${item}"
    done
    sed -i 's/#M4//g' "${CONF_RTORRENT}"
    $(type -P rtorrent) -o import="${CONF_RTORRENT}"
    #-o schedule=import,0,120,try_import="${CONF_RTORRENT}"
}
network_bandwidth_down() {
    local item item2 items=(
        "http://cachefly.cachefly.net/100mb.test"
        "http://speedtest.wdc01.softlayer.com/downloads/test500.zip"
        "http://speedtest.reliableservers.com/10MBtest.bin"
    )
    select item in "${items[@]}"; do
        break
    done
    item2="${item#http://}"
    item2="${item2%%/*}"
    geoiplookup "${item2%/*}" -f /etc/geoip/GeoLiteCity.dat -i
    curl --output '/dev/null' "${item}"
}
shoutcast() {
    python3 ~/data/tmp/shoutcast-search --limit=50 --sort=l --bitrate=">96" \
            --format="[%s](%p)%bZ_%t %u" "${@}"
}
ipfilter() {
    wget "http://upd.emule-security.org/ipfilter.zip"
    7z x ipfilter.zip
    mv guarding.{p2p,dat}
}
## Put
google_bookmark() {
    curl -v
    -k -b "${DIR_TMP_CACHE}/google.cookie" -b "${DIR_TMP_CACHE}/google.cookie" \
       -s 'https://www.google.com/accounts/ServiceLogin?service=bookmarks&passive=true&nui=1&continue=https://www.google.com/bookmarks/l&followup=https://www.google.com/bookmarks/l'
}

# System
## Process
ps_cgroup() {
    ps xawf -eo pid,user,cgroup,args
}
wait_for() {
    #local item
    [[ ${#} ]] || return

    #for item in "${@}"; do
    trap 'return 0' USR1
    (
        trap '' USR1
        exec ${@}
    ) &
    wait
    #return 1
    #done
}
loop() {
    local item="${1}"; shift
    (( item )) || return
    while clear; do
        ${@}
        sleep "${item}"
    done
}
switch_tty() {
    # Suspend
    bg
    disown %1
    reptyr $PID
    # echo 0|sudo tee /proc/sys/kernel/yama/ptrace_scope
    # echo 1|sudo tee /proc/sys/kernel/yama/ptrace_scope
}
## Disk
disk_test() {
    local device="${1#/dev/}"
    local deviceModel pid
    local REPLY item items

    [[ -f /sys/class/block/${device}/device/model ]] || return
    deviceModel=( $(</sys/class/block/${device}/device/model) )
    device="/dev/${device}"
    [[ -b ${device} ]] || return

    printf 'Log file: %s\n' "${PWD}/${deviceModel}.*"

    read -s -e -p "Clear; ${device}/${deviceModel}?" -n1
    if [[ ${REPLY} == +([yY]) ]]; then
        time { sudo dd if='/dev/zero' of="${device}" bs=8M; }
        pid="${!}"
        unset REPLY
        [[ ${REPLY} ]] || REPLY='y'
        while [[ ${REPLY} == +([yY]) ]]; do
            kill -s USR1 "${pid}"
            read -e -s -p 'Print Log?'
        done
    fi

    read -e -s -p "SMART Test; ${device}/${deviceModel}?" -n1
    if [[ ${REPLY} == +([yY]) ]]; then
        log_redirect 'sudo smartctrl --all "${device}"' "${deviceModel}.log"

        for item in 'short' 'long'; do
            sudo smartctl --test="${item}" "${device}"
            unset REPLY
            [[ ${REPLY} ]] || REPLY='y'
            while [[ ${REPLY} == +([yY]) ]]; do
                sudo smartctl --attributes --log=selftest "${device}"
                read -e -s -p 'Print Log?'
            done
        done
    fi

    read -e -s -p "Badblocks Test; ${device}/${deviceModel}?" -n1
    if [[ ${REPLY} == +([yY]) ]]; then
        sudo badblocks -w -o "${deviceModel}.badblocks" -sv "${device}"
    fi

    read -e -s -p "Read test; ${device}/${deviceModel}?" -n1
    if [[ ${REPLY} == +([yY]) ]]; then
        log_redirect "sudo hdparm -t ${device}" "${deviceModel}.log"
    fi

    read -e -s -p "Write test; ${device}/${deviceModel}?" -n1
    if [[ ${REPLY} == +([yY]) ]]; then
        log_redirect "sudo dd count=25 bs=8M if='/dev/zero' of=${device} oflag=sync" "${deviceModel}.log"
    fi
}
## Benchmark
bench_loop() {
    while :; do date --utc; done |
        uniq -c
}
### Locate
updatedb() {
    sudo $(type -P updatedb) --output "${FILE_MLOCATE_DB}" "${@}"
}
locate() {
    $(type -P locate) --database "${FILE_MLOCATE_DB}" "${@}"
}
man() {
    $(type -P man) --manpath="${PATH_MAN}" "${@}"
}
## Log
log_redirect() {
    local item
    local file="${@:${#}}"
    local cmds=( "${*:1:${#}-1}" )
    local IFS=';'; cmds=( ${cmds[*]} ); unset IFS


    for item in "${cmds[@]}"; do
        printf '\n#> %s\n' "${item/+(sudo+( )|?( ))/}"
        eval "${item}" 2>&1 |
            while read; do
                printf '\t%s\n' "${REPLY}"
            done
    done &>>"${file}"
}
log_net() {
    local count
    local log="${DIR_TMP_CACHE}/network.log"

    while :; do
        clear
        echo "$(<"${log}")"
        {
            log_wifi
            log_listen
        } >"${log}"
        sleep 2
    done
    rm "${log}"
}
log_wifi() {
    printf '# Interface\n'
    ip addr show scope global
    printf '# Network Association\n'
    iwconfig 2>&1 |grep -v lo
    printf '# wpa_supplicant\n'
    sudo tail -n 6 "${LOG_WPASUPPLICANT}"
    printf '# DHCP\n'
    journalctl --no-pager --quiet -n 10 -u dhcpcd
    printf '# Route\n'
    #ip route list scope global
    ip route list |grep -v 'lo'
    #grep . /etc/resolv.conf
}
log_listen() {
    printf '# Servers\n'
    sudo netstat -tulpn |tail -n '+2'
    printf '# Connections\n'
    sudo netstat -tunp |tail -n '+3'
}
log_iptables() {
    local time host level indev outdev mac src dst rest
    local items=( ${*} )
    [[ ${items} ]] || items='.*'

    printf '# Iptables\n'
    journalctl --no-pager --output short-iso |grep "IN=.*OUT=" |grep -iE "${items[*]}" |
        while read time host level indev outdev mac src dst rest; do
            if [[ ${mac} == +(MAC=.*) ]]; then
                printf '%s\n\t%s %s %s %s %s\n\t%s\n' \
                       "${time}" "${indev}" "${outdev}" "${mac}" "${src}" "${dst}" "${rest}"
            else
                printf '%s\n\t%s %s %s %s\n\t%s\n' \
                       "${time}" "${indev}" "${outdev}" "${src}" "${dst}" "${rest}"
            fi
        done
}
log_voltage() {
    local path='/sys/class/power_supply/BAT0/uevent'
    local volt
    local temp="$(mktemp -p "${DIR_TMP_CACHE}" voltage.XXX)"

    printf '%s\n' "${temp}"
    while :; do
        volt=( $(<${path}) )
        volt=( "${volt[6]#*=}" "${volt[7]#*=}" )
        volt=( "${volt[0]%000}" "${volt[1]%000}" )

        printf "%s:%s%b%s%b%s%s%b%s%b\n" \
               "$(date '+%H%M%S')" \
               'Voltage=f' "${YELLOW}" "${volt[0]}" "${NORMAL}" \
               "/" \
               'Current=' "${YELLOW}" "${volt[1]}" "${NORMAL}" \
            | tee "${temp}"
        sleep .5
    done
}
## Server
cpu_headless() {
    LC_ALL=C lscpu|grep Virtualization
    grep -E --color=auto 'vmx|svm|0xc0f' /proc/cpuinfo
    egrep -c '(vmx|svm)' /proc/cpuinfo
    ls /dev/kvm
    zgrep CONFIG_KVM /proc/config.gz
    lsmod|grep kvm

    zgrep VIRTIO /proc/config.gz
    lsmod|grep virtio
}
sv_run() {
    virsh net-list
    #virsh net-create net.xml
    virsh net-edit
    virsh net-autostart default
    virsh net-start default
    brctl show
    ip link delete virbr0-nic master virbr0
    # wifi
    ebtables -t nat -A POSTROUTING -o wl_dev -j snat --to-src $MAC_OF_BRIDGE --snat-arp --snat-target ACCEPT
    ebtables -t nat -A PREROUTING -p IPv4 -i wl_dev --ip-dst $IP -j dnat --to-dst $MAC --dnat-target ACCEPT
    ebtables -t nat -A PREROUTING -p ARP -i wl_dev --arp-ip-dst $IP -j dnat --to_dst $MAC --dnat-target ACCEPT
    ip link set dev master virbr0
    qemu -enable-kvm
}
sv_install() {
    sudo pacman -S qemu-headless libvirt
    sudo systemctl enable libvirtd
    sudo systemctl start libvirtd
    sudo virt-install --name One --ram=512 --vcpus=1 --cpu host -hvm --disk path=/var/lib/libvirt/images/one,size=8 --cdrom /var/lib/libvirt/book/one.iso
}
sv_create() {
    local loop
    local img="${PWD}/image.img"
    truncate --size 25g "${img}"
    loop="$(losetup --show -f ${img})"
    image-bootstrap --hostname host --password pass arch "${loop}" || return
    losetup -d "${loop}"
    systemd-nspawn -bi "${img}"
}
sv_init() {
    local ret
    # Disable cloud-init
    for srv in "cloud-init" "cloud-init-local" "cloud-config" "cloud-final"; do
        ret="$(systemctl is-enabled ${srv})"
        if [[ "${ret}" == "enabled" ]]; then
            echo
            #systemctl disable "${srv}"
        fi
    done

    git clone https://bitbucket.org/buglouse/ucs.git
    git clone -n https://bitbucket.org/buglouse/dot.git
    git checkout master bash readline
    rm /etc/resolv.conf
    ln -s /run/systemd/resovle/stub-resolv.conf /etc/resolv.conf
    vi /etc/locale.gen
    locale-gen
    systemctl enable systemd-networkd
    systemctl enable systemd-resolved

    useradd -m ssh
    cp -r /root/.ssh /home/ssh
    chown -R ssh:ssh /home/ssh/.ssh

    # OpenSSH
    if [[ $(type -p sshd) ]]; then
        echo
    fi
}
sv_irc() {
    pacman-key --init
    pacman-key --populate
    pacman -S znc sudo
    sudo -u znc znc --makeconf
}
sv_sql() {
    #loginctl enable-linger user
    #/etc/systemd/nspawn/sql.nspawn { [Network] \ Port=5432 }
    pacman -Sy postgresql
    cd dot
    git checkout master postgresql
    #useradd -d /var/lib/postgres/tryton -m tryton
    su -l postgres
    initdb -D /var/lib/postgres/data/db
    mkdir -p ~/.config/systemd/user
    cp /usr/lib/systemd/system/postgresql.service ~/.config/systemd/user/
    createdb db
}
container_create() {
    local path="${1}"

    mkdir -p "${1}" &>/dev/null
    pacstrap -icdG "${1}" base git \
             --ignore linux,linux-firmware,nano,man-db,man-pages,licenses \
             --ignore logrotate,diffutils,inetutils,dhcpcd,jfsutils,lvm2,mdadm,pciutils,pcmciautils,reiserfsprogs,usbutils,xfsprogs
    printf "cp %s > /var/lib/machines; systemd-nspawn -bD ${1}\n" "${1}; machinectl start <>"
}
## Install
### Arch
install_arch() {
    # Keyboard Layout
    # /usr/share/kdb/keymaps/**/*.map.gz
    #loadkeys us|dvorak
    # /usr/share/kdb/consolefonts
    #setfont
    # /sys/firmware/efi/efivars
    # Network
    # Time
    #timedatectl set-ntp true
    #timedatectl
    # Partition
    # BIOS 1M
    # EFI 550MB
    # Root
    # Swap 5% root
    # Size 512*4=1M*(sizeM?)
    # Flush
    sgdisk -Z /dev
    # GPT table
    sgdisk -og /dev
    # BIOS Boot
    sgdisk -n 1:2048:4095 -c 1:"BIOS" -t 1:ef02 /dev
    # EFI Boot
    sgdisk -n 2:4096:413695 -c 2:"EFI" -t 2:ef00 /dev
    # Boot
    sgdisk -n 3:413696:823295 -c 3:"boot" -t 3:8300 /dev
    # SWAP
    free=$(free -b |grep -i "mem" |cut -d' ' -f5)
    if [[ -n ${free} ]]; then
        free=$(free -b |grep -i "mem" |cut -d' ' -f6)
    fi
    [[ ${free} ]] || return
    free=$((${free}/512))
    sgdisk -n 4:823296:${free} -c 4:"swap" -t 4:8200 /dev
    # Recovery
    sgdisk -n 5:$((${free}+1)):$(($(sgdisk -F /dev)+(512*4*5000))) -c 5:"Recovery" -t 5:8e00 /dev
    # Root
    sgdisk -n 6:$(sgdisk -F /dev):$(sgdisk -E /dev) -c 6:"root" -t 6:8e00 /dev
    sgdisk -A=2:set:2 /dev/
    sgdisk -p

    mkfs.vfat -F 32 -n EFI /dev/sda2
    mkfs.vfat -F 32 -n BOOT /dev/sda3
    mkswap -L swap /dev/sda4
    mkfs.ext4 -L Recovery /dev/sda5
    mkfs.ext4 -L root /dev/sda6

    swapon /dev/sda4
    mount /dev/sda5 /mnt/root
    mkdir -p /mnt/root/boot
    mount /dev/sda3 /mnt/root/boot
    mkdir -p /mnt/root/boot/EFI/BOOT/
    mount /dev/sda2 /mnt/root/boot/EFI/BOOT


    # /etc/pacman.d/mirrorlist
    # server; development; desktop
    pacstrap /mnt base efibootmgr grub sudo rxvt-unicode git tmux emacs fvwm xorg elinks wpa_supplicant firefox openssh tigervnc xsel alsa-utils alsa-tools mplayer feh evince sysstat x11vnc qemu p7zip connman cmst
    # NetworkManager lxqt sddm gimp libreoffice
    # (AUR) wpa_cute tkpacman

    genfstab -U /mnt >> /mnt/etc/fstab
    TERM=xterm arch-chroot /mnt
    # Time
    ln -sf /usr/share/zoneinfo/America/ /etc/localtime
    hwclock --systohc
    # /etc/locale.gen
    locale-gen
    # /etc/locale.conf > LANG=en_US.UTF-8
    # /etc/vconsole.conf > KEYMAP=
    # /etc/hostname
    systemctl enable systemd-networkd
    systemctl enable systemd-resolved
    systemctl enable sshd
    # Copy sshd.conf/key
    ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
    echo "blacklist pcspkr" >> /etc/modprobe.d/blacklist.conf

    # Initramfs
    mkinitcpio -p linux
    useradd -G
    passwd
    passwd -d root
    visudo

    # Boot
    grub-install --target=i386-pc --verbose /dev/sda
    grub-install --target=x86_64-efi --efi-directory=boot --bootloader-id=GRUB --verbose /dev/sda
    grub-mkconfig -o /boot/grub/grub.cfg


}
### Debian/Ubuntu
install_ubuntu() {
    # http://deb.debian.org/debian/pool/main/d/debootstrap/debootstrap_1.0.89.tar.gz
    # sudo debootstrap --arch=amd64 </usr/share/debootstrap/scripts/|stable,testing,unstable,experimental> /mnt/root
    # /etc/apt/sources.list
    ## Chroot
    sudo mount --bind /dev /mnt/root/dev
    sudo mount --bind /dev/pts /mnt/root/dev/pts
    sudo mount -t proc proc /mnt/root/proc
    sudo mount -t sysfs sys /mnt/root/sys
    sudo chroot /mnt/root
    # OR
    sudo systemd-nspawn -d /mnt/root
    # /etc/localtime # /usr/share/zoneinfo/America/
    # /etc/default/locale # LANG=
    # /etc/timezone # America/
    # /etc/fstab
    # /etc/hostname
    # /etc/network/interfaces
    # locale-gen <locale>
    # dpkg-reconfigure -f non-interactive tzdata
    apt-get update
    apt-get install language-pack-en-base
    apt-get upgrade
    apt-get install grub-pc linux-image

    adduser
    gpasswd -a user sudo
    passwd
}
# Utility
generate_random() {
    local file="/dev/urandom" int="${int:-8}"

    #key=$($(exe tr) -dc 'A-Za-z0-9' <"${file}" |$(exe head) -c "${int}")
    key=( $(dd if="${file}" bs=128 count=1 2>/dev/null |sha1sum) )
    printf '%s\n' "${key}"|xsel -ib
}
find_list() {
    local path="${1}" expr="${2}"; shift 2
    path="${path:-./}" expr="${expr:-*}"
    $(type -P find) "${path}" -printf "${FORMAT_FIND}" -iname "${expr}" "${@}"
}
find_list_directory() {
    local path="${1}" expr="${2}"
    path="${path:-./}" expr="${expr:-*}"
    l "${path}" "${expr}" -maxdepth 1 -type d
}
# VM
vm_win() {
    # https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso
    qemu-system-x86_64 -enable-kvm \
                       -cpu hot \
                       -drive file=./win.img,if=virtio \
                       -net nic -net user,hostname=win \
                       -m 1G \
                       -monitor stdio \
                       -name "Win" \ # -display sdl,frame=off,window_close=off
                       -bood d -drive file=win.iso,media=cdrom -dir file=driver.iso,media=cdrom \
                       "${@}"
}
## Android
android_eclipse_ready() {
    local item id name target

    if ! (( ${#@} )); then
        android list targets |grep -ie '\(id:\|name:\)'
        read -e -p 'Target:' target
    fi

    android update project --path . --target "${target}" --subprojects
}

## Clojure
clj_repl() {
    local breakchars="(){}[],^%$\"\";:''|\\"
    local dir="${HOME}/.clojure" jar="/usr/share/clojure/clojure.jar"

    if ! (( ${#@} )); then
        rlwrap --remember -c -b "${breakchars}" \
               -f "${dir}/repl/completion" \
               java -cp "${jar}" clojure.main
    else
        java -cp "${jar}" clojure.main "${1}" -- "${@}"
    fi
}

## Python
py-instal() {
    sudo pacman -S python cairo pkgconf gobject-introspection gtk3
    }
