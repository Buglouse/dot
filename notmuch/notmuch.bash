export NOTMUCH_CONFIG="${HOME}/.notmuch/notmuch.conf"

notmuch-tags() {
    notmuch search --output=tags '*'
}
notmuch-pre() {
    :
    #notmuch search --output=files tag:deleted |xargs -l rm
    #knotmuch tag +trash -- 'folder:gmail/[Gmail]/.Trash'
    #notmuch tag +inbox folder:inbox
    #notmuch tag +sent folder:sent
    #notmuch tag +archived folder:archived
    #notmuch tag +spam folder:spam
    }
notmuch-post() {
    notmuch tag +inbox +unread -new -- tag:new
}
