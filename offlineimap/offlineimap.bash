kill-plock() {
    local i=0
    while ps ${1} &>/dev/null; do
        if (( i++ > 5 )); then
            kill -9 ${1}
            return 1
        fi

        sleep 10
    done
    return 0
}
fingerprint-get() {
    echo -n |
        openssl s_client -connect "${1}" 2>/dev/null |
        openssl x509 -noout -fingerprint |
        cut -f2 -d'=' |
        sed 's/://g'
}

offlineimap() {
    $(type -P offlineimap) -o -c "${UCS_INSTALL}/.offlineimap/offlineimap.conf" "${@}"
}
offlineimapd() {
    # -u quite
    offlineimap
    while sleep 900; do
        printf "%s\n" "$(date)"
        [[ -e "${UCS_INSTALL}/.offlineimap/pid" ]] || offlineimap
    done
}
