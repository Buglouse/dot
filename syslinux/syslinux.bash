syslinux-partition() {
    local dev="${1}"
    [[ ${1} ]] || return
    # GPT/Boot
    sgdisk --mbrtogpt "${dev}" &&
        sgdisk --new=1:0:1M "${dev}" &&
        sgdisk --typecode="1:0xEF02" &&
        sgdisk --new=2:0:200M "${dev}" &&
        sgdisk --typecode="2:0xEF00" "${dev}" &&
        # Root
        sgdisk --new=3:0:0 "${dev}" &&
        sgdisk --typecode="3:0x8300" "${dev}"
    # Data
    #sgdisk --new=3:0:0 --typecode="0x8300" "${dev}"
}
syslinux-format() {
    local dev="${1}"
    [[ ${1} ]] || return

    mkfs.fat -F32 -nBIOS "${dev}1"
    mkfs.ext4 -L root "${dev}2"
    #tune2fs -o journal_data_writeback "${dev}3"
    #tune2fs -O ^has_journal "${dev}3"
    #e2fsck -f "${dev}3"
    #dumpe2fs "${dev}3"
    #fstab:data=writeback,noatime

}
syslinux-bootable() {
    local file="syslinux-6.03"
    local dest="${1}"
    local bin="${2:-${file}}/bios/mbr/gptmbr.bin"

    [[ ${1} && -e ${bin} ]] || return

    sgdisk "${dest}" --attributes=1:set:2
    sgdisk "${dest}" --attributes=1:show

    [[ $(blkid -s PTTYPE -o value "${dest}") = gpt ]] &&
        dd bs=440 conv=notrunc count=1 if="${bin}" of="${dest}"
}
syslinux-install() {
    local src="/usr/lib/syslinux"
    local dst="${1}" dev="${2}"

    sudo mkdir -p ${dst}/{BIOS,EFI/BOOT}

    sudo cp -u ${src}/bios/*.c32 "${1}/BIOS"
    # sudo syslinux --directory "BIOS" --install "${dev}"
    sudo cp -u ${src}/efi64/*.c32 "${dst}/EFI/BOOT"
    sudo extlinux --install "${dst}/BIOS"
    sudo efibootmgr --create --disk "${dev%[123456789]}" --part "${dev##/*[^123456789]}" --loader /EFI/BOOT/syslinux.efi --label "EFI/BOOT" --verbose
    [ -e ${dst}/EFI/BOOT/bootx64.efi ] || sudo cp /usr/lib/syslinux/efi64/syslinux.efi "${dst}/EFI/BOOT/boox64.efi"

    cat <<EOF > "/tmp/syslinux.cfg"
SERIAL 0 38400
UI menu.c32
ALLOWOPTIONS 1
IMPLICIT 0
TIMEOUT 5
NOESCAPE 0
PROMPT 1

MENU TITLE Linux
#MENU BACKGROUND splash.png

#MENU WIDTH 78
#MENU MARGIN 4
#MENU ROWS 7
#MENU TABMSGROW 14
#MENU CMDLINEROW 14
#MENU HELPMSGROW 16
#MENU HELMSGENDROW 29

#MENU COLOR border       30;44   #40ffffff #a0000000 std
#MENU COLOR title        1;36;44 #9033ccff #a0000000 std
#MENU COLOR sel          7;37;40 #e0ffffff #20ffffff all
#MENU COLOR unsel        37;44   #50ffffff #a0000000 std
#MENU COLOR help         37;40   #c0ffffff #a0000000 std
#MENU COLOR timeout_msg  37;40   #80ffffff #00000000 std
#MENU COLOR timeout      1;37;40 #c0ffffff #00000000 std
#MENU COLOR msg07        37;40   #90ffffff #a0000000 std
#MENU COLOR tabmsg       31;40   #30ffffff #00000000 std

#DEFAULT select
#LABEL select
#      MENU LABEL Select
#      KERNEL ifcpu64.c32
#      APPEND x86_64 -- i686

LABEL core
MENU LABEL Core
KERNEL vmlinuz
INITRD core.gz
APPEND loglevel=3
#debug ignore_loglevel log_buf_len=10M print_fatal_signals=1 loglevel=0 earlyprintk=vga,keep sched_debug

LABEL dos
  MENU LABEL DOS
  KERNEL chain.c32
  APPEND hd0 1

LABEL pxe
  MENU LABEL PXE
  KERNEL ethersel.c32

LABEL lua
  MENU LABEL LUA
  KERNEL lua.c32 select.lua

LABEL ----
  MENU LABEL System Utilities:
  MENU DISABLE

LABEL mem
  MENU LABEL MemTest86+
  LINUX memtest86.bin

LABEL hdt
  MENU LABEL Hardware Information
  KERNEL hdt.c32
  APPEND modules_alias=modules.alias pciids=pci.ids memtest=mem

LABEL reboot
  MENU LABEL Reboot
  KERNEL reboot.c32

LABEL off
  MENU LABEL Power Off
  KERNEL poweroff.com

LABEL back
  MENU LABEL Back
  localboot 0x80

EOF
    sudo cp /tmp/syslinux.cfg "${dst}/BIOS/"
    sudo cp /tmp/syslinux.cfg "${dst}/EFI/BOOT/"

    cat <<EOF > "/tmp/select.lua"

if (dmi.supported()) then
dmitable = dmi.gettable()
for k,v in pairs(dmitable) do
print(k, v)
end

print(dmitable["system.manufacturer"])
print(dmitable["system.product_name"])
print(dmitable["bios.bios_revision"])

if ( string.match(dmitable["system.product_name"], "TEST STRING") ) then
print("Matches")
syslinux.run_command("memdisk initrd=img raw")
else
print("Null Match")
initrd = syslinux.initramfs_init()
syslinux.initramfs_load_archive(initrd, "file")
syslinux.boot_it(kernel, initrd, "init=/bin/bash")
end
end

EOF
    sudo cp /tmp/select.lua "${dst}/BIOS/"
    sudo cp /tmp/select.lua "${dst}/EFI/BOOT"

    #sudo umount "${dev}"
    #sudo dd bs=440 count=1 conv=notrunc if=/usr/lib/syslinux/bios/gptmbr.bin of=${dev}
}
syslinux-install_() {
    # efibootmgr

    local file="syslinux-6.03.tar.xz" sig="syslinux-6.03.tar.sign"
    local dest="${1:-./syslinux}" src="${2:-${file%\.tar.*}}"
    local urisig="https://www.kernel.org/pub/linux/utils/boot/syslinux/${sig}"
    local uri="https://www.kernel.org/pub/linux/utils/boot/syslinux/${file}"
    local memuri="http://www.memtest.org/download/5.01/memtest86+-5.01.bin.gz"
    local mem pci
    local malias=( /lib/modules/*/modules\.alias )
    local uripci="http://pciids.sourceforge.net/v2.2/pci.ids"
    [[ ${src} == /* ]] || src="${PWD}/${src}"

    if ! [[ -e ${src} ]]; then
        if [[ -e ${file} ]]; then
            tar xf "${file}"
        else
            read -e -p "Download Sources? "
            [[ ${REPLY} = +(1|[yY]*) ]] && { wget "${uri}" || return; }
            [[ -e ${file} ]] && tar xf "${file}"
            unset REPLY
        fi
    fi
    [[ -e ${src} ]] || return
    [[ -e ${dest} ]] || mkdir -p "${dest}"
    pushd ${dest} >/dev/null
    mkdir -p ./{BIOS,EFI/BOOT}

    mem=( memtest*\.bin* )
    if ! [[ -e ${mem} ]]; then
        read -e -p "Download Memtest? "
        [[ ${REPLY} = +(1|[yY]*) ]] &&
            wget "${memuri}" && mem="${memuri##*/}"
        unset REPLY
    fi
    [[ -e ${mem} && ${mem} = *.gz ]] &&
        { gunzip -d "${mem}" && mem="${mem%\.gz}"; }

    pci="$(find /usr/share -iname "${uripci##*/}" 2>/dev/null)"
    [[ -e ${pci} ]] || { pci="${uripci##*/}"
                         [[ -e ${pci} ]] || { read -e -p "Download PCI IDS? "
                                              [[ ${REPLY} = +(1|[yY]*) ]] && wget "${uripci}"
                                              unset REPLY; }; }


    ## Bootloader
    cp -u "${src}/efi64/efi/syslinux.efi" "./EFI/BOOT/BOOTX64.EFI"
    cp -u "${src}/efi32/efi/syslinux.efi" "./EFI/BOOT/BOOTIA32.EFI"
    cp -u "${src}/bios/core/ldlinux.sys" "./BIOS"
    ## Core
    cp -u "${src}/efi64/com32/elflink/ldlinux/ldlinux.e64" "./EFI/BOOT"
    cp -u "${src}/efi32/com32/elflink/ldlinux/ldlinux.e32" "./EFI/BOOT"
    cp -u "${src}/bios/com32/elflink/ldlinux/ldlinux.c32" "./BIOS"
    ## Modules
    cp -u ${src}/bios/com32/{lua/src/*.c32,gpllib/libgpl.c32,libutil/libutil.c32,chain/chain.c32,menu/menu.c32,sysdump/sysdump.c32,lib/libcom32.c32,modules/*.c32} "./BIOS"
    #cp -u ${src}/efi32/com32/{lua/src/*.c32,gpllib/libgpl.c32,libutil/libutil.c32,chain/chain.c32,menu/menu.c32,sysdump/sysdump.c32,lib/libcom32.c32,modules/*.c32} "./EFI/BOOT/32"
    cp -u ${src}/efi64/com32/{lua/src/*.c32,gpllib/libgpl.c32,libutil/libutil.c32,chain/chain.c32,menu/menu.c32,sysdump/sysdump.c32,lib/libcom32.c32,modules/*.c32} "./EFI/BOOT"

    cat <<EOF > "./syslinux.conf"
SERIAL 0 38400
UI menu.c32
ALLOWOPTIONS 1
IMPLICIT 0
TIMEOUT 5
NOESCAPE 0
PROMPT 1

MENU TITLE Linux
#MENU BACKGROUND splash.png

#MENU WIDTH 78
#MENU MARGIN 4
#MENU ROWS 7
#MENU TABMSGROW 14
#MENU CMDLINEROW 14
#MENU HELPMSGROW 16
#MENU HELMSGENDROW 29

#MENU COLOR border       30;44   #40ffffff #a0000000 std
#MENU COLOR title        1;36;44 #9033ccff #a0000000 std
#MENU COLOR sel          7;37;40 #e0ffffff #20ffffff all
#MENU COLOR unsel        37;44   #50ffffff #a0000000 std
#MENU COLOR help         37;40   #c0ffffff #a0000000 std
#MENU COLOR timeout_msg  37;40   #80ffffff #00000000 std
#MENU COLOR timeout      1;37;40 #c0ffffff #00000000 std
#MENU COLOR msg07        37;40   #90ffffff #a0000000 std
#MENU COLOR tabmsg       31;40   #30ffffff #00000000 std

#DEFAULT select
#LABEL select
#      MENU LABEL Select
#      KERNEL ifcpu64.c32
#      APPEND x86_64 -- i686

LABEL core
MENU LABEL Core
KERNEL vmlinuz
INITRD core.gz
APPEND loglevel=3
#debug ignore_loglevel log_buf_len=10M print_fatal_signals=1 loglevel=0 earlyprintk=vga,keep sched_debug

LABEL dos
  MENU LABEL DOS
  KERNEL chain.c32
  APPEND hd0 1

LABEL pxe
  MENU LABEL PXE
  KERNEL ethersel.c32

LABEL lua
  MENU LABEL LUA
  KERNEL lua.c32 select.lua

LABEL ----
  MENU LABEL System Utilities:
  MENU DISABLE

LABEL mem
  MENU LABEL MemTest86+
  LINUX memtest86.bin

LABEL hdt
  MENU LABEL Hardware Information
  KERNEL hdt.c32
  APPEND modules_alias=modules.alias pciids=pci.ids memtest=mem

LABEL reboot
  MENU LABEL Reboot
  KERNEL reboot.c32

LABEL off
  MENU LABEL Power Off
  KERNEL poweroff.com

LABEL back
  MENU LABEL Back
  localboot 0x80

EOF

    cat <<EOF > "./select.lua"

if (dmi.supported()) then
dmitable = dmi.gettable()
for k,v in pairs(dmitable) do
print(k, v)
end

print(dmitable["system.manufacturer"])
print(dmitable["system.product_name"])
print(dmitable["bios.bios_revision"])

if ( string.match(dmitable["system.product_name"], "TEST STRING") ) then
print("Matches")
syslinux.run_command("memdisk initrd=img raw")
else
print("Null Match")
initrd = syslinux.initramfs_init()
syslinux.initramfs_load_archive(initrd, "file")
syslinux.boot_it(kernel, initrd, "init=/bin/bash")
end
end

EOF

    popd >/dev/null
}
