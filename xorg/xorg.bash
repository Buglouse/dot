# Xorg
# FVWM


export DISPLAY=":0.0"
export DIR_XORG="${HOME}/.xorg"; export DIR_XKB="${DIR_XORG}/xkb"; export CONF_XKB="/us.xkb"
export CONF_XAUTHORITY="${HOME}/.xorg/auth.xorg"
export CONF_XRESOURCES="${HOME}/.xorg/xresources.conf"
export XORG_BACKLIGHT=""

xorg_session() {
    local c=0 max=100 # .5m
    Xorg -nolisten tcp -sharevts -noreset -verbose 2 -logfile "${DIR_TMP_LOG}/xorg.log" vt${XDG_VTNR} ${DISPLAY} &
    #systemctl --user start xorg@${USER}
    while ! xdpyinfo -display "${DISPLAY}" &>/dev/null; do
        sleep .3
        c=$(( c + 1 ))
        (( c >= max )) && return 1
    done #|| return
    xorg_xkb
    xorg_xrdb
}
xorg_xrdb() {
    xrdb -display "${DISPLAY}" -load "${CONF_XRESOURCES}"
    true
}
xorg_xkb() {
    xkbcomp -I"${DIR_XKB}" "${DIR_XKB}/us.xkb" "${DISPLAY}"
}
xorg_dpms_off() {
    xset dpms force off
}
devices_class_backlight() {
    # Find available sysfs backlight devices
    local item items=( ${DIR_SYSFS}/class/backlight/* )
    (( ${#items[@]} )) || return
    if [[ -z ${PS1} ]]; then # non-interactive
        XORG_BACKLIGHT="${item}"
    else
        (( ${#items[@]} > 1 )) &&
            select item in ${items[@]##*/}; do
                return
            done
    fi
    XORG_BACKLIGHT="${items:-${item}}"
}
xorg_brightness() {
    devices_class_backlight #XORG_BACKLIGHT
    [[ -n ${XORG_BACKLIGHT} ]] || return
    local digit item="${XORG_BACKLIGHT}"
    local cur="$(<${item}/brightness)" max="$(<${item}/max_brightness)"
    case ${1} in
        "+") digit="1" ;;
        "-") digit="-1" ;;
        *)
            printf '%s\n' "${item}"
            select digit in {1..${max}}; do
                (( digit > max || digit < 1 )) && return
            done ;;
    esac
    echo $(( cur + digit )) |sudo tee "${item}/brightness" 2>/dev/null
}

## Touchpad
xorg_touchpad_button() {
    synclient TapButton1=1 TapButton2=2 TapButton3=3
}
xorg_touchpad() {
    local item state
    #lsmod |grep -iE "apple|cyapa|sermouse|synap|psmouse|vsxx|bcm"
    # Synaptic Driver loaded
    ##if lsmod |grep -iE "synap"; then
    if synclient &>/dev/null; then
        xorg_touchpad_button
        if (( ${#} > 0 )); then
            (( ${1} )) && item=0 || item=1
        else
            state="$(synclient | grep -i 'touchpadoff' | cut -d'=' -f2)"
            (( state )) && item=0 || item=1
        fi
        synclient TouchpadOff="${item}"
    else
        # Parse xinput for device
        item="$(xinput list |grep -i "mouse" |sed -e 's/.*id=\([0-9]*\).*/\1/')"
        if [[ ${item} ]]; then
            if (( ${#} > 0 )); then
                (( ${1} )) && state=enable || state=disable
            else
                state="$(xinput list-props "${item}" |grep "Device Enable" |sed -e 's/.*:\s*\([0-9]*\)/\1/')"
                (( state )) && state="disable" || state="enable"
            fi
        fi
        xinput "${state}" "${item}"
    fi
}

## Capture
### Display
xorg_capture() {
    # Capture local screen
    local out="${DIR_TMP_DATA}/capture-$(date '+%Y%m%d_%H%M%S').png"
    import -window root "${out}"
}
xorg_record() {
    # Capture attached camera
    local cmd="$(type -P ffmpeg)"
    local out="${1:-${DIR_TMP_DATA}/record-$(date '+%%m%d_%H%M%S').mkv}"
    local size="$(xrandr |sed -n -e 's/ //g' -e 's/.*current\([^,]*\).*/\1/p')"
    local codec="libx264"
    ## libx264. huffyuv
    [[ ${cmd} ]] || return
    ${cmd} -f x11grab \
           -video_size "${size}" \
           -framerate 30 \
           -i "${DISPLAY}" \
           -qscale 0 -vcodec "${codec}" \
           -qp 0 -pix_fmt yuv444p \
           "${out}"
}
xorg_record_camera() {
    # Capture local screen with attached camera
    local cmd="$(type -P ffmpeg)"
    local out="${DIR_TMP_DATA}/record_camera-$(date '+%Y%m%d_%H%M%S').mkv"
    local size="$(xrandr |sed -n -e 's/ //g' -e 's/.*current\([^,]*\).*/\1/p')"
    local size_cam="320x180"
    local codec="libx264"

    [[ ${cmd} ]] || return
    ${cmd} -f x11grab -thread_queue_size 64 \
           -video_size "${size}" -framerate 30 -i "${DISPLAY}" \
           -f v4l2 -thread_queue_size 64 -video_size "${size_cam}" \
           -r 30 -i /dev/video0 \
           -filter_complex 'overlay=main_w-overlay_w-1:1:format=yuv444' \
           -vcodec "${codec}" -qp 0 -pix_fmt yuv444p \
           "${out}"
    #-filter_complex 'overlay=main_w-overlay_w:main_h-overlay_h-10:format=yuv444' \
        }

### Camera
xorg_camera() {
    local out="${DIR_TMP_DATA}/camera-$(date '+%Y%m%d_%H%M%S').mkv"
    mencoder \
        tv:// -tv driver=v4l2:gain=1:width=640:height=480:device=/dev/video0:fps=10:outfmt=yuy2:forceaudio:alsa:adevice=hw.0,0:audiorate=41000 \
        -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=1800 \
        -ffourcc xvid \
        -oac mp3lame -lameopts cbr=128 \
        -o "${out}" "${@}"
    #-ovc lavc -lavcopts vcodec=mpeg4:vhq:vbitrate=600 \
        #-oac mp3lame -lameopts cbr:br=64 \
        }
xorg_camera_stream() {
    mplayer -vf screenshot \
            tv:// -tv driver=v4l2:gain=1:width=640:height=480:device=/dev/video0:fps=10:outfmt=yuy2:forceaudio:alsa:adevice=hw.0,0:audiorate=41000 \
            "${@}"
}
back() {
    mplayer tv:// -tv driver=v4l2:height=50 -geometry 0:0
}
