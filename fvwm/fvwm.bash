export CONF_FVWM="${HOME}/.fvwm/fvwm.conf"

fvwm() {
    $(type -P fvwm) -d "${DISPLAY}" -f "${CONF_FVWM}" "${@}" &
}
