export CONF_SSH="${HOME}/.ssh/ssh.conf"
export SSH_HOSTS="${DIR_CACHE}/hosts.ssh"
export SSH_AUTH="${HOME}/.ssh/auth.keys"

ssh() {
    TERM=xterm $(type -P ssh) -F "${CONF_SSH}" "${@}"
}
