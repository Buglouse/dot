export DIR_AUTH="${HOME}/.auth"
export AUTH_DB="identity.db"
export AUTH_PATH="${DIR_AUTH}/${AUTH_DB}"
export AUTH_CACHE="${DIR_CACHE}/${AUTH_DB}"

alias auth_env='auth_environment'
auth_cache() { [[ -e ${AUTH_CACHE} ]]; }
auth_dcrypt() {
    local cmd="$(type -P identity)"
    [[ -z ${cmd} ]] && return 0
    auth_cache || ${cmd} -E
}
auth_encrypt() {
    local cmd="$(type -P identity)"
    [[ ${cmd} ]] && ${cmd} -e
}
gpg-gen() {
    gpg --full-generate-key
}
gpg-key() {
    gpg --list-secret-keys --keyid-format LONG
}
gpg-block() {
    gpg --armor --export "${1}"
}
gpg-export() {
    gpg --export -a "${1}" > "${1}.pub.gpg"
}
gpg-export-prv() {
    gpg --export-secret-key -a "${1}" > "${1}.prv.gpg"
}
cert-gen() {
    openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout cert.key -out cert.crt
}
ssh-key() {
    ssh-keygen -f key
}
buku-save() {
    mv "${XDG_DATA_HOME}/buku/bookmarks.db.enc" "${HOME}/dot/auth/buku_mark.db.enc"
}
