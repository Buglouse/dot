# https://github.com/hartwork/image-bootstrap.git
image_create() {
    # https://wiki.archlinux.org/index.php/Installation_Guide
    # https://wiki.archlinux.org/index.php/EFI_system_partition
    fdisk
    # GPT
    # 1:EFI+512M(1)
    # 2:Linux
    mkfs.fat -F32 /dev/sdx1
    mkdir -p /dev/sdx1/EFI/arch
    mkfs.ext4 /dev/sdx2
}
mkinitcpio() {
    local cmd="$(type -P mkinitcpio)"
    ${cmd} --config "${UCS_INSTALL}/.arch/mkinitcpio.conf" \
           --preset "${UCS_INSTALL}/.arch/linux.preset" "${@}"
    }

chroot_script() {
    cat <<EOF
printf "a\na"| (passwd root)
locale-gen
#mkinitcpio -p linux
useradd -m -g users -G wheel user
rm /etc/resolv.conf
ln -s /usr/share/systemd/resolv.conf /etc/resolv.conf
systemctl enable systemd-networkd
systemctl enable systemd-resolved
EOF
}
post_part() {
    grub-install --removable --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB # --verbose
    grub-mkconfig -o /boot/grub/grub.cfg
}

main() {
    boot_path="/var/lib/machines/boot"
    mkdir -p "${boot_path}"
    pacstrap "${boot-path}" base base-devel grub iwd git sudo rxvt-unicode tmux
    image-bootstrap --hostname one --password pass --bootloader auto
    rm -rf "${boot-path}/var/cache/pacman/pkgs/*"
    rm -rf "${boot-path}/usr/share/man/*"
    find "${boot-path}/usr/share/locale" \( ! -iname "en*" ! -iname "locale.alias" \) -delete &>/dev/null
    printf "en_US.UTF-8 UTF-8\nen_US ISO-8859-1" >> "${boot_path}/etc/locale.gen"
    printf "boot" >> "${boot_path}/etc/hostname"
    printf "127.0.0.1\tlocalhost\n::1\t\tlocalhost\n" >> "${boot_path}/etc/hosts"
    mkdir "${boot_path}/efi"
    chroot_script "${boot_path}" > "${boot_path}/first.sh"
    arch-chroot "${boot_path}"
    machinectl export-tar boot /tmp/boot-arch.tar.bz2
}
