(use-modules (ice-9 match)
             )
(define (loop count args)
  (make-parameter args)
  (cond
   ((> count 0)
    args
    (display count)
    (loop (- count 1) args))))

;; (ice-9 ftw)
(define ftw:stat-f
  (match-lambda
   ((name stat)
    name)
   ((name stat child ...)
    (list name (map ftw:stat-f child)))))

