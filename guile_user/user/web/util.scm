(define-module (user web util)
  #:use-module (web uri)
  #:use-module (web request)
  #:use-module (srfi srfi-1)
  #:export (request-path-components
            rile-extension
            directory?)
  )

(define (request-path-components request)
  (split-and-decode-uri-path (uri-path (request-uri request))))

(define (file-extension path)
  (last (string-split path #\.)))
(define (directory? path)
  (string=? path (dirname path)))


