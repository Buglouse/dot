(define-module (user web view html)
  #:use-module (rnrs io ports)
  #:use-module (ice-9 binary-ports)
  #:use-module (web uri)
  #:use-module (web request)
  #:use-module (web response)
  #:use-module (user web config)
  #:use-module (user web sxml)
  #:use-module (user web util)
  #:export (render
            file-extension
            mime-types
            render-static
            render-scm))

(define mime-types '(("css" . (text/css))
                     ("js" . (text/javascript))
                     ("png" . (image/png))))
(define stylesheets (list "/css/default.css"))

(define (render-file path)
  ;; Return file path if valid system file or false.
  (let ((file (string-join (cons user-web-dir-scm path) "/")))
    (if (and (file-exists? file) (not (directory? file)))
        file
        #f)))
(define (render-stylesheet stylesheet-uri)
  `(link (@ (rel "stylesheet")
            (href ,stylesheet-uri))))
(define (render-sxml sxml)
  (list '((content-type . (text/html)))
        (lambda (port)
          (sxml->html sxml port))))
(define (render-404 path)
  (redirect-404 (build-uri 'http
                           ;;#:host user-web-host
                           ;;#:port user-web-port
                           #:path (string-join path "/" 'prefix))))
(define (render-scm path)
  (let ((file (render-file path)))
    (if file
        (main-layout
         (string-capitalize (format #f "~s" path))
         (open-file-input-port file read))
         ;;(read (open-file-input-port file)))
        (render-404 path))))
(define (render-static path)
  (let ((file (render-file path)))
    (if file
        (list `((content-type . ,(assoc-ref mime-types (file-extension file))))
              (call-with-input-file file get-bytevector-all))
        (render-404 path))))

(define (redirect path)
  (let ((uri (build-uri 'http
                        #:host user-web-host
                        #:port user-web-port
                        #:path (string-append "/" (encode-and-join-uri-path path)))))
    (list (build-response
           #:code 301
           #:headers `((content-type . (text/html))
                       (location . ,uri)))
          (format #f "Redirect to ~a" (uri->string uri)))))
(define (redirect-404 uri)
  (list (build-response #:code 404)
        (string-append "Resource not found: " (uri->string uri))))

(define (main-layout subtitle body)
  (render-sxml
   `((doctype "HTML")
     (html
      (head
       (meta (@ (content "text/html;charset=utf-8")
                (http-equiv "Content-Type")))
       (title ,(string-append "Web - " subtitle))
       ,@(map render-stylesheet stylesheets))
      (body ,body)))))

(define (web-index)
  (main-layout "Index"
               `(div (@ (class "container")))
               ))
