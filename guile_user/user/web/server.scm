(define-module (user web server)
  #:use-module (ice-9 format)
  #:use-module (web server)
  #:use-module (web request)
  #:use-module (web uri)
  #:use-module (user web config)
  #:use-module (user web util)
  #:use-module (user web controller)
  #:export (server-http)
  )

(define (controller-cons controller request)
  (controller (cons (request-method request)
                    (request-path-components request))))
(define (handler request body controller)
  (format #t "~a ~a\n"
          (request-method request)
          (uri-path (request-uri request)))
  (apply values
         (append
          ;; Create cons of (METHOD . COMPONENTS)
          (controller (cons (request-method request)
                            (request-path-components request)))
          (list controller))))

(define (server-http controller)
  (run-server (lambda args (apply handler args))
              'http `(#:addr ,user-web-host #:port ,user-web-port)
              controller))
