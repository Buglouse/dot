(define-module (user web controller)
  #:use-module (ice-9 match)
  #:use-module (user web config)
  #:use-module (user web view html)
  #:export (web-controller)
  )

(define (render path)
  (let* ((file (string-join (list user-web-dir-pub path) "/"))
        (ext (file-extension file)))
    (cond
     ((assoc-ref mime-types ext)
      (render-static path))
     ((string=? ext "scm")
      (render-scm path))
     ))
  )

(define web-controller
  (match-lambda
   ((GET)
    (render-scm "index.scm"))
   ((GET path ...)
   (render-static path))
   ))

