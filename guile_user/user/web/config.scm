(define-module (user web config)
  #:export (user-web-dir-pub
            user-web-dir-scm
            user-web-host
            user-web-port
            user-dir-resource)
  )

(define user-web-dir-pub (string-concatenate `(,(getenv "HOME") "/" "doc/pub")))
(define user-web-dir-scm (string-concatenate `(,(getenv "HOME") "/" "doc/scm")))
(define user-web-host (inet-pton AF_INET "127.0.0.1"))
(define user-web-port 8080)
(define user-dir-resource (getcwd))
