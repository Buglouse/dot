#!/bin/env guile -s
!#

(define-module (user tmux))

(define sysfs-path "/sys/")
(define sysfs-path-battery (string-concatenate (list sysfs-path "/class/power_supply/BAT0")))

(define (sysfs-battery)
  (let ((path sysfs-path-battery))
    (if (file-exists? path)
        (let (with-input-from-file (string-concatenate path "/uevent")
               (lambda ()
                 (do ((line (read-line) (read-line)))
                     ((eof-object? line))
                   (error "Invalid sysfs path for %s" "battery")
                      
                     )
          (let (status ))
          
        )
    )
  )
        )))

(define (find-path filename specs flag)
  (or
   (and 
        (string-match "power_supply/BAT0" filename)
        (list filename specs))
   #t))
(ftw "/sys/devices/" find-path)

(define sysfs-devices '("power_supply"))

(define sysfs-get (device)
  
  (unless )
  
  
  (call-with-input-file
      
      )  
  )

(define )
