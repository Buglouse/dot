(add-to-load-path (dirname (current-filename)))
(add-to-load-path (string-concatenate `(,(dirname (current-filename)) "/site")))

(use-modules (ice-9 history))
(use-modules (ice-9 readline))
(use-modules (system vm traps)
             (system vm trap-state)
             (system vm vm))

(define debug #t)
(define (debug)
  (when debug
    (debug-set! width 300)
    ))

(define readline #t)
(define (readline)
  (when readline
    (activate-readline)
    ;;(repl-default-option-set! prompt ":>")
    ))

(readline)
(debug)


