#!/usr/bin/csi -script
;; Tmux prompt

(import scheme chicken)
(use posix (srfi 1 13 69))

(load "/home/user/dot/scm/sysfs.so")
(use sysfs)

(define DOCU
  '((title "Tmux Prompt")
    (div (@ (class "logic"))
         (h1 "Logic")
         (p "Find devices from (module sysfs); battery."
            "Validate device path, generate associated paths."
            "Read device path values.")
         )
    (div (@ (class "code"))
         ((set-battery keys)
          "Set battery value from keys.")
         )
    ))
(define ARGS
  '(
    ))

;; ## Battery
;; /sys/class/power_supply/BAT0[status, present, capacity, voltage_min_design, voltage_now, current_now, charge_full_design, charge_full, charge_now]
;; Get values from sysfs.
;; Calculate [dis]charge time from wattage(charge) and amperage(current).
;; 
;; Variable
(define battery (make-hash-table))
(hash-table-set! battery
                 'keys
                 '(status present capacity
                          voltage_min_design voltage_now current_now
                          charge_full_design charge_full charge_now))

;; Get
(define power-devices (get-sysfs 'power))

;; Set
(define (set-battery)
  (unless (hash-table-exists? battery 'path)
    (hash-table-set! battery
                     'path
                     (find (lambda (elm)
                             (string-contains elm "BAT"))
                           power-devices)))
  (let ((get (lambda (key) (get-sysfs-value (conc (hash-table-ref battery 'path) "/BAT0") key))))
    (when (equal? (get 'present) 1)
      (map
       (lambda (key)
         (hash-table-set! battery
                          key
                          (cond
                           ((or (equal? key 'charge_now)
                                (equal? key 'current_now)
                                (equal? key 'charge_full)
                                (equal? key 'voltage_now)
                                (equal? key 'voltage_min_design)
                                (equal? key 'charge_full_design))
                            (/ (get key) 1000))
                           (else
                            (get key)))))
       (hash-table-ref battery 'keys))
      )))
(define (set-battery2)
  ;;"Print remaining rate of charge|discharge."
  (let* ((status (hash-table-ref battery 'status))
        (charge-now (hash-table-ref battery 'charge_now))
        (current-now (hash-table-ref battery 'current_now))
        (charge-full (hash-table-ref battery 'charge_full))
        (charge-full-design (hash-table-ref battery 'charge_full_design))
        (hour 0) (minute 0) (second 0) (loss 0)
        (check (lambda (c) (string-ci= (symbol->string status) c))))
    ;;hours = seconds / 3600;
    ;;seconds -= 3600 * hours;
    ;;minutes = seconds / 60;
    ;;seconds -= 60 * minutes;
    (set! second (floor
                  (cond
                   ((check "charging")
                    (/ (* 3600 (- charge-full charge-now)) current-now))
                   ((check "discharging")
                    (/ (* 3600 charge-now) current-now))
                   ((check "full")
                    0))))
    (set! hour (floor (/ second 3600)))
    (set! minute (floor (/ (- second (* hour 3600)) 60)))
    (set! second (- (- second (* hour 3600)) (* minute 60)))
    (set! loss (floor (/ (* charge-full 100) charge-full-design)))
    (hash-table-set! battery 'hour hour)
    (hash-table-set! battery 'minute minute)
    (hash-table-set! battery 'second second)
    (hash-table-set! battery 'loss loss)
    ))
;; Print
(define (print-battery)
  (hash-table-walk battery (lambda (k v) (display (conc k ":" v "\n"))))
  )

;; ## AC
;; /sys/class/power_supply_AC0/[online]
;; Print online(1) or offline(0)

;; Main
;;power
(set-battery)
(set-battery2)
(print-battery)
