(module password
    (import chicken scheme)
  (use stty)

  #|pee:
  ;;Stores encrypted list of (account user password comment (current-seconds) )
  ;;Get passphrase.
  ;;Decrypt file.
  ;;Perform action.
  
  (do-init)
  >(get-hashed-passphrase) ;;read passphrase
  >>(with-stty)
  >>>(hash-passphrase) ;;(crypto-helper)
  >>>>(make-blob [64|32])
  >>>>(blake2s)
  >(encrypt-file)
  >>(symmetric-box) ;;send passphrase with message and random vector.

  >(do-add)
  >>(new-password)
  |#
  #|
  ;;Get passphrase.
  ;;Decrypt file, output to temp location for reuse.
  ;;Perform action.

  ;;Get encrypted files.
  ;;Get passphrase.
  |#
  (let ((db-dir)
        (db-file)
        (db-cache)
        ))

  ;;Passphrase
  (define (password-prompt)
    (with-stty
     '(not echo)
     (lambda ()
       (printf "Password: ")
       (let ((l (read-line)))
         (print "\r")
         l))))
  (define (password-new)
    (let ((p1 (password-prompt))
          (p2 (password-prompt)))
      (unless (equal? p1 p2)
        (print "Invalid match!")
        (password-new))
      p1))
  
  (define auth-dir (conc (HOME) "/dot/auth"))
  ;; Exported key dir.
  (define cache-dir "/tmp/data/auth")
  ;; SSH files.
  (define ssh-prv ".ssh.gpg")
  (define ssh-pub ".rsa.ssh")
  (define (get-hashed-passphrase)
    (with-stty
     '(not echo)
     (lambda ()
       (display "Enter passphrase: " (current-error-port))
       (let ((l (read-line)))
         (newline (current-error-port))
         (hash-passphrase l)))))

  ;; Find keys.
  (define (files-prv)
    (lambda ()
      (find-files auth-dir
                  test: (irregex (conc ".*" ssh-prv "$"))
                  limit: 0)))
  (define (files-pub)
    (lambda ()
      (find-files auth-dir
                  test: (irregex (conc ".*" ssh-pub "$"))
                  limit: 0)))
  (define (denc-gpg files)
    (map (lambda (file)
           (system (conc "gpg" "--batch --passphrase" pass " --decrypt" " -output=" file-output " " file))
           )
         files)
    )

  )
