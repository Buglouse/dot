(module crypto-helper (random-bytes hash-passphrase)
        (import chicken scheme foreign)
        (use blob-hexadecimal (srfi 4))
        (foreign-declare "#include \"blake2.h\"")
        (foreign-declare "#include \"blake2-impl.h\"")

        (define-foreign-variable blake2s-outbytes integer "BLAKE2S_OUTBYTES")

        (define blake2s
          (foreign-lambda*
           int
           ((c-string passphrase)
            ((scheme-pointer unsigned-char) out))
           "
           int res = 0;
           res = blake2s(out, passphrase, NULL, BLAKE2S_OUTBYTES, strlen(passphrase), 0);
           C_return(res);
           "))

        (define (hash-passphrase passphrase)
          (let ((out (make-blob blake2s-outbytes)))
            (if (not (zero? (blake2s passphrase out)))
                (error "Unable to hash passphrase with blake2s")
                out)))

        (define (random-bytes len)
          (cond-expand (openbsd:
                        (arc4random len))
                       (linux:
                        (with-input-from-file "/dev/urandom" (lambda () (read-u8vector len))))
                       (else
                        (with-input-from-file "/dev/random" (lambda () (read-u8vector len))))))
        #+openbsd
        (define (arc4random len)
          (let ((buf (make-blob len)))
            ((foreign-lambda
              void
              "arc4random_buf"
              (scheme-pointer void)
              size_t) buf len)
            (blob->u8vector buf)))
)
