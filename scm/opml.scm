(xml)
(opml
 (head
  (title
   "OPML")
  )
 (body
  (outline (@ (title "Folder")
              (text "A folder.")
              (description "Folder description.")
              (type "folder")
              )
           (outline (@ (title "Entry")
                       (text "Entry text.")
                       (description "Entry description.")
                       (type "rss")
                       (xlmUrl "http://rss.entry.org")
                       (htmlUrl "http://entry.org")
                       )) 
           )
  )
