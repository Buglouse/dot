#! /usr/bin/csi -script 
#!
(import scheme chicken)
(use data-structures posix irregex utils srfi-1)
;;library
;;lm_sensors: pwmconfig; fancontrol.

(define-constant DOCU
  '((title "Fan Control")
    (div (@ (class "Logic"))
         (h2 "Logic")
         (p "Find PWM devices.")
         (p "Verify automatic mode, use if available.")
         (p "Find Fan devices."))
    (div (@ (class "Reference"))
         (link "Sysfs" "https://www.kernel.org/doc/Documentation/hwmon/sysfs-interface"))
    ))
(define-constant HELP
  '("Fan Control"
    "Usage: \t\t fan [OPTION]"
    "OPTION:"
    "help \t\t Print usage."
    "set [auto'|:digit:] \n\t\t Set fan RPM to AUTO or DIGIT."
    "rpm \t\t Read fans RPM."
    "temp \t\t Read CPU temp."
    "status \t\t Print PWM state."
    ))

;; Variables
(define-constant HWMON "/sys/class/hwmon")

(define path-hwmon
  (let ((path HWMON))
    (if (directory-exists? HWMON)
        HWMON
        (abort (make-property-condition 'exn 'message
                                        (conc "Invalid path: " path))))))
(define (pwms)
  (find-files path-hwmon
              test: (irregex ".*/pwm[0-9]_enable")
              limit: 1
              follow-symlinks: #t))
(define (fans)
  (find-files path-hwmon
              test: (irregex ".*/fan[0-9]_input")
              limit: 1
              follow-symlinks: #t))
(define (temps)
  (find-files path-hwmon
              test: (irregex ".*/temp[0-9]_input")
              limit: 1
              follow-symlinks: #t))
;; Setters
(define (pwm-set pwm stat)
  ;; 0, full; 1, manual; 2, auto.
  (if (or (< stat 0)
          (> stat 2))
      (abort (make-property-condition 'exn 'message
                                      (conc "Invalid PWM control value: " stat)))
      (with-output-to-file pwm (lambda () (display stat)))))

;; Getters
(define (fan-rpm fans)
  (map
   (lambda (fan)
     (display (conc fan ": " (read-all fan))))
   fans))
(define (temp fans)
  (map
   (lambda (fan)
     (let ((s1 (conc fan ": " (string-trim-both (read-all fan))))
           (s2 (conc " (" (string-trim-both (read-all (temp-label fan))) ")"))
           )
       (print (conc s1 s2))))
   fans))
(define (pwm-status pwm)
  (display (conc pwm ": " (read-all pwm))))

;; Constructors
(define (temp-label fan)
  (let ((drop (string-index-right fan #\_)))
    (when drop
      (conc (string-take fan drop) "_label"))))

;; Main
(define (main args)
  (map
   (lambda (arg)
     (cond 
      ((equal? arg "set")
       (let ((stat (or (and (> (length (cdr args)) 0)
                            (string->number (cadr args)))
                       2)))
         (map (lambda (pwm)
                (when (pwm-set pwm stat)
                  (pwm-status pwm)))
              (pwms))))
      ((equal? arg "rpm")
       (fan-rpm (fans)))
      ((equal? arg "temp")
       (temp (temps)))
      ((or (equal? arg "stat")
           (equal? arg "status"))
       (map (lambda (pwm) (pwm-status pwm)) (pwms)))
      ((irregex "[:digit:]")
       )
      (else
       (map
        (lambda (line)
          (print line))
        HELP))))
   args))

(main (or (and (> (length (command-line-arguments)) 0)
               (command-line-arguments))
          '("help")))
