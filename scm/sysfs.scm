(module sysfs
    (get-sysfs
     get-sysfs-value)
  
  (import scheme chicken data-structures)
  (use posix srfi-13 irregex)

  (define DOCU
    '((title "Sysfs Interface")
      (div (@ (class "Logic"))
           (p "Find sysfs path for specified device."))
      (div (@ (class "code"))
           ((get-params device)
            "Match DEVICE to two return values, a path to append to 'sysfs and a regular expression to match 'find-files.")
           ((get-device device)
            "Return a list of paths for DEVICE.")
           ((get-value path key)
            "Read value from PATH/KEY. Return value.")
           )
      (div (@ (class "ref"))
           ("acpi.c"))
      ))

  ;; Variable
  (define sysfs "/sys/class")
  (define battery "Battery")

  ;; Get
  (define (get-params device)
    (cond
     ((equal? device 'power)
      '("/power_supply" ".*/device/power_supply"))
     (else
      #f)
     ))
  (define (get-device device)
    ;;/sys/class/power_supply/BAT0/device/power_supply
    (let ((params (get-params device)))
      (find-files (conc sysfs (car params))
                    test: (irregex (cadr params))
                    limit: 3
                    follow-symlinks: #t))
    )
  (define (get-value path key)
      (let ((file (conc path "/" key)))
        (read (open-input-file file))))

  ;; Export
  (define get-sysfs get-device)
  (define get-sysfs-value get-value)
  )
