#!/usr/bin/env bash
source 'bash.sh'

map.variables() {
true
}

map.readline() {
    local IFS='|'; local re="${*}"; unset IFS

    builtin bind -p 2>/dev/null |
    $(exe grep) -v '^$\|(not bound)\|self-insert\|do-lowercase-version' |
    $(exe grep) "\(${re}\)"
}
map.alias() {
    local IFS='|'; local re="${*}"; unset IFS

    builtin alias |
    $(exe cut) -d' ' -f2- |
    $(exe grep) "\(${re}\)"
}

map.tmux() { $(exe tmux) -f "${CONF_TMUX}" list-keys; }

map.arguments() {
    local item 
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
            esac
        done
    done
    [[ ${ITEMS} ]]
}

map.test() {
    source 'test'

    $(exe cmd)
}

source 'test.sh'
map.variables
return 2>/dev/null
re='readline|alias|tmux'

_ARGS='<command|application>'
_DESC="
    Print associated key mappings.
    Command; [${re}]."

map.arguments "${@:-?}" || exit

case "${ITEMS}" in
    +(${re})) map.${ITEMS} "${ITEMS[@]:2}" |${CMD_VIEW} - ;;
esac

