#!/usr/bin/env bash
source 'bash.sh'

capture.variables() {
    export 'LOG_CAPTURE_XORG' "${DIR_TMP_CACHE}/capture-xorg.mpg"
}

#capture.screen() {
#false
    #$(exe ffmpeg) -f x11grab -s wxga -r 25 -i "${DISPLAY}" -sameq "${LOG_CAPTURE_XORG}"
    #xwininfo -vcodec huffyuv
    #mplayer tv:// -tv driver=v4l2:width=640:height=480:device=/dev/video0:fps=30:outfmt=yuy2
    #arecord -R 1000 -f cd -t wav $1.wav & RECPID=$!; echo "Starting screencast in new shell. Exit subshell to quit."; script -t 2> $1.timing -a $1.session; kill $RECPID
#}
capture.screen() {
    local action itemE _ id name file time=$($(exe date) '+%s')
    local re='png|jpg' out="${DIR_TMP_DATA}/screen_image-${time}"

    while [[ ${1} == +(-)* ]]; do
        item="${1##(-)}"
        case "${item}" in
            png|jpg) EXT="${item}" ;;
            thumb) action='thumb' ;;
        esac
        shift
    done
    itemE="${1:-${EXT:-png}}"
    file="${out}.${itemE}"

    case "${itemE}" in
        +(${re})) ;;
        *) return ;;
    esac
    
    read _ _ _ id name <<<"$($(exe xwininfo) |$(exe grep) 'Window id')"

    [[ ${id} ]] || return
    # -screen
    $(exe import) \
        -verbose \
        -window "${id}" \
        -quality 100 -frame \
        -display "${DISPLAY}" \
        "${file}" || return

    [[ -e ${file} ]] || return
    $(exe convert) "${file}" \
        -thumbnail 160x100 -background black \
        "${out}_160x100.${itemE}"
    $(exe convert) "${file}" \
        -thumbnail 461x300 -background black \
        "${out}_461x300.${itemE}"
}

capture.arguments() {
    local item
    unset ACTION ITEMS EXT

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                xorg) ACTION='xorg' ;;
                s|screen) ACTION='screen' ;;
                v|verbose) (( verb++ )) ;;
                #--Extension--#
                png|jpg) EXT="${ARG}" ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

capture.variables
return 2>/dev/null

_ARGS='(s)creen (png|jpg)'
_DESC="
    PNG|JPG, specify extension.
    Screen, capture root xorg window."

capture.arguments "${@:-?}" || exit

case "${ACTION}" in
    xorg) capture.xorg ;;
    screen) capture.screen "${ITEMS[@]}" ;;
esac
