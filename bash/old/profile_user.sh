#!/usr/usr/env bash
source 'bash.sh'

profile.user.variables() {
    local item

    export DIR_MEDIA "${HOME}/.media"  #:order:0
    export CONF_NAMED "${HOME}/.bind/named.conf" #:order:0
    export DIR_DATA_YOUTUBE "${DIR_TMP_DATA}/youtube"  #:order: 0
    export DIR_KISMET "${HOME}/.kismet"  #:order:0
    export LOG_NGINX_PREFIX "${DIR_TMP_LOG}/nginx_" #:order:0

    export DIR_DNS_ZONE "${CONF_NAMED%/*}"  #:order:1
    export DIR_KISMET_PLUGIN "${DIR_KISMET}/plugin"  #:order:1
    export FORMAT_YOUTUBE "./youtube/%(uploader)s-%(stitle)s(%(id)s).%(ext)s"  #:order: 1

    export CMD_WM 'flwm_topside'
    export CMD_LOG 'syslogd'
    export CMD_PDF 'xpdf'
    export CMD_XORG 'Xorg'
    export CMD_HTTP 'elinks -remote'
    export CMD_VIEW 'view.sh'
    export CMD_VIDEO 'mplayer'
    export CMD_XCLIP 'xsel'
    export CMD_RECOVER 'recover'

    export DIR_WL "${HOME}/.wanderlust"
    export DIR_SED "${HOME}/.sed"
    export DIR_ZNC "${HOME}/.znc"
    export DIR_AUTH "${DIR_AUTH:-${HOME}/.auth}"
    export DIR_BASH "${HOME}/.bash"
    export DIR_HOME "${HOME}"
    export DIR_LIST "${DIR_MEDIA}/list"
    export DIR_PERL "${HOME}/.perl"
    export DIR_AUDIO "${DIR_MEDIA}/audio"
    export DIR_EMACS "${HOME}/.emacs"
    export DIR_NAMED "${DIR_TMP_CACHE}/named"
    export DIR_REPOS 'dot' 'src'
    export DIR_PYTHON "${HOME}/.python"
    export DIR_DEFAULT "${DIR_TMP_DATA}"
    export DIR_MNT_NFS "${DIR_MNT}/nfs"
    export DIR_AUTH_TMP "${DIR_AUTH_TMP:-${DIR_TMP_CACHE}}"
    export DIR_DATA_LOG "${DIR_VAR_DATA}/log"
    export DIR_HOME_BIN "${HOME}/.local/bin"
    export DIR_HOME_LIB "${HOME}/.local/lib"
    export DIR_MLDONKEY "${HOME}/.mldonkey"
    export DIR_DATA_HTTP "${DIR_VAR_DATA}/http"
    export DIR_DATA_TOR "${DIR_TMP_CACHE}/tor"
    export DIR_DATA_HTTPD "${DIR_VAR_DATA}/httpd"
    export DIR_DATA_MLNET "${DIR_VAR_DATA}/mlnet"
    export DIR_CACHE_CUPSD "${DIR_TMP_CACHE}/cupsd"
    export DIR_WANDERLUST "${HOME}/.wanderlust"
    export DIR_DATA_VNSTAT "${DIR_VAR_DATA}/vnstat"
    export DIR_CACHE_MLNET "${DIR_VAR_CACHE}/mlnet"
    export DIR_CACHE_WANDERLUST "${DIR_TMP_CACHE}/wanderlust"
    export DIR_CACHE_EMACS "${DIR_VAR_CACHE}/emacs"
    export DIR_CHROOT_NAMED "${DIR_CHROOT}/named"
    export DIR_DATA_BITLBEE "${DIR_VAR_DATA}/bitlbee"
    export DIR_DATA_TORRENT "${DIR_VAR_DATA}/torrent"
    export DIR_FREECIV_SAVE "${HOME}/.freeciv/save"
    export DIR_PROFILE_USER "${HOME}/.user"
    export DIR_CACHE_RTORRENT "${DIR_TMP_CACHE}/rtorrent"
    export DIR_CACHE_DICOD "${DIR_TMP_CACHE}/dicod"

    export DIRS_EXPORT "${DIR_TMP_EXPORT}"

    export FILE_MIMETYPE "${HOME}/.mime.type"
    export FILE_LIST_NEWSBEUTER "${DIR_LIST}/feed.newsbeuter"
    export FILE_WANDERLUST_IMAP "${DIR_TMP_CACHE}/wanderlust/account"
    export FILE_DICOD_MOTD "${HOME}/.dicod/dicod.motd"

    export CONF_HG "${HOME}/.mercurial/hg.conf"
    export CONF_FEH "${HOME}/.feh/feh.conf"
    export CONF_NCM "${HOME}/.ncmpy/ncmpy.conf"
    export CONF_MPD "${HOME}/.mpd/mpd.conf"
    export CONF_SSH "${HOME}/.ssh/ssh.conf"
    export CONF_ZNC "${DIR_ZNC}/configs/znc.conf"
    export CONF_TOR "${HOME}/.tor/tor.conf"
    export CONF_NCMP "${HOME}/.ncmpcpp/config"
    export CONF_MUTT "${HOME}/.mutt/mutt.conf"
    export CONF_RNDC "${HOME}/.bind/rndc.conf"
    export CONF_XPDF "${HOME}/.xpdf/xpdf.conf"
    export CONF_SSHD "${HOME}/.sshd/sshd.conf"
    export CONF_TMUX "${HOME}/.tmux/tmux.conf"
    export CONF_ACPID "${HOME}/.acpid/acpid.conf"
    export CONF_CUPSD "${HOME}/.cups/cupsd.conf"
    export CONF_DICOD "${HOME}/.dicod/dicod.conf"
    export CONF_EMACS "${DIR_EMACS}/emacs.el"
    export CONF_GOXSH "${HOME}/.goxsh/goxsh.conf"
    export CONF_HTTPD "${HOME}/.httpd/httpd.conf"
    export CONF_NGINX "${HOME}/.nginx/nginx.conf"
    export CONF_ELINKS "${HOME}/.elinks/elinks.conf"
    export CONF_EXPORT "${HOME}/.nfs/exports"
    export CONF_KISMET "${HOME}/.kismet/kismet.conf"
    export CONF_VNSTAT "${HOME}/.vnstat/vnstat.conf"
    export CONF_BITCOIN "${HOME}/.bitcoin/bitcoin.conf"
    export CONF_BITLBEE "${HOME}/.bitlbee/bitlbee.conf"
    export CONF_FREECIV "${HOME}/.freeciv/freeciv.conf"
    export CONF_MPLAYER "${HOME}/.mplayer/config"
    export CONF_OPENSSL "${HOME}/.openssl/openssl.conf"
    export CONF_SHELLFM "${HOME}/.shellfm/shell-fm.rc"
    export CONF_WEECHAT "${HOME}/.weechat/weechat.conf"
    export CONF_CONKEROR "${HOME}/.conkeror/conkeror.js"
    export CONF_MODPROBE "${DIR_ETC}/modprobe.conf"
    export CONF_RTORRENT "${HOME}/.rtorrent/rtorrent.conf"
    export CONF_TMUX_ONE "${HOME}/.tmux/tmux_one.conf"
    export CONF_TMUX_SSH "${HOME}/.tmux/tmux_ssh.conf"
    export CONF_ACPID_MAP "${HOME}/.acpid/acpid.map"
    export CONF_KISMET_UI "${HOME}/.kismet/kismet_ui.conf"
    export CONF_TINYPROXY "${HOME}/.tinyproxy/tinyproxy.conf"
    export CONF_WIRESHARK "${HOME}/.wireshark/preferences"
    export CONF_XBINDKEYS "${HOME}/.xbindkeys/xbindkeys.conf"
    export CONF_XULRUNNER "${HOME}/.xulrunner/application.ini"
    export CONF_FANCONTROL "${HOME}/.fancontrol/fancontrol.conf"
    export CONF_IMAPFILTER "${HOME}/.imapfilter/imapfilter.lua"
    export CONF_NEWSBEUTER "${HOME}/.newsbeuter/config"
    export CONF_MLDONKEY_DC "${DIR_MLDONKEY}/directconnect.ini"
    export CONF_MPDSCRIBBLE "${HOME}/.mpd/mpdscribble.conf"
    export CONF_NGINX_PROXY "${HOME}/.nginx/nginx_proxy.conf"
    export CONF_TRANSMISSION "${HOME}/.transmission/settings.json"
    export CONF_WEECHAT_XFER "${HOME}/.weechat/xfer.conf"
    export CONF_MUTT_IMAP_GG "${HOME}/.mutt/imap_gg.mutt"
    export CONF_NGINX_FASTCGI "${HOME}/.nginx/nginx_fastcgi.conf"
    export CONF_WEECHAT_ALIAS "${HOME}/.weechat/alias.conf"
    export CONF_WEECHAT_RELAY "${HOME}/.weechat/relay.conf"
    export CONF_WPASUPPLICANT "${HOME}/.wpasupplicant/wpasupplicant.conf"
    export CONF_WEECHAT_LOGGER "${HOME}/.weechat/logger.conf"
    export CONF_MLDONKEY_MLNET "${DIR_MLDONKEY}/downloads.ini"
    export CONF_MLDONKEY_USERS "${DIR_MLDONKEY}/users.ini"
    export CONF_WEECHAT_PLUGIN "${HOME}/.weechat/plugins.conf"
    export CONF_WEECHAT_SERVER "${HOME}/.weechat/irc.conf"
    export CONF_MLDONKEY_DONKEY "${DIR_MLDONKEY}/donkey.ini"
    export CONF_NEWSBEUTER_BIND "${HOME}/.newsbeuter/bind"
    export CONF_WEECHAT_URLGRAB "${HOME}/.weechat/urlgrab.conf"
    export CONF_MLDONKEY_TORRENT "${DIR_MLDONKEY}/bittorrent.ini"
    export CONF_SHELLFM_BOOKMARK "${HOME}/.shellfm/bookmarks"
    export CONF_NEWSBEUTER_FORMAT "${HOME}/.newsbeuter/format"

    export CACHE_MPDSCRIBBLE "${DIR_TMP_CACHE}/mpdscribble.cache"

    export DB_MPD "${DIR_TMP_CACHE}/mpd.db"
    export DB_LOCATE "${DIR_TMP_CACHE}/locate.db"
    export DB_DICT "/usr/local/share/dict"

    export FIFO_MPD "${DIR_TMP_SOCK}/mpd.fifo"
    export FIFO_MPLAYER "${DIR_TMP_SOCK}/mplayer.fifo"

    export LOG_MPD "${DIR_TMP_LOG}/mpd.log"
    export LOG_PIP "${DIR_TMP_LOG}/pip.log"
    export LOG_ZNC "${DIR_TMP_LOG}/znc.log"
    export LOG_GIMP "${DIR_TMP_LOG}/gimp.log"
    export LOG_XSEL "${DIR_TMP_LOG}/xsel.log"
    export LOG_ACPID "${DIR_TMP_LOG}/acpid.log"
    export LOG_DICOD "${DIR_TMP_LOG}/dicod.log"
    export LOG_CUPSD "${DIR_TMP_LOG}/cupsd.log"
    export LOG_RSYNC "${DIR_TMP_LOG}/rsync.log"
    export LOG_NAMED "${DIR_TMP_LOG}/named.log"
    export LOG_MLNET "${DIR_TMP_LOG}/mlnet.log"
    export LOG_KISMET "${DIR_TMP_LOG}/kismet_%l.log"
    export LOG_VNSTAT "${DIR_TMP_LOG}/vnstat.log"
    export LOG_FREECIV "${DIR_TMP_LOG}/freeciv.log"
    export LOG_INOTIFY "${DIR_TMP_LOG}/inotify.log"
    export LOG_MPLAYER "${DIR_TMP_LOG}/mplayer.log"
    export LOG_SYSLOGD "${DIR_TMP_LOG}/syslogd.log"
    export LOG_TINYPROXY "${DIR_TMP_LOG}/tinyproxy.log"
    export LOG_WIRESHARK "${DIR_TMP_LOG}/wireshark.log"
    export LOG_XULRUNNER "${DIR_TMP_LOG}/xulrunner.log"
    export LOG_IMAPFILTER "${DIR_TMP_LOG}/imapfilter.log"
    export LOG_MPDSCRIBBLE "${DIR_TMP_LOG}/mpdscribble.log"
    export LOG_NGINX_ERROR "${LOG_NGINX_PREFIX}error.log"
    export LOG_TRANSMISSION "${DIR_TMP_LOG}/transmission.log"
    export LOG_NGINX_ACCESS "${LOG_NGINX_PREFIX}access.log"
    export LOG_WPASUPPLICANT "${DIR_TMP_LOG}/wpasupplicant.log"
    export LOG_RTORRENT_TRACKER "${DIR_TMP_LOG}/rtorrent/tracker.log"

    export PID_MPD "${DIR_TMP_PID}/mpd.pid"
    export PID_ZNC "${DIR_TMP_PID}/znc.pid"
    export PID_SSHD "${DIR_TMP_PID}/sshd.pid"
    export PID_ACPID "${DIR_TMP_PID}/acpid.pid"
    export PID_DICOD "${DIR_TMP_PID}/dicod.pid"
    export PID_MLNET "${DIR_TMP_PID}/mlnet.pid"
    export PID_NAMED "${DIR_TMP_PID}/named.pid"
    export PID_NGINX "${DIR_TMP_PID}/nginx.pid"
    export PID_UDHCPC "${DIR_TMP_PID}/udhcpc.pid"
    export PID_VNSTAT "${DIR_TMP_PID}/vnstat.pid"
    export PID_BITCOIN "${DIR_TMP_PID}/bitcoin.pid"
    export PID_BITLBEE "${DIR_TMP_PID}/bitlbee.pid"
    export PID_INOTIFY "${DIR_TMP_PID}/inotify.pid"
    export PID_MPLAYER "${DIR_TMP_PID}/mplayer.pid"
    export PID_DROPBEAR "${DIR_TMP_PID}/dropbear.pid"
    export PID_TINYPROXY "${DIR_TMP_PID}/tinyproxy.pid"
    export PID_MPDSCRIBBLE "${DIR_TMP_PID}/mpdscribble.pid"
    export PID_TRANSMISSION "${DIR_TMP_PID}/transmission.pid"
    export PID_WPASUPPLICANT "${DIR_TMP_PID}/wpasupplicant.pid"

    export STAT_MPD "${DIR_TMP_CACHE}/mpd.stat"

    export SCRIPT_DHCP 'network.sh'

    export SOCK_RXVT "${DIR_TMP_SOCK}/urxvt"
    export SOCK_TMUX "${DIR_TMP_SOCK}/tmux"
    export SOCK_EMACS "${DIR_TMP_SOCK}/emacs/emacs"
    export SOCK_WPACLI "${DIR_TMP_SOCK}/wpa_cli"
    export SOCK_TOR "${DIR_TMP_SOCK}/tor"
    export SOCK_CUPSD "${DIR_TMP_SOCK}/cupsd"
    export SOCK_DICOD "${DIR_TMP_SOCK}/dicod"
    export SOCK_SHELLFM "${DIR_TMP_SOCK}/shellfm"
    export SOCK_RTORRENT "${DIR_TMP_SOCK}/rtorrent"
    export SOCK_WPASUPPLICANT "${DIR_TMP_SOCK}/wpasupplicant"

    export CERT_CA "${DIR_AUTH}/CAs.pem"
    export CERT_SIZE '2024'
    export CERT_SSH "${DIR_TMP_CACHE}/key.ssh"
    export CERT_HOSTNAME "${DIR_TMP_CACHE}/${HOSTNAME}.pem.rsa"
    export CERT_HOST_PRV "${DIR_AUTH_TMP}/${HOSTNAME}.prv.rsa"
    export CERT_HOST_PUB "${DIR_AUTH}/${HOSTNAME}.pub.rsa"
    export CERT_IDENTITY "${DIR_AUTH}/identity.pub.rsa"

    export FORMAT_DATE '%Y.%m.%d{%w}%R%z[%j.%U]'
    export FORMAT_DATE_LS '%Y.%m.%d %R:%S'
    export FORMAT_DATE_IRC '%Y.%m.%d %R'
    export FORMAT_DATE_IRC_ZNC '%m.%d %H:%M'  #:compat:%H:%M
    export FORMAT_DATE_LOOP '%Y.%m.%d %R:%S %s'
    export FORMAT_DATE_NEWS '%m.%d %R'
    export FORMAT_VNSTAT_DAY '%d.%m'
    export FORMAT_VNSTAT_MONTH '.%y'
    export FORMAT_VNSTAT_ALL '%d.%m.%y'

    export HOST_DNS 'resolver1.opendns.com' 'resolver2.opendns.com'
    export HOST_SSH "${DIR_TMP_CACHE}/host.ssh"
    export HOST_TIME 'north-america.pool.ntp.org'
    export DOMAIN "buglouse.info"

    export MSG_IRC_QUIT "IRC FTW"
    export MSG_IRC_QUIT_OFTC "SSL IRC FTW"
    export MSG_IRC_QUIT_FREENODE "#WeeChat #Mises #emacs"

    export XSET_KEYS '113' '115'

    [[ ${SSH_TTY} ]] && export TERM 'screen-256color'
    #[[ ${TMUX} ]] &&
    #    if
    #    else
    #        export TERM 'screen-256color'
    #        #export TERM 'rxvt-unicode-256color'
    #    fi

    export EDITORS 'emacs' 'vim'
    for item in "${EDITORS[@]}"; do
        type -P "${item}" >/dev/null || continue
        export EDITOR "${item}"
        break
    done

    #-----#
    export PERL_CPANM_OPT "--local-lib=${HOME}/.perl5"
    export PERL5LIB "${HOME}/.perl5/lib"

    export PAGER 'less'
    export ESHELL "${SHELL}"
    export INPUTRC "${HOME}/.readline/readline.conf"
    export MAILCAP "${HOME}/.mailcap"
    export CACHE_LD "${DIR_TMP_CACHE}/ld.so.cache"
    export HGRCPATH "${CONF_HG}"  #:order:2
    export WWW_HOME ''
    export RXVT_SOCKET "${SOCK_RXVT}"  #:order: 2
    export INI_CONKEROR "/usr/local/share/conkeror/application.ini"
    export MLDONKEY_DIR "${DIR_MLDONKEY}"  #:order: 2
    export LOCATEDB_EXCL '/mnt' '/tmp'
    export LOCATEDB_INCL '/'
    export SHELL_FM_HOME "${HOME}/.shellfm"  #:order:2
    export SLEEP_SUSPEND '35'
    export X509_CLIENT_CERT "${DIR_AUTH}/rsa.pem"
    export EMACSLOCKDIR "${DIR_CACHE_EMACS}"
    #####

    prompt_append 'profile.user.prompt'

    profile.user.variables_path
}
profile.user.variables_path() {
    local item items

    #-----#
    unset CDPATH
    items=( "${DIR_TMP}" "${HOME}"
        "${DIR_REPO}" "${DIR_MNT}" )
    for item in "${items[@]}"; do
        exist_dir "${item}" || continue
        [[ ${CDPATH} ]] && CDPATH+=':'
        CDPATH+="${item}"
    done
    export CDPATH
    export PATH_DIRS "${CDPATH[@]}"
}
profile.user.variables_prompt() {
    local item items=( $(jobs -rp) )
    export PROMPT_COLOR "${YELLOW}"  # "${ESC}38;5;7m"

    profile.user.variables_prompt_load

    item="\[${NORMAL}\]"
    (( REMOTE )) && item+="\[${PROMPT_COLOR}\](${HOSTNAME%-*})"
    item+="\[${ESC}38;5;8m\]{\[${NORMAL}\]"
    item+="\[${PROMPT_COLOR_LOAD}\]${PROMPT_LOAD}"
    item+="\[${ESC}38;5;8m\]}\[${NORMAL}\]"
    [[ ${items} ]] && item+="\[${YELLOW}\](${#items[@]})"
    item+="\[${NORMAL}\][${PWD##*/}]>"
    item+="\[${NORMAL}\]"

    export PS1 "${item[*]}"
}
profile.user.variables_prompt_load() {
    local item=( $(<"${PROC_LOADAVG}") )

    export LOADAVG "${item[@]::3}"
    export PROCESS "${item[@]:3:1}"

    export PROMPT_LOAD "${LOADAVG[0]}"
    if [[ ${PROMPT_LOAD} > 1 ]]; then
        PROMPT_COLOR_LOAD="${RED}"
    elif [[ ${PROMPT_LOAD} > 0.5 ]]; then
        PROMPT_COLOR_LOAD="${YELLOW}"
    else
        PROMPT_COLOR_LOAD="${GREEN}"
    fi
}
profile.user.prompt() {
    (( REMOTE )) ||
        if (( ! PROMPT && SHLVL > 1 )); then
           # request.ping && request.clifu
           true
        fi
    [[ ${TZ} ]] || timezone
    #[[ ${TMUX} ]] && exit() { $(exe tmux) detach; }
    profile.user.variables_prompt
}

profile.user.login() {
    local item items

    items=( 'udevd' 'syslogd' 'vnstatd' )
    for item in "${items[@]}"; do
        print_init "[${item^}]"
        if ${item} >/dev/null; then
            print_true 'True\n'
        else
            print_false 'False\n'
        fi
    done
    unset items

    #--SSH--#
    (( REMOTE )) && { profile.user.login_ssh; return; }

    read_true -3 'Profile User Login' || return

    #--DotRoot--#
    print_init "[ROOT] (${DIR_ETC})"
    if profile.user.dot_root; then
        print_true 'True\n'
    else
        print_false 'False\n'
    fi

    #--Hardware--#
    # device_bluetooth.sh --off --rfkill
    fan.sh -10

        #--Network--#
        # if network; then
        #     if read_false -5 'Repository'; then
        #         repository && . bash.sh
        #     fi
        # fi

    #--ACPID|Scheduler--#
    # items+=( 'acpid' 'loopd' )
    # for item in "${items[@]}"; do
    #     read_true -5 --erase "${item^}" || continue
    #     print_init "[${item^}]"
    #     if ${item} >/dev/null; then
    #         print_true 'True\n'
    #     else
    #         print_false 'False\n'
    #     fi
    # done
    # 
    # #--Xorg--#
    # read_true -5 --erase 'Xorg' && xorg
}
profile.user.login_ssh() {
    local item items

    read_true -3 'Profile User Login(SSH)' || return

    crypt.decrypt "${PATH_SERVICE}"

    #--Scheduler--#
    items=( 'loopd' )
    for item in "${items[@]}"; do
        read_true -5 --erase "${item^}" || continue
        print_init "[${item^}]"
        if ${item} >/dev/null; then
            print_true 'True\n'
        else
            print_false 'False\n'
        fi
    done

    #--Tmux--#
    read_true -5 --erase 'Tmux' && tmux
}

profile.user.alias() {
    alias vmSwappiness 'sudo sysctl -w vm.swappiness=30'
    alias freeCache '(sync; sudo sysctl -w vm.drop_caches=3)'

    alias Ndoc 'pushd ${DIR_REPO}/doc'
    alias Nsrc 'pushd ${DIR_REPO}/src'
    alias Ndot 'pushd ${DIR_REPO}/dot'

    e() { $(exe "${1}") "${@:2}"; }

    alias a 'math'
    alias m 'mpd_playlist'
    alias n 'pushd'
    alias p 'popd'
    alias h 'history'
    alias s 'source'
    alias v '${CMD_VIEW}'
    alias f 'type -a'
    alias nl '$(exe netstat) -ltunp 2>/dev/null |$(exe tail) -n +3|$(exe sort) -t ":" -nk2'
    alias nt '$(exe netstat) -tunp 2>/dev/null |$(exe tail) -n +3|$(exe sort) -rk6'
    alias td 'request.dictionary'
    alias ty 'request.dictionary --acry'
    alias ts 'request.dictionary --thes'
    alias tt 'request.dictionary --foldoc'
    alias lf 'list.file --dot --sort-dir'
    alias ld 'list.data'
    alias lm 'list.mount'
    alias lk 'list.module'
    alias lp 'list.process'
    alias xd '$(exe tmux) detach-client'
    alias pa 'kill "{DIR_TMP_PID}/power.pid'
    alias pg 'request.ping -l -0 8.8.4.4'
    alias pm 'request.ping -l -0 momento.buglouse.info'
    alias .. 'n "${PWD%/*}"'
    alias md 'alsa.sh --down'
    alias mp 'mplayer pause'
    alias mm 'alsa.sh --toggle'
    alias mu 'alsa.sh --up'
    alias mpc 'mpd_client'
    alias lfd 'list.file --dot --dir --sort-name'
    alias lff 'list.file --dot --file --sort-name'
    alias lft 'list.file --dot --sort-time'
    alias lfs 'list.file --dot --sort-size'
    alias ncmp 'ncmpcpp'
    alias edit '${EDITOR}'

    alias sf 'source_force'
    alias dateSec 'date "+%H:%M:%S:%N %s"'

    alias cd 'pushd'  #:obsolete:
}

profile.user.terminal() {
    [[ -t 0 && -t 1 ]] || return

    { $(exe stty) ispeed 115200 ospeed 115200 hupcl 
    $(exe stty) kill undef quit undef werase undef eof undef start undef stop undef
    $(exe stty) intr ; } 2>/dev/null
    #stop  start 
}

profile.user.dot() {
    local item item2 item3 itemD itemT 

    for item in "${DIR_REPOS[@]}"; do
        item="${DIR_REPO}/${item}"
        [[ -d ${item} ]] || continue

        for item2 in ${item}/!(.*); do
            link "${item2}" "${HOME}/.${item2##*/}"
        done
    done
    [[ ${HOME}/.skel ]] &&
        for item in ${HOME}/.skel/!(.)*; do
            link -H "${item}" "${HOME}/.${item##*/}"
        done
    parse.conf "${MAILCAP}"
}
profile.user.dot_root() {
    local item itemT itemM
    local reRO='sudoers' reNF='hosts.*'
    local pathD="${DIR_ETC}" pathS="${HOME}/.root"
    [[ -d ${pathS} ]] || return

    for item in ${pathS}/!(.*); do
        #[[ ${item} == +(${re}) ]] && { (( USE_NETFILTER )) && continue; }
        (( VERBOSE_LINK )) && print_link "${item}" "${pathD}/${item##*/}"
        parse.conf "${item}"
        sudo $(exe chown) -f root:root "${item}"
        if [[ ${item##*/} == +(${reRO}) ]]; then
            itemM='0440'
        elif [[ -d ${item} ]]; then
            itemM='0755'
        else
            itemM='0644'
        fi
        sudo $(exe chmod) "${itemM}" "${item}"
        sudo $(exe cp) -Hrp "${item}" "${pathD}"
    done
}
profile.user.dot_relink() {
    local item

    for item in "${@}"; do
        [[ -d ${item} ]] || continue

        _rm "${item}"
    done
    profile.user.dot
}

# local itemD itemsC=( "${CONF_}" )
# profile.user.dot_relink "${itemsC%/*}";
# parse.conf "${itemsC[@]}"
# $(exe true) &&
#     for item in "${itemsC[@]}"; do
#            itemD="${item#${HOME}/.}"
#            parse.diff "${DIR_REPO_DOT}/${itemD}" "${item}"
#     done

## 

mv() { $(exe mv) -n "${@}"; }
bc() { $(exe bc) "${@}"; }
gimp() { log.move "${LOG_GIMP}"; $(exe gimp) "${@}" &>"${LOG_GIMP}" & disown ${!}; }
lsof() { sudo $(exe lsof) "${@}" 2>/dev/null; }
make() { local item="${CDPATH}": unset CDPATH; $(exe make) "${@}"; CDPATH="${item}"; }
math() { printf 'scale=4; %s\n' "${*}" |$(exe bc); }
xsel() { xclip "${@}"; }
dnss() { $(exe sed) -n '/^nameserver.*/s/[^[:digit:]]*\([[:digit:]]*\)/\1/p' "${FILE_DNS}"; }
hosts() { $(exe grep) -v '^\(#\|127\|[:[:alpha:]]\|$\)' "${FILE_HOSTS}"; }
blkid() { $(exe blkid) "${@}" |$(exe grep) -v 'squashfs'; }
paths() { printf '%s\n' ${PATH//:/ }; }
aspell() { $(exe aspell) "${@}"; }
mounts() { $(exe mount) |$(exe grep) -v 'loop'; }
python() { $(exe python) "${@}"; }
sgdisk() { $(exe sgdisk) "${@}"; }
recover() { $(exe vlock) -a; fan.sh -A; }
badblock() { $(exe badblock) -n -o "${DIR_TMP_LOG}/badblock_${1}.log" -sv "${@:2}"; }
ldconfig() { sudo $(exe ldconfig) -C "${CACHE_LD}" '/usr/local/lib'; }
xclip() {
    local _DESC="
        Call, xclip if type -P ${CMD_XCLIP}.
        Execute only if DISPLAY is set."

    [[ ${DISPLAY} ]] || continue
    $(exe "${CMD_XCLIP}") --clipboard \
        --logfile "${LOG_XSEL}" \
        --display "${DISPLAY}" \
        "${@}"
}
readline() {
    bind -x '"\M-Y":"printf %s\\n "${READLINE_LINE:0:${READLINE_POINT}}"|xclip'

}
loopd() {
    (( REMOTE )) && return

    $(exe pgrep) -lf "loop.sh .* fan.sh" >/dev/null ||
        nohup loop.sh -5 'fan.sh --auto' &>/dev/null & disown ${!}
    $(exe pgrep) -lf "loop.sh .* request.dns" >/dev/null ||
        nohup loop.sh -30 'request.dns' &>/dev/null & disown ${!}
    $(exe pgrep) -lf "loop.sh .* repository" >/dev/null ||
        nohup loop.sh -15 'repository' &>/dev/null & disown ${!}
    $(exe pgrep) -lf "loop.sh .* battery.sh" >/dev/null ||
        nohup loop.sh -15 'battery.sh --loop' &>/dev/null & disown ${!}
}

rsync() {
    $(exe rsync) --archive --compress --verbose --progress --human-readable \
	--copy-dirlinks --copy-links --log-file="${LOG_RSYNC}" "${@}"
}

cal() {
    local _ARGS='<month> <year>'
    local year=$($(exe date) '+%Y') day=$($(exe date) '+%a')
    local mnth=$($(exe date) '+%B') dayD=$($(exe date) '+%d')

    if (( ${#} )); then
        local item item 
        for item in "${@}"; do [[ ${item} == +([[:digit:]]) ]] || return; done

        if (( ${#} < 2 )); then  # Month of current year.
            items=( "${@}" "${year}" )

        else  # Format year.
            item=( "${@:2:1}" )
            (( ${#item} == 2 )) &&
                items=( "${@:1:1}"
                    "${year%+([[:digit:]][[:digit:]])}${item}" ) #:todo: Quantifier?
        fi

        $(exe cal) "${items[@]}"

    else
        item=$($(exe cal) |$(exe head) -7)
        item="${item/${mnth}/${YELLOW}${mnth}${NORMAL}}"
        item="${item/${year}/${BOLD}${BLACK}${year}${NORMAL}}"
        [[ ${item} =~ ([ ]?${dayD/#0/ }) ]] &&
            item="${item/${BASH_REMATCH}/${MAGENTA}${BASH_REMATCH}${NORMAL}}"
        item="${item/${day%+([[:alpha:]])}/${GREEN}${day%+([[:alpha:]])}${NORMAL}}"

        printf '%b\n' "${item}"
        $(exe date) "+$(
            printf '%b{%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%w%b' "${YELLOW}" "${NORMAL}"
            printf '%b}%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%R%b' "${GREEN}" "${NORMAL}"
            printf '%b%%z%b' "${RED}" "${NORMAL}"
            printf '%b[%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%j%b' "${CYAN}" "${NORMAL}"
            printf '%b.%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%U%b' "${CYAN}" "${NORMAL}"
            printf '%b]%b' "${BOLD}${BLACK}" "${NORMAL}"
            )"
    fi
}
date() {
    if [[ ${1} && ${1} != +(-)* ]]; then
        $(exe date) "${@}"
    else
        $(exe date) "+$(
            printf '%b%%Y' "${NORMAL}"
            printf '%b.%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%m' "${NORMAL}"
            printf '%b.%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%d' "${NORMAL}"
            printf '%b{%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%w%b' "${YELLOW}" "${NORMAL}"
            printf '%b}%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%R%b' "${GREEN}" "${NORMAL}"
            printf '%b%%z%b' "${RED}" "${NORMAL}"
            printf '%b[%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%j%b' "${CYAN}" "${NORMAL}"
            printf '%b.%b' "${BOLD}${BLACK}" "${NORMAL}"
            printf '%b%%U%b' "${CYAN}" "${NORMAL}"
            printf '%b]%b' "${BOLD}${BLACK}" "${NORMAL}"
            )" "${@}"
    fi
}

backup() {
    local passR passF

    read_true -3 'Repository' && (( passR++ ))
    read_false -3 'Backup' && (( passF++ ))

    (( passR )) && repository_bb
    (( passF )) && filetool.sh -b
}
power() {
    (( REMOTE )) || backup

    power.sh "${@:---ask}"
}
halt() { power --halt "${@}"; }
reboot() { power --reboot "${@}"; }
poweroff() { power --power "${@}"; }
shutdown() { power --power "${@}"; }
abrt() { kill --abrt 'power.sh' ;}
view() { view.sh "${@}"; }
man() { view.sh "${@}"; }
xpdf() {
    $(exe xpdf) -cfg "${CONF_XPDF}" "${@}" &
}

#--File--#
dd_zero() {
    $(exe dd) if='/dev/zero' of="${DIR_TMP_DATA}/test.zero" bs=1024 count="$(( 1024*50 ))"
}
####

#--Kernel--#
modprobe_backlist() {
    #touch "${CONF_MODPROBE}"
    (( ${#} )) || return
    local item mode=$($(exe stat) -c '%a' "${CONF_MODPROBE}")

    sudo $(exe chmod) 666 "${CONF_MODPROBE}"
    trap '{ sudo $(exe chmod) "${mode}" "${CONF_MODPROBE}"; }' RETURN

    for item in "${@}"; do
        $(exe grep) "${item}" "${CONF_MODPROBE}" && continue
        printf 'blacklist %40s\n' "${item}" >>"${CONF_MODPROBE}"
    done
}
#####

#--Terminal--#

terminfo() {
    for item in "${@}"; do
        $(exe tic) -o"${DIR_TMP_CACHE}" "${item}"
    done
}

terminal_title() { printf '\033]2;%s\033\\' "${*}"; }
#####

#--Security--#

sec_perm() {
    $(exe find) ${PATH} \( -perm -4000 -o -perm -2000 \) -print
    $(exe find) ${PATH} -prune -o -type f -perm +6000 -ls
    $(exe find) / -xdev -type d \( -perm -0002 -a ! -perm -1000 \) -print
    $(exe find) / -xdev \( -nouser -o -nogroup \) -print
}

#####

#--Query--#

locate() {
    $(exe locate) -i \
        --database="${DB_LOCATE}" \
        "${@}" 2>/dev/null
}
updatedb() {
    local _ARGS='(slocate) (path-ignore) [search-path]'
    local _DESC="
        Update locate database.

        # Bypass suid and passwd."
    local pathO="${DB_LOCATE}"
    local pathI=( "${LOCATEDB_EXCL[@]}" )
    local pathS=( "${LOCATEDB_INCL[@]}" )

    if (( USE_MLOCATE )); then
        $(exe updatedb) \
            --database-root '/' \
            --prunepaths "${pathS}" \
            --output "${pathO}"
    else
        local args item
        #local cmdCode="$($(exe find) "${DIR_USR}" -regex '.*/lib/.*' -name 'code')"
        local cmdCode="$($(exe find) "${DIR_USR}" -name 'code' -path '*lib*')"
        local cmdBigram="$($(exe find) "${DIR_USR}" -name 'bigram' -path '*lib*')"
        local cmdFrcode="$($(exe find) "${DIR_USR}" -name 'frcode' -path '*lib*')"
        local argFrcode=( '-0' )
        [[ -e ${cmdCode} && -e ${cmdBigram} && -e ${cmdFrcode} ]] ||
            { print_false 'Found!\n'
            [[ -e ${cmdCode} ]] || print_warn 'code '
            [[ -e ${cmdBigram} ]] || print_warn 'bigram '
            [[ -e ${cmdFrcode} ]] || print_warn 'frcode '
            printf '\n'; return 1; }

        while [[ ${1} ]]; do
            parse.arg "${@}"; shift ${SHIFT}
            [[ ! ${ARG} && ${ARG_OPT} ]] && pathS=( "${ARG_OPT[*]}" )
            for item in "${ARG[@]}"; do
                case "${item}" in
                    \?) return 1 ;;
                    path) pathO="${ARG_OPT}" ;;
                    slocate) argFrcode=( "-S 1" ${argFrcode} ) ;;
                    path-ignore) pathI+=( "${pathI:+|}${ARG_OPT[@]}" ) ;;
                esac
            done
        done
        local IFS='|'; pathI="-type d -regex ${pathI[*]}|/dev|/sys|/proc"; unset IFS
        local itemT=$($(exe mktemp) -p "${DIR_TMP}" "locate.XXXXXX")
        local itemTB=$($(exe mktemp) -p "${DIR_TMP}" "locate.XXXXXX")
        trap '{ rm -f "${itemT}" "${itemTB}"; }' RETURN

        #:todo: FS types or paths, busybox find ! support fstype?
        set +C
        $(exe find) "${pathS[@]}" \
            \( ${pathI//|/\\\|} \) -prune -o -print0 2>/dev/null |
            $(exe sort) -f |
            ${cmdFrcode} ${argFrcode[@]} \
            >"${itemT}"
        [[ -e ${itemT} ]] || { print_false 'Error! find\n'; return 1; }
        ${cmdBigram} <"${itemT}" |
            $(exe uniq) -c |$(exe sort) -nr |
            $(exe awk) '{ if (NR <= 128) print $2 }' |
            $(exe tr) -d '\012' \
            >"${itemTB}"
        [[ -e ${itemTB} ]] || { print_false 'Error! bigram\n'; return 1; }
        ${cmdCode} "${itemTB}" <"${itemT}" >"${pathO}"
        set -C
        [[ -e ${pathO} ]] || { print_false 'Error!\n'; return 1; }
    fi
}

syslogd() {  #:daemon:
    if exist_proc 'syslogd'; then
        kill --hup 'syslogd'

    else
        #local items=( "${HOSTNAME}_syslog" )
        #local port="${HOSTNAME^^}_SYSLOG_PORT"

        #[[ ${items} ]] && { identity.define_host "${items[@]}" || return; }

        # -L -R "${HOSTNAME}:${!port}"
        sudo $(exe syslogd) \
            -O "${LOG_SYSLOGD}" \
            "${@}"
    fi
}
#####

#--Macro--#

xbindkeys() {
    [[ -e ${CONF_XBINDKEYS} ]] || return

    if (( ${#} )); then
        $(exe xbindkeys) --display "${DISPLAY}" --file "${CONF_XBINDKEYS}" "${@}"
    elif exist_proc 'xbindkeys'; then
        kill --hup 'xbindkeys'
    else
        $(exe xbindkeys) --display "${DISPLAY}" --file "${CONF_XBINDKEYS}"
    fi
}
#####

#--Auto--#

udev() {
    local item

    for item in "${@}"; do
        exist "${item}" || continue
        $(exe udevadm) info --query=all --name="${item}"
    done
}

loop() {
    local out item digit=3

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            +([[:digit:]])) digit="${item}" :;
        esac
        shift
    done

    while true; do
        out=( "$(${@})" )
        clear; printf '%s\n' "${out[@]}"
        sleep $(( digit ))
    done
}

udevd() {
    if exist_proc 'udevd'; then
        return
        $(exe udevadm)  control --reload-rules
    else
        sudo $(exe udevd) --daemon
    fi
}

acpid() {
    exist_proc "${PID_ACPID}" ||
        sudo $(exe acpid) \
            -p "${PID_ACPID}" \
            -a "${CONF_ACPID}" \
            -c "${CONF_ACPID%/*}" \
            -M "${CONF_ACPID_MAP}" \
            "${@}"
}

#####

#--Display--#

urxvtc() { $(exe urxvtc) +meta8 --perl-lib "${DIR_PERL}/urxvt" "${@}"; }
urxvt_font_increase() {
    printf '\33]50;%s\007' "xft:Terminus:pixelsize=20"

}

xorg() {
    export DISPLAY ':0.0'
    [[ ${VERSION_TC} ]] && tc.module
    x.sh --init || return
    xorg_xset
    xbindkeys
    tmux
    touchpad.sh --off
    $(exe vlock)
}
xorg_xset() {
    $(exe xset) r rate 400 30
    for item in "${XSET_KEYS[@]}"; do
        $(exe xset) -r "${item}"
    done
}

tmux() {
    #mkdir '/usr/libexec'
    #[[ -e /usr/libexec/pt_chown ]] || sudo $(exe cp) -s /usr/lib/glibc/pt_chown /usr/libexec/pt_chown

    if (( REMOTE )); then
        local sock="${SSH_CLIENT%% *}_${SSH_CLIENT##* }"

        if [[ ${TMUX} ]]; then
            $(exe tmux) -S "${SOCK_TMUX}-${sock}" "${@}"
        else
            $(exe tmux) -q \
                -S "${SOCK_TMUX}-${sock}" \
                -f "${CONF_TMUX_SSH}" \
                "${@:-attach-session}" && exit
        fi

    elif [[ ${TMUX} ]]; then
	# Tmux command.
        $(exe tmux) -S "${SOCK_TMUX}" "${@}"

    else
        #EVENT_NOEPOLL=1
        ( $(exe tmux) -q -S "${SOCK_TMUX}" -f "${CONF_TMUX_ONE}" &)

        if [[ ${DISPLAY} ]]; then
            exist_proc 'urxvtd' || $(exe urxvtd) -q -f
            urxvtc -e $(exe tmux) -S "${SOCK_TMUX}" "${@:-attach-session}"
        else
	    $(exe tmux) -S "${SOCK_TMUX}" "${@:-attach-session}"
        fi
    fi
}
tmux_redraw() { tmux refresh-client -S; }

feh() {
    parse.conf "${CONF_FEH}"
    $(exe feh) --rcfile "${CONF_FEH}" "${@}"
}
gifview() { $(exe gifview) "${@}"; }
#####

#--Network--#

timezone() {
    local col line int deg mrk='-' intC=0

    request.ping &&
        while read col line; do
            [[ ${col} == +(lon)* ]] || continue
            #:fix: Busybox? 
            if [[ ${line} == +(-)* ]]; then
                mrk='+'
            else
                mrk='-'
            fi
            line="${line#+([-+])}"
            deg="${line%+(.)*}"

            for (( int=0;int<=165;int=$((int+15)) )); do
                (( ${deg} <= int )) && break
                (( intC++ ))
            done
            break
        done <<<"$(request.geo)"

    # M<month>.<week>.<day>/%R,M<month>.<week>.<day>/%R
    # First; forward. Second; backward.
    # %R[02:00:00], time of effect.
    # Day; 0-6, sunday offset.
    # Week; 1-5, 5 last.
    export TZ "UTC${mrk}${intC}DST,M3.2.0,M11.1.0"
}
clock() {
    local _ date time item pass

    unset pass
    for item in "${HOST_TIME[@]}"; do
        sudo $(exe ntpclient) -h "${item}" -s >/dev/null || continue
        #read _ date time _ <<<$($(exe nc) -w 3 "${HOST_TIME}" 13 |$(exe grep) 'UTC(NIST)')
        #[[ ${date} && ${time} ]] || { print_false 'Server: %s\n' "${HOST_TIME}"; return 1; }
        (( pass++ ))
        break
    done
    (( pass )) || return

    timezone

    #sudo $(exe date) -u -s "20${date} ${time}" >/dev/null
    sudo $(exe hwclock) -w -u
    date
}

network() {
    trap '{ return; }' EXIT INT

    network.sh "${@:--w}" || return
    request.dns
    clock

    trap EXIT
}

rndc() {
    local item="${1}" pass="${1^^}_DNS_PASS" user="${1^^}_DNS_USER"

    identity.define "${item}_dns" &&
        identity.define_host "${item}_dns" ||
            { print_false 'Undefined! %s\n' "${item}"; return 1; }
    [[ ${!pass} && ${!user} ]] || return

    $(exe grep) -q "\b${!user}\b" "${CONF_RNDC}" ||
        printf 'key %s { algorithm hmac-md5; secret "%s"; };\n' \
            "${!user}" "${!pass}" >>"${CONF_RNDC}"

    $(exe rndc) \
        -c "${CONF_RNDC}" \
        -s "${!user}" -y "${!user}" \
        "${@:2}"
}

named() {
    exist_proc 'named' && { kill --hup 'named'; return; }

    identity.define "${HOSTNAME}_dns" && identity.define_host "${HOSTNAME}_dns" ||
        { print_false 'Undefined! %s\n' "${HOSTNAME}"; return 1; }

    mkdir "${DIR_TMP_CACHE}/named"

    named_root

    export HOST_ADDRESS $(request.address)
    export HOST_NETWORK $(request.address 'network')
    [[ ${HOST_ADDRESS} && ! ${IP_NETWORK} ]] && IP_NETWORK="${HOST_ADDRESS}/32"
    [[ ${HOST_ADDRESS} && ${IP_NETWORK} ]] || return
    export TIME $($(exe date) -u '+%s')
    parse.conf "${CONF_NAMED}" ${CONF_NAMED%/*}/*.zone

    #-u nobody -t "${DIR_CHROOT_NAMED}" \
    named-checkconf "${CONF_NAMED}" &&
        sudo $(exe named) -d 9 \
            -c "${CONF_NAMED}" "${@}"
}

named_zone() {
    local _ARGS='<host> <domain>'
    local _DESC="
        Update DOMIAIN zone on HOST.

        Host, connect to named.
        Domain, vaild subs."
    local item="${1}" item2="${2:-${HOSTNAME}}" host pass
    set -x
    [[ ${item} && ${item2} ]] || { print_help; return 1; }
    identity.define "${item}_dns" || return
    host="${item^^}_DNS_USER" pass="${item^^}_DNS_PASS"
    host="${!host}" pass="${!pass}"

    { printf 'server %s\n' "${host}"
    printf 'zone %s.\n' "${item2}"
    printf 'update delete %s. A\n' "${item2}"
    printf 'update add %s 60 A %s\n' "${item2}" $(request.address)
    printf 'send\n'; } | $(exe nsupdate) -y "${item}:${pass}"
    set +x
}
named_root() {
    [[ -e ${DIR_DNS_ZONE}/root.zone ]] && return
    $(exe dig) @a.root-servers.net . ns |
        $(exe grep) -v '^;\|^$' >"${DIR_DNS_ZONE}/root.zone"
}

chromium() {
    $(exe chromium-browser) --renderer-cmd-prefix='env' "${@}" &
}

#####

    #--Connectivity--#

wpa_supplicant() {
    [[ ${1} ]] || return
    [[ -e ${CONF_WPASUPPLICANT} ]] || return

    kill 'wpa_supplicant'
    log.move "${LOG_WPASUPPLICANT}"
    parse.conf "${CONF_WPASUPPLICANT}"

    sudo $(exe wpa_supplicant) -Bddt \
        -f"${LOG_WPASUPPLICANT}" -P"${PID_WPASUPPLICANT}" -c"${CONF_WPASUPPLICANT}" \
        -i"${1}"

    #wpa_cli \
    #    -P "${PID_WPASUPPLICANT}" \
    #    -p "${SOCK_WPASUPPLICANT}" \
    #    -i "${1}"
}

udhcpc() {
    local exe
    [[ ${1} ]] || return

    if [[ ${VERSION_TC} ]]; then #:fix: broken malloc().
        exe='/bin/busybox udhcpc'
    else
        exe="$(exe udhcpc)"
    fi
    sudo ${exe} -aqfB \
        -p "${PID_UDHCPC}" -s "${SCRIPT_DHCP}" -i "${1}"
}

cupsd() {
    mkdir "${DIR_CACHE_CUPSD}"

    identity_cups || return
    
    parse.conf "${CONF_CUPSD}"
    sudo $(exe cupsd) -c "${CONF_CUPSD}"
}
lpstat() {
    identity_cups || return

    $(exe lpstat) -h "${CUPSD_HOST}:${CUPSD_PORT}" "${@}"
}
#lpr() {
#    identity_cups || return
#    
#    $(exe lpr) -H "${CUPSD_HOST}:${CUPSD_PORT}" "${@}"
#}
identity_cups() {
    identity.define_host "localhost_cups" || return
    CUPSD_PORT="${LOCALHOST_CUPSD_PORT}"
    CUPSD_HOST="${LOCALHOST_CUPSD_HOST}"
}
#####

    #--Repository--#

git() { $(exe git) "${@}"; }

repository_bb() {
    local items=( "${PATH_SERVICE}" "${PATH_IDENTITY}" )

    crypt.decrypt "${items[@]}" || return
    identity.define 'ssh.bitbucket.org' || return
    crypt.decrypt ${DIR_AUTH}/${SSH_BITBUCKET_ORG_PASS}.* || return

    repository.sh --data "${SSH_BITBUCKET_ORG_USER[@]}"
}
repository() { repository_bb; }

hg() {
    local items=( "${CONF_HG}" "${CONF_SSH}" )

    identity.define 'mail' 'name' >/dev/null || return
    parse.conf "${items[@]}"
    $(exe hg) "${@}"
}

imapfilter() {
    identity_mail
    parse.conf "${CONF_IMAPFILTER}"
    
    $(exe imapfilter) -c "${CONF_IMAPFILTER}" -l "${LOG_IMAPFILTER}" "${@}"
}
#####

    #--Communication--#

weechat() {
    # Assumes proxy.
    if exist_proc 'weechat'; then
        $(exe weechat-curses) --dir "${CONF_WEECHAT%/*}" "${@}"
        return
    fi

    local itemD items itemsH item="${1,,}" items=( 'nick' )
    local itemsC=( "${CONF_WEECHAT}" "${CONF_WEECHAT_SERVER}" "${CONF_WEECHAT_XFER}"
        "${CONF_WEECHAT_URLGRAB}" "${CONF_WEECHAT_LOGGER}" "${CONF_WEECHAT_RELAY}"
        "${CONF_WEECHAT_PLUGIN}" "${CONF_WEECHAT_ALIAS}" )
    if identity.contains_host "${item}"; then
        items+=( "${item}_znc" )
        itemsH=( "${item}_znc" "${item}_bee" )

    else
        items+=( 'freenode.net' 'oftc.net' 'springlobby.info' )
    fi

    [[ ${items} ]] && { identity.define "${items[@]}" || return; }
    [[ ${itemsH} ]] && { identity.define_host "${itemsH[@]}" || return; }

    profile.user.dot_relink "${CONF_WEECHAT%/*}"

    crypt.pem "${HOSTNAME}" || return
    parse.conf "${itemsC[@]}"
    #inotify
    if TERM=rxvt-256color $(exe weechat-curses) \
            --dir "${CONF_WEECHAT%/*}" "${@}"; then
        for item in "${itemsC[@]}"; do
            itemD="${item#${HOME}/.}"
            parse.diff -= "${DIR_REPO_DOT}/${itemD}" "${item}"
        done
    fi
}

znc() {  #:daemon:
    local item itemH
    local items=( 'nick' "${HOSTNAME}_irc" "${HOSTNAME}_znc" "${HOSTNAME}_bee" )
    local itemsH=( "${HOSTNAME}_znc" "${HOSTNAME}_znc_telnet" "${HOSTNAME}_bee" )

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            kill)
                kill "${PID_ZNC}"
                znc_log -unmount
                $(exe pkill) -lf "znc_archive"
                return
            ;;
            *) break ;;
        esac
        shift
    done

    itemH="${HOSTNAME^^}_ZNC_USER[@]"
    for item in "${items[@]}" "${!itemH}"; do identity.define "${item}"; done
    identity.define_host "${itemsH[@]}" || return

    crypt.pem "${HOSTNAME}" || return

    export HOST_ADDRESS $(request.address)
    [[ ${HOST_ADDRESS} ]] || return
    parse.conf "${CONF_ZNC}"

    $(exe pgrep) -lf "loop.sh .* znc_archive" >/dev/null ||
        nohup loop.sh -30 'znc_archive' &>/dev/null & disown ${!}
    znc_log -mount
    exist_proc "${PID_ZNC}" && { kill --hup "${PID_ZNC}"; return; }

    $(exe znc) "${@}" || return
    $(exe ln) -s "${DIR_ZNC}/moddata/adminlog/znc.log" "${LOG_ZNC}" 2>/dev/null
    znc_modules
}
znc_cert() {
    local _DESC="
        Cert module, local certs proxied."
    local item pass itemO itemX items=( ${HOME}/.znc/users/* )
    local itemR="${HOME}/.znc/moddata/certauth/.registry"
    local itemC="${DIR_AUTH_TMP}/${HOSTNAME}.pem.rsa"

    mkdir "${HOME}/.znc/moddata/certauth"

    for item in "${items[@]}"; do
        itemO="${item}/moddata/cert/user.pem"
        [[ -e ${itemC} ]] ||
            { print_false 'Invalid! '; print_warn '%s\n' "${itemC}"; return 1; }

        # user.pem
        exist_size "${itemO}" ||
            { set +C; $(exe cat) "${itemC}" >"${itemO}"; set -C; }
        $(exe grep) -q "${item##*/}" "${itemR}" 2>/dev/null && continue
        itemX="$(crypt.md5 "${itemC}")"
        printf '%s %s%%20;\n' "${item##*/}" "${itemX,,}" >>"${itemR}"
        # CertAuth/registry; user fingerprint'%20;'.
        (( pass++ ))
    done
    (( pass ))
}
znc_modules() {
    local port user pass nick item botNick botUser botPass

    local items=( 'nick' "${HOSTNAME}_znc" )
    local itemsH=( "${HOSTNAME}_znc" "${HOSTNAME}_znc_telnet" )

    [[ ${items} ]] && { identity.define "${items[@]}" || return; }
    [[ ${itemsH} ]] && { identity.define_host "${itemsH[@]}" || return; }

    port="${HOSTNAME^^}_ZNC_TELNET_PORT" port="${!port}"
    user="${HOSTNAME^^}_ZNC_USER[@]" user=( ${!user} )
    pass="${HOSTNAME^^}_ZNC_PASS" pass="${!pass}"
    nick="${NICK_USER}"
    [[ ${port} ]] || return

    for item in "${user[@]}"; do
        printf 'PASS %s/%s:%s\nNICK %s\nUSER %s %s %s :%s\n' \
            'user' "${item}" "${pass}" \
            "${nick}" \
            "${nick}" "${nick}" "${HOSTNAME}.${DOMAIN}" "${nick}"

        continue
        printf 'PRIVMSG ~watch ADD * *watch *%%nick%%*\n'
        printf 'PRIVMSG ~simple_away REASON %%s: Leave messge\n'

        case "${item}" in
            p2p-network.net|freenode.net) botNick='IdleBot' ;;
            abjects.net|efnet.org|gamesurge.net) botNick='IdleRPG' ;;
            rizon.net) botNick='RizonIRPG' ;;
            oftc.net) botNick='Bot' ;;
            *) continue ;;
        esac
        identity.define "${item}_${botNick,,}" || continue
        item="${item//./_}"
        botUser="${item^^}_${botNick^^}_USER" botUser="${!botUser}"
        botPass="${item^^}_${botNick^^}_PASS" botPass="${!botPass}"
        [[ ${botUser} && ${botPass} ]] || continue

        printf 'PRIVMSG %s LOGIN %s %s\n' \
            "${botNick}" "${botUser}" "${botPass}"
        printf 'PRIVMSG ~perform ADD msg %s LOGIN %s %s\n' \
            "${botNick}" "${botUser}" "${botPass}"
    done |$(exe nc) 'localhost' "${port}" >/dev/null
}
znc_log() {
    local action item item2 itemD itemS itemsU itemsX itemsXF itemsX2
    local items

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            archive) action='archive' ;;
            clear) action='clear' ;;
            mount) action='mount' ;;
            unmount) action='unmount' ;;
        esac
        shift
    done
    
    if [[ ${action} == archive ]]; then  # Archive
        items=( ${DIR_TMP_LOG}/znc/* )

        $(exe 7z) u -l -mx=9 \
            "${DIR_DATA_LOG}/znc.7z" "${items[@]}" && $(exe rm) -f ${items[@]/%//*}

    else
        items=( ${HOME}/.znc/users/* ) 

        for item in "${items[@]}"; do
            itemS="${item}/moddata/log"
            itemD="${DIR_TMP_LOG}/znc/${item##*/}"

            if [[ ${action} == clear ]]; then  # Remove log files older than limit.
                declare -A itemsU
                itemsX=( ${itemD}/* )

                unset itemsXF
                for item2 in "${itemsX[@]%_+([[:digit:]]).log}"; do
                    (( ${itemsU["${item2}"]} )) && continue
                    (( itemsU["${item2}"]++ ))

                    itemsX2=( ${item2}* )
                    (( ${#itemsX2[@]} > 4 )) || continue
                    itemsXF+=( "${itemsX2[@]::(${#itemsX2[@]}-5)}" )
                done

                #printf '%s\n' "${itemsXF[@]}"
                $(exe rm) -f "${itemsXF[@]}"

            elif [[ ${action} == unmount ]]; then
                $(exe grep) -qw "${itemS}" "${PROC_MOUNTS}" || continue
                sudo $(exe umount) "${itemS}"

            elif [[ ${action} == mount ]]; then
                $(exe grep) -qw "${itemS}" "${PROC_MOUNTS}" && continue
                mkdir "${itemD}" "${itemS}"
                sudo $(exe mount) --bind "${itemD}" "${itemS}"
            fi
        done
    fi
}
znc_archive() { znc_log -archive; }
znc_umount() { znc_log -unmount; }
znc_clear() { znc_log -clear; }
znc_web() {
    local item="${1}" host port user
    [[ ${1} ]] || { print_false 'Missing! '; printf 'Host\n'; return 1; }

    identity.define_host "${item}_znc" && identity.define "${item}_znc" ||
        { print_false 'Missing! '; printf "${item}\n"; return 1; }
    host="${item^^}_ZNC_HOST" host="${!host}"
    port="${item^^}_ZNC_PORT" port="${!port}"
    user="${item^^}_ZNC_USER" user="${!user}"

    printf "https://${user}@${host}:${port}\n"
    $(exe elinks) -remote "https://${host}:${port}"
}

bitlbee() {  #:daemon:
    exist_proc "${PID_BITLBEE}" && { kill --hup "${PID_BITLBEE}"; return; }
    local itemsH port host

    itemsH=( "${HOSTNAME}_bee" )
    port="${HOSTNAME^^}_BEE_PORT" host="${HOSTNAME^^}_BEE_HOST"
    #pass="${HOSTNAME^^}_BEE_PASS"

    [[ ${itemsH} ]] && { identity.define_host "${itemsH[@]}" || return; }

    crypt.pem "${HOSTNAME}" || return

    #key=$($(exe bitlbee) -x enc $($(exe bitlbee) -x hash "${!pass}") "${!pass}")
    #export KEY "${key}"
    parse.conf "${CONF_BITLBEE}"
    $(exe bitlbee) -F \
        -d "${CONF_BITLBEE%/*}" \
        -c "${CONF_BITLBEE}" \
        -P "${PID_BITLBEE}" \
        -p "${!port}" \
        -i 127.0.0.1 \
        "${@:--F}"
}
bitlbee_backup() {
    local item itemD="${DIR_DATA_BITLBEE}" items=( ${CONF_BITLBEE%/*}/*.xml )

    if [[ ${items} ]]; then  # Archive
        $(exe 7z) u -l -mx=9 "${itemD}/bitlbee.7z" "${items[@]}" &&
            crypt.encrypt "${itemD}/bitlbee.7z" "${DIR_DATA_BITLBEE}"
    else  # Extract
        #if ! [[ -e ${itemD}/bitlbee.7z ]]; then 
        #    true  # Decrypt
        #fi
        $(exe 7z) e "${itemD}/bitlbee.7z" -o"${CONF_BITLBEE%/*}"
    fi
}

tord() { #:daemon:
    local item="${HOSTNAME}"
    identity.define_host "${item}_tor" || return
    PORT_TOR="${item^^}_TOR_PORT" PORT_TOR="${!PORT_TOR}"

    parse.conf "${CONF_TOR}" || return
    $(exe tor) -f "${CONF_TOR}" "${@}"
}
#####

    #--Viewer--#

elinks(){
    local args host port
    local item itemD itemsC=( "${CONF_ELINKS}" )

    if (( PROXY )); then
	unset HTTP_PROXY
        args=( '-no-connect' '1' )

    elif false; then #[[ ! ${HTTP_PROXY} ]] && identity.define_host "http_proxy"; then
        host="${HTTP_PROXY_HOST}"
        port="${HTTP_PROXY_PORT}"

        if ssh "${port}:${host}"; then
            export HTTP_PROXY "localhost:${port}"
        else
            export HTTP_PROXY "${host}:${port}"
        fi
    fi

    if exist_proc 'elinks'; then
        $(exe elinks) \
            -config-dir "${CONF_ELINKS%/*}" \
            -config-file "${CONF_ELINKS##*/}" \
            "${args[@]}" "${@}"

    else
        #profile.user.dot_relink "${itemsC%/*}";

        parse.conf "${itemsC[@]}"
        $(exe elinks) \
            -config-dir "${CONF_ELINKS%/*}" \
            -config-file "${CONF_ELINKS##*/}" \
            "${@}" || return
        for item in "${itemsC[@]}"; do
            itemD="${item#${HOME}/.}"
            parse.diff "${DIR_REPO_DOT}/${itemD}" "${item}"
        done
    fi
    unset HTTP_PROXY
}

mutt() {
    identity.define 'mail' || return
    $(exe mutt) -F "${CONF_MUTT}" "${@}"
} 

newsbeuter() {
    local item itemD itemsC=( 
        "${CONF_NEWSBEUTER}" 
        "${CONF_NEWSBEUTER_BIND}"
        "${CONF_NEWSBEUTER_FORMAT}" )

    DIR_NEWSBEUTER="${HOME}/.newsbeuter"
    profile.user.dot_relink "${itemsC%/*}";
    parse.conf "${itemsC[@]}" 
    #$(exe newsbeuter) \
    #    -C "${CONF_NEWSBEUTER}" \
    #    -i "${DIR_MEDIA}/list/feed.opml" \
    #    -u "${DIR_TMP_CACHE}/newsbeuter.urls"
    if TERM='rxvt-256color' $(exe newsbeuter) -l 2 \
        -C "${CONF_NEWSBEUTER}" \
        -d "${DIR_TMP_LOG}/newsbeuter.log" \
        -u "${DIR_LIST}/feed.newsbeuter" \
        -c "${DIR_TMP_CACHE}/newsbeuter.cache" \
        "${@}"; then
        for item in "${itemsC[@]}"; do
            itemD="${item#${HOME}/.}"
            parse.diff "${DIR_REPO_DOT}/${itemD}" "${item}"
        done
    fi
}

conkeror() {
    local item="${1}"

    if [[ ${item} ]]; then
        item="${item##*?(://)?(www.)}"
        identity.define "${item%%/*}"
    fi

    parse.conf "${INI_CONKEROR}" || return
    log.move "${LOG_XULRUNNER}"
    $(exe xulrunner) "${INI_CONKEROR}" "${@}" &>"${LOG_XULRUNNER}" & disown ${!}
    #-batch -e "user_pref('conkeror.rcfile', '${HOME}/.conkeror');"
}

kismetd() {  #:daemon:
    local items

    kill 'wpa_supplicant' 'kismet'

    source 'device_network'
    selects -1 "${DEVICE_NETWORK_WLAN[@]#*,}"

    parse.conf "${CONF_KISMET}"

    items=( ${DIR_TMP_LOG}/kismet* )
    log.move "${items[@]}"
    sudo $(exe kismet_server) --daemonize \
        --config-file "${CONF_KISMET}" \
        --capture-source "${SELECT}" \
        "${@}"
}
kismet() {
    parse.conf "${CONF_KISMET_UI}"

    TERM='rxvt-256color' $(exe kismet_client) -r \
        --config-file "${CONF_KISMET}" \
        "${@}"
}

dumpcap() {
    local item
    local _DESC="<proto.field>
        comparison [<|>|>=|<=|=|!=|contains <string>|matches <PCRE>|[#:#]]
            contains
                Incompatible with atomic fields.
            slice
                Offset:Length. Support; negative offset, comma concatenation.
                [off:len]
                [off-len] inclusive
                [off] len=1
                [:len] off=0
                [off:] len=end
            bitwise [&]
            logical [&&|\|\||!]
        protocol
            ip.[addr|host|src|dst]
            http."

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            *) $(exe dumpcap) "${@}"; return ;;
        esac
        shift
    done

    selects -1 'any' "${DEVICE_NETWORK[@]}" 'nflog' 'lo'
    item="${DIR_TMP_LOG}/${SELECT}.pcap"

    $(exe dumpcap) -i "${SELECT}" -w "${item}" ${1:+-f "${*}"}
    log.move "${item}"
}
wireshark() {
    local arg item itemD pass itemsC=( "${CONF_WIRESHARK}" )
    [[ ${1} == *+(-)* ]] && { $(exe wireshark) "${@}"; return; }

    for item in "${@}"; do [[ -e ${item} ]] && (( pass++ )); done
    (( pass )) || { selects -1 ${DIR_TMP_CACHE}/*.pcap?(_*) && arg="${SELECT}"; }

    profile.user.dot_relink "${itemsC%/*}";
    parse.conf "${itemsC[@]}"
    log.move "${LOG_WIRESHARK}"
    if $(exe wireshark) "${@}" ${arg} &>"${LOG_WIRESHARK}"; then
        for item in "${itemsC[@]}"; do
            itemD="${item#${HOME}/.}"
            parse.diff "${DIR_REPO_DOT}/${itemD}" "${item}"
        done
    fi
}
tshark() {
    local _DESC="
        Format.output(-T) [pdml|psml|ps|text|fields]
        "
    local item items args

    for item in "${@}"; do
        if [[ -e ${item} ]]; then
            items+=( "-r ${item}" )
        else
            args+=( "${item}" )
        fi
    done
    [[ ${items} ]] ||
        { selects ${DIR_TMP_LOG}/*.pcap?(_*) && items=( "${SELECT[@]}" ); }

    $(exe tshark) ${items[@]} "${args[@]}" |${CMD_VIEW} -
}
pcap_remove() { _rm ${DIR_TMP_LOG}/*.pcap_*; }

vnc() {
    local port host pass

    identity.define "${1}_vnc"; identity.define_host "${1}_vnc"
    host="${1^^}_VNC_HOST" port="${1^^}_VNC_PORT" pass="${1^^}_VNC_PASS"
    [[ ${!host} && ${!pass} && ${!port} ]] || return

    printf '%s' "${!pass}" |
        $(exe vncviewer) -autopass -shared -compresslevel 5 \
            "${!host}:${!port}" &
}

sshd() {  #:daemon:
    local _ARGS='port'
    local _DESC="SSHD, SSH Daemon.
        PORT from argument or identity.sh(HOSTNAME).
        HUP daemon if running.
        Use decrypt.sh(${HOSTNAME}) to;
            - Generate AuthKey(${CERT_SSH}_PORT)
            - Extract HostKey(${DIR_AUTH_TMP})
        parse.config ${CONF_SSHD}.
        sshd with PidFile_PORT."
    local item itemC host items itemsH port pass cert
    [[ ${1} == +(-)* ]] && { $(exe sshd) "${@}"; return; }
    [[ ${1} == +([[:alpha:]]) ]] && { host="${1}"; shift; }
    [[ ${host} ]] || host="${HOSTNAME}"
    local items=( "${host}_ssh" ) itemsH=( "${host}_ssh" )

    [[ ${items} ]] && { identity.define "${items[@]}" || return; }
    [[ ${itemsH} ]] && { identity.define_host "${itemsH[@]}" || return; }

    pass="${host^^}_SSH_PASS" port="${host^^}_SSH_PORT"
    port="${!port}" pass="${!pass}"
    [[ ${port} && ${pass} ]] || return
    [[ ${1} == +([[:digit:]]) ]] && { port="${1}"; shift; }
    cert="${CERT_SSH}_${port}"
    [[ ${port} -lt 1025 ]] && { print_false 'Invald! Port\n'; return 1; }

    items=( ${DIR_AUTH}/${pass}.* )
    crypt.decrypt "${items[@]}" || return

    [[ ${pass} == *+(.prv.)* ]] && itemC="${pass%%.*}"
    item=( ${DIR_AUTH_TMP}/${itemC:-${host}}.prv.rsa )
    items=( ${DIR_AUTH}/${itemC:-${host}}.pub.*.ssh )
    [[ ${items} ]] &&  # ?? OPENSSH_PRV -> OPENSSH_PUB
        $(exe ssh-keygen) -y -f "${DIR_AUTH_TMP}/${pass}" >>"${cert}"

    parse.conf "${CONF_SSHD}"

    $(exe pgrep) -lf "loop.sh .* sshd ${host} ${port}" >/dev/null ||
        nohup loop.sh -5 "sshd ${host} ${port}" &>/dev/null & disown ${!}
    exist_proc ${PID_SSHD}_${port} && { kill --hup ${PID_SSHD}_${port}; return; }

    # -dd
    $(exe sshd) \
        -f "${CONF_SSHD}" -h "${item}" \
        -o ListenAddress="0.0.0.0:${port}" \
        -o PidFile="${PID_SSHD}_${port}" \
        -o AuthorizedKeysFile="${cert}" \
        "${@}" || return

    set +C
    if selects "${items[@]}"; then
        print_warn 'Import Public Certificate\n'
        for item in "${SELECT[@]}"; do
            $(exe grep) . "${item}" >>"${cert}" &&
                print_true '\t%s\n' "${item}" || print_false '\t%s\n' "${item}"
        done
    fi && chmod 600 "${cert}"
    set -C
}
scp() { ssh "${@}"; }
ssh() {
    local _ARGS='<[user@]identity[[:]port <host>]]> (srcPath) (:destPath)'
    local _DESC="
        Port, alternative port or forwarding port.
        User, alternative login.
        Host, forwarding host.
        Identity, use IdDb information for connection.
        LocalPath, file or directories to rsync on host at REMOTEPATH.
        
        Forwarding, <port>:<host>."
    [[ ${1} == +(-)* ]] && { $(exe ssh) "${@}"; return; }
    [[ ${1} == +(\?) ]] && { print_help; return 1; }
    local user host port pass cert rsync
    local args item items itemP

    if [[ ${1} == +([[:digit:]]?(:)*) ]]; then
        itemP="${1%%:*}"
        item="${1#*:}" item="${item//.${DOMAIN}/}"

    elif [[ ${1} == +(*[@:]*) ]]; then
        host="${1#*@}" host="${host%:*}" item="${host}"
        [[ ${1} == +(*:*) ]] && port="${1##*:}"
        [[ ${1} == +(*@*) ]] && user="${1%%@*}"

    elif [[ ${1} ]]; then
        item="${1}"
    fi
    shift

    if ! [[ ${user} ]]; then
        identity.define "${item}_ssh"  || return
        user="${item^^}_SSH_USER" user="${!user}"
        pass="${item^^}_SSH_PASS" pass="${!pass}"
    fi
    if ! [[ ${port} ]]; then
        identity.define_host "${item}_ssh" || return
        host="${item^^}_SSH_HOST" host="${!host}"
        port="${item^^}_SSH_PORT" port="${!port}"
    fi
    #[[ ${host} && ${user} && ${port} ]] ||
     #   { print_false 'Invalid! '; printf '%s\n' "${item}"; return 1; }

    if [[ ${pass} == +(*.*) ]]; then
        # Find cert from identity, decrypt and cache.
        item=( ${DIR_AUTH_TMP}/${pass} )
        [[ -e ${item} ]] ||
            { crypt.decrypt ${DIR_AUTH}/${pass}.*
            item=( ${DIR_AUTH_TMP}/${pass} ); }
        [[ -e ${item} ]] && cert="${item}"
    fi

    parse.conf "${CONF_SSH}"

    if [[ ${1} && ${1} != +(-*) ]]; then # Rsync
        unset items
        for item in "${@}"; do
            (( ${#items[@]} < ${#}-1 )) || break  # Ignore last item.
            [[ ${item} == +(:*) ]] || item=( "${host}:\"${item}\"" )
            items+=( "${item}" )
        done
        item="${@:${#}}"
        # [[ ${item} == +(*:*) ]] || item="${item##*/}"

        rsync \
            -e "$(exe ssh) -F ${CONF_SSH} -l ${user} -p ${port} ${cert:+-i "${cert}"}" \
            "${items[@]}" "${item}" #>/dev/null

    elif [[ ${itemP} ]]; then  # Forward
        local _ proto addrL addrL prog

        while read proto _ _ addrL addrR _ prog; do
            [[ ${proto} == tcp ]] || continue
            [[ ${prog} == +(*/ssh) ]] || continue
            [[ ${addrL} == +(*:${itemP}) ]] && return
        done <<<"$($(exe netstat) -ltunp 2>/dev/null)"

        $(exe ssh) -f -N \
            -F "${CONF_SSH}" \
            -l "${user}" -p "${port}" ${cert:+-i "${cert}"} \
            -L "${itemP}:${host}:${itemP}" "${host}"

    else  # SSH
        $(exe ssh) \
            -F "${CONF_SSH}" \
            -l "${user}" -p "${port}" ${cert:+-i "${cert}"} \
            "${host}" "${@}"
    fi
    # ssh localhost 'Xnest -query localhost'
}

telnet() {
    local _ARGS='<host|identity> (port)'
    local _DESC="
        Connect to telnet using HOST|IDENTITY on PORT."
    local item host port user

    (( ${#} )) || { print_help; return; }
    [[ ${1} == +(-*) ]] && { $(exe telnet) "${@}"; return; }

    item="${1}"

    if identity.define_host "${item}_telnet"; then
        identity.define "${item}_telnet"
        host="${item^^}_TELNET_HOST" host="${!host}"
        port="${item^^}_TELNET_PORT" port="${!port}"
        user="${item^^}_TELNET_USER" user="${!user}"
    fi
    [[ ${host} && ${port} || ${2} ]] || { print_false 'Invalid!\n' "${item}"; return; }

    if [[ ${user} ]]; then
        printf '%s@%s:%s\n' "${user}" "${host}" "${port}"
    else
        printf '%s:%s\n' "${host}" "${port}"
    fi

    $(exe telnet) ${user:+-l "${user}"} "${2:-${host}}" "${2:-${port}}"
}

dropbear() {
    local _ARGS='port'
    local _DESC="
        PORT from argument or identity.sh(HOSTNAME).
        HUP daemon if running.
        Use decrypt.sh(${HOSTNAME}) to;
            - Generate AuthKey(${CERT_SSH}_PORT)
            - Extract HostKey(${DIR_AUTH_TMP})
        parse.config ${CONF_SSHD}.
        Dropbear with PidFile_PORT."
    local item itemC host items itemsH port pass cert
    [[ ${1} == +(-)* ]] && { $(exe dropbear) "${@}"; return; }
    [[ ${1} == +([[:alpha:]]) ]] && { host="${1}"; shift; }
    [[ ${host} ]] || host="${HOSTNAME}"
    local items=( "${host}_dropbear" ) itemsH=( "${host}_dropbear" )

    [[ ${items} ]] && { identity.define "${items[@]}" || return; }
    [[ ${itemsH} ]] && { identity.define_host "${itemsH[@]}" || return; }

    pass="${host^^}_DROPBEAR_PASS" port="${host^^}_DROPBEAR_PORT"
    port="${!port}" pass="${!pass}"
    [[ ${port} && ${pass} ]] || return
    [[ ${1} == +([[:digit:]]) ]] && { port="${1}"; shift; }
    cert="${CERT_SSH}_${port}"
    [[ ${port} -lt 1025 ]] && { print_false 'Invald! Port\n'; return 1; }

    items=( ${DIR_AUTH}/${pass}.* )
    crypt.decrypt "${items[@]}" || return

    [[ ${pass} == *+(.prv.)* ]] && itemC="${pass%%.*}"
    item=( ${DIR_AUTH_TMP}/${itemC:-${host}}.prv.rsa )
    items=( ${DIR_AUTH}/${itemC:-${host}}.pub.*.ssh )
#    [[ ${items} ]] &&  # ?? OPENSSH_PRV -> OPENSSH_PUB
#        $(exe ssh-keygen) -y -f "${DIR_AUTH_TMP}/${pass}" >>"${cert}"

    #$(exe pgrep) -lf "loop.sh .* dropbear ${host} ${port}" >/dev/null ||
    #    nohup loop.sh -5 "dropbear ${host} ${port}" &>/dev/null & disown ${!}
    exist_proc ${PID_DROPBEAR}_${port} && { kill --hup ${PID_DROPBEAR}_${port}; return; }

    #-F
    $(exe dropbear) -m -w -s -g \
        -P "${PID_DROPBEAR}_${port}" \
        -p 0.0.0.0:${port} \
        -b "${FILE_MOTD}" \
        -r "${items}" || return

    set +C
    if selects "${items[@]}"; then
        print_warn 'Import Public Certificate\n'
        for item in "${SELECT[@]}"; do
            $(exe grep) . "${item}" >>"${cert}" &&
                print_true '\t%s\n' "${item}" || print_false '\t%s\n' "${item}"
        done
    fi && chmod 600 "${cert}"
    $(exe ln) -s "${cert}" "${HOME}/.ssh/authorized_keys" 2>/dev/null
    set -C
}
#####

    #--Get--#
dicod() {
    identity_dicod
    parse.conf "${CONF_DICOD}" || return
    $(exe dicod) --config="${CONF_DICOD}" "${@}"
}
dico() {
    identity_dicod
    $(exe dico) --host='localhost' -p "${LOCALHOST_DICOD_PORT}" "${@}" 
    }

shellfm() {
    exist "${CONF_SHELLFM}" || return
    identity.define 'last.fm' || return
    parse.conf "${CONF_SHELLFM}" "${CONF_SHELLFM_BOOKMARK}"
    alsa.sh --write
    clear; $(exe shell-fm) "${@}"
    #-i "${HOSTNAME}" -p "${PORT_SHELLFM}"
}

youtube() {
    local item items
    unset HTTP_PROXY

    identity.define 'youtube.com' || return
    #identity.define_host "http_proxy" &&
    #    { host="${HTTP_PROXY_HOST}"
    #    port="${HTTP_PROXY_PORT}"
    #    export http_proxy "${host}:${port}"; }

    # --max-quality {22,45,35,44,34,18,43,5} \
    #[[ ${1} == *+(/user/|/p/)* ]] ||
    #   $(exe youtube-dl) --ignore-errors --get-filename --get-description "${@}" 
    $(exe youtube-dl) --ignore-errors --retries 4 --continue --no-overwrites \
        --max-quality 44 --prefer-free-formats --write-srt --srt-lang en \
        --cookies "${DIR_TMP_CACHE}/youtube.cookie" \
        --output "${FORMAT_YOUTUBE}" \
        --username "${YOUTUBE_COM_USER}" \
        --password "${YOUTUBE_COM_PASS}" \
        "${@}"
}
youtube_audio() {
    # -1 45kbit
    # 0 64
    # 1 80
    # 2 96
    # 3 112
    # 4 128
    # 5 160
    # 6 192
    # 7 224
    # 8 256
    # 9 320
    # 10 500
    
    youtube --extract-audio --audio-quality 6 "${@}"
}
youtube_writeout() {
    local item itemS args items=( "${@}" )

    for item in "${items[@]}"; do
        [[ ${item} ]] || continue
        [[ -d ${item} ]] && continue
        [[ ${item} == *+(.part) ]] && continue
        item="${item%.srt}" itemS=( ${item}.srt )
        [[ -e ${item} ]] || continue
        (( $($(exe stat) -c '%s' "${item}") > 1 )) || continue

        [[ -e ${itemS} ]] && args=( "-srt ${itemS}" )

        read_clear
        print_warn 'Play: %s\n' "${item}"
        $(exe mplayer) \
            -osdlevel 3 \
            ${args[@]} "${item}" &>/dev/null || continue
        { set +C
        read_true -3 "Writeout -- ${item}" &&
            { for item2 in flv webm mp4; do >"${item%.*}.${item2}"; done
            [[ -e ${itemsS} ]] && >"${itemS}"; }
        set -C; }
    done
}

transmission() {
    log.move "${LOG_TRANSMISSION}"
    parse.conf "${CONF_TRANSMISSION}"
    $(exe transmission-gtk) --display "${DISPLAY}" --config-dir "${CONF_TRANSMISSION%/*}" "${@}" &>"${LOG_TRANSMISSION}" & disown ${!}
    # --log-[error|info] --lpd --portmap
    ##$(exe transmission-daemon) --blocklist --encryption-required --log-debug --dht \
    #    --incomplete-dir "${DIR_TMP_CACHE}" --watch-dir "${DIR_TMP_DATA}" \
    #    --pid-file "${PID_TRANSMISSION}" --logfile "${LOG_TRANSMISSION}" \
    #    --download-dir "${DIR_TMP_DATA}" --config-dir "${CONF_TRANSMISSION%/*}" \
    #    --port "${PORT_TRANSMISSION}" --peerport "${PORT_TRANSMISSION_PEER}" "${@}"
}

mlnet() {
    local netUp netDn port user pass
    local items=( "${CONF_MLDONKEY_DC}" "${CONF_MLDONKEY_MLNET}"
        "${CONF_MLDONKEY_USERS}" "${CONF_MLDONKEY_DONKEY}" "${CONF_MLDONKEY_TORRENT}" )

    identity.define "${HOSTNAME}_mlnet" "${HOSTNAME}_net_dn" "${HOSTNAME}_net_up"
    identity.define_host "${HOSTNAME}_mlnet" "${HOSTNAME}_mlnet_dc" "${HOSTNAME}_mlnet_gui" \
        "${HOSTNAME}_mlnet_http" "${HOSTNAME}_mlnet_telnet" \
        "${HOSTNAME}_mlnet_torrent" "${HOSTNAME}_mlnet_torrent_tracker" \
        "${HOSTNAME}_mlnet_torrent_dht" "http_proxy"

    port="${HOSTNAME^^}_MLNET_TELNET_PORT" port="${!port}"
    user="${HOSTNAME^^}_MLNET_USER" user="${!user}"
    pass="${HOSTNAME^^}_MLNET_PASS" pass="${!pass}"
    netUp="${HOSTNAME^^}_NET_DN_PASS" netDn="${HOSTNAME^^}_NET_UP_PASS"
    export NETWORK_RATE_UPLOAD_MAX "${!netUp}"
    export NETWORK_RATE_DOWNLOAD_MAX "${!netDn}"
    export HOST_ADDRESS $(request.address)

    parse.conf "${items[@]}"

    exist_proc "${PID_MLNET}" && { kill -hup "${PID_MLNET}"; return; }
    $(exe mlnet) -pid "${DIR_TMP_PID}" "${@}" & disown "${!}"

    sleep 4
    $(exe ln) -s "${DIR_MLDONKEY}/mlnet.log" "${LOG_MLNET}" 2>/dev/null
    printf 'useradd %s %s\nq\n' "${user}" "${pass}" |
        $(exe nc) 'localhost' "${port}" |
        $(exe grep) 'changed\|authorized'
}
mlnet_web() {
    local host port user pass item="${1}"
    [[ ${1} ]] || { print_false 'Invalid! '; printf 'Host\n'; return 1; }

    identity.define_host "${item}_mlnet_http"
    host="${item^^}_MLNET_HTTP_HOST" host="${!host}"
    port="${item^^}_MLNET_HTTP_PORT" port="${!port}"
    #user="${item^^}_MLNET_HTTP_USER" user="${!user}"
    #pass="${item^^}_MLNET_HTTP_PASS" pass="${!pass}"
    [[ ${host} && ${port} ]] || return

    ssh "${port}:${host}" || return
    identity.define "${item}_mlnet"
    printf 'http://%s:%s\n' 'localhost' "${port}"
    conkeror "localhost:${port}"
}
mlnet_telnet() {
    local _ARGS='<command>'
    local _DESC="
        Send valid mlnet commands to running instance."
    local host port user pass item="${1}" items=( "${@:2}" )
    [[ ${1} ]] || { print_false 'Invalid! '; printf 'Host\n'; return 1; }

    identity.define_host "${item}_mlnet_telnet"
    identity.define "${item}_mlnet"
    host="${item^^}_MLNET_TELNET_HOST" host="${!host}"
    port="${item^^}_MLNET_TELNET_PORT" port="${!port}"
    user="${item^^}_MLNET_USER" user="${!user}"
    pass="${item^^}_MLNET_PASS" pass="${!pass}"
    [[ ${host} && ${port} ]] || return

    ssh "${port}:${host}" || return
    #for item in "${items[@]}"; do
    printf 'auth %s %s\n%s\nq\n' "${user}" "${pass}" "${items[*]}" |
        $(exe nc) 'localhost' "${port}" |
        $(exe grep) -v '>\|MLdonkey\|?\|Welcome\|^$'
    #done
}
mlnet_magnet() {
    local _ARGS='<uri>'
    local _DESC="
        Add parsed URI to running mlnet."
    local host port user pass item items

    (( ${#} > 1 )) || { print_false 'Missing! '; printf 'Argument\n'; return 1; }

    for item in "${@:2}"; do
        case "${item}" in
            http:*) item="$(parse.http "${item}")" ;;
        esac
        case "${item}" in
            magnet:*) mlnet_telnet "${1}" 'dllink' "${item}" ;;
        esac
    done
}

rtorrent() {
    local netUp="${HOSTNAME^^}_NET_DN_USER" netDn="${HOSTNAME^^}_NET_UP_USER"

    # Load magnets as files;
        #[[ ${1} =~ xt=urn:btih:([^&/]+) ]]
        #printf 'd10:magnet-uri${#1}:${1}e' >meta-${BASH_REMATCH[1]}.torrent

    identity.define "${HOSTNAME}_rtorrent" "${HOSTNAME}_net_dn" "${HOSTNAME}_net_up"
    identity.define_host "${HOSTNAME}_rtorrent" "${HOSTNAME}_rtorrent_dht" \
        "${HOSTNAME}_rtorrent_rpc"

    export HOST_ADDRESS $(request.address)
    export NETWORK_RATE_UPLOAD_MAX "${!netUp}"
    export NETWORK_RATE_DOWNLOAD_MAX "${!netDn}"

    mkdir "${LOG_RTORRENT_TRACKER%/*}" "${DIR_CACHE_RTORRENT}"

    parse.conf "${CONF_RTORRENT}"
    $(exe rtorrent) \
        -o schedule=import,0,120,try_import="${CONF_RTORRENT}"
}
rutorrent() {
    identity.define "${HOSTNAME}_rtorrent_rpc" || return
    identity.define_host "${HOSTNAME}_ssl" || return

    nginx
}

vnstatd() { #:daemon:
    local item
    (( ${#DEVICE_NETWORK[@]} )) || return

    parse.conf "${CONF_VNSTAT}"
    mkdir "${DIR_DATA_VNSTAT}"

    for item in "${DEVICE_NETWORK[@]}"; do
        [[ -e ${DIR_DATA_VNSTAT}/${item} ]] && continue

        $(exe vnstat) --update --enable \
            --config "${CONF_VNSTAT}" \
            --iface "${item}"
    done

    exist_proc "${PID_VNSTAT}" && return
    $(exe vnstatd) --daemon \
        --config "${CONF_VNSTAT}" \
        --pidfile "${PID_VNSTAT}" \
        "${@}"
}
vnstat() {
    local _ARGS='[<time> <interface>]'
    local item itemI itemC digit action items IFS
    local itemO="${DIR_TMP_DATA}/vnstat_$($(exe date) '+%s').png"
    IFS='|'; local re2="${DEVICE_NETWORK[*]}"; unset IFS

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            +([[:digit:]])) digit="${item}" ;;
            image) action='image' ;;
            *) break ;;
        esac
        shift
    done

    if [[ ${action} == image ]]; then
        items=( 'days' 'hours' 'hsummary' 'months' 'top10' 'vsummary' )
    else
        items=( 'live' 'days' 'hours' 'weeks' 'months' 'dumpdb' 'top10' )
    fi
    IFS='|'; local re="${items[*]}"; unset IFS


    if [[ ${1} && ${1} == +(${re}) ]]; then
        itemC="--${1}"
    elif selects -1 "${items[@]}"; then
        for item in "${SELECT[@]}"; do itemC="--${item}"; done
    fi
    if [[ ${2} && ${2} == +(${re2}) ]]; then
        itemI="${2}"
    else
        selects -1 "${DEVICE_NETWORK[@]}" && itemI="${SELECT}"
    fi

    if [[ ${action} == image ]]; then
        $(exe vnstati) --style 3 \
            --output "${itemO}" \
            --config "${CONF_VNSTAT}" \
            --iface "${itemI}" \
            ${itemC} "${@}" && print_true 'Output: %s\n' "${itemO}"
    else
        $(exe vnstat) \
            --config "${CONF_VNSTAT}" \
            ${digit:+--tr "${digit}"} \
            --iface "${itemI}" \
            ${itemC} "${@}"
    fi
}
vnstati() { vnstat -image; }

tinyproxy() {
    local host="${HOSTNAME^^}_TINYPROXY_HOST"

    exist_proc "${PID_TINYPROXY}" && { kill -hup "${PID_TINYPROXY}"; return; }

    identity.define_host "${HOSTNAME}_tinyproxy" || return
    [[ ${!host} ]] || return

    parse.conf "${CONF_TINYPROXY}"
    $(exe tinyproxy) -c "${CONF_TINYPROXY}"
}
#####

    #--Put--#

py_httpd() {
    local ARGS='dir port'
    local DESC="
        Start python HTTP daemon using PORT in DIR.

        Order is insignificant; last DIR|PORT is used, default './' and '8000'."
    local item port='8000' dir='./'

    (( ${#} )) &&
        for item in "${@}"; do
            if [[ -d ${item} ]]; then
                dir="${item}"
            else
                port="${item}"
            fi
        done

    builtin cd "${dir}"
    if $(exe python3) -m http.server "${port}"; then
        return
    else
        $(exe python2.7) -m SimpleHTTPServer "${port}"
    fi
}

httpd() {
    local item host port path="${DIR_TMP_DATA}/www"

    for item in "${@}"; do
        [[ -d ${item} ]] && path="${item}"
    done

    identity.define_host "${HOSTNAME}_http" || return
    #host="${HOSTNAME^^}_HTTP_HOST" host="${!host}"
    port="${HOSTNAME^^}_HTTP_PORT" port="${!port}"
    [[ ${port} ]] || return
    host=$(request.address "${HOSTNAME}")

    mkdir "${path}"

    sudo $(exe httpd) \
        -h "${path}" \
        -c "${CONF_HTTPD}" \
        -u "${USER_ID}:${GROUP_ID}" \
        -p "${host}:${port}" "${@}"
}

nginx() {
    local item user pass

    identity.define "${HOSTNAME}_http" || return
    identity.define_host "${HOSTNAME}_php" || return
    user="${HOSTNAME^^}_HTTP_USER" user="${!user}"
    pass="${HOSTNAME^^}_HTTP_PASS" pass="${!pass}"
    [[ ${user} && ${pass} ]] || return

    item="${DIR_DATA_HTTP}/.htpasswd"
    mkdir "${DIR_DATA_HTTP}"
    [[ -e ${item} ]] ||
        { printf '%s:%s\n' "${user}" "$(openssl passwd -crypt "${pass}")" >"${item}"
        $(exe chown) root:root "${item}"
        $(exe chmod) 640 "${item}"; }

    parse.conf "${CONF_NGINX}"
    $(exe nginx) -c "${CONF_NGINX}"
}

put() {
    local _ARGS='<file>'
    local _DESC="
        Format, file dot suffix.
        Title, file excluding dot suffix."
    local nick user pass out
    local item itemE itemT itemF #items=( 'pastebin' )
    # cxg.de
    # fpaste.org
    # p.defau.lt
    # paste.debian.net
    # paste.drizzle.org
    # paste.kde.org
    # paste.openstack.org
    # paste.pocoo.org[Offline]
    # paste.pound-python.org
    # paste.ubuntu.com
    # paste.ubuntu.org.cn
    # paste2.org[Javascript]
    # pastebin.com[Broken]
    # pastie.org [Broken]
    # pb.daviey.com [Minimal]
    # slexy.org [User+Time]
    # sprunge.us [Minimal]
    # yourpaste.net[Offline]

    pip install 'configobj'
    unset HTTP_PROXY

    for item in 'nick' "${items[@]}"; do
        [[ ${item} ]] || continue
        identity.define "${item}"
    done
    nick="${NICK_USER}"
    #user="${PASTEBIN_COM_USER}" pass="${PASTEBIN_COM_PASS}"

    for item in "${@}"; do
        itemF="${item##*/}"
        itemE="${itemF##*.}" itemT="${itemF%%.*}"
        #-u "${user}" -p "${pass}" \
        out=$($(exe pastebinit) -b http://slexy.org \
            -f "${itemE}" -t "${itemT}" \
            -a "${nick}" "${@}")
        [[ ${out} == +(*://*) ]] || continue
        printf '%s' "${out}" |xclip -i
        printf '%s:%s\n' "${itemT}" "${out}"
    done
}

nfsd() {  #:daemon:
    local _ARGS=''
    local _DESC=" :!:sudo
        "
    local item port items=( ${HOSTNAME}_{nfs,mount,stat} )

    [[ ${items} ]] && { identity.define_host "${items[@]}" || return; }

    set +C; for item in ${DIR_TMP_NFS}/{etab,rmtab,xtab}; do
        [[ -e ${item} ]] || >"${item}"
    done; set -C
    [[ -d /var/lib/nfs ]] || sudo $(exe mkdir) -p '/var/lib/nfs/v4recovery'
    for item in /var/lib/nfs/{etab,rmtab,xtab}; do
        [[ -e ${item} ]] || sudo touch "${item}"
    done

    # rpcbind|portmap rpc.{mountd,nfsd,statd,lockd,rqoutad}
    if exist_exe 'rpcbind'; then  # IPv6
        items=( 'rpcbind' )
    else
        items=( 'portmap' )
    fi; items+=( rpc.{mountd,nfsd} )
    # $(exe rpcinfo) -p 2>/dev/null |$(exe grep) -q "${item#*.}"
    for item in "${items[@]}"; do
        exist_proc "${item}" && continue
        case "${item}" in
            portmap) # -dfv
                sudo $(exe "${item}") \
                    -u ${USER_ID} -g ${GROUP_ID}
            ;;
            rpcbind) # -dfv
                sudo $(exe "${item}") -s
                    #-h "${HOSTNAME}"
            ;;
            rpc.mountd) # -Fd all
                port="${HOSTNAME^^}_MOUNT_PORT"

                sudo $(exe "${item}") --nfs-version 4 \
                    --state-directory-path "${DIR_TMP_NFS}" \
                    --exports-file="${CONF_EXPORT}" \
                    --port "${!port}"
            ;;
            rpc.statd) # --foreground
                port="${HOSTNAME^^}_STAT_PORT"

                sudo $(exe "${item}") \
                    --state-directory-path "${DIR_TMP_NFS}" \
                    --name "${HOSTNAME}" --port "${!port}"
            ;;
            rpc.nfsd) # --debug 
                port="${HOSTNAME^^}_NFS_PORT"

                modprobe 'nfsd'
                exist_mnt '/proc/fs/nfsd' ||
                    sudo $(exe mount) -t 'nfsd' 'nfsd' '/proc/fs/nfsd'

                sudo $(exe "${item}") --no-nfs-version 2 \
                    --port "${!port}" 4
                    # -H "${HOSTNAME}"
            ;;
        esac || return
    done
    nfs_exports
}
nfsd_clean() {
    local _ item itemv

    while read _ itemV _ _ item; do
        [[ ${itemV} == +([[:digit:]]) ]] || continue

        sudo $(exe rpcinfo) -d "${item}" "${itemV}" 2>/dev/null
    done <<<"$($(exe rpcinfo) -p 2>/dev/null)"

    kill 'portmap' 'rpcbind' 'rpc.statd' \
        'rpc.idmap' 'mountd' 'nfsd4' 'nfsd'
    sudo $(exe umount) -a -t nfs,nfsd
    sudo $(exe exportfs) -au
    #sudo $(exe modprobe) -r 'nfs'
    #sudo $(exe modprobe) -r 'nfsd'
}
nfs() {
    local item items

    if exist_exe 'rpcbind'; then  # IPv6
        items=( 'rpcbind' )
    else
        items=( 'portmap' )
    fi
    #items+=( rpc.{mountd,nfsd} )
    for item in "${items[@]}"; do
        exist_proc "${item}" && continue
        case "${item}" in
            portmap) # -dfv
                sudo $(exe "${item}") \
                    -u ${USER_ID} -g ${GROUP_ID}
            ;;
            rpcbind) # -dfv
                sudo $(exe "${item}") -s
                    #-h "${HOSTNAME}"
            ;;
            rpc.statd) # --foreground
                port="${HOSTNAME^^}_STAT_PORT"

                sudo $(exe "${item}") \
                    --state-directory-path "${DIR_TMP_NFS}" \
                    --name "${HOSTNAME}"
            ;;
        esac
    done
}
nfs_mount() {
    local _DESC="
        :!:sudo :!:ssh_forward."

    [[ ${1} ]] || return
    local host hostN hostM hostT port portN portM portT
    local _ pid int item items
    items=( ${1}_{nfs,mount} )
    local reC='nul*|0'

    [[ ${items} ]] && { identity.define_host "${items[@]}" || return; }
    hostN=( "${1^^}_NFS_HOST[@]" ) portN=( "${1^^}_NFS_PORT[@]" )
    hostM=( "${1^^}_MOUNT_HOST[@]" ) portM=( "${1^^}_MOUNT_PORT[@]" )

    for item in host{N,M}; do
        host=( "${!item}" ); item="${item//host/port}"; port=("${!item}" )
        host=( "${!host}" ) port=("${!port}" )

        for (( int=0;int<${#host[@]};int++ )); do
            hostT="${host[${int}]}" portT="${port[${int}]}"
            [[ ${hostT} && ${portT} ]] || continue

            $(exe rpcinfo) -p "${hostT}" |$(exe grep) -q 'nfs\b' || return

            if [[ ${2} == +(${reC}) ]]; then
                read _ pid _ <<<$($(exe lsof) -i "TCP:${portT}" |
                    $(exe grep) -i 'listen')
                [[ ${pid} ]] && kill "${pid}"
            else
                ssh "${1}" -f -N -L "${portT}:${hostT}:${portT}" || return
            fi
        done
    done

    local path="${DIR_MNT_NFS}/${1}${2#*${DIR_TMP_EXPORT}}"
    if [[ ${2} == +(${reC}) ]]; then
        host="${1^^}_NFS_HOST"
        umount "$(grep "${!host}" "${PROC_MOUNTS}" |
            sed -e 's/[^ ]* \([^ ]*\).*/\1/')"

    else
        [[ -d ${path} ]] || sudo $(exe mkdir) -p "${path}"
        host="${1^^}_NFS_HOST"
        exist_mnt "${path}" || sudo $(exe mount) "${!host}:${2}" "${path}"
    fi
}
nfs_exports() {
    local item

    #parse.conf "${CONF_EXPORT}" "${CONF_EXPORT}_${HOSTNAME}"
    #cat "${CONF_EXPORT}_${HOSTNAME}" |write -a "${CONF_EXPORT}"
    sudo $(exe exportfs) -r
    for item in "${DIRS_EXPORT}"; do
        sudo $(exe exportfs) -i \
            -o rw,fsid=0,crossmnt,nohide,no_subtree_check,insecure \
            "*:${item}"
    done
}

btctrader() { ${DIR_TC_BUILD}/btc-trader/bin/btctrader; }

mtgox() {
    identity.define 'mtgox.com' 'goxsh' || return

    pip install readline

    parse.conf "${CONF_GOXSH}"
    $(exe goxsh) "${@}"
}

bitcoin() {
    local item="${1:-${HOSTNAME}}" host port
    shift

    identity.contains_host "${item}_btc" || return
    #identity.define "${item}_btc"
    identity.define_host "${item}_btc"
    #user="${item^^}_BTC_USER" user="${!user}"
    #pass="${item^^}_BTC_PASS" pass="${!pass}"
    host="${item^^}_BTC_HOST" host="${!host}"
    port="${item^^}_BTC_PORT" port="${!port}"
    [[ ${host} && ${port} ]] || return

    $(exe bitcoind) -listen=0 \
        -conf="${CONF_BITCOIN}" \
        -connect="${host}" -port="${port}" \
        "${@}"
}

bitcoind() {
    local user pass host port

    identity.define "${HOSTNAME}_btc"
    identity.define_host "${HOSTNAME}_btc"
    user="${HOSTNAME^^}_BTC_USER" user="${!user}"
    pass="${HOSTNAME^^}_BTC_PASS" pass="${!pass}"
    host="${HOSTNAME^^}_BTC_HOST" host="${!host}"
    port="${HOSTNAME^^}_BTC_PORT" port="${!port}"
    [[ ${host} && ${port} ]] || return

    crypt.pem "${HOSTNAME}" || return
    mkdir "${DIR_VAR_DATA}/bitcoind"

    $(exe bitcoind) -daemon -gen \
        -logtimestamps -listen -irc -dns \
        -datadir="${DIR_VAR_DATA}/bitcoind" \
        -conf="${CONF_BITCOIN}" \
        -pid="${PID_BITCOIN}" \
        -rpcssl -rpcallowip="*" \
        -rpcsslcertificatechainfile="${CERT_HOSTNAME}" \
        -rpcprivatekeyfile="${CERT_HOST_PRV}" \
        -rpcpassword="${pass}" \
        -rpcuser="${user}" \
        -rpcport="${port}" \
        "${@}"
}
#####

#--Editor--#

vim() { $(exe vim) -u "${HOME}/.vim/vimrc" "${@}"; }
emacs() {
    identity.define 'nick'
    identity_twitter
    identity_mail
    identity_mpd 'momento'
    identity_radio
    identity_dicod
    identity_ssh 'momento' && parse.conf "${CONF_SSH}"

    [[ -d ${SOCK_EMACS%/*} ]] || $(exe mkdir) -m 700 "${SOCK_EMACS%/*}"
    terminal_title 'emacs'
    if ! [[ -S ${SOCK_EMACS} ]]; then
	$(exe emacs) \
            --no-window-system \
            --display "${DISPLAY}" \
            --directory "${DIR_EMACS}" \
            --load "${CONF_EMACS##*/}" # & #"${@}"
    else
	emacsclient "${@}"
    fi
}
emacsclient() {
    $(exe emacsclient) --tty \
        --socket-name="${SOCK_EMACS}" \
        "${@}"
}
identity_mail() {
    local items
    items=( gmail_{imap,smtp} )
    
    identity.define 'gmail.com' 'gmail.com_list' 'gmail.com_feed' || return
    identity.define_host "${items[@]}" || return
}
identity_twitter() {
    identity.define 'twitter.com' || return
}
identity_mpd() {
    local item="${1:-${HOSTNAME}}"

    identity.define "${item}_mpd" || { print_false 'Identity: %s' "${item}"; return 1; }
    identity.define_host "${item}_mpd" || { print_false 'Identity: %s' "${item}"; return 1; }
    host="${item^^}_MPD_HOST" port="${item^^}_MPD_PORT"
    user="${item^^}_MPD_USER" pass="${item^^}_MPD_PASS"
    export MPD_HOST "${!host}"
    export MPD_PORT "${!port}"
    export MPD_PASS "${!pass}"
    export MPD_USER "${!user}"
    # ssh "${port}:${host}" || return
}
identity_ssh() {
    local item="${1:-${HOSTNAME}}"

    identity.define "${item}_ssh" || { print_false 'Identity: %s' "${item}"; return 1; }
    identity.define_host "${item}_ssh" || { print_false 'Identity: %s' "${item}"; return 1; }
    host="${item^^}_SSH_HOST" host="${!host}"
    port="${item^^}_SSH_PORT" port="${!port}"
    user="${item^^}_SSH_USER" user="${!user}"
    pass="${item^^}_SSH_PASS" pass="${!pass}"
}
identity_radio() {
    local items=( 'last.fm' 'libre.fm' )

    identity.define "${items[@]}" || { print_false 'Identity: %s' "${items[@]}"; return 1; }
    }
identity_dicod() {
    identity.define_host "localhost_dicod" || { print_false 'Identity: %s' "${items[@]}"; return 1; }
    }

feed_rss() {
    local _ARGS=''
    local _DESC="
        Modify feed file."
    #local item="${DIR_LIST}/feed.newsbeuter"
    local item="/tmp/data/foo"

    set -x
    $(exe sed) \
        -e "1{h;r ${@};D;};2{x;G;}" <"${item}"
    set +x
}

#####

#--Media--#

play() {
    alsa.sh --write
    [[ ${CMD_VIDEO} ]] && ${CMD_VIDEO} "${@}"
}

mplayer() {
    $(exe mplayer) -include "${CONF_MPLAYER}" "${@}"

    return
    
    local _ item items args digit='1500'
    unset items
    # Collect valid mplayer exec
    while read -r item _; do
        items+=( "${item}" )
    done <<<"$($(exe mplayer) -input cmdlist)"
    local IFS='|'; local re="${items[*]}"; unset IFS

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            srt) set 'shift' 'sub_load' "${2}" 'loadfile' "${@:3}" ;;
            +([[:digit:]])) digit="${item}" ;;
            *) break ;;
        esac
        shift
    done
    (( ${#} )) || return

    parse.pid "${PID_MPLAYER}"
    if [[ ${1} == +(${re}) ]]; then
        for item in "${@}"; do
            [[ ${item} ]] || continue

            case "${item}" in
                file) item='get_file_name' ;;
                load) item='loadfile' ;;
                title) item="get_meta_${item}" ;;

                *+(/)*) item='loadfile'; set "${item}" "${@}" ;;
            esac; shift
            [[ ${item} == +(${re}) ]] ||
                { print_false 'Invalid! '; print_warn '%s\n' "${item}"; return 1; }
            { printf '%s ' "${item}" "${@}"; printf '\n'; } >"${FIFO_MPLAYER}"
        done

    else
        [[ -e ${FIFO_MPLAYER} ]] || $(exe mkfifo) "${FIFO_MPLAYER}"
        alsa.sh --write
        log.move "${LOG_MPLAYER}"

        $(exe mplayer) -slave -quiet -noconsolecontrols \
            -cache "${digit}" \
            -include "${CONF_MPLAYER}" \
            -input file="${FIFO_MPLAYER}" \
            "${@}" &>"${LOG_MPLAYER}" & disown "${!}"
        printf '%s\n' "${!}" >>"${PID_MPLAYER}"
    fi
}

playlist() {
    [[ -e ${DIR_LIST} ]] || return
    local item=( 'last.fm' )
    items=( ${DIR_LIST}/* )

    identity.define "${item[@]}"
    parse.conf "${items[@]}"
}

mpd() {  #:daemon:
    [[ ${1} == kill ]] &&
        { $(exe mpd) --kill; kill mpdscribble; return; }
    [[ ${1} == +(-)* ]] && { $(exe mpd) "${@}"; return; }

    local item
    local items=( "${HOSTNAME}_mpd" 'last.fm' 'libre.fm' )
    local itemsH=( "${HOSTNAME}_mpd" "${HOSTNAME}_mpd_http" )

    [[ ${items} ]] && { identity.define "${items[@]}" || return; }
    [[ ${itemsH} ]] && { identity.define_host "${itemsH[@]}" || return; }

    #crypt.pem "${HOSTNAME}" || return

    parse.conf "${CONF_MPD}"
    exist_proc "${PID_MPD}" && { kill --hup "${PID_MPD}"; return; }
    $(exe mpd) --verbose "${@}" "${CONF_MPD}" && mpd_scrobble
}
mpd_client() {
    local item cache host port itemH itemsH digit=320

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            +([[:digit:]])) digit="${item}" ;;
        esac
        shift
    done
    [[ ${1} ]] && { itemH="${1}"; shift; }
    [[ ${itemH} ]] || itemH="${HOSTNAME}"

    itemsH=( "${itemH}_mpd_http" )
    [[ ${itemsH} ]] && { identity.define_host "${itemsH[@]}" || return; }
    host="${itemH^^}_MPD_HTTP_HOST" host="${!host}"
    port="${itemH^^}_MPD_HTTP_PORT" port="${!port}"
    [[ ${host} && ${port} ]] || return

    ssh "${port}:${host}" || return

    $(exe mplayer) -cache "${digit}" -loop 0 "http://localhost:${port}" #&&
        #{ print_true 'Play: '; printf '%s\n' "${host}:${port}"; }
}
mpd_playlist() {
    local itemsO=( 'add' 'addid' 'clear' 'clearerror' 'close'
        'commands' 'consume' 'count' 'crossfade' 'currentsong'
        'decoders' 'delete' 'deleteid' 'disableoutput' 'enableoutput'
        'find' 'findadd' 'idle' 'kill' 'list' 'listall' 'listallinfo'
        'listplaylist' 'listplaylistinfo' 'listplaylists' 'load' 'lsinfo'
        'mixrampdb' 'mixrampdelay' 'move' 'moveid' 'next' 'notcommands'
        'outputs' 'password' 'pause' 'ping' 'play' 'playid' 'playlist'
        'playlistadd' 'playlistclear' 'playlistdelete' 'playlistfind'
        'playlistid' 'playlistinfo' 'playlistmove' 'playlistsearch'
        'plchanges' 'plchangesposid' 'previous' 'random' 'rename'
        'repeat' 'replay_gain_mode' 'replay_gain_status' 'rescan'
        'rm' 'save' 'search' 'seek' 'seekid' 'setvol' 'shuffle'
        'single' 'stats' 'status' 'sticker' 'stop' 'swap' 'info'
        'swapid' 'tagtypes' 'update' 'urlhandlers' )
    local itemsOA=( 'add' 'crossfade' 'listall' 'listallinfo' 'lsinfo' 'move'
        'moveid' 'pause' 'play' 'playlistid' 'playlistinfo' 'random' 'repeat'
        'save' 'search' 'seek' 'update' )
    local itemsT=( 'any' 'filename' 'artist' 'album' 'title' 'track'
        'name' 'genre' 'date' 'composer' 'performer' 'comment' 'disc' )
    local itemsS=( 'lastfm' 'librefm' )
    local IFS='|'
    local reO="${itemsO[*]}" reA="${itemsOA[*]}" reT="${itemsT[*]}" reS="${itemsS[*]}"
    unset IFS
    local _ARGS='(digit) <playlistType|command>'
    local _DESC="
        Command, [${reO}]|[${reE}].
            Aliases are second [].
            Arguments, [${reOA}].
            Tag, [${reT}].
        PlaylistType, lastfm.
        Digit, repeate command.
        Control MPD."
    local intC int item action port host hostLength count
    unset URI
    # add <string>
    # crossfade <int>
    # listallinfo <path>
    # listall <path>
    # list <meta> [<meta> <query>]
    # lsinfo <dir>
    # moveid <intFrom> <intTo>
    # move <intFrom> <intTo>
    # pause <bool>
    # play <int>
    # playlistid <int>
    # playlistinfo <int>
    # random <bool>
    # repeat <bool>
    # save <string>
    # search <type> <what>
    # seek <int> <time>
    # update <path>

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            +([[:digit:]])) int="${item}" ;;
            *) host="${item}" ;;
        esac
        shift
    done
    int="${int:-1}"

    (( ${#} )) || selects -1 "${itemsS[@]}" "${itemsO[@]}"
    items=( "${@:-${SELECT[@]}}" )

    unset URI
    case "${items}" in
        +(${reO}))
            for item in "${items[@]}"; do
                #[[ ${items} == +(${reOA}) ]] && { (( ${#items[@]} > 1 )) || return; } 
                case "${item}" in
                    info) URI+=( 'currentsong' ) ;;
                    *) URI+=( "${item}" ) ;;
                esac
            done
        ;;
        +(${reS})) mpd_playlist.${items} "${items[@]:2}" "${@:2}" ;;
        +(*://*)) URI=( "${items[@]}" ) ;;
        *) print_false 'Invalid: '; print_warn '%s\n' "${items}"; return 1 ;;
    esac
    [[ ${URI} ]] || return

    item="${host:-${HOSTNAME}}"
    identity.define "${item}_mpd"
    identity.define_host "${item}_mpd" || { print_false 'Host: %s' "${item}"; return 1; }
    host="${item^^}_MPD_HOST" port="${item^^}_MPD_PORT" pass="${item^^}_MPD_PASS"
    host="${!host}" port="${!port}" pass="${!pass}"
    [[ ${host} && ${port} ]] || { print_false 'Host: %s' "${item}"; return 1; }

    ssh "${port}:${host}" || return

    unset count
    printf -v hostLength "%$((${#host}+7))s"
    [[ ${URI} == *+(://)* ]] && print_true 'MPD[%s]: ' "${host}"
    for item in "${URI[@]%/}"; do
        (( count++ ))
        if [[ ${item} == *+(://)* ]]; then
            if (( count > 1 )); then
                print_warn '%s\t%s\n' "${hostLength}" "${item}"
            else
                print_warn '\t%s\n' "${item}"
            fi
            item="load ${item}"
        fi

        request.ping -fail "${host}" || continue
        for (( intC=0;intC<int;intC++ )); do
            (( intC > 1 )) && sleep 2

            #:todo: password optional?
            printf 'password %s\n%s\nclose\nclose' "${pass}" "${item}" |
                $(exe nc) -w 10 'localhost' "${port}" |
                $(exe grep) -v '^OK'
        done
    done

    tmux_redraw
}
mpd_playlist.lastfm() {
    source 'request_audioscrobble' || return
    request.audioscrobble.play lastfm "${@}" || return
}
mpd_playlist.librefm() {
    source 'request_audioscrobble' || return
    request.audioscrobble.play librefm "${@}" || return
}
mpd_playlist.scrobble() {
    local _ARGS='<server> <command>'
    local _DESC="
        Server, host providing database [lastfm].
        Command, supported scrobble command.

        Send track information to scrobble."
    local item lineT lineV itemA itemT action

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            lastfm) action='lastfm' ;;
        esac
        shift
    done

    (( ${#} == 2 )) || return

    while read lineT lineV; do
        [[ ${lineT%:*} == Artist ]] && itemA="${lineV}"
        [[ ${lineT%:*} == Title ]] && itemT="${lineV}"
    done <<<"$(mpd_playlist currentsong)"

    [[ ${itemA} && ${itemT} ]] &&
        case "${action}" in
            lastfm) request_audioscrobble.sh --command "${2}" "${itemA}" "${itemT}" ;;
        esac
}
mpd_scrobble() {  #:daemon:
    local item host port pass
    local items=( "${HOSTNAME}_mpd" 'last.fm' 'libre.fm' )
    local itemsH=( "${HOSTNAME}_mpd" )

    [[ ${items} ]] && { identity.define "${items[@]}" || return; }
    [[ ${itemsH} ]] && { identity.define_host "${itemsH[@]}" || return; }

    host="${HOSTNAME^^}_MPD_HOST"
    port="${HOSTNAME^^}_MPD_PORT"
    pass="${HOSTNAME^^}_MPD_PASS"
    host="${!host}" port="${!port}" pass="${!pass}"
    [[ ${host} && ${port} ]] || return

    parse.conf "${CONF_MPDSCRIBBLE}"

    exist_proc "${PID_MPDSCRIBBLE}" && { kill --hup "${PID_MPDSCRIBBLE}"; return; }
    $(exe mpdscribble) --verbose 2 \
        --pidfile "${PID_MPDSCRIBBLE}" \
        --conf "${CONF_MPDSCRIBBLE}" \
        --log "${LOG_MPDSCRIBBLE}" \
        --host "${pass:+${pass}@}${host}" --port "${port}"
}

ncmpcpp() {
    local re='play|pause|toggle|stop|next|prev|volume'
    [[ ${1} == +(-)* ]] && { $(exe ncmpcpp) "${@}"; return; }
    [[ ${1} == +(${re}) ]] && { $(exe ncmpcpp) "${@}"; return; }
    local host port item="${1:-${HOSTNAME}}"

    identity.define "${item}_mpd"
    identity.define_host "${item}_mpd"
    host="${item^^}_MPD_HOST" host="${!host}"
    port="${item^^}_MPD_PORT" port="${!port}"
    pass="${item^^}_MPD_PASS" pass="${!pass}"
    [[ ${host} && ${port} ]] || return

    ssh "${port}:${host}" || return

    parse.conf "${CONF_NCMP}"
    alsa.sh --write
    TERM='rxvt-256color' $(exe ncmpcpp) \
        --host "${pass:+${pass}@}localhost" --port "${port}"
}

#####

freeciv() {
    local nick

    identity.define 'nick'

    profile.user.dot_relink "${CONF_FREECIV%/*}";
    parse.conf "${CONF_FREECIV}"
    ln -fs "${CONF_FREECIV}" "${HOME}/.freeciv-client-rc-2.3"

    $(exe freeciv-gtk2) \
        --log "${LOG_FREECIV}" &&
            parse.diff "${DIR_REPO_DOT}/${CONF_FREECIV#${HOME}/.}" "${CONF_FREECIV}"
}
freecivd() {
    $(exe freeciv-server) \
        --log "${LOG_FREECIV}" \
        --saves "${DIR_FREECIV_SAVE}" "${@}"
}

pip() {
    sudo $(exe pip) --exists-action=i \
        --log="${LOG_PIP}" "${@}"
}

#--Parse|Redirect--#
pokenum2() {
    local item err int itemT itemH items

    while :; do
        unset err
        read -e -i "${items[*]}" -p '> ' -a items

        unset itemH itemT
        for item in "${items[@]//10/t}"; do
            # Invalid grouping.
            (( ${#item}%2 )) &&
                { print_false 'Invalid! '; printf '%s\n' "${item}"
                (( err++ )); continue; }

            if (( ${#item} >= (3*2) )); then  #Table
                # Two cards per iteration.
                for (( int=0;int<(${#item}/2);int++ )); do
                    itemT+=( "${item:(int*2):2}" )
                done
            else #Hand
                itemH+=( "${item:0:2} ${item:2:2}" )
            fi
        done
        (( err )) && continue

        $(exe pokenum) ${itemH[@]/%/ -}${itemT:+-} ${itemT[@]}
    done
}
fish2() {
    local item err int items itemsH itemsT itemO itemV itemsV

    printf '<hand> <table>\n'
    while :; do
        unset err
        read -e -i "${items[*]}" -p '> ' -a items
        items=( "${items[@]//10/t}" )

        unset itemsH itemsT itemO
        # Invalid grouping.
        (( ${#items}%2 )) &&
            { print_false 'Invalid! '; printf '%s\n' "${items}"
            (( err++ )); continue; }

        # Opponents
        for item in "${items[@]}"; do
            (( ${#item} == 1 )) && itemO="${item}"
        done

        # Hand
        for (( int=0;int<(${#items}/2);int++ )); do
            itemsH+=( "${items:(int*2):2}" )
        done

        # Table
        for (( int=0;int<(${#items[1]}/2);int++ )); do
            itemsT+=( "${items[1]:(int*2):2}" )
        done

        (( err )) && continue

        $(exe fish) -n 7 ${itemsH[@]/#/-d } ${itemsT[@]}
    done
}

nmap_smb_discover() {
    $(exe nmap) -p 445 -Pn -script=smb-os-discovery "${@}"
    
}

#-----#
podbeuter() {
    export 'CONF_PODBEUTER' "${HOME}/.podbeuter/podbeuter.conf"
    $(exe podbeuter) \
        -q "${DIR_TMP_CACHE}/podbeuter.queue" \
        -C "${CONF_PODBEUTER}"
}

paste_lshw() {
    local server="http://pastehtml.com/upload"
    local serverOpt="/create?input_type=html&result=address"

    $(exe curl) -s -S --data-urlencode "txt=$(sudo $(exe lshw) -html)" "${server}${serverOpt}"
}

inotify() {
    if [[ -e ${PID_INOTIFY} ]]; then
        exist_proc $(<"${PID_INOTIFY}") || $(exe rm) -f "${PID_INOTIFY}"
    fi

    if [[ -e ${PID_INOTIFY} && ${@} == -* ]]; then
        $(exe inotify) "${@}"
        return
    fi

    print_warn 'INOTIFY'
    if [[ -e ${PID_INOTIFY} ]]; then
        print_true '[%s]\n' $(<"${PID_INOTIFY}")
    else
        log.move "${LOG_INOTIFY}"
        if $(exe inotify) --method libnotify --port "${PORT_INOTIFY}" "${@}" ; then
        #if $(exe inotify) --method libnotify --port "${PORT_INOTIFY}" "${@}" &>"${LOG_INOTIFY}"; then
            print_true '[%s]\n' $(<"${PID_INOTIFY}")
        else
            print_false '!\n'
        fi
    fi
}

qemu_tinycore() {
    local _ARGS=':str:path' _DESC="Use path as boot root."
    local path="${1}"
    shift

    exist "${path}" || return

    #[[ -e tinycore.img ]] || qemu-img -c tinycore.img 512M
    $(exe qemu) -curses -nographic -kernel "${path}/kernel/vmlinuz" -initrd "${path}/tc/tinycore.gz" -usb -append "quiet waitusb=5 nodhcp noicons text" "${@}"
}

extract() {
    local item itemN itemX itemD ext

    for item in "${@}"; do
        [[ -e ${item} ]] || continue

        case "${item}" in
            *.tar.bz2) ext='.tar.bz2' ;;
            *.tar.gz) ext='.tar.gz' ;;
            *.tar.xz) ext='.tar.xz' ;;
            *.tar) ext='.tar' ;;
            *.tgz) ext='.tgz' ;;
            *.gz) ext='.gz' ;;
            *.lzma) ext='.lzma' ;;
            *.zip) ext='.zip' ;;
            *.rar) ext='.rar' ;;
            *.sfs) ext='.sfs' ;;
            *.7z) ext='.7z' ;;
            *.crt) ext='.crt' ;;
            *) print_false "Undefined: ${NORMAL}%s\n" "${item}"; continue ;;
        esac
        itemN="${item//${ext}/}"

        if [[ ${PWD##*/} == ${itemN##+([./])} ]]; then  # ../
            itemD='.'
        else
            itemD="${itemN##*/}"
        fi
        mkdir "${itemD}"

        print_warn '%s\n' "${item##*/}"
        case "${ext#.}" in
            tar.bz2) $(exe tar) -C "${itemD}" -vxjf "${item}" ;;
            tar.gz|tgz) $(exe tar) -C "${itemD}" -vxzf "${item}" ;;
            gz) $(exe gzip) -d "${item}" >"${itemN##*/}" ;;
            tar|tar.xz) $(exe tar) -C "${itemD}" -vxf "${item}" ;;
            zip|rar|7z|lzma) $(exe 7z) x -o"${itemD}" "${item}" ;;
            crt) $(exe openssl) x509 -inform der -outform pem <"${item}" >"${itemN}.pem" ;;
            sfs) $(exe unsquashfs) -dest "${itemD}" "${item}" ;;
        esac
        #if (( ${?} )); then
        #    print_false '\t\tFalse\n'
        #else
        #    print_true '\t\tTrue\n'
        #fi
    done
}
extract_audio() {
    local item itemOut codecType codecName
    
    for item in "${@}"; do
	[[ -e ${item} ]] || { print_false "Invalid File \"${item}\"\n"; continue; }
	unset codecType codecName
	itemOut="${item##*/}"
	
	while read; do
	    [[ ${REPLY} == +(codec_type=*) ]] && codecType="${REPLY#*=}"
	    [[ ${REPLY} == +(codec_name=*) ]] && codecName="${REPLY#*=}"
	    [[ ${codecType} == +(*audio) && ${codecName} ]] && break
	done <<<"$($(exe ffprobe) -v quiet -show_streams "${item}")"

	[[ ${codecType} == +(audio*) ]] || continue
	case "${codecName}" in
	    aac) codecName='m4a' ;;
	    wmva2) codecName='wma' ;;
	esac
	
	print_true '%s:%s\n' "${itemOut}" "${codecName}"
	$(exe ffmpeg) -i "${item}" -vn -acodec copy "${item%.*}.${codecName}"
    done
}
extract_ogg() {
    local item itemOut dirOut remove list
    local bitRateD=192000 sampleRateD=44100 sampleFmtD=s16

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            #\?) print_help; return 1 ;;
            d) [[ -d ${2} ]] && dirOut="${2}"; shift ;;
	    r) (( remove++ )) ;;
        esac
        shift
    done
    
    for item in "${@}"; do
    	[[ -f ${item} ]] || continue
	read <<<$($(exe file) -b "${item}")
	[[ ${REPLY} == +(FLAC*|RIFF*) ]] || continue
	itemOut="${item}"
	[[ ${dirOut} ]] && itemOut="${item##*/}"
    	itemOut="${dirOut}/${itemOut%.*}.ogg"
	
	# if $(exe oggenc) --discard-comments \
	#     --resample=44100 --bitrate=192 \
	#     --output="${itemOut}" "${item}"; then
	    # (( remove )) && list+=( "${item}" )
	# fi

	$(exe ffmpeg) -n -i "${item}" -acodec libvorbis \
	    -vn -ac 2 -sample_rate "${sampleRateD}" -bit_rate "${bitRateD}" \
	    "${itemOut}"

	#$(exe sox) --show-progress \
	#    "${item}" \
	#    --bits 192 \
	#    "${itemOut}" \
	#    rate 44100 channels 2 
    done

    if (( ${#list} )); then
	printf '%s\n' "${list[@]}"
	read_true 'Remove' && $(exe rm) -rf "${list[@]}"
    fi
}
extract_ffmpeg() {
    local item bitRate codec sampleFmt sampleRate ext file
    local bitRateD=192000 sampleRateD=44100 sampleFmtD=s16
    local dirOut="${DIR_TMP_DATA}" remove opts dirOut2 out

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            #\?) print_help; return 1 ;;
            d) dirOut="${2%/}"; shift ;;
	    r) (( remove++ )) ;;
        esac
        shift
    done

    for item in "${@}"; do
	unset bitRate sampleRate sampleFmt codecType codec opts dirOut2 out
	
	while read; do
	    [[ ${REPLY} == +(codec_name=*) ]] && codec="${REPLY#*=}"
	    [[ ${REPLY} == +(sample_fmt=*) ]] && sampleFmt="${REPLY#*=}"
	    [[ ${REPLY} == +(codec_type=audio) ]] && codecType="${REPLY#*=}"
	    [[ ${REPLY} == +(bit_rate=[[:digit:]]*) ]] && bitRate="${REPLY#*=}"
	    [[ ${REPLY} == +(sample_rate=[[:digit:]]*) ]] && sampleRate="${REPLY#*=}"
	    [[ ${sampleRate} && ${bitRate} ]] && break
	done <<<"$($(exe ffprobe) -v quiet -show_streams "${item}")"
	[[ ${codecType} = +(audio) ]] || continue
	[[ ${item} == *+(/)* ]] && dirOut2="${item%/*}"
	ext="${item##*.}" file="${item##*/}" file="${file%.*}"
	dirOut2="${dirOut}/${dirOut2:+${dirOut2}/}"

	case "${codec}" in
	    flac) ext='ogg' bitRate='320000' opts='-acodec libvorbis' ;;
	esac
	out="${dirOut2}${file}.${ext}"
	
	print_true '%s: ' "${item}"
	printf '%s_%s@%s\n' "${sampleRate:-NA}" "${bitRate:-NA}" "${sampleFmt:-NA}"
	
	[[ -e ${out} ]] && continue
	[[ ${sampleRate} == +([[:digit:]]) && ${bitRate} == +([[:digit:]]) ]] || continue
	if (( bitRateD < bitRate || sampleRateD < sampleRate )); then
	    (( bitRateD < bitRate )) || bitRateD="${bitRate}"
	    (( sampleRateD < sampleRate )) || sampleRateD="${sampleRate}"
	    
	    mkdir "${dirOut2}"
	    case "${ext}" in
		null-ogg)
		    $(exe oggenc) \
			--bitrate="${bitRateD%000}" --resample "${sampleRateD}" \
			--output="${out}" "${item}"
		    ;;
		*)
		    $(exe ffmpeg) -n -i "${item}" ${opts[@]} \
			-vn -ac 2 -ar "${sampleRateD}" -ab "${bitRateD}" \
			"${out}"
		    ;;
	    esac
	    (( ${?} )) && { printf '%s %s\n' "${item}" "${out}"; rm -f "${out}" ; return; }
	fi
    done
}

clamav() {
    $(exe freshclam) \
        --log="${DIR_TMP_LOG}/freshclam.log" --datadir="${DIR_TMP_CACHE}" \
        --pid="${DIR_TMP_PID}/freshclam.pid" --user="${USER}" \
        "${@}"

    mkdir "${DIR_TMP_DATA}/clamav/infected"
    $(exe clamscan) \
        --verbose \
        --tempdir="${DIR_TMP_CACHE}" --log="${DIR_TMP_LOG}/clamav.log" \
        --database="${DIR_TMP_CACHE}" --move="${DIR_TMP_DATA}/clamav/infected" \
        --official-db-only=yes --infected --recursive=yes --detect-pua=yes "${@}"
}

gpg_decrypt_bitcoin() {
    $(exe wget) -qO - "${1}" |$(exe gpg) -q --output - --decrypt |xclip -i
}

python_install() {
    local _ARGS='<package>'
    local _DESC="
        Automate python package install.
        Package, 
            pip"
    local item items itemF itemU

    (( ${#} )) || { print_help; return;}

    for item in "${@}"; do
        case "${item}" in
            distribute)
                itemU="http://python-distribute.org"
                itemF="distribute_setup.py"

                $(exe wget) -c \
                    -O "${DIR_TMP_DATA}/${itemF}" \
                    "${itemU}/${itemF}"

                sudo $(exe python) "${DIR_TMP_DATA}/${itemF}" &&
                    $(exe rm) -rf "${DIR_TMP_DATA}/${itemF}"
            ;;
            pip)
                itemU="http://pypi.python.org/packages/source/p/pip"
                itemF="pip-1.1.tar.gz"

                $(exe wget) -c \
                    -O "${DIR_TMP_DATA}/${itemF}" \
                    "${itemU}/${itemF}" || continue
                $(exe tar) -zx \
                    -f "${DIR_TMP_DATA}/${itemF}" \
                    -C "${DIR_TMP_DATA}" || continue

                pushd "${DIR_TMP_DATA}/${itemF%.tar.gz}"
                sudo $(exe python) setup.py install &&
                    $(exe rm) -rf "${DIR_TMP_DATA}/${itemF}" \
                        "${DIR_TMP_DATA}/${itemF%.tar.gz}"
                popd
            ;;
            *) print_false 'Invalid!'; printf '%s\n' ;;
        esac
    done
}

gpg2ssh() {
    local item

    for item in "${@}"; do
        exist "${item}" || continue
        $(exe gpg) --export "${item}" |$(exe openpgp2ssh) "${item}" >"${item}.key"
    done
}

umount() {
    local item

    for item in "${@}"; do
        exist_mnt "${item}" || continue
        sudo $(exe umount) "${item}"
    done
}

perl_cpan() {
    false
    # curl -L http://cpanmin.us |perl -  App::cpanminus
    # cpan App::cpanminus
    # cpanm Module::Name
}

list() {
    unset ACTION DIGIT
    local item ITEMS ACTION digit

    while [[ ${@} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            # type[0] mode uid gid siz name
            case "${item}" in
                \?) break ;;
                f|file) ACTION='file' ;;
                p|proc) ACTION='proc' ;;
                n|net) ;;
                d|dir) ;;
                n|name) key=6 ;;
                b|byte) byte=1 ;;
                c|char) char=1 ;;
                T|total) total=1 ;;
                [[:digit:]]) digit="${ARG}" ;;
            esac
        done
    done

    list_action "${action:-file}" -${digit:1} "${@}"
}
list_action() { #:fix:
    local item items action key byte char total

    case "${1}" in
        file)
            local itemD itemF itemX
            local sum sumD sumF total chr col colT key=6
            local mode typ uid gid siz name ext
            local color colorM colorU colorG colorT colors
            read -a colors <<<"${LS_COLORS//:/ }"

            [[ ${items[@]} ]] || return

            for item in "${items[@]}"; do
                itemX=( $($(exe stat) -c '%f %a %u %g %s %n' "${item}") )

                case "${itemX[0]}" in
                    41ed) itemD+=( "${itemX[*]}" ) ;;
                    81a4) itemF+=( "${itemX[*]}" ) ;;
                esac

                col=$(( ${#itemX[4]}/2 ))
                (( col > colT )) && colT="${col}" 
            done

            [[ ${itemD} ]] &&
                itemD=( "$(printf '%s\n' "${itemD[@]}" |$(exe sort) --key="${key}")" )
            [[ ${itemF} ]] &&
                itemF=( "$(printf '%s\n' "${itemF[@]}" |$(exe sort) --key="${key}")" )
            itemX=( "${itemD[@]}" "${itemF[@]}" )

            color="${WHITE}" colorM="${WHITE}" colorU="${WHITE}" 
            colorG="${WHITE}" colorT="${WHITE}" colT=$(( colT-2 ))
            while read typ mode uid gid siz name; do
                printf -v mode '%.04d' "${mode}"
                sum=$(( sum+siz ))
                case "${typ}" in
                    41ed)
                        sumD=$(( sumD+siz ))
                        chr='/'
                    ;;
                    81a4)
                        sumF=$(( sumF+siz ))
                        chr=''
                    ;;
                esac
                case "$(( ${mode:0:1}+${mode:1:1}+${mode:2:1}+${mode:3:1} ))" in
                    17) colorM="${GREEN}" ;;
                    14) colorM="${WHITE}" ;;
                esac
                case "${uid}" in
                    0) colorU="${RED}" ;;
                esac
                case "${gid}" in
                    0) colorG="${RED}" ;;
                esac
                case "${typ}" in
                    41ed) colorT="${MAGENTA}" ;;
                    81a4) colorT="${YELLOW}" ;;
                esac

                for ext in "${colors[@]}"; do
                    if [[ ${ext%=*} == ${name##*.} ]]; then
                        colorT="${ESC}38;05;${ext#*=}m"
                        break
                    fi
                done
                (( byte )) && siz=$(( siz*1024*1024 ))

                printf "${NORMAL}"
                printf "${colorM}%s " "${mode}"
                printf "${colorU}%s:" "${uid}"
                printf "${colorG}%s " "${gid}"

                #for (( col=${colT}; col%2; col-2 )); do
                #done
                printf "${color}%d " "${siz}"
                for (( col=${colT}; col/2; col-- )); do
                    printf '\t'
                done
                printf "${colorT}%s" "${name}"
                (( char )) && printf "%s" "${chr}"
                printf "${NORMAL}\n"
            done <<<"$(printf '%s\n' "${itemX[@]}")"
            (( total )) && printf "## [%d(%d:%d)]\n" "${sum}" "${sumD}" "${sumF}"
        #net) lsof -ni ;;
    esac
}

profile.user.variables
profile.user.terminal
profile.user.alias

if (( REMOTE )); then
    unset hg
fi

source 'device'
source 'log'
source 'list'
source 'math'
source 'crypt'
source 'identity'
source 'request'
return 2>/dev/null

