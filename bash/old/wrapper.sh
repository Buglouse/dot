#!/usr/bin/env bash
source 'bash.sh'

wrapper.acpi() {
    local _ARGS='<name> <code> <id> <count>'
    local _DESC="
        Use xbindkeys.
        #--Example--#
        button/sleep SLPB 00000080 00000001."
    local item

    (( ${#} )) || return

    printf '%s\n' "${*}" >>"${LOG_ACPID}"

    case "${2}" in
        ATKD)
            case "${3}" in
                00000016) display.sh --off ;; # Disable
                00000030) ;; # Output
                00000038) ;; # Calibrate
                00000039) ;; # Run
            esac
        ;;
        LCDD)
            case "${3}" in
                00000086) ;; # Increase
                00000087) ;; # Decrease
            esac
        ;;
        AC*)
            #if (( ${4} )); then  # Charge
            #else # Discharge
            #fi
        ;;
        LID)
            [[ -e ${PROC_ACPI}/${1}/${2}/state ]] || continue
            item=( $(<"${PROC_ACPI}/${1}/${2}/state") )

            if [[ ${item[1]} == closed ]]; then
                touchpad.sh --off
                display.sh --off
            else
                display.sh --on
            fi
        ;;
        BAT*) ;;
        CPU*) ;;
        SLPB) ;;
        *) printf '%s\n' "${*}" >>"${LOG_ACPID}" ;;
    esac
}

wrapper.tmux() {
    if (( REMOTE )); then
        wrapper.tmux_exe
        wrapper.tmux_mem
        wrapper.tmux_net
    else
        source 'cpu'
        source 'request'
        source 'fan'
        source 'battery'
        source 'device_fan'

        wrapper.tmux_msg
        wrapper.tmux_bat
        wrapper.tmux_cpu
        wrapper.tmux_fan
        wrapper.tmux_exe
        wrapper.tmux_mem
        wrapper.tmux_net
        wrapper.tmux_mpd
    fi
}
wrapper.tmux_exe() {
    local item color color2
    local colorDef='colour242' colorSep='colour94' colorItem='green'

    #item="${PROCESS#*/}"
    #if (( item >= LIMIT_PROCESS_MAX )); then
    #    color2='red'
    #elif (( item >= LIMIT_PROCESS_MAX-(LIMIT_PROCESS_MAX/3) )); then
    #    color2='yellow'
    #else
        color2='green'
    #fi
    item="${PROCESS%/*}"  # Running
    if (( item >= LIMIT_PROCESS_RUN_MAX )); then
        color='red'
    elif (( item >= LIMIT_PROCESS_RUN_MAX/2 )); then
        color='yellow'
    else
        color='green'
    fi

    printf '#[fg=%s]{' "${colorSep}"
    printf '#[fg=%s]%s' "${color}" "${PROCESS%/*}"
    printf '#[fg=%s]/' "${colorDef}"
    printf '#[fg=%s]%s' "${color2}" "${PROCESS#*/}"
    printf '#[fg=%s]}' "${colorSep}"
}
wrapper.tmux_msg() {
    local mailCount=$(request.mail)
    local colorDef='colour242' colorSep='colour124' colorItem='colour38'

    [[ ${mailCount} == +([[:digit:]]) ]] || return
    printf '#[fg=%s](#[fg=%s]' "${colorSep}" "${colorItem}"
    printf '%s' "${mailCount}"
    printf '#[fg=%s])' "${colorSep}"
}
wrapper.tmux_net() {
    local itemL=$($(exe netstat) -ltun 2>/dev/null |
        $(exe tail) -n +3 |
        $(exe grep) -v '127.0.0.1|localhost' |
        $(exe wc) -l)
    local itemE=$($(exe netstat) -tun 2>/dev/null|
        $(exe tail) -n +3 |
        $(exe grep) -i 'established' |
        $(exe wc) -l)
    (( itemL || itemE )) || return
    local colorDef='colour242' colorSep='colour124' colorItem='colour38'

    printf '#[fg=%s](#[fg=%s]' "${colorSep}" "${colorItem}"
    printf '%s' "${itemL}"
    printf '#[fg=%s]/#[fg=%s]' "${colorDef}" "${colorItem}"
    printf '%s' "${itemE}"
    printf '#[fg=%s])' "${colorSep}"
}
wrapper.tmux_cpu() {
    local item items temp freq color IFS
    local colorDef='colour242' colorSep='colour94' colorItem='green'

    items=( $(cpu.list --cvs) )
    for item in "${items[@]}"; do
        [[ ${item} ]] || continue
        IFS=','; item=( ${item} ); unset IFS
        (( temp == ${item[1]} && freq == ${item[2]} )) && continue  # Duplicate?
        temp="${item[1]}" freq="${item[2]}"

        if (( temp >= LIMIT_CPU_TEMP_MAX )); then
            color='red'
        elif (( temp >= LIMIT_CPU_TEMP_MIN )); then
            color='yellow'
        else
            color='green'
        fi

        printf '#[fg=%s]{#[fg=%s]' "${colorSep}" "${colorItem}"
        printf '#[fg=%s]%s' "${color}" "${temp}"
        printf '#[fg=%s]@#[fg=%s]' "${colorDef}" "${colorItem}"
        printf '%s' "${freq}"
        printf '#[fg=%s]}' "${colorSep}"
    done
}
wrapper.tmux_mem() {
    local colorDef='colour242' colorSep='colour94' colorItem='green'
    local color mem="$($(exe sed) -n \
        -e 's/^MemFree:[^[:digit:]]*\([[:digit:]]*\).*/\1/p' \
        "${PROC_MEMINFO}")"
    mem=$(( ${mem}/1024 ))

    if (( mem < 9 )); then
        color="red"
    elif (( mem < 15 )); then
        color="yellow"
    else
        color="green"
    fi

    printf '#[fg=%s]{#[fg=%s]' "${colorSep}" "${colorItem}"
    printf '#[fg=%s]%d' "${color}" "${mem}"
    printf '#[fg=%s]}' "${colorSep}"
}
wrapper.tmux_fan() {
    local item items id rpm color
    local colorDef='colour242' colorSep='colour94' colorItem='green'

    items=( $(fan.list) )
    for item in "${items[@]}"; do 
        [[ ${item} ]] || continue
        id="${item%:*}" rpm="${item#*:}"

        if (( rpm > 4000 )); then
            color="red"
        elif (( rpm > 3965 )); then
            color="yellow"
        else
            color="green"
        fi

        printf '#[fg=%s]{#[fg=%s]' "${colorSep}" "${colorItem}"
        printf '%s' "${id}" 
        printf '#[fg=%s]@#[fg=%s]' "${colorDef}" "${colorItem}"
        printf '#[fg=%s]%s' "${color}" "${rpm}"
        printf '#[fg=%s]}' "${colorSep}"
    done
}
wrapper.tmux_bat() {
    local items=( $(battery.print cvs) )
    local item stat perc chrg curr diff time hour min symbol IFS
    local color colorDef='colour242' colorSep='colour94' colorItem='green'

    for item in "${items[@]}"; do
        IFS=','; item=( ${item} ); unset IFS
        stat="${item[1]}" chrg="${item[2]}"
        curr="${item[4]}" perc="${item[6]}"
        time="${item[7]}" diff="${item[8]}"
        hour="${time%:*}" min="${time#*:}"
        case "${stat}" in
            Charging) symbol='+' ;;
            Discharging) symbol='-' ;;
            Full|*) symbol='%' ;;
        esac

        if (( curr )); then
            if (( perc > 50 )); then
                color='green'
            elif (( perc > 15 )); then
                color='yellow'
            else
                color='red'
            fi
        else
            if (( diff > 1000 )); then  # 10%
                color='red'
            elif (( diff > 600 )); then
                color='yellow'
            else
                color='green'
            fi
        fi

        printf '#[fg=%s]{' "${colorSep}"
        if (( curr )); then
            printf '#[fg=%s]%02d' "${color}" "${perc}"
        else
            printf '#[fg=%s]%d.%d' "${color}" "${diff:0:2}" "${diff:2:2}"
        fi
        printf '#[fg=%s]%s#[fg=%s]' "${colorDef}" "${symbol}" "${colorItem}"
        if (( curr )); then
            if [[ ${hour} && ${min} ]]; then
                printf '%02d' "${hour}"
                printf '#[fg=%s]:#[fg=%s]' "${colorDef}" "${colorItem}"
                printf '#[fg=%s]%02d' "${colorItem}" "${min}"
            fi
            printf '#[fg=%s]@#[fg=%s]' "${colorDef}" "${colorItem}"
            printf '%d' "${curr}"
        else
            printf '%s' "${chrg}"
        fi
        printf '#[fg=%s]}' "${colorSep}"
    done
}
wrapper.tmux_mpd() {
    local item item2 host port itemA itemT
    local colorDef='colour255' colorSep='colour220' colorItem='colour144'

    identity.define_host 'mpd' &>/dev/null || return
    host="${MPD_HOST}" port="${MPD_PORT}"

    while read item item2; do
        [[ ${item} == state: && ${item2} == stop ]] && return

        [[ ${item} == Artist: ]] && itemA="${item2}"
        [[ ${item} == Title: ]] && itemT="${item2}"
    done <<<"$(printf '%s\n%s\nclose\n' 'status' 'currentsong' |
        $(exe nc) -w 10 "${host}" "${port}" 2>/dev/null )"

    [[ ${itemA} && ${itemT} ]] || return

    printf '#[fg=%s](#[fg=%s]' "${colorSep}" "${colorItem}"
    printf '%s/%s' "${itemA}" "${itemT}"
    printf '#[fg=%s])' "${colorSep}"
}

wrapper.execute() { ${@}; }

wrapper.arguments() {
    local item items
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                e|exe*) ACTION='exe' ;;
                a|acpi) ACTION='acpi' ;;
                t|tmux) ACTION='tmux' ;;
            esac
        done
    done

    [[ ${ACTION} == acpi ]] && { [[ ${ITEMS} ]] || return; }
    [[ ${ACTION} ]]
}

return 2>/dev/null

_ARGS='(a)cpi (e)xecute (t)mux [args]'
_DESC="
    Acpi, process events.
    Tmux, print '(Mail){Battery}{CPU}{Fan}{Process}(TCP)'.
    Execute, run ARGS (use '--' to prevent argument processing)."

wrapper.arguments "${@:-?}" || exit

case "${ACTION}" in
    exe) wrapper.execute "${ITEMS[@]}" ;;
    acpi) wrapper.acpi "${ITEMS[@]}" ;;
    tmux) wrapper.tmux ;;
esac
