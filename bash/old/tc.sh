#!/usr/bin/env bash
source bash.sh

tc.variables() {
    [[ -e /usr/share/doc/tc/release.txt ]] || return
    local item="/usr/share/doc/tc/release.txt"  #:order:0
    item=$(<"${item}")  #:order:1
    export VERSION_TC "${item#*_}"  #:order:1
    
    if [[ -e ${DIR_OPT}/.tce_dir ]]; then
	export DIR_TC $(<"${DIR_OPT}/.tce_dir")  #:order:0
    elif [[ -e ${DIR_ETC_CONF}/tcedir ]]; then
	export DIR_TC $(realpath "${DIR_ETC_CONF}/tcedir}")
    fi
	
    exist_elem 'tc.execute' "${CMD_EXE[@]}" || CMD_EXE+=( 'tc.execute' )
    export CMD_EXE
    export CMD_MOD 'tc.module'
    export CMD_LOGIN 'tc.login'

    export DIR_TC_EXT "${DIR_TC}/optional"
    export DIR_TC_LOOP "/tmp/tcloop"
    export DIR_ETC_CONF "${DIR_ETC}/sysconfig"
    export DIR_TC_BUILD "${DIR_TC}/build"  #:order:1 
    export DIR_TC_INSTALLED '/usr/local/tce.installed'

    export FILE_TC_INFO "${DIR_TMP_CACHE}/tc.info"

    export TC_EXT '.tcz'
    export TC_EXT_DEP '.dep'
    export TC_EXT_INFO '.info'
    export TC_EXT_LIST '.list'
    export TC_EXT_HASH '.md5.txt'
    export TC_EXTS "${TC_EXT_DEP}" "${TC_EXT_LIST}" "${TC_EXT_HASH}"
    export TC_HTTP "http://tinycorelinux.net"
    export TC_HTTP_LIST 'cgi-bin/whatprovides4xx86.cgi'
    export TC_HTTP_FIND 'cgi-bin/words4xx86.cgi'

    if [[ -d ${DIR_TC_BUILD} ]]; then  #:order:2
        unset CMD_EXE_FORCE; export CMD_EXE_FORCE
        for item in ${DIR_TC_BUILD}/!(.)*; do
            CMD_EXE_FORCE+=( "${item##*/}" )
        done
    fi

    #-----#
    export KERNEL "${VERSION_KERNEL}"
    export I18NPATH '/usr/share/i18n/charmaps'
}
tc.variables_mirror() {
    request.ping 'ibiblio.org' || return
    [[ ${TC_SERVER} && ${TC_REPO} ]] && return

    local item
    local items=(
        "http://distro.ibiblio.org/tinycorelinux/"
        "http://ftp.cc.uoc.gr/mirrors/linux/tinycorelinux/"
        "http://ftp.nluug.nl/os/Linux/distr/tinycorelinux/"
        "http://ftp.vim.org/os/Linux/distr/tinycorelinux/"
        "http://mirrors.163.com/tinycorelinux/"
        "ftp://distro.ibiblio.org/pub/linux/distributions/tinycorelinux/"
        "ftp://ftp.cc.uoc.gr/mirrors/linux/tinycorelinux/"
        "ftp://ftp.nluug.nl/os/Linux/distr/tinycorelinux/"
        "ftp://ftp.oss.cc.gatech.edu/pub/linux/distributions/tinycorelinux/"
        "ftp://ftp.vim.org/os/Linux/distr/tinycorelinux/"
        "ftp://sunsite2.icm.edu.pl/pub/Linux/sunsite.unc.edu/distributions/tinycorelinux/"
        "ftp://sunsite.icm.edu.pl/pub/Linux/sunsite.unc.edu/distributions/tinycorelinux/" )
    export TC_SERVERS "${items[@]}"  #:order:0
    export TC_SERVER_INFO 'info.lst.gz'

    for item in "${TC_SERVERS[@]}"; do
        if $(exe curl) --silent --fail --head "${item}" &>/dev/null; then
            export TC_SERVER "${item}"
            break
        else
            print_false 'Mirror: %s\n' "${item}"
            continue
        fi
    done

    [[ ${TC_SERVER} ]] && export TC_REPO "${TC_SERVER%/}/${VERSION_TC%.*}.x/x86/tcz"
}

tc.login() {
    local item items

    sudo $(exe rm) -f '/usr/local/bin/scp'  # 

    #--BuiltPackages--#
    print_init "[BUILD](${DIR_TC_BUILD})"
    if tc.build_copy; then
        print_true 'True\n'
    else
        print_false 'False\n'
    fi

    #--Module--#
    items+=( 'tc.module' )
    for item in "${items[@]}"; do
        print_init "[${item}]"
        if ${item} >/dev/null; then
            print_true 'True\n'
        else
            print_false 'False\n'
        fi
    done

    tc.update
}

tc.dot_root() {
    local item pathD="${DIR_ETC}" 

    exist_dir "${DIR_TC}" "${pathD}" || return
    for item in "${DIR_TC}"; do
        (( VERBOSE_LINK )) && print_link "${item}" "${pathD}/${item##*/}"
        parse.conf "${item}"
        printf '%s\n' "$(<"${item}")" >>"${pathD}/${item##*/}"
    done
}

tc.build_copy() {
    local item

    [[ -d ${DIR_TC_BUILD} ]] || return

    for item in ${DIR_TC_BUILD}/!(.*); do  # Parents
        (( VERBOSE_LINK )) && print_link "${item}" '/'
        sudo $(exe cp) -Hrp ${item}/* '/' 2>/dev/null
    done

    {
    sudo $(exe chown) 0:0 /usr/local/bin/busybox
    sudo $(exe chmod) 4775 /usr/local/bin/busybox
    }
}

##

tc.execute() {
    local _DESC="
        Load package contianing binary.
        Print path."
    local items

    case "${1}" in
        *cap) items=( 'libcap' 'tshark' ) ;;
        +([s|g])disk) items=( 'icu' ) ;;
        7z) items=( 'p7zip' ) ;;
        ?(fr)code|bigram) items=( 'findutils' ) ;;
        X|Xorg) items=( 'Xorg-7.6' 'Xorg-7.6-lib' 'Xorg-7.6-bin' ) ;;
        makeinfo) items=( 'texinfo' ) ;;
        acpi_listen) items=( 'acpid' ) ;;
        spring) items=( 'libdevil' 'libopenal' 'libogg' 'libvorbis' 'icu' ) ;;
        amixer) items=( 'alsa' ) ;;
        aspell) items=( 'aspell' 'aspell-en' ) ;;
        youtube-dl) items=( 'python' ) ;;
        base64) items=( 'coreutils' ) ;;
        bc) items=( 'bc-1.06.94' ) ;;
        cp) items=( 'coreutils' ) ;;
        date) items=( 'coreutils' ) ;;
	rsync) items=( 'rsync' 'popt' ) ;;
        dd) items=( 'coreutils' ) ;;
        dig|rndc|named) items=( 'bind' ) ;;
        dropbear|dropbearkey|dbclient) items=( 'dropbear' ) ;;
        easy_install) items=( 'python-distribute' ) ;;
        elinks) items=( 'libgcrypt' 'gnutls' 'libidn' 'libiconv' 'bzip2-lib' ) ;;
        emacs)  items=( 'graphics-libs-1' 'gnutls' 'GConf' 'libasound' 'gtk2' 'ImageMagick' ) ;;
        extlinux) items=( 'syslinux' ) ;;
        fancontrol) items=( 'lm_sensors' ) ;;
        fc-cache) items=( 'fontconfig' 'Xlibs' ) ;;
        feh)  items=( 'feh-1.3.4' ) ;;
        ffmpeg) items=( 'ffmpeg-0.11' ) ;;
        find) items=( 'findutils' ) ;;
        freshclam|clamscan) items=( 'clamav' ) ;;
        freeciv) items=( '' ) ;;
        gifview)  items=( 'gifsicle' ) ;;
        gimp) items=( 'gimp2' 'libopenraw' 'umfpack' 'aalib' 'poppler' 'webkit' 'SDL' 'pygtk-2.7' 'jasper' 'gnutls2' ) ;;
        gnutls*) items=( 'gnutls' 'gnutls2' ) ;;
        goxsh) items=( 'pycrypto' ) ;;
        gpg) items=( 'gpgme' ) ;;
        groups) items=( 'coreutils' ) ;;
        hg) items=( 'mercurial' ) ;;
        hg) items=( 'python' 'openssh' ) ;;
        hwclock) items=( 'util-linux' ) ;;
        import) items=( 'ImageMagick' ) ;;
        inotify)  items=( 'notify-python-2.7' ) ;;
        ip|ss) items=( 'iproute2' ) ;;
        java) items=( 'openjdk-7-jre' ) ;;
        kismet*) items=( 'pcre' 'libnl' 'libpcap' ) ;;
        lspci) items=( 'pci-utils' ) ;;
        lsusb) items=( 'usb-utils' ) ;;
        madplay) items=( 'libid3tag' ) ;;
        make) items=( 'automake' 'compiletc' ) ;;
        mkdir) items=( 'coreutils' ) ;;
        mount) items=( 'util-linux' ) ;;
        mplayer) items=( 'libasound' ) ;;
        mv) items=( 'coreutils' ) ;;
        ncmpcpp) items=( 'ncmpcpp' ) ;;
        newsbeuter) items=( 'ncursesw' 'libxml2' 'libasound' 'json-c' ) ;;
        openssl) items=( 'openssl-1.0.0' ) ;;
        pastebint) items=( 'python_configobj-2.7' ) ;;
        portmap) items=( 'portmap' 'tcp_wrappers' ) ;;
        xpdf) items=( 'libpaper' 'openmotif' ) ;;
        rpc.*) items=( 'nfs-utils' ) ;;
        rpcinfo|xtrace) items=( 'eglibc_apps' ) ;;
        sha2256sum) items=( 'coreutils' ) ;;
        shell-fm) items=( 'socat' 'libmad' 'libao' ) ;;
        ssh?(d|-*)) items=( 'openssh' ) ;;
        stat) items=( 'coreutils' ) ;;
        synclient) items=( 'Xorg-7.6' ) ;;
        tmux) items=( 'tmux' 'glibc_apps' 'libxft' 'perl5' ) ;;
        dicod) items=( 'm4' 'libgsasl') ;;
        updatedb|locate) items=( 'findutils' ) ;;
        urxvt+(c|d)) items=( 'perl5' 'libevent' ) ;;
        vncviewer) items=( 'tightvnc' ) ;;
        weechat-curses) items=( 'ncursesw' 'curl' 'gnutls' 'python' 'aspell' 'aspell-en' 'libgcrypt' 'ruby' 'lua' 'perl_URI' 'libiconv' ) ;;
        wpa_supplicant) items=( 'wpa_supplicant' ) ;;
        xauth|xset|xrdb) items=( 'Xorg-7.6-bin' ) ;;
        xbindkeys) items=( 'guile' 'xbindkeys' ) ;;
        xulrunner?(-bin)) items=( 'dbus' 'libasound' 'gtk2' 'flash11' ) ;;

        *) items=( "${1}" ) ;;
    esac
    [[ ${items} ]]  && tc.load "${items[@]/%/${TC_EXT}}" <"${TTY}" >"${TTY}"
}

tc.module() {
    local _DESC="
        Packages containing modules for devices."
    local _ items id bus dev ctrl

    tc.load \
        "hwmon-${VERSION_KERNEL}" \
        "pci-hotplug-${VERSION_KERNEL}" \
        "wireless-${VERSION_KERNEL}" || return

    unset items
    while read bus _ ctrl id _; do
        case "${id}" in
            '1002:4150') items+=( 'Xorg-7.6-3d' 'xf86-video-ati' ) ;;
	    '8086:0116'|'8086:a011'|'8086:27a2')
		items+=( 'Xorg-7.6-3d' 'xf86-video-intel' )
		;;
            '1002:94c3') items+=( 'firmware-radeon' ) ;;
	    '168c:001a') items=( 'firmware-atheros' ) ;;
            '8086:4232'|'8086:0885')
		items+=( 'firmware-iwlwifi' )
		;;
            '14e4:4727')
                items+=( 'firmware-broadcom' )
                modprobe -r 'brcmsmac' 
            ;;
        esac
    done <<<"$($(exe lspci))"

    #while read _ bus _ dev _ id _; do
    #    case "${id}" in
    #        *) ;;
    #    esac
    #done <<<"$($(exe lsusb))"

    [[ ${items} ]] && tc.load "${items[@]}"
    true
}

tc.modprobe() {
    local _ARGS='<modules>'
    local _DESC="
        Load packages containing MODULES."
    local item items

    unset items
    for item in "${@}"; do
        case "${item}" in
            nfs[d]) items+=( "filesystems-${VERSION_KERNEL}" ) ;;
            snd*) items+=( "alsa-modules-${VERSION_KERNEL}" ) ;;
            brcmsmac|mac80211) items+=( 'firmware-broadcom' ) ;;
        esac
    done

    [[ ${items} ]] && tc.load "${items[@]}"
    (( 1 ))
}

tc.find() {
    local _ARGS='<package>'
    local _DESC="
        Find package name."
    [[ ${1} ]] || return
    local IFS='|'; local re="${*}"; unset IFS
    local arg

    #tc.update || return

    [[ ${re} == *+(${TC_EXT})* ]] && arg='^'
    [[ -e ${FILE_TC_INFO} ]] && $(exe grep) -i "${arg}\(${re}\)" "${FILE_TC_INFO}"
}
tc.locate() {
    local _ARGS='<file>'
    local _DESC="
        Find package containg argument."
    local item uri="${TC_HTTP}/${TC_HTTP_LIST}"
    local IFS='|'; local re="${*//${TC_EXT}/}"; unset IFS

    if request.ping "${TC_HTTP}"; then
        for item in "${@}"; do
            [[ ${item} ]] || continue
            item="${uri}?${item,,}"

            $(exe curl) --silent --location "${item}" |
                $(exe grep) -v '<.*>' |
                $(exe sort) -d
        done
    else
        $(exe grep) -i "\(${re}\)" ${DIR_TC_EXT}/*${TC_EXT_LIST}
    fi
}
tc.library() {
    local _ARGS='<binary>'
    local _DESC="
        List realpath of linked libraries of compiled binary."
    local item itemL itemF items

    for item in "${@}"; do
        [[ ${item} ]] || continue
        [[ ${item} == +(/)* ]] || item=$(type -P "${item}")

        while read itemF _ itemL _;do
            [[ ${itemL} ]] || continue
            if [[ ${item} == not ]]; then
                continue
                #tc.locate "${itemF}"
            elif [[ ${itemL} != +(/)* ]]; then
                continue
            fi
            items+=( $($(exe realpath) "${itemL}") )

        done <<<"$($(exe ldd) $(type -P "${item}"))"
    done
    items=( "${items[@]/${DIR_TC_LOOP}\/}" )
    items=( $(_uniq "${items[@]/\/usr\/*}") )
    items=( ${items[@]/\/*/} )
    printf '%s\n' "${items[@]}" |${CMD_VIEW} -
}
tc.update() {
    local _DESC="
        Available packge list."
    local item="${FILE_TC_INFO}"

    tc.variables_mirror || return
    [[ -e ${item} && $($(exe stat) -c '%s' "${item}") -gt 1 ]] && return
    $(exe rm) -f "${item}"
    $(exe wget) -cq "${TC_REPO}/${TC_SERVER_INFO}" -O - |$(exe gunzip) -c >"${item}"
}

tc.load() {
    local item items

    for item in "${@}"; do
        tc.loaded "${item}" >/dev/null && continue

        if tce-load -i "${item}" &>/dev/null; then
            until tc.loaded "${item}" >/dev/null; do sleep 0.3; done
            continue
        fi
        items+=( "${item}" )
    done
    [[ ${items} ]] || return 0
    tc.fetch "${items[@]}"
}
tc.loaded() {
    local IFS='|'
    local re="$(printf '%q' "${*//${TC_EXT}/}")"
    unset IFS

    $(exe grep) -oi "${DIR_TC_LOOP}/\(${re:-[^ ]*}\)[ ]" "${PROC_MOUNTS}"
}
tc.unload() {
    local _ARGS=':str:pkgs'
    local _DESC="Unmount and remove register(tc.installed)."
    local item line

    for item in "${@//${TC_EXT}/}"; do
        [[ -e ${DIR_TC_LOOP}/${item} ]] || continue

        tc.loaded "${item}" >/dev/null && sudo $(exe umount) -dl "${DIR_TC_LOOP}/${item}"

        [[ -e ${DIR_TC_INSTALLED}/${item} ]] && $(exe rm) -f "${DIR_TC_INSTALLED}/${item}"

        rm_unresolved
    done
}
tc.remove() {
    local item item2

    for item in "$(tc.find "^${@//${TC_EXT}/}${TC_EXT}")"; do
        [[ ${item} ]] || continue

        for item2 in "" "${TC_EXTS[@]}"; do
            item2="${DIR_TC_EXT}/${item}${item2}"
            [[ -e ${item2} ]] && _rm "${item2}"
        done
    done
}

tc.fetch() {
    local _ARGS='<package>'
    local _DESC="
        GET package."
    local item item2 items dev prompt PS3="${TC_EXT#.}: "
    unset TC_EXT_IGNORE
    export TC_EXT_IGNORE

    while [[ ${1} == +(-)* ]]; do
        case "${1##+(-)}" in
            \?) print_help; return 1 ;;
            d|dev) (( dev++ )) ;;
            p|prompt) (( prompt++ )) ;;
        esac
        shift
    done

    exist_dir "${DIR_TC_EXT}" || return
    tc.update || return

    items=( $(tc.find "${@}") )
    [[ ${items} ]] || return
    print_true '(%s)\n' "${DIR_TC_EXT}"
    selects "${items[@]/${TC_EXT}/}"
    if (( dev )); then
        for item in "${SELECT[@]/%/${TC_EXT}}"; do
            [[ ${item} ]] || continue
            items=( $(tc.fetch_ext --dep "${item}") )

            for item2 in "${items[@]}"; do
                [[ ${item2} ]] || continue

                [[ ${item2} == *+(-lib)+(${TC_EXT}) ]] && item2="${item2/-lib/}"
                item2="${item2%${TC_EXT}}-dev${TC_EXT}"
                item2=$(tc.find "${item2}")
                [[ ${item2} ]] || continue

                tc._fetch "${item2}"
                tce-load -i "${item2}" &>/dev/null
            done
        done
    else
        for item in "${SELECT[@]/%/${TC_EXT}}"; do
            [[ ${item} ]] || continue

            tc._fetch "${item}" || continue
            tce-load -i "${item}" &>/dev/null
        done
    fi
}
tc._fetch() {
    local item itemS itemSF itemH itemHH pathD pathS

    tce-load -i 'bc-1.06.94' 'curl' &>/dev/null

    for item in "${@}"; do
        [[ ${item} == *+(KERNEL)* ]] && continue
        [[ ${item} == *${TC_EXT} ]] || item="${item}${TC_EXT}"
        pathD="${DIR_TC_EXT}/${item}" pathS="${TC_REPO}/${item}"

        exist_elem "${item}" "${TC_EXT_IGNORE[@]}" && continue
        TC_EXT_IGNORE+=( "${item}" )

        print_warn '%s\n' "${item/${TC_EXT}/}"

        set +C
        trap '{ set -C; trap - EXIT RETURN; }' EXIT RETURN
        tc.fetch_ext --info "${item}" >"${pathD}${TC_EXT_INFO}"
        if (( prompt )); then
            read_false -3 'Info' && ${CMD_VIEW} "${pathD}${TC_EXT_INFO}"
            [[ -e ${pathD} ]] && { read_false -5 'Remove' && tc.remove "${item}"; }
        fi
        if [[ -e ${pathD} ]]; then
            tc.fetch_ext --dep "${item}" >"${pathD}${TC_EXT_DEP}"
            [[ -e ${pathD}${TC_EXT_DEP} ]] || continue
            tc._fetch $(<"${pathD}${TC_EXT_DEP}")
            continue
        fi
        tc.fetch_ext --dep "${item}" >"${pathD}${TC_EXT_DEP}"
        tc.fetch_ext --hash "${item}" >"${pathD}${TC_EXT_HASH}"
        tc.fetch_ext --list "${item}" >"${pathD}${TC_EXT_LIST}"
        (( prompt )) && { read_true -3 'Continue' || continue; }

        itemS=$($(exe curl) --silent --fail --head "${pathS}" |
                $(exe sed) -n -e 's/Content-Length: \(.*\)/\1/p' |
                $(exe tr) -d '\r')
        if (( itemS/1024**2 > 1 )); then
            itemSF="$(( itemS/1024**2 ))M"
        elif (( itemS/1024 > 1 )); then
            itemSF="$(( itemS/1024 ))K"
        fi
        print_info 'Download: '
        print_warn "%15s\t\t" "[${itemSF}]"
        (tc.fetch_ext "${item}" >"${pathD}" &) &>/dev/null
        wait_write --perc "${pathD}:${itemS}"
        set -C
        trap - EXIT RETURN

        unset itemH itemHH
        [[ -e ${pathD}${TC_EXT_HASH} ]] &&
            itemH=( $($(exe md5sum) "${pathD}") ) itemHH=( $(<"${pathD}${TC_EXT_HASH}") )

        if [[ ${itemH} && ${itemH} == ${itemHH} ]]; then
            printf '\n'
        else
            tc.remove "${item}"
            print_false '!\n'
            continue
        fi

        [[ -e ${pathD}${TC_EXT_DEP} ]] || continue
        tc._fetch $(<"${pathD}${TC_EXT_DEP}")
    done
    [[ -e ${DIR_TC_EXT}/${item} ]]
}
tc.fetch_ext() {
    local _ARGS="<package${TC_EXT}>"
    local _DESC="
        GET package ext."
    local ext item uri

    case "${1##+(-)}" in
        dep) ext="${TC_EXT_DEP}" ;;
        hash) ext="${TC_EXT_HASH}" ;;
        info) ext="${TC_EXT_INFO}" ;;
        list) ext="${TC_EXT_LIST}" ;;
    esac
    [[ ${ext} ]] && shift

    [[ ${TC_REPO} ]] || tc.variables_mirror
    uri="${TC_REPO}"

    for item in $(tc.find "${@}"); do
        [[ ${item} ]] || continue

        $(exe curl) --silent --fail --head \
            "${uri}/${item}${ext}" &>/dev/null || continue

        $(exe curl) --silent "${uri}/${item}${ext}" |$(exe less)
    done
}

##

tc.extract() {
    local item

    for item in "${@}"; do
        [[ -e ${item} ]] || continue

        $(exe zcat) "${item}" | {
            mkdir "${item%%.*}"
            pushd "${item%%.*}"
            sudo $(exe cpio) -i -H newc -d 
            popd; }
    done
}

tc.archive() {
    local version

    tce-load -i 'advcomp'

    if read_true 'Modules'; then
        _read 'Kernel version: ' version
        #sudo $(exe chroot) "${PWD}" depmod "${version}"
    fi
    if read_true 'Shared Libraries'; then
        sudo $(exe ldconfig) -r .
    fi
    print_true "Archiving ${PWD} > ../tiyncore.gz."
    sudo $(exe find) |sudo $(exe cpio) -o -H newc |gzip -2 >'../tinycore.gz'
    print_true "Compressing ../tiyncore.gz."
    $(exe advdef) -z4 '../tinycore.gz'
}

tc.rsync() {
    local rsyncArg=( '--verbose' '--progress' '--archive'
        '--compress' '--update' '--human-readable' )

    tc.load 'rsync'

    $(exe rsync) ${rsyncArg[@]} "${DIR_TC_EXT}" "${@}"
}

tc.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                load) ACTION='load' ;;
                M|mod*) ACTION='module' ;;
                E|exe*) ACTION='execute' ;;
                A|autoload|module) ACTION='module' ;;
                \?|*) return 1 ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

source 'request'
tc.variables || return 2>/dev/null
return 2>/dev/null

_ARGS='(A)utoload (E)xecute (M)odprobe (l)oad'
_DESC="
    TinyCore Extension wrapper.

    #--Contents--#
    Load, install package.
    Find, repository search for package.
    Fetch, retrieve package.
    Loaded, list installed packaged.
    Locate, repository search for package containing file.
    Module, install package for manual module insert.
    Remove, local repository package removal.
    Update, retrieve package list from repository.
    Execute, install missing packages.
    Module, auto-detect modules and install package."

tc.arguments "${@:-?}" || exit

case "${ACTION}" in
    load) tc.load "${ITEMS[@]}" ;;
    module) tc.module "${ITEMS[@]}" ;;
    execute) tc.execute "${ITEMS[@]}" ;;
    module) tc.module ;;
esac
