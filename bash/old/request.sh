#!/usr/bin/env bash
source 'bash.sh'

request.variables() {
    local item

    export ATOM_GMAIL 'https://mail.google.com/mail/feed/atom/'
    export AGENT_HTTP 'self'

    export ADDR_NYAN 'miku.acm.uiuc.edu'
    export ADDR_ARCH_PKG 'projects.archlinux.org'
    export ADDR_GOOGLE_DNS '8.8.4.4'

    export HTTP_GEO 'http://geoiplookup.wikimedia.org'
    export HTTP_GOES 'http://www.goes.noaa.gov/MOVIES'
    export HTTP_WWO 'http://free.worldweatheronline.com'
    export HTTP_EGLIBC 'http://www.eglibc.org'
    export HTTP_EGLIBC_LOCALE 'cgi-bin/viewvc.cgi/trunk/libc/localedata/locales'
    export HTTP_EGLIBC_SUPPORT 'cgi-bin/viewvc.cgi/trunk/libc/localedata/SUPPORTED?view=co'
    export HTTP_CLIFU 'http://commandlinefu.com'
    export HTTP_CLIFU_TAGS 'acpi' 'alsa' 'api' 'archive' 'array' 'ascii' 'atom' 'awk' 'bash' \
        'calc' 'calculation' 'cat' 'cli' 'commandlinefu' 'cpio' 'cpuinfo' 'du' 'emacs' \
        'ffmpeg' 'free' 'GPG' 'hdparm' 'iptables' 'kernel' 'math' 'mercurial' 'meminfo' \
        'mpd' 'mutt' 'nc' 'nfs' 'nmap' 'notify' 'ogg' 'pcre' 'pdf' 'posix' 'print' 'python' \
        'sensors' 'ssh' 'socat' 'sysfs' 'tcpdump' 'udev' 'utf8' 'vorbis' 'X11' 'xml' 
    export HTTP_CLIFU_RANDOM 'commands/random'
    export HTTP_CLIFU_MATCHING 'commands/matching'
    export HTTP_GOOGLE_SPELL 'https://www.google.com/tbproxy/spell'
    export HTTP_GOOGLE_QUERY 'http://google.com/search?hl-en&num=100q='
    export HTTP_GOOGLE_SPEECH 'http://translate.google.com/translate_tts?ie=UTF-8&'
    export HTTP_GOOGLE_SPELL_GET 'spellresult'
    export HTTP_GOOGLE_SPELL_POST 'spellrequest'
    export HTTP_NOAA 'http://www.weather.gov'
    export HTTP_NOAA_XML 'xml/current_obs'
    export HTTP_AMINCHE 'http://odds.aminche.com'
    export HTTP_AMINCHE_QUERY 'poker-eval-webservice.rest'
    export HTTP_SPELL_DICT 'dict://dict.org'
    export HTTP_YOUTUBE_API 'http://gdata.youtube.com/feeds/api'
    export HTTP_ADDRESS_RESOLVE 'whatismyip.org' 'ifconfig.me' 'chisono.it/ip.asp' 'ip.boa.nu'

    export FTP_INTERNIC 'ftp://ftp.rx.internic.net'
    export FTP_INTERNIC_ROOT 'domain/db.cache'

    export NOAA_WI 'Racine:KRAC' 'Milwaukee:KMKE' 'Waukesha:KUES' 'Sheboygan:KSBM' \
        'FondDuLac:KFLD' 'GreenBay:KGRB' 'Burington:KBUU'
    export NOAA_AREA 'WI'
    export NOAA_INDEX "${DIR_TMP_CACHE}/noaa_index.xml"

    export WWO_QUERY 'feed/weather.ashx'

    [[ -e ${FILE_DNS} ]] &&
        { item=( $(<"${FILE_DNS}") )
        [[ ${item} ]] && IP_DNS="${item[${#item[@]}-1]}"; }
    [[ ${IP_DNS} ]] || IP_DNS="${ADDR_GOOGLE_DNS}"
    export IP_DNS
}

request.dictionary() {
    local _ARGS='(acry) (thes) (gcide) (foldoc) (a)ll (f)ind (l)ist (s)trat (d)fine'
    local _DESC="
        Define, define selection if misspelled."
    local item action url out re reE select

    [[ ${1} ]] || return
    while [[ ${1} == +(-)* ]]; do
        case "${1##+(-)}" in
            \?) print_help; return 1 ;;
            a|all) action='all' ;;
            f|find) action='find' ;;
            l|list) action='list' ;;
            s|strat) action='strat' ;;
            d|define) (( select++ )) ;;

            #--dict.org--#
            acry) action='vera' ;;
            thes) action='moby-thes'; (( select++ )) ;;
            gcide) action='gcide' ;;
            foldoc) action='foldoc' ;;
        esac
        shift
    done
    [[ ${url} ]] || url="${HTTP_SPELL_DICT}"

    for item in "${@:-0}"; do
        out="$(printf '%s\n' "${item}" |
            $(exe aspell) pipe |
            $(exe tail) -n +2)"

        if $(exe grep) -q '^&' <(printf '%s' "${out}"); then
            if (( select )); then
                out=( ${out##*:} )
                selects -1 ${out[@]//,/} || continue
                item="${SELECT}"
            else
                printf '%s\n' "${out}"
                continue
            fi
        fi

        case "${url##*://}" in
            +(dict.org))
                re='^\([[:digit:]]\|\.\)' reE='250' reO='552'
                if [[ ${action} == all ]]; then
                    url+="/d:${item}:all"
                elif [[ ${action} == find ]]; then
                    url+="/m:${item}"
                elif [[ ${action} == list ]]; then
                    url+="/show:db"
                elif [[ ${action} == strat ]]; then
                    url+="/show:strat"
                else
                    url+="/d:${item}:${action:-gcide}"
                fi
            ;;
        esac
        out=$($(exe curl) --silent "${url}")
        $(exe grep) -q "^\(${reE}|${reO}\)" <(printf '%s' "${out}") || continue
        printf '%s\n' "${out}" |
            $(exe grep) -v "\(${re}\)" |
            "${CMD_VIEW}" -
    done
}

request.address() {
    local _ item items addr name pass

    if (( ${#} )); then
        for item in "${@}"; do
            [[ ${item} ]] || continue
            if $(exe grep) -q "\b${item}\b" "${FILE_HOSTS}"; then
                items+=( $($(exe sed) -n \
                    -e "/\b${item}\b/{s/\([^ ]*\).*/\1/p}" "${FILE_HOSTS}") )
                continue
            elif ! request.ping "${item}"; then
                print_false 'Unresolved: %s\n' "${item}"
                continue
            fi

            while read _ name addr _; do
                ! (( pass )) && { [[ ${name} == ${item} ]] && (( pass++ )); }
                (( pass )) || continue

                [[ ${addr} == *+(.)* ]] && items+=( "${addr}" )
            done <<<"$($(exe nslookup) "${item}" |$(exe tail) -n +4)"
        done
    else
        items=( $($(exe curl) --silent "${HTTP_ADDRESS_RESOLVE[1]}") )
    fi

    [[ ${items} ]] && printf '%s\n' "${items[@]}"
}

request.zone() {
    local item

    for item in "${@}"; do
        case "${item}" in
            root)
                request.ping "${FTP_INTERNIC}" || continue
                $(exe wget) -c \
                    -O "${FILE_DNS_ROOT}" \
                    "${FTP_INTERNIC//\/\////ftp:ftp@}/${FTP_INTERNIC_ROOT}"
                #--user=ftp --password=ftphh
            ;;
        esac
    done
}

request.locale() {
    local _ARGS='[locale]'
    local _DESC="
        Retrive locale from eglibc repository.
        Only UTF support (or implement charset)."
    local item items itemsS re='.|@' itemP="${DIR_I18N}/locale"
    [[ -d ${itemP} ]] || itemP="${DIR_TMP_CACHE}"

    request.ping -f "${HTTP_EGLIBC}" || return

    while read -r item; do
        #[[ ${item} == +(#)* ]] && continue
        #[[ ${item} == *+(/)* ]] || continue
        [[ ${item} == *+(UTF)* ]] || continue
        printf '%s\n' ":${item}"

        item=( "${item%%/*}" )
        item="${item%+(${re})*}"
        items+=( "${item}" )
        for item2 in "${@}"; do
            [[ ${item,,} == ${item2,,} ]] && itemsS+=( "${item}" )
        done
    done <<<"$($(exe curl) --silent "${HTTP_EGLIBC}/${HTTP_EGLIBC_SUPPORT}")"

    # local item2 itemC
    # Sanitize; remove duplicate (keep UTF), remove '@'.
    #[[ ${itemsS} ]] ||
    #    { for item in "${items[@]}"; do
    #        [[ ${item} == *+(UTF)* ]] ||
    #            for item2 in "${items[@]}"; do
    #                [[ ${item2} == *+(${re})* ]] || continue  # Original
    #                [[ ${item2} == *+(${item})* ]] && continue 2
    #            done
    #        itemsC+=( "${item}" )
    #    done; selects "${itemsC[@]}"; }

    [[ ${itemsS} ]] || selects "${items[@]}"
    items=( "${itemsS:-${SELECT[@]}}" )
    for item in "${items[@]}"; do
        $(exe wget) -c \
            -O "${itemP}/${item}" \
            "${HTTP_EGLIBC}/${HTTP_EGLIBC_LOCALE}/${item}?view=co"
    done
}

request.geo() {
    request.ping || return
    local out IFS
    local out="$($(exe curl) --silent "${HTTP_GEO}")"
    out="${out#*\{}" out="${out%\}*}"
    IFS=','; out=( ${out//\"/} ); unset IFS
    out=( "${out[@]::(${#out[@]}-2)}" )

    printf '%-15s %s\n' ${out[@]//:/: }
}

request.dns() {
    local _ARGS=''
    local _DESC="
        Update SOA using identity, hostname, and external address."
    local pass user

    identity.define "${HOSTNAME}" || return
    pass="${HOSTNAME^^}_PASS" pass="${!pass}"
    user="${HOSTNAME^^}_USER" user="${!user}"
    [[ ${pass} && ${user} ]] || return

    case "${pass}" in
        http*) $(exe curl) --silent "${pass}" ;;
    esac
}

request.ping() {
    local _ARGS='(l)ist (v)erbose (f)ail (digit) [address|identity]'
    local _DESC="
        List, print ping stdout.
        Fail, print 'False'.
        Verbose, print 'True' or 'False'.
        Digit, repeat ping*DIGIT.
        Address, valid IP/DN address.
        Identity, IdentityDB support."
    local arg item itemOld host fail port list verb rtnF int=1 rtn=1

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return ;;
            l|list) (( list++ )) ;;
            f|fail) (( fail++ )) ;;
            v|verb*) (( verb++ )) ;;
            +([[:digit:]])) int="${item}" ;;
        esac
        shift
    done

    for item in "${@:-${IP_DNS}}"; do
        [[ ${item} ]] || continue

        case "${item}" in
            +(*://*)) item="${item##*://}" item="${item%%/*}" ;;
            +(*.*)) ;;
            +([_[:alpha:]])) # DN/Identity
                itemOld="${item}"
                item="$($(exe sed) -n \
                    -e "/[[:space:]]${item}/{s/\([^[:space:]]*\).*${item}.*/\1/p;}" \
                    "${FILE_HOSTS}")"
                [[ ${item} ]] ||
                    { identity.define_host "${itemOld}"
                    item="${itemOld^^}_HOST" item="${!item}"; }
            ;;
        esac
        [[ ${item} ]] || continue

        { if (( list )); then
            $(exe ping) -W 2 -c ${int} "${item}"
        else
            $(exe ping) -w 2 -W 2 -c ${int} "${item}" >/dev/null
        fi; } 2>/dev/null
        rtn=${?}
        (( rtn )) && (( rtnF++ ))

        if (( verb || VERBOSE || fail )); then
            if (( rtn )); then
                print_false '[Ping]False: '; print_warn '%s\n' "${item}"
            elif ! (( fail )); then
                print_true '[Ping]True: '; print_warn '%s\n' "${item}"
            fi
        fi
    done
    return ${rtnF:-${rtn}}
}

request.poker_eval() {
    local _ARGS='[<cards>]'
    local _DESC="
        Use poker-eval processed on ${HTTP_AMINCHE} to return card calculations.
        If no arguments, prompt for card values.

            Cards, space separate each grouping;
                Hand1, ac qh
                Hand2, 3s 5d
                Table, 5s qd 6c
                Example, acqh 3s5d 5sqd6c"
    local int out err item item2 itemH itemT items IFS

    if (( ${#} )); then  # arguments
        unset itemH itemT
        for item in "${@//10/T}"; do
            # Invalid grouping.
            (( ${#item}%2 )) &&
                { print_false 'Invalid! '; printf '%s\n' "${item}"
                (( err++ )); continue; }

            if (( ${#item} >= (3*2) )); then  #Table
                # Two cards per iteration.
                for (( int=0;int<(${#item}/2);int++ )); do
                    itemT+=( "'${item:(int*2):2}'," )
                done
            else #Hand
                itemH+=( "['${item:0:2}','${item:2:2}']" )
            fi
        done
        (( err )) && return 1
    else  # prompt
        printf 'Two cards per Hand [ah 3c].\n'
        unset itemH
        while :; do
            read -e -i "${item:+${item} }" -p 'Hand: ' item item2
            [[ ${itemH} ]] && { [[ ${item} ]] || break; }
            [[ ${item2} ]] || continue
            itemH+=( "['${item}','${item2}']" )
            unset item item2
        done
        printf 'List all cards.\n'
        read -e -p 'Table: ' -a items
        unset itemT
        for item in "${items[@]}"; do
            [[ ${item} ]] || continue
            itemT+=( "'${item}'," )
        done

    fi

    # Generate query.
    item="${HTTP_AMINCHE}/${HTTP_AMINCHE_QUERY}?game='holdem'"
    IFS=','
    [[ ${itemT} ]] && item+="&board=[${itemT[*]%,}]"
    item+="&pockets=[${itemH[*]%,}]"
    unset IFS
    # POST error with single-quotes.
    item="${item//\'/\"}"
    # Curl error without escaped brackets.
    item="${item//[/\[}" item="${item//]/\]}"
    # Query result.
    #printf '%s\n' "${item}"
    out="$($(exe curl) --silent "${item}")"
    # Parse result.
    out="${out#*eval*[}" out="${out%]\}}"
    out="${out//\{/}" out="${out//\},/;}" out="${out//\}/}" out="${out//\"/}"
    IFS=';'; out=( ${out} ); unset IFS

    for (( int=0;int<=${#itemH[@]};int++ )); do
        printf '%s %s\n' "${itemH[int]}" "${out[int]}"
    done
}

request.mail() {
    identity.define 'mail' || return

    local args=( '--silent' '--insecure' "--url ${ATOM_GMAIL}" )

    $(exe curl) --head ${args[@]} &>/dev/null || return

    $(exe curl) ${args[@]} --user ${MAIL_USER}:${MAIL_PASS} |
    $(exe sed) -n -e 's@<fullcount>\(.*\)</fullcount>@\1@p'
}

request.youtube() {
    local _ARGS='[order|duration|time|request] (l)ist [(p)rint*|(g)et] <users>'
    local _DESC="
        Perform action for users.

        List, items of users.
        Request, replace REQUEST with;
            - favorites
            - uploads
            - newsubscriptionvideos
            - playlists
            - contacts
            - profile
            - subscriptions
            - watch_history

            #--Alias--#
            - description
            - history
        Order, replace ORDER with;
            - commentCount
            - duration
            - published
            - title
            - viewCount

            #--Alias--#
            - view
        Duration, replace DURATION with; 
            - short
            - medium
            - long
        Time, replace TIME with; 
            - top_rated
            - top_favorites
            - most_viewed
            - most_popular
            - most_discussed
            - most_responded
            - most_subscribed"
    local item items url urlOpt urlOpt2 duration order action='print'
    local reO='commentCount|duration|published|title'
    local reD='short|medium|long'
    local reT="|top_rated|top_favorites|most_viewed|most_popular|most_discussed|most_responded|most_subscribed"
    local reU='subscriptions|newsubscriptionvideos|watch_history|favorites|playlists|contacts|profile|uploads'
    local reV=''
    local reUS='profile|uploads'
    local reVS=''
    local reUU='viewCount'  # user/upload
    #--Alias--##
    reUA='history|viewcount|view'
    reV='description'
    reRVS+='description'
    #####

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            get) action='get' ;;
            print) action='print' ;;
            list|videos)
                url='users' urlOpt='/uploads'
            ;;
            +(${reD})) duration="&duration=${item}" ;;
            +(${reT})) time="&time=${item}" ;;
            +(${reO}|${reOA}))
                url='videos'
                if [[ ${item} == +(${reOA}) ]]; then
                    case "${item}" in
                        *) ;;
                    esac
                fi
                order="&orderby=${item}"
            ;;
            +(${reU}|${reUA}))
                url='users'
                if [[ ${item} == +(${reUA}) ]]; then
                    case "${item}" in
                        history) item='watch_history' ;;
                        view|viewcount) item='viewCount' ;;
                    esac
                fi
                if [[ ${item} == +(${reUU}) ]]; then
                    urlOpt='/uploads'
                elif [[ ${item} == +(${reUS}) ]]; then
                    urlOpt="/${item}"
                    urlOpt2="&max-results=50"
                else
                    urlOpt="/${item}"
                fi
            ;;
            +(${reV}))
                url='videos'
            ;;
        esac
        shift
    done

    for item in "${@}"; do
        [[ ${item} ]] || continue

        item="${HTTP_YOUTUBE_API}/${url}/${item}${urlOpt}?"
        item+="alt=rss&v=2${urlOpt2}${order}${duration}${time}"
        if [[ ${action} == print ]]; then
            printf '%s\n' "${item}"
        elif [[ ${action} == get ]]; then
            $(exe curl) --silent --insecure "${item}"
        fi
    done
}

request.get() {
    local item items=( "${@}" )

    for item in "${items[@]}"; do
        case "${item}" in
            git) $(exe git) clone "${item}" ;;
        esac
        if (( ${?} )); then
            print_false 'Invalid: '
            print_warn '%s\n' "${item}"
            return 1
        else
            continue
        fi
    done
}

request.nynd() {
    [[ ${ADDR_NYAN} ]] && $(exe nc) "${ADDR_NYAN}" 23
}

request.clifu() {
    local uri uriEnc uriItem
    local curlArg=( '--silent' '--location' )

    uri="${HTTP_CLIFU}/${HTTP_CLIFU_RANDOM}"
    #riItem="${HTTP_CLIFU_TAGS[RANDOM%=${#HTTP_CLIFU_TAGS[@]}]}"
    #uriEnc=$( printf '%s' "${uriItem}" |$(exe base64) )

    $(exe curl) ${curlArg[@]} --url "${uri}/plaintext" \
        |$(exe grep) -v '^$' |$(exe tail) -n 2
}

request.gnutls() {
    [[ ${1} ]] || print_false '!Argument 1:server'
    [[ ${2} ]] || print_false '!Argument 2:port'
    [[ ${3} && ! -e ${3} ]] && print_false '!Invalid file'
    [[ ${1} && ${2} ]] || return

    $(exe gnutls-cli) --x509cafile "${3:-${CERT_CA}}" --port "${2}" "${1}"
}

request.tts() {
    local curlArg=( '--silent' ) output="${DIR_TMP_DATA}/google-tts.mp3"
    local IFS='+'; local url="${*}"; unset IFS

    print_true 'Writing: '; printf '%s\n' "${*}"
    print_warn '>> %s' "${output}"
    if $(exe curl) ${curlArg[@]} --user-agent 'Mozilla' \
        --output "${output}" --url "${HTTP_GOOGLE_SPEECH}${url}"; then
        print_erase ">> ${output}"; print_true ">> ${output}"
    else
        print_erase ">> ${output}"; print_false ">> ${output}"
    fi
}

request.cert() {
    local item host

    for item in "${@}"; do
        host="${item##*//}"
        case "${item%%:*}" in
            https)
                echo |
                    $(exe openssl) s_client -showcerts -connect "${host%%/*}:443" 2>&1 |
                    $(exe sed) -n -e '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
            ;;
        esac
    done
}

request.cert_info() {
    local item

    for item in "${@}"; do
        [[ -f ${item} ]] || continue

        $(exe openssl) x509 -text -in "${item}"
    done
}

request.exchange() {
    #curl -s wap.kitco.com/exrate.wml | awk ' BEGIN { x=0; FS = "<" } { if ($0~"^<br/>") {x=0} if (x==1) {print $1} if ($0~"EUR/US") {x=1} }'
    false
}

#curl -s http://ws.audioscrobbler.com/1.0/user/<user_id>/recenttracks.rss|grep '<title>'|sed -n '2s/ *<\/\?title>//gp'

request.spell() {
    local server="${HTTP_GOOGLE_SPELL}"
    local tagPost="${HTTP_GOOGLE_SPELL_POST}" tagGet="${HTTP_GOOGLE_SPELL_GET}"
    local curlArg=( '--insecure' '--location' '--silent' "--url ${server}" )

    for item in "${@}"; do
        $(exe curl) ${curlArg[@]} --data "<${tagPost}><text>${item}</text</${tagPost}>" \
#|sed \ -e "s@.*<${tagGet} [^>]*>\(.*\)</${tagGet}>@\1@;s@<c \([^>]*\)>\([^<]*\)</c>@\1;\2\n@g"
#|grep 's="1"' |$(exe sed) -e 's/^.*;\([^\t]*\).*$/\1/'

    done
    
#curl -sd "<spellrequest><text>$1</text></spellrequest>" https://www.google.com/tbproxy/spell | $(exe sed) 's/.*<spellresult [^>]*>\(.*\)<\/spellresult>/\1/;s/<c \([^>]*\)>\([^<]*\)<\/c>/\1;\2\n/g' | grep 's="1"' | $(exe sed) 's/^.*;\([^\t]*\).*$/\1/'; }
}

request.nasa() {
true
     #links -dump "http://spaceflight.nasa.gov/realdata/sightings/cities/view.cgi?country=United_States&region=Wisconsin&city=Portage" | $(exe sed) -n '/--/,/--/p'
}

request.rss() {
    local item

    for item in "${@}"; do
        #-e 's@<[^>]*>//g'
        request.ping "${item}"
        $(exe curl) "${item}" | $(exe sed) -e 's@</[^>]*>@\n@g'
    done
}

request.weather() {
    local _ARGS='<location>'
    local _DESC="
        Print weather parsed from ${HTTP_WWO}.

        Location, State|Station|Longitude+Latitude."
    [[ ${HTTP_WWO} && ${WWO_QUERY} ]] || return
    local item int int2 out out1 out2 out3 out4 re pass skip lat lon day=1 IFS

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            +([[:digit:]])) day="${item}" ;;
        esac
        shift
    done

    identity.define 'worldweatheronline.com'
    pass="${WORLDWEATHERONLINE_COM_PASS}"
    [[ ${pass} ]] || { print_false 'Invalid! API key\n'; return 1; }

    re=( 'current_condition'
    'observation_time' 'temp_C' 'temp_F' 'windspeedMiles' 'windspeedKmph'
    'winddirDegree' 'winddir16Point' 'precipMM' 'humidity' 'visibility'
    'pressure' 'cloudcover'
    'weather'
    'date' 'tempMaxC' 'tempMaxF' 'tempMinC' 'tempMinF' 'windspeedMiles'
    'windspeedKmph' 'winddirection' 'winddir16Point' 'winddirDegree'
    'precipMM' )
    IFS='|'; re="${re[*]}"; unset IFS

    # Location
    read lat lon <<<"$(request.geo |printf '%s %s' \
            $($(exe sed) -n -e 's/^l[[:alpha:]:]*[[:space:]]*\([[:digit:]]*\)/\1/p'))"

    # Collect data; determin order.
    while parse.xml; do
        [[ ${REPLY} == +(${re}) ]] || continue
        case "${REPLY}" in
            current_condition) continue ;;
            weather) (( skip++ )); continue ;;
        esac
        if (( skip )); then
            case "${REPLY}" in
                date) int=0 ;;
                precipMM) int=5 ;;
                tempMaxC) int=1 ;;
                tempMaxF) int=3 ;;
                tempMinC) int=2 ;;
                tempMinF) int=4 ;;
                winddir16Point) int=6 ;;
                winddirDegree) int=7 ;;
                winddirection) int=8 ;;
                windspeedKmph) int=9 ;;
                windspeedMiles) int=10 ;;
            esac

            # :fix:Dynamic AA?
            case "${skip}" in
                1) out1[int]="${REPLY}:${REPLY[1]//[:|-]/.}" ;;
                2) out2[int]="${REPLY}:${REPLY[1]//[:|-]/.}" ;;
                3) out3[int]="${REPLY}:${REPLY[1]//[:|-]/.}" ;;
                4) out4[int]="${REPLY}:${REPLY[1]//[:|-]/.}" ;;
            esac
        else
            case "${REPLY}" in
                cloudcover) int=1 ;;
                humidity) int=2 ;;
                observation_time) int=0 REPLY[1]="${REPLY[1]// /}" ;;
                precipMM) int=6 ;;
                pressure) int=3 ;;
                temp_C) int=4 ;;
                temp_F) int=5 ;;
                visibility) int=7 ;;
                winddir16Point) int=8 ;;
                winddirDegree) int=9 ;;
                windspeedKmph) int=10 ;;
                windspeedMiles) int=11 ;;
            esac

            out[int]="${REPLY}:${REPLY[1]//[:|-]/.}"
        fi
    done <<<"$($(exe curl) --silent --location \
        "${HTTP_WWO}/${WWO_QUERY}?key=${pass}&format=xml&date=today&num_of_days=${day}&q=${lat},${lon}")"

    for (( int=0;int<=${#out[@]};int++ )); do
        IFS=':'; item=( ${out[int]} ); unset IFS
        [[ ${item} ]] || continue

        printf '%-30s %s\n' "${item}:" "${item[1]}"
    done

    if (( skip )); then
        printf '\nForcast\n'
        for (( int=0;int<=skip;int++ )); do
            printf -v out 'out%s[@]' "${int}"; out=( ${!out} )
            for (( int2=0;int2<=${#out[@]};int2++ )); do
                printf -v item 'out%s[%s]' "${int}" "${int2}"
                IFS=':'; item=( ${!item} ); unset IFS
                [[ ${item} ]] || continue

                printf '%-30s %s\n' "${item}:" "${item[1]}"
            done
        done
    fi
}
request.weather_radar() {
    local types=( 'IR' 'VS' 'WV' ) locs=( 'EC' 'WC' 'pr' 'al' )
    true
}
request.weather_image() {
    local url_host="http://weather.unisys.com/" url_pre='previous/'
    local radars=( 'satellite' 'surface' 'upper_air' 'radar' )
    local file_ext=".gif"
    local delay=45
    local item item2 item3 items item_out ext radar region count urls file_pre
    local region regions types file_dir pass
    local PS3
    local dir_out="${DIR_TMP_CACHE}/weather_image/"

    mkdir "${dir_out}"

    PS3='Radar? ' selects "${radars[@]}"
    radar=( "${SELECT[@]}" )
    
    # Generate URLs.
    for item in "${radar[@]}"; do
	case "${item}" in
	    sat*)
		file_dir='satellite/' file_pre='sat'
		types=( 'ir' 'ir_enh' 'sfc' 'vis' 'wv' )
		
		PS3="Type (${item})? " selects "${types[@]}"
		type=( "${SELECT[@]}" )
		for item2 in "${type[@]}"; do
		    regions=( ' ' 'at' 'cp' 'east' 'ecan' 'hem' 'mw'
			'ne' 'nw' 'se' 'sp' 'sw' 'us' 'wcan' 'west' )
		    case "${item2}" in
			ir) regions+=( 'rad' ) ;;
			ir_enh) regions+=( 'namer' ) ;;
			sfc) regions=( 'map' ) ;;
			vis) ;;
			wv) ;;
		    esac
		    
		    PS3="Region (${item2})? " selects "${regions[@]}"
		    region=( "${SELECT[@]}" )
		    for item3 in "${region[@]}"; do
			unset count file_con
			case "${item3}" in
			    ' ') limit=5 count=1 item3='' ;;
			    *) limit=12 item3="_${item3}" ;;
			esac
			while (( count <= limit )); do
			    ext="${count:+-${count}}${file_ext}"
			    urls+=( "${file_dir}${url_pre}${file_pre}_${item2}${item3}${ext}" )
			    (( count++ ))
			done
		    done
		done
		;;
	    upper*)
		file_dir='upper_air/' file_pre=''
		types=( 'sfc' 'ua' 'con' 'nhem' 'shem' 'vect' )
		
		PS3="Type (${item})? " selects "${types[@]}"
		type=( "${SELECT[@]}" )
		for item2 in "${type[@]}"; do
		    case "${item2}" in
			sfc) regions=( 'ak' 'at' 'carib' 'cp' 'ecan' 'mex'
			    'mw' 'ncan' 'ne' 'nw' 'se' 'sp' 'sw' 'wcan' ) ;;
			ua) regions=( '100' '150' '200' '250' '300' '400'
				'500' '700' '850' '925' '3000ft' '6000ft'
				'9000ft' '12000ft' '18000ft' '24000ft'
				'30000ft' '36000ft' ) ;;
			con) regions=( '300s' '500h' '850t' 'cape' 'hel'
				'ki' 'lift' 'prec' 'thick' 'tt' ) ;;
			nhem) regions=( '300' '500p' '850' ) ;;
			shem) regions=( '500p' ) ;;
			vect) regions=( '300' ) ;;
		    esac
		    
		    PS3="Region (${item2})? " selects "${regions[@]}"
		    region=( "${SELECT[@]}" )
		    case "${item2}" in
			con|nhem|shem|vect) item2="ua_${item2}" ;;
		    esac
		    for item3 in "${region[@]}"; do
			unset count file_con
			limit=6 
			while (( count <= limit )); do
			    ext="${count:+-${count}}${file_ext}"
			    urls+=( "${file_dir}${url_pre}${file_pre}${item2}_${item3}${ext}" )
			    (( count++ ))
			done
		    done
		done
		;;
	    surf*)
		file_dir='surface/' file_pre=''
		types=( 'prec' 'sfc' 'sfc_con' 'sfcmap' 'snow' 'temp' )
		
		PS3="Type (${item})? " selects "${types[@]}"
		type=( "${SELECT[@]}" )
		for item2 in "${type[@]}"; do
		    case "${item2}" in
			prec) regions=( 'day' ) ;;
			sfc) regions=( 'ak' 'at' 'carib' 'cp' 'depict'
				'ecan' 'front' 'map' 'mex' 'mw' 'ncan'
				'ne' 'nw' 'se' 'sp' 'sw' 'us' 'wcan' ) ;;
			sfc_con) regions=( '3pres' '24temp' 'dewp' 'heat'
				'pres' 'qconv' 'stream' 'temp' 'wchill'
				'wspd' ) ;;
			sfcmap) regions=( '240_675' '240_750' '240_825' '240_900'
				'240_975' '240_1050' '240_1125' '240_1200'
				'280_675' '280_750' '280_825' '280_900'
				'280_975' '280_1050' '280_1125' '280_1200'
				'320_675' '320_750' '320_825' '320_900'
				'320_975' '320_1050' '320_1125' '320_1200'
				'360_675' '360_750' '360_825' '360_900'
				'360_975' '360_1050' '360_1125' '360_1200'
				'400_675' '400_750' '400_825' '400_900'
				'400_975' '400_1050' '400_1125' '400_1200'
				'440_675' '440_750' '440_825' '440_900'
				'440_975' '440_1050' '440_1125' '440_1200'
				'480_675' '480_750' '480_825' '480_900'
				'480_975' '480_1050' '480_1125' '480_1200'
				'at' 'nola' 'se' 'sepa' 'talleyb' 'us' ) ;;
			snow) regions=( 'cover' ) ;;
			temp) regions=( 'con_hi' 'con_lo' 'hi' 'lo' ) ;;
		    esac
		    
		    PS3="Region (${item2})? " selects "${regions[@]}"
		    region=( "${SELECT[@]}" )
		    case "${item2}" in
			prec|snow|temp) limit=6 ;;
			*) limit=12 ;;
		    esac
		    for item3 in "${region[@]}"; do
			unset count
			while (( count <= limit )); do
			    ext="${count:+-${count}}${file_ext}"
			    urls+=( "${file_dir}${url_pre}${file_pre}${item2}_${item3}${ext}" )
			    (( count++ ))
			done
		    done
		done
		;;
	    rad*)
		file_dir='radar/' file_pre=''
		types=( 'rad' 'wrad' )
		
		PS3="Type (${item})? " selects "${types[@]}"
		type=( "${SELECT[@]}" )
		for item2 in "${type[@]}"; do
		    case "${item2}" in
			rad) regions=( 'at' 'cp' 'ind' 'mw' 'ne' 'nw' 'se'
				'sepa' 'sp' 'sw' 'us' ) ;;
			wrad) regions=( 'at' 'cp' 'in' 'ind' 'japan' 'mw'
				'ne' 'nola' 'nw' 'nw_pac' 'nwpac' 'se'
				'sepa' 'sp' 'sw' 'talley' 'tallyb' 'us') ;;
		    esac
		    
		    PS3="Region (${item2})? " selects "${regions[@]}"
		    region=( "${SELECT[@]}" )
		    for item3 in "${region[@]}"; do
			unset count
			limit=12
			while (( count <= limit )); do
			    ext="${count:+-${count}}${file_ext}"
			    urls+=( "${file_dir}${url_pre}${file_pre}${item2}_${item3}${ext}" )
			    (( count++ ))
			done
		    done
		done
		;;
	esac
    done
    
    if [[ ${urls} ]]; then
    # Fetch URLs.
	print_warn 'Retriving'
	{ $(exe wget) -P "${dir_out}" "${urls[@]/#/${url_host}}" & } 2>/dev/null
	wait_exe
	print_true ' Done\n'
	
    # Merge files into loop sequence.
	print_warn 'Generating:\n'
	for item in ${dir_out}!(*-+([[:digit:]]|animate)*)${file_ext}; do
	# If not retrieved, skip merge.
	    unset pass
	    items=( "${urls[@]##*/}" )
	    for item2 in $(_uniq "${items[@]%%?(-[[:digit:]]*)${file_ext}}"); do
		[[ ${item##*/} == ${item2}${file_ext} ]] && (( pass++ ))
	    done
	    (( pass )) || continue
	    
	    item_out="${item%${file_ext}}-animate${file_ext}"
	    rm -f "${item_out}"
	    
	    items=( ${item%${file_ext}}* )
	    [[ ${items} ]] || continue
	    items=( $(reverse $(printf '%s\n' "${items[@]}" |
			$(exe sort) -n -t'-' -k2)) )
	    printf '%s' "${item_out#${dir_out}}"
	    { $(exe gifsicle) --delay=${delay} --loop "${items[@]}" \
		>"${item_out}" & } 2>/dev/null
	    wait_exe
	    printf '\n'
	done
    fi
    
    # Prompt for arguments.
    items=( ${dir_out}*-animate* )
    selects "${items[@]#${dir_out}}"
    for item in "${SELECT[@]/#/${dir_out}}"; do
	$(exe gifview) --animate "${item}"
    done
}

request.links() {
    true
}
request.links_weather() {
    items=(
	'http://www.hpc.ncep.noaa.gov/noaa/noaa.gif'
	'http://www.hpc.ncep.noaa.gov/sfc/lrgnamsfcwbg.gif'
	'http://weather.unisys.com/radar/rcm_rad.php?inv=0&t=cur&region=mw'
	'http://weather.unisys.com/satellite/sat_ir_enh_mw_loop.gif'
	'http://weather.unisys.com/radar/rcm_rad.php?image=rad&inv=0&t=l&region=mw'
	)
    items_prev_gif=(
	'http://www.hpc.ncep.noaa.gov/dailywxmap/dwm_stnplot_20121103.html'
	'http://www.hpc.ncep.noaa.gov/dailywxmap/dwm_minmax_20121103.html'
	'http://www.hpc.ncep.noaa.gov/dailywxmap/dwm_500ht_20121103.html'
	'http://www.hpc.ncep.noaa.gov/dailywxmap/dwm_prcn_20121103.html'
	)
    items_gif_anim=(
	'http://www.hpc.ncep.noaa.gov/basicwx/92f.gif'
	'http://www.hpc.ncep.noaa.gov/basicwx/94f.gif'
	'http://www.hpc.ncep.noaa.gov/basicwx/96f.gif'
	'http://www.hpc.ncep.noaa.gov/basicwx/98f.gif'
	'http://www.hpc.ncep.noaa.gov/medr/9jh.gif'
	'http://www.hpc.ncep.noaa.gov/medr/9kh.gif'
	'http://www.hpc.ncep.noaa.gov/medr/9lh.gif'
	'http://www.hpc.ncep.noaa.gov/medr/9mh.gif'
	'http://www.hpc.ncep.noaa.gov/qpf/94qwbg.gif'
	'http://www.hpc.ncep.noaa.gov/qpf/98qwbg.gif'
	'http://www.hpc.ncep.noaa.gov/qpf/99qwbg.gif'
	)
    pushd "${DIR_TMP_DATA}"
    $(exe wget) "${items[@]}"
    $(exe wget) -nd -A gif -r "${items_prev_gif[@]}"
    popd
}

request.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
                t|tts) ACTION='tts' ;;
                r|cert) ACTION='cert' ;;
                p|ping) ACTION='ping' ;;
                g|gmail) ACTION='gmail' ;;
                c|clifu) ACTION='clifu' ;;
                w|weather) ACTION='weather' ;;
                V|verbose) (( VERBOSE++ )) ;;
            esac
        done
    done
    if [[ ${ACTION} ]]; then
        if [[ ${ACTION} == tts ]]; then
            [[ ${ITEMS} ]]
        else
            (( 1 ))
        fi
    else
        (( 0 ))
    fi
}

request.shoutcast() {
false
    #$(exe shoutcast-search) 
    #--limit=50
    #--sort=l
    #--bitrate=">96"
    #--format="[%s](%p)%bZ_%t %u" 
}

request.network() {
    [[ ${DEVICE_NETWORK} ]] && $(exe ping) -qc 1 "${IP_DNS}" >/dev/null
}

request.test() {
    source 'test'

    request.network || { print_false 'Network!\n'; return 1; }

    test.true request.ping
    test.false request.ping bar.foo  # DNS may interfere with redirect.

    # request.ping
}

source 'device_network'
source 'identity'
request.variables
return 2>/dev/null

_ARGS='(p)ing (g)mail (c)lifu (w)eather ce(r)t'
_DESC="
    Ping, ping ${IP_DNS}, argument of dns or identity_host."

request.arguments "${@:-?}" || exit
request.ping || exit

case "${ACTION}" in
    gmail) request.gmail ;;
    clifu) request.clifu ;;
    weather) request.weather ;;
    tts) request.tts "${ITEMS[@]}" ;;
    cert) request.cert "${ITEMS[@]}" ;;
    ping) request.ping "${ITEMS[@]}" ;;
esac
