#!/usr/bin/env bash

disk_syslinux_mbr() {
    local pathSyslinux='/usr/lib/syslinux/bios'
    [[ -d ${pathSyslinux} ]] || { printf 'Fix Syslinux path\n'; return 1; }
    local device="${1%[[:digit:]]}"
    [[ -b ${device} ]] || { printf 'Fix device path\n'; return 1; }
    local mbr="$(sudo blkid -s PTTYPE -o value "${device}")"
    [[ ${mbr} == +(dos|gpt) ]] || { printf 'Invalid MBR type\n'; return 1; }
    
    case "${mbr}" in
	dos) pathSyslinuxMbr="${pathSyslinux}/mbr.bin" ;;
	gpt) pathSyslinuxMbr="${pathSyslinux}/gptmbr.bin" ;;
    esac
    [[ -f ${pathSyslinuxMbr} ]] || {
	printf 'Invalid MBR path, %s\n' "${pathSyslinuxMbr}"; return 1
    }
    read -s -n 1 -p "dd bs=440 count=1 conv=notrunc if=${pathSyslinuxMbr} of=${device}; OK?"
    printf '\n'
    case "${REPLY}" in
	y|Y) ;;
	*) return 1 ;;
    esac
    sudo dd bs=440 count=1 conv=notrunc \
	if="${pathSyslinuxMbr}" of="${device}"
}
###

disk.partition.zero() {
    local _ARGS='(rand) (digit)'
    local _DESC="
        Zero disk.
        Digit, print interval."
    local int='1' line line2 pid item file='zero'

    while [[ ${1} == +(-)* ]]; do
        item="${1##*+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            rand) file='urandom' ;;
            +([[:digit:]])) int="${item}" ;;
        esac
        shift
    done

    for item in "${@}"; do
        item="${DIR_DEV}/${item}"
        [[ -e ${item} ]] || continue

        read_true "Zero: ${item}" || continue

        { $(exe dd) bs=1M if="${DIR_DEV}/${file}" of="${item}" 2>&1 & echo "${!}"; } |
            { read pid
            trap '{ kill ${pid}; trap - EXIT; }' EXIT
            while [[ -e ${DIR_PROC}/${pid:-0} ]]; do
                $(exe kill) -USR1 "${pid}"
                read line2; read line2; read line
                print_erase "${line}"
                printf '%s' "${line}"
                sleep ${int}
            done
            trap - EXIT; }
        printf '\n'
    done
}

disk.partition.table() {
    local _ARGS='<disks>'
    local _DESC="
        Write GPT tables for DISKS.

        GPT, partition table; default."
    local item action='gpt'

    while [[ ${1} == +(-)* ]]; do
        item="${1##*+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
        esac
        shift
    done

    for item in "${@}"; do
        item="${DIR_DEV}/${item}"
        [[ -e ${item} ]] || { print_false 'Device: %s\n' "${item}"; continue; }

        case "${action}" in
            gpt)
                print 'Boot: ef02. Data: 8300\n'
                sudo $(exe sgdisk) --print --verify "${item}" || continue
                sudo $(exe sgdisk) --zap-all --verify "${item}" || continue
                sudo $(exe sgdisk) "${item}" || continue
                read_false 'Boot' &&
                    sudo $(exe sgdisk) --attributes=1:set:2 "${item}" || continue
                sudo $(exe sgdisk) --randomize-guids "${item}"
                sudo $(exe sgdisk) --print "${item}"
            ;;
        esac

        # Kernel Partition Hash Table
        sudo $(exe blockdev) --rereadpt "${item%+([[:digit:]]*)}" || return
        sleep 1
    done
}

disk.partition.format() {
    local _ARGS='(data) (boot) <parts>'
    local _DESC="
        Format partitions of PARTS.

        Boot, EXT4 without journaling.
        Data, BTRFS."
    local item action
    local labelData='data' labelBoot='boot'

    while [[ ${1} == +(-)* ]]; do
        item="${1##*+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            ext2) action='ext2' ;;
            ext4) action='ext4' ;;
            boot) action='boot' ;;
            data) action='btrfs' ;;
            btrfs) action='btrfs' ;;
        esac
        shift
    done

    for item in "${@}"; do
        item="${DIR_DEV}/${item}"
        [[ -d ${item} ]] || { print_false 'Device: %s' "${item}"; continue; }

        case "${action}" in
            boot)
                $(exe mkfs.ext4) -O ^has_journal \
                    -L "${labelBoot}" "${item}" || continue
            ;;
            ext4)
                $(exe mkfs.ext4) \
                    -L "${labelBoot}" "${item}" || continue
            ;;
            btrfs|data)
                $(exe mkfs.btrfs) \
                    --label "${labelData}" "${item}" || continue
            ;;
	    vfat)
		# fdisk -H (32|64) -S 32 "${item}"
		# (dataByte/clusterByte), if not even
		# ((dataByte/clusterByte)-((round(dataByte/clusterByte))*clusterByte)/sectorByte)+sectorReserve
		$(exe mkfs.vfat) \
		    -F 32 -R 6 -s 64 -S 1024
		"${item}" || continue
		;;
        esac
    done

}
disk.partition.format_valid() {
    case "${1}" in
        btrfs) return ;;
        *) 
            print_false 'Not Supported: %s\n' "${1}"
            return 1
        ;;
    esac
}

disk.partition.mount_disk() {
    local dev

    for dev in "/dev/${1}"{1,2}; do
        [[ ${dev} == *[[:digit:]] ]] || continue
        sudo $(exe mount) "${dev}" "/mnt/${dev##*/}" || return
    done
}
disk.partition.umount_disk() {
    local dev

    for dev in "/dev/${1}"*; do
        [[ ${dev} == *[[:digit:]] ]] || continue
        sudo $(exe umount) "${dev}" &>/dev/null
    done
}

disk.partition.bootloader() {
    local file
    DIR_SYSLINUX="/usr/local/share/syslinux"
    SYSLINUX_EXT=( 'chain.c32' 'config.c32' 'dmitest.c32' 'elf.c32' 'ethersel.c32' 'gptmbr.bin'
                'hdt.c32' 'isolinux.bin' 'kbdmap.c32' 'linux.c32' 'memdisk' 'meminfo.c32'
                'menu.c32' 'poweroff.com' 'pxechain.com' 'reboot.c32' 'vesamenu.c32' )

    case "${ACTION_BOOT}" in
        extlinux)
            tce-load -i 'syslinux' &>/dev/null

            mkdir -p "/mnt/${1}1/boot/extlinux"
            if ! exist_dir "${DIR_SYSLINUX}"; then
                print_false 'Missing: %s\n' "${DIR_SYSLINUX}"
                return 1
            else
                if dd bs=440 conv=notrunc count=1 if="${DIR_SYSLINUX}/gptmbr.bin" of="/dev/${1}"; then
                    print_true 'Syslinux GPT_MBR.\n'
                else
                    print_false 'Syslinux GPT_MBR.\n'
                    return 1
                fi
                for file in "${SYSLINUX_EXT[@]}"; do
                    if ! exist "${DIR_SYSLINUX}/${file}"; then
                        print_false 'Missing: %s\n' "${DIR_SYSLINUX}/${file}"
                        (( rtn++ ))
                    else
                        $(exe cp) "${DIR_SYSLINUX}/${file}" "/mnt/${item}1/boot/extlinux"
                    fi
                done
                file='/usr/local/share/pci.ids.gz'
                if ! exist "${file}"; then
                    print_false 'Missing: %s\n' "${file}"
                    (( rtn++ ))
                else
                    $(exe gunzip) -cd "${file}" >"/mnt/${item}1/boot/extlinux/pci.ids"
                fi
            fi
            $(exe extlinux) --install "/mnt/${1}1/boot/extlinux"
        ;;
    esac
    ! (( rtn ))
}
disk.partition.bootloader_valid() {
    case "${1}" in
        extlinux) return ;;
        *)
            print_false 'Not Supported: %s\n' "${1}"
            return 1
        ;;
    esac
}

disk.partition.arguments() {
    local item
    unset ACTION ITEMS ARGS DIGIT

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                z|zero) ACTION='zero' ;;
                    R|rand) ARGS='-rand' ;;
                t|table) ACTION='table' ;;
                f|format) ACTION='format' ;;
                +([[:digit:]])) DIGIT="${ARG[@]}" ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

