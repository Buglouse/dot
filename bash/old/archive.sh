#!/bin/env bash

archive_home() {
    local archive_tmp="${DIR_TMP_CACHE}/${FILE_ARCHIVE##*/}"

    if tar -ac \
	-C "${HOME}" \
	-f "${archive_tmp}" \
	-X "${FILE_ARCHIVE_EXCLUDE}" \
	-T "${FILE_ARCHIVE_INCLUDE}"; then
	mv "${archive_tmp}" "${FILE_ARCHIVE}"
	printf '%b%s:%d%b\n' "${GREEN}" "${FILE_ARCHIVE}" $(stat -c %s "${FILE_ARCHIVE}") "${NORMAL}"
    else
	printf '%b%s%b\n' "${RED}" "${FILE_ARCHIVE}" "${NORMAL}"
    fi
}

case "${1}" in
    home) archive_home ;;
esac
