#!/usr/bin/env bash
source 'bash.sh'

identity.variables() {
    identity.unset

    export FILE_SERVICE 'service.cvs.gpg'  #:order:0
    export FILE_IDENTITY 'identity.cvs.gpg'  #:order:0

    export PATH_SERVICE "${DIR_AUTH}/${FILE_SERVICE}"
    export PATH_IDENTITY "${DIR_AUTH}/${FILE_IDENTITY}"
    export PATH_SERVICE_TMP "${DIR_AUTH_TMP}/${FILE_SERVICE%.*}"
    export PATH_IDENTITY_TMP "${DIR_AUTH_TMP}/${FILE_IDENTITY%.*}"
}

identity.edit() {
    local item size

    for item in "${@:-${PATH_IDENTITY_TMP}}"; do
        [[ -e ${item} ]] || continue
        size=$($(exe stat) -c '%s' "${item}")

        ${EDITOR} "${item}"
        (( $($(exe stat) -c '%s' "${item}" ) != size )) && crypt.encrypt "${item}"
    done
}
identity.edit_host() { identity.edit "${PATH_SERVICE_TMP}"; }

identity.append() {
    local item item2 address username password tag IFS
    local pathT="${1:-${PATH_IDENTITY_TMP}}"
    local columns=( 'address' 'username' 'password' 'tag' )
    # address;address2,user;user2,pass;pass2,tag;tag2 

    [[ -d ${pathT} ]] || return
    size=$($(exe stat) -c '%s' "${pathT}")

    while (( 1 )); do
        for item in "${columns[@]}"; do
            unset "${item}"
            until [[ ${!item} && ${!item} != *,* ]]; do
                _read -a "${item^}: " "${item}"
            done
        done
        printf -- '--Verify--\n'
        IFS=';'; for item in "${columns[@]}"; do
            printf -v item2 '%s[*]' "${item}"
            until read_true "${item^}=${!item2}"; do
                read_clear; unset IFS
                printf -v item2 '%s[*]' "${item}"
                until [[ ${!item} && ${!item} != *,* ]]; do
                    read -e -p "${item^}: " -i "${!item2}" -a "${item}"
                done
                IFS=';'; printf -v item2 '%s[*]' "${item}"
            done
            printf '%s,' "${!item2}" >>"${pathT}"
        done; unset IFS
        printf '\n' >>"${pathT}"

        read_false 'Break' && break
    done
    (( $($(exe stat) -c '%s' "${pathT}" ) != size )) && crypt.encrypt "${pathT}"
}
identity.append_host() { identity.append "${PATH_SERVICE_TMP}"; }

identity.define() {
    #[[ -t 1 ]] || return
    [[ -p /dev/fd/0 ]] && return
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _ARGS='<identities>'
    local _DESC="
        Extract identity information.
        Identities, parse and process.

        #--Entries--#
        <address,user;user2,pass;pass2,tag;tag2,>
        Address, export as valid variable.
        If multiple user and single pass, export array of all user."
    local _ item itemT item2 item3 items int row col rtn re reS='#' IFS PS3
    local addr addrT user userX pass passX tag pathT

    # Determin valid files.
    unset pathT items
    for item in "${@}"; do
        if [[ ${item} == +(*/*) ]]; then
            if [[ -e ${item} ]]; then
                pathT+=( "${item}" )
            else
                print_false 'Invalid! '
                print_warn '%s\n' "${item}"
                continue
            fi
        else
            items+=( "${item}" )
        fi
    done
    [[ ${pathT} ]] || pathT="${PATH_IDENTITY_TMP}"
    [[ -e ${pathT} ]] || return

    for item in "${items[@]}"; do
        unset col row int

        {  # Parse
        # First loop match addr, then tag, then expr.
        for item2 in addr tag addr; do
            (( int++ ))

            IFS=', '
            while read addr user pass tag _; do  # Elements
                [[ ${addr} == +(${reS})* ]] && continue

                IFS=';'
                for item3 in ${!item2}; do  # item;item2
                    if (( int >= 3 )); then  # Match all.
                        re="*+(${item})*"
                    elif [[ ${item2} == addr ]]; then  # Match explicit.
                        re="+(${item})"
                    else  # Match explicit.
                        re="+(${item})"
                    fi
                    [[ ${item3} == ${re} ]] || continue

                    row+=( "${addr},${user},${pass}" ) col="${item2}"
                done; IFS=','
            done <<<"$($(exe grep) "\(${item}\)" "${pathT[@]}")"
            unset IFS
            itemT="$($(exe grep) -l "\(${item}\)" "${pathT[@]}")"

            [[ ${row} ]] && break
        done
        [[ ${row} ]] || continue
        }

        {  # Select
        #IFS=$'\n'; row=( $(unset IFS; _uniq "${row[@]}") ); unset IFS
        # Single row.
        PS3='Host: ' selects -1 ${row[@]%%,*} || continue
        row="${row[${REPLY}-1]}" addr="${SELECT}"
        [[ ${row} ]] || continue
        }

        {  # Export
        case "${itemT##*/}" in  # Custom
            service*) userX='HOST' passX='PORT' ;;
            identity*|*) userX='USER' passX='PASS' ;;
        esac

        # First element, replace '.'.
        item="${row%%,*}" item="${item//[.-]/_}"
        # Exported variable.
        userX="${item^^}_${userX}" passX="${item^^}_${passX}"
        # Already defined.
        [[ ${!userX} && ${!passX} ]] && continue
        }

        {  # Parse (user pass)
        row="${row#*,}"  # user,pass
        IFS=';'
            shopt -u extglob
        int=( ${row#*,} )  # pass;pass2
            shopt -s extglob
        if (( ${#int[@]} > 1 )); then  # Based on pass.
            PS3='User: ' selects -1 ${row%,*} || continue
            user="${SELECT}" row=( ${row#*,} ) pass="${row[${REPLY}-1]}"
            # Single pass element.
            [[ ${pass} ]] || pass="${row}"
        else
            user=( ${row%,*} ) pass="${row#*,}" #:fix: expansion
        fi
        unset IFS
        }

        if [[ ${user} && ${pass} ]]; then
            export "${userX}" "${user[@]}"
            export "${passX}" "${pass[@]}"
            if [[ -t 1 ]]; then
                print '%s: %s\n' "${addr}" "${user[*]}"
                printf '%s' "${pass}" |${CMD_XCLIP} -ib
                printf '%s' "${pass}" |${CMD_XCLIP} -ip
            else
                (( 1 ))
            fi
        fi
    done
}
identity.define_host() { identity.define "${PATH_SERVICE_TMP}" "${@}"; }

identity.unset() {
    local var re='USER|PASS|HOST|PORT'

    while read var; do
        [[ ${var} == *_+(${re}) ]] && unset "${var}"
    done <<<"$(compgen -v)"
}

identity.host() {
    local _ARGS='<host>'
    local _DESC="
        Host, pass id_service lookup to network.hosts_write()."
    local item items itemT

    for item in "${@}"; do
        identity.define_host "${item}" || continue
        itemT="${item^^}_HOST"
        [[ ${!itemT} ]] && items+=( "${!itemT}:${item}" )
    done
    [[ ${items} ]] || return

    source 'network' && network.host_write "${items[@]}"
}

identity.contains() {
    local re item items pathT IFS

    for item in "${@}"; do
        if [[ ${item} == +(*/*) ]]; then
            if [[ -e ${item} ]]; then
                pathT+=( "${item}" )
            else
                print_false 'Invalid! '
                print_warn '%s\n' "${item}"
                continue
            fi
        else
            items+=( "${item}" )
        fi
    done
    [[ ${pathT} ]] || pathT="${PATH_IDENTITY_TMP}"
    [[ -e ${pathT} ]] || return
    [[ ${items} ]] || return
    IFS='|'; re="${items[*]}"; unset IFS

    $(exe grep) -q "\(${re}\)" "${pathT[@]}"
}
identity.contains_host() { identity.contains "${PATH_SERVICE_TMP}" "${@}"; }

identity.remove() {
    local item items=( ${DIR_AUTH_TMP}/*+(prv|cvs)!(bfe|gpg)* )

    [[ ${items} ]] || return
    print_false '#--Removing--#\n'
    for item in "${items[@]}"; do
        print_warn '    %s\n' "${item}"
    done
    read_false 'Continue' || return

    for item in "${items[@]}"; do
        _rm "${item}"
    done
}

identity.extract() { crypt.decrypt ${DIR_AUTH}/*+(cvs|${HOSTNAME})*; }

identity.print() {
    local re item items col col2 IFS

    unset re items
    for item in "${@}"; do
        if [[ -e ${item} ]]; then
            items+=( "${item}" )
        else
            re+=( "${item}" )
        fi
    done
    (( ${#items[@]} )) ||
        { selects -1 ${DIR_AUTH_TMP}/+(*.cvs) && items=( "${SELECT[@]}" ); }
    IFS='|'; re="${re[*]}"; unset IFS

    for item in "${items[@]}"; do
        [[ -e ${item} ]] || continue

        while read col col2; do
            [[ ${col} == +(#*) ]] && continue
            [[ ${col} == +(*${re}*) ]] || continue

            printf '%-40s %s\n' "${col}" "${col2}"
        done <<<"$($(exe sed) -e 's/\([^,]*\),[^,]*,\([^,]*\),.*/\1 \2/' "${item}" |
            $(exe sort) -n -k2)"
    done
}

identity.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
                E|edit) ACTION='edit' ;;
                A|append) ACTION='append' ;;
                d|define) ACTION='define' ;;
            esac
        done
    done

    [[ ${ACTION} ]]
}

identity.variables
source 'crypt'
return 2>/dev/null

_ARGS='(E)dit (A)ppend (d)efine'
_DESC="Identity Crypt.
    Append, insert rows.
    Define, request colums of rows. Print all user, copy first pass in to ${CMD_XCLIP}."

identity.arguments "${@:-?}" || exit

case "${ACTION}" in
    edit) identity.edit "${ITEMS[@]}" ;;
    define) identity.define "${ITEMS[@]}" ;;
    append) identity.append "${ITEMS[@]}" ;;
esac
