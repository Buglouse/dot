#!/usr/bin/env bash
source 'bash.sh'

device.input.variables() {
    local item items name

    export SYS_CLASS_INPUT '/sys/class/input'

    items=( ${SYS_CLASS_INPUT}/* )
    for item in "${items[@]/%//uevent}"; do
        [[ -e ${item} ]] || continue

        name=$($(exe sed) -ne 's/.*NAME=\(.*\)/\1/p' "${item}")
        [[ ${name,,} == *touchpad* ]] && DEVICE_INPUT_TOUCHPAD+=( "${item%/*}" )
    done

    export DEVICE_INPUT_TOUCHPAD

    [[ ${DEVICE_INPUT_TOUCHPAD} ]]
}

device.input.touchpad.init() {
    [[ ${DEVICE_INPUT_TOUCHPAD} ]] || return
    $(exe synclient) \
        CircularScrolling=1 CircScrollTrigger=5 \
        TapButton1=1 TapButton2=2 TapButton3=3 2>/dev/null
}

device.input.arguments() {
    local item 
    unset ACTION ITEMS STATE

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

device.input.variables
device.input.touchpad.init
return 2>/dev/null

_ARGS=''
_DESC="Input Devices.
    Touchpad."

device.input.arguments "${@:-?}" || exit

case "${ACTION}" in
    *) ;;
esac
