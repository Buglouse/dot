#!/usr/bin/env bash
source 'bash.sh'

shellfm.variables() {
    export DIR_CACHE_SHELLFM "${DIR_TMP_CACHE}/shellfm"
    export CMD_SOCK_SHELLFM "socat - TCP4:${HOSTNAME}:${PORT_SHELLFM}"
    export PID_SHELLFM "${DIR_TMP_PID}/shellfm.pid"
    export LIMIT_CACHE_SHELLFM_DIR '46080' # Bytes
}

shellfm.parse_track() {
	local item title
	unset ARTIST ALBUM TRACK LENGTH TRACK

	for item in "${@}"; do
		title+=( "$(printf '%s' "${item}" |sed \
			-e "s/'//g" -e "s/[^[:alnum:]_-]/_/g" -e "s/_-_/-/g" \
			-e "s/__\{1,\}/_/g" -e "s/^_//" -e "s/_$//")" )
	done
	read ARTIST ALBUM TRACK LENGTH <<< "${title[@]}"
	TRACK="${ARTIST};${ALBUM};${TRACK};${LENGTH}"
}

shellfm.play() {
    local saveDir="${2:-${DIR_CACHE_SHELLFM}}"

    mkdir "${saveDir}"

	{ $(exe tee) "${saveDir}/${1}.tmp" |$(exe madplay) --very-quiet -; } &>/dev/null
}

shellfm.exit() {
    local item
	local items=(
        $($(exe stat) -c '%Y,%n' ${DIR_CACHE_SHELLFM}/* |$(exe sort) -n) )
    local itemD=( $($(exe du) -c "${DIR_CACHE_SHELLFM}") )

    [[ ${items} ]] &&
        until (( ${itemD%% *} <= LIMIT_CACHE_SHELLFM_DIR )); do
            item="${items[${#items[@]}-1]}"
            _rm "${item#*,}"
            items=( ${items[@]::${#items[@]}-1} )
            itemD=( $($(exe du) -c "${DIR_CACHE_SHELLFM}") )
        done
}

#save() {
#	for file in "${tmpDir}/"*"$track"*.tmp; do
#		seconds=$(mp3info -p %S "$file"); (( seconds = seconds + 15 ))
#		if (( $seconds > $length )); then
#			[[ -d ${songDir}/${artist}/$album ]] || mkdir -p "${songDir}/${artist}/$album"
#			id3v2 -a "$id3Artist" -t "$id3Track" -A "$id3Album" "$file" &&
#				mv "$file" "${songDir}/${artist}/${album}/${artist}-${track}.mp3" &&
#					find "${songDir}" -type f -name '*.mp3' -printf '%P\n' \
#						|sort >"${songDir}/$songList"
#		else
#			printf 'Removed: %s-%s(%s)\n' "$artist" "$track" "$(( $seconds / $length ))"
#		fi
#		[[ -e $file ]] && rm "$file" 2>/dev/null
#	done
#}
###

shellfm.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
                dir) ACTION='save' DIR_DATA_SHELLFM="${ARG_OPT}" ;;
                C|cache) ACTION='cache' ;;
                artist|album|track) printf '%s-tags' "${1}" |${CMD_SOCK_SHELLFM} ;;
                i|info) printf 'info %b[%b%%a%b-%b%%t%b (%b%%R%b/%b%%d%b) %b%%V%b]' \
                    "${WHITE}" "${GREEN}" "${WHITE}" "${CYAN}" "${WHITE}" \
                    "${RED}" "${WHITE}" "${MAGENTA}" "${WHITE}" \
                    "${YELLOW}" "${WHITE}" |${CMD_SOCK_SHELLFM}
                ;;
                b|ban) printf 'ban\n' |${CMD_SOCK_SHELLFM} ;;
                l|love) printf 'love\n' |${CMD_SOCK_SHELLFM} ;;
                s|skip) printf 'skip\n' |${CMD_SOCK_SHELLFM} ;;
                p|pause) printf 'pause\n' |${CMD_SOCK_SHELLFM} ;;
                Q|quit) printf 'quit\n' |${CMD_SOCK_SHELLFM} ;;
            esac
        done
    done
    [[ ${ACTION} || ${ITEMS} ]]
}

shellfm.variables
return 2>/dev/null

shellfm.arguments "${@:-?}" || exit
parse.pid --lock "${PID_SHELLFM}" || exit
trap '{ shellfm.exit; parse.pid --term "${PID_SHELLFM}"; kill -term "${$}" }' EXIT
shellfm.parse_track "${ITEMS[@]}"

case "${ACTION}" in
    save) "${DIR_DATA_SHELLFM}" ;;
    cache) shellfm.play "${TRACK}" ;;
esac
