#!/usr/bin/env bash
source bash.sh

debian.variables() {
    [[ -e /etc/debian_version ]] || return
    export DEBIAN_SERVERS 'http://http.us.debian.org'  #:order:0
    export VERSION_DEBIAN '/etc/debian_version'  #:order:0

    export VERSION_DEBIAN $(<"${VERSION_DEBIAN}")

    export DIR_DPKG "${DIR_VAR_LIB}/dpkg"  #:order:1

    export DIR_APT "${DIR_APT:-${HOME}/.apt}" #:order:1
    export DIR_ETC_CONF "${DIR_ETC}/default"
    export DIR_APT_CACHE "${DIR_VAR_CACHE}/apt"
    export DIR_APT_CACHE_ARCHIVE "${DIR_VAR_CACHE}/apt/archives"
    export DIR_DPKG_INFO "${DIR_DPKG}/info"

    export FILE_APT_STATUS "${DIR_TMP_CACHE}/status.dpkg"

    export CONF_APT "${DIR_APT}/apt.conf"  #:order:2

    export LOG_DPKG "${DIR_TMP_LOG}/dpkg.log"

    exist_elem 'debian.execute' "${CMD_EXE[@]}" || CMD_EXE+=( 'debian.execute' )
    export CMD_EXE
    export CMD_MOD 'debian.module'
    export CMD_LOGIN 'debian.login'
    export CMD_BUSYBOX_LINK 'bzip2'

    export DEBIAN_SERVER "${DEBIAN_SERVERS}"

    #-----#
    export APT_CONFIG "${CONF_APT}"
}

debian.login() {
    debian.mksymlink
    parse.conf "${CONF_APT}"
    debian.update

    return
    items=( 'dist-upgrade' 'debian-keyring' 'debian-archive-keyring'
        'sudo' 'busybox' 'apt-file' 'ssh' "${EDITORS[@]}" )
    debian.execute "${items[@]}"
    adduser
    /etc/sudoers
    /etc/fstab
}

debian.mksymlink() {
    local item items=( "${CMD_BUSYBOX_LINK}" )

    for item in "${items[@]}"; do
        sudo $(exe cp) -fs $(type -P busybox) "/bin/${item}"
    done
}

debian.dot_root() {
    items=( ${DIR_ETC}/bash* "${DIR_ETC}/motd" )
    sudo $(exe rm) -rf "${items[@]}"

    return
    /etc/passwd
}

debian.mkapt() {
    local itemsF=( 'status' )
    #local itemsD=( 'lists' )

    for item in "${itemsF[@]}"; do $(exe touch) "${item}"; done
}
debian.mkdpkg() {
    local itemsF=( 'available' )
    local itemsD=( 'updates' )
}

#--Package--#

debian.execute() {
    local item items

    case "${1}" in  # Executable
        locate|updatedb) item=( "${1}.findutils" ) ;;
    esac
    item="${item:-${1}}"

    case "${item}" in  # Package
        updatedb) items=( 'locate' ) ;;
        rpc.+(nfsd|mountd)) items=( 'nfs-kernel-server' ) ;;
        *) items=( "${1}" ) ;;
    esac
    [[ ${items} ]] && debian.load "${items[@]}"
}
debian.module() {
    local item items

    for item in "${@}"; do
        case "${item}" in
            nfs[d]) items+=( 'nfs-common' ) ;;
        esac
    done
    debian.load "${items[@]}"
}

debian.find() { apt-cache search "${@}"; }
debian.locate() { apt-file search "${@}"; }
debian.update() { apt-file update; apt-get update --quiet; }
debian.upgrade() { apt-get dist-upgrade "${@}"; }

debian.load() { apt-get install "${@}"; }
debian.loaded() { apt-get pkgnames "${@}"; }
debian.remove() { apt-get purge "${@}"; }

debian.fetch() { apt-get source "${@}"; }

debian.help() { apt-get commands; }

debian.report() {
    local action

    read_false 'Package' && action='pkg'

    case "${action}" in
        pkg) out="$(apt-cache policy)" ;;
    esac
    printf 'Distro Release:\t\t %s\n' "${VERSION_DEBIAN}"
    printf 'Kernel:\t\t %s\n' $($(exe uname) -r)
    printf 'Architecture:\t\t %s\n' $($(exe uname) -m)
    printf '#--Report--#\n'
    printf '%s\n' "${out}"
}

debian.fix_dpkg_info() {
    local _DESC="
        List files of package."
    local item items

    items=( ${DIR_APT_CACHE_ARCHIVE}/dpkg_*
        ${DIR_APT_CACHE_ARCHIVE}/debconf_*
        ${DIR_APT_CACHE_ARCHIVE}/apt_*
        ${DIR_APT_CACHE_ARCHIVE}/apt-utils_* )
    selects "${items[@]}"
    [[ ${SELECT} ]] && sudo $(exe dpkg) --force-depends -i "${SELECT[@]}"

    debian.update

    unset items
    while read _ item _; do
        items+=( "${item}" )
    done <<<"$($(exe dpkg) -l |$(exe grep) '^ii')"
    debian.load --reinstall "${items[@]}"
}

debian.info() { $(exe dpkg) --info "${@}"; }
debian.list() { $(exe dpkg) --contents "${@}"; }
debian.archive() { $(exe dpkg) --build "${@}"; }
debian.extract() {
    local _ARGS='<packages> <directory>'
    local _DESC="
        Prompt for package if PACKAGES not specified.
        Extract in dir of package name if DIRECTORY not specified."
    local item items itemD

    (( ${#} )) ||
        { selects ${DIR_APT_CACHE_ARCHIVE}/* && items=( "${SELECT[@]}" ); }

    for item in "${items[@]:-${@}}"; do
        [[ -d ${@:${#}} ]] ||
            { itemD="${item##*/}" itemD="${DIR_TMP_DATA}/${itemD%.*}"
            $(exe mkdir) -p "${itemD}"; }

        $(exe dpkg) --extract "${item}" ${itemD}
    done
}

#-----#

wajig() { $(exe wajig) "${@}"; }
apt-get() { sudo $(exe apt-get) --force-yes -c="${CONF_APT}" "${@}"; }
apt-file() { $(exe apt-file) "${@}"; }
dpkg() {
    sudo $(exe dpkg) --abort-after 1 \
        --instdir=/ --admindir="${DIR_APT_CACHE}" \
        --log="${LOG_DPKG}" "${@}"
}

#-----#

debian.arguments() {
    local item 
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                E|exe*) ACTION='execute' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

debian.variables || return 2>/dev/null
return 2>/dev/null

_ARGS='(e)xecute'
_DESC="
    Debian configuration.
    #----#
    $(debian.help)"

[[ ${VERSION_DEBIAN} ]] || exit
debian.arguments "${@:-?}" || exit

case "${ACTION}" in
    execute) debian.execute "${ITEMS[@]}" ;;
esac
