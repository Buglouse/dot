#!/usr/bin/env bash
disk_ssd_test() {
    local item='/' items
    
    for item in "${@}"; do
	[[ -d /sys/block/${item} ]] || continue
	items+=( "/sys/block/${item}/queue/rotational" )
    done
    
    for item in "${items[@]}"; do
	grep -H . "${item}"
    done
}
