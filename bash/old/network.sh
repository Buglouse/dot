#!/usr/bin/env bash
source 'bash.sh'

network.variables() {
    local item itemL IFS

    # Print identification.

    if [[ ${1} ]]; then
        if [[ ${1} == 11 ]]; then
            items=( "${DEVICE_NETWORK_WLAN[@]}" )
        elif [[ ${1} == 3 ]]; then
            items=( "${DEVICE_NETWORK_ETH[@]}" )
        fi
    else
        items=( "${DEVICE_NETWORK_ETH[@]}" "${DEVICE_NETWORK_WLAN[@]}" )
    fi

    for item in "${items[@]}"; do
        itemL="$($(exe realpath) ${item%,*})"
        IFS='/'; itemL=( ${itemL} ); unset IFS

        printf '%s:%s\n' "${item#*,}" "${itemL[5]}"
    done
    selects -1 "${items[@]##*,}"
    [[ ${SELECT} ]] && export NETWORK_DEVICE_DEFAULT "${SELECT}"
}

network.dns() {
    local _ARGS='[address]'
    local _DESC="
        Local resolve before Internet.
        
        Address, use ADDRESS for resolve."
    local _ item itemX itemsR itemsL gate
    sudo chmod 666 "${FILE_DNS}"
    trap '{ sudo chmod 644 "${FILE_DNS}"; }' RETURN

    # Local
    while read _ _ gate _; do
        [[ ${gate} == *+(.)* ]] || continue
        itemsL+=( "${gate}" )
    done <<<"$($(exe ip) route show table main)"
    [[ ${itemsL} ]] || return

    # Write
    for item in 8.8.4.4 "${itemsL[@]}"; do
        [[ ${item} ]] || continue
        $(exe grep) -q "${item}" "${FILE_DNS}" && continue

        printf 'nameserver\t%s\n' "${item}" >>"${FILE_DNS}"
    done

    # Internet
    itemX=$(request.address)
    for item in "${@}" "${HOST_DNS[@]}"; do
        [[ ${item} ]] || continue
        item=$(request.address "${item}")
        [[ ${item} ]] || continue
        [[ ${itemX} == ${item} ]] && continue

        itemsR+=( "${item}" )
        print_true '%-20s %40s\n' 'Route:' "${itemsR[${#itemsR[@]}-1]}"
    done
    [[ ${itemsR} ]] || return

    # Clear so Internet is before Local.
    set +C; >"${FILE_DNS}"; set -C

    # Write
    for item in 127.0.0.1 "${itemsR[@]}" "${itemsL[@]}"; do
        [[ ${item} ]] || continue
        $(exe grep) -q "${item}" "${FILE_DNS}" && continue

        printf 'nameserver\t%s\n' "${item}" >>"${FILE_DNS}"
    done
}

network.host() {
    local _DESC="
        Get external, network, DNS and broadcast."
    local _ item itemL itemR items hostS mask gate ip
    hostS=$(request.address)

    while read mask _ gate _ _ _ ip; do
        [[ ${mask} == default || ${mask} == *+(/)* ]] || continue

        [[ ${ip} == *+(.)* ]] && items+=( "${ip}:${HOSTNAME}" )
        [[ ${gate} == *+(.)* ]] && items+=( "${gate}:gateway" )
        [[ ${mask} == *+(/)* ]] && items+=( "${mask}:network" )
        # Ignore if local is global.
        [[ ${ip} && ${hostS} == *+(.)* && ${hostS} != ${ip} ]] &&
            items+=( "${hostS}:_${HOSTNAME}" )
    done <<<"$($(exe ip) route show table main)"

    [[ ${items} ]] && network.host_write "${items[@]}"
}

network.host_write() {
    local _ARGS='<address>:<host>'
    local _DESC="
        Write new and modified arguments.
        Remove localhost, add localhost of domain and 127."
    local item itemL itemR re IFS

    for item in "127.0.0.1:localhost:${HOSTNAME}.${DOMAIN}" "${@}"; do
        [[ ${item} ]] || continue
        itemL="${item%%:*}" itemR="${item#*:}"
        [[ ${itemL} && ${itemR} ]] || continue
        [[ ${itemR} == ?(_)+(localhost) ]] && continue
        #$(exe grep) "\b${itemR}\b" "${FILE_HOSTS}" |  # Exists.
        #    $(exe grep) -q "\b${itemL}\b" || continue

        re="${itemL}|${itemR//:/|}"
        sudo $(exe sed) -i -e "\@\b\(${re//|/\|}\)[ |\$]@d" "${FILE_HOSTS}"

        printf '%-20s\t%s\n' "${itemL}" "${itemR//:/ }" |
            sudo $(exe tee) -a "${FILE_HOSTS}" >/dev/null
        print_true '%-40s %40s\n' "${itemR//:/ }:" "${itemL}"
    done
}

network.udhcpc_route() {
    local _ARGS='(deconfig|bound|renew|nak)'
    local _DESC="
        Called via udhcpc.

        Deconfig, interface initialization or lease release.
        Bound, interface bound or unbound.
        Renew, lease modification.
        Nak, NAK packet from server.
        #--Environment--#
        boot_file
            BOOTP file.
        bootfile
            File, string.
        bootsize
            Bootfile length, 512-octet blocks.
        broadcast
            Address.
        cookiessvr
            RFC 865, address list.
        dhcptype
            Message type.
        dns
            DNS, address list.
        domain
            String.
        hostname
            String.
        interface
            String.
        ip
            Address.
        lease
            Time, seconds.
        logsvr
            MIT_LCS UDP, address list.
        lprsvr
            LPR, address list.
        message
            DCHPNAK, string.
        mtu
            MTU.
        namesvr
            IEN 116, address list.
        ntpsrv
            NTP, address list.
        rootpath
            Root disk, client.
        router
            Router, address list.
        serverid
            Address of server.
        siaddr
            BOOTP next server.
        sname
            BOOTP server name.
        subnet
            Subnet mask.
        swapsrv
            Swap server, client.
        tftp
            TFTP server, string.
        timesvr
            Time, address list.
        timezone
            UTV offset, seconds.
        upttl
            TTL.
        wins
            WINS, address list."
    local int item routers dnss
    [[ ${dns} ]] && dns=( ${dnss} )
    [[ ${router} ]] && routers=( ${router} )
    [[ ${hostname} ]] && $(exe hostname) "${hostname}"

    $(exe ip) route del dev "${interface}" 2>/dev/null
    $(exe ip) addr flush dev "${interface}" 2>/dev/null

    $(exe ip) addr \
        add "${ip}/${subnet}" \
        dev "${interface}" \
        broadcast "${broadcast}" 2>/dev/null &&
        print_true '%s: %s_%s\n' "${interface}" "${ip}" "${subnet}"

    [[ ${routers} ]] && print_warn 'Route: '
    for item in "${routers[@]}"; do
        $(exe ip) route add \
            dev "${interface}" \
            table main \
            via "${item}" \
            src "${ip}" || continue
        print_true '%s ' "${item}"
    done
    printf '\n'

    #[[ ${dnss} ]] &&
    #    for item in "${dnss[@]}"; do
    #        network.dns "${item}"
    #    done
}

network.set() {
    local _ARGS="<interface>"
    local _DESC="
        Configure interface state."
    local item items=( "${@:-${NETWORK_DEVICE_DEFAULT}}" )

    # Disable all devices.
    for item in "${DEVICE_NETWORK[@]}"; do
        [[ ${SYS_CLASS_NET}/${item}/flags ]] || continue
        [[ $(<"${SYS_CLASS_NET}/${item}/flags") == 0x1002 ]] && continue

        sudo $(exe ip) link set "${item}" down
    done

    # Enable selected device.
    for item in "${items[@]}"; do
        [[ ${SYS_CLASS_NET}/${item}/flags ]] || continue
        [[ $(<"${SYS_CLASS_NET}/${item}/flags") == 0x1003 ]] && continue

        sudo $(exe ip) link set "${item}" up
    done
}

network.auth() {
    [[ ${NETWORK_DEVICE_DEFAULT} ]] || return

    print_init "Authenticating: ${NETWORK_DEVICE_DEFAULT}"
    if [[ ${1} == 11 ]]; then
        if wpa_supplicant "${NETWORK_DEVICE_DEFAULT}"; then
            print_true 'True\n'
        else
            print_false 'False\n'
            return 1
        fi
    else
        printf '\n'
    fi

    udhcpc "${NETWORK_DEVICE_DEFAULT}"
}

network.list() {
    local item items IFS

    for item in "${DEVICE_RFKILL_WLAN[@]}"; do
        IFS=','; items=( ${item} ); unset IFS

        printf '%s:%d\n' "${items[1]}" \
            $($(exe sed) -ne 's/.*_STATE=\(.*\)/\1/p' "${items}/uevent")
    done
}

network.mode() {
    local item items IFS

    for item in "${@:2}"; do
        (( item )) || continue

        items+=( "${DEVICE_RFKILL_WLAN[${item}-1]}" )
    done
    [[ ${items} ]] || items=( "${DEVICE_RFKILL_WLAN[@]}" )

    for item in "${items[@]}"; do
        IFS=',' items=( ${item} ); unset IFS
        item=$($(exe sed) -ne 's/.*_STATE=\(.*\)/\1/p' "${items}/uevent")

        case "${1}" in
            on)
                (( item )) && continue
                printf '1' |sudo $(exe tee) "${items}/state" >/dev/null
            ;;
            off)
                (( item )) || continue
                printf '0' |sudo $(exe tee) "${items}/state" >/dev/null
            ;;
            tog)
                if (( item )); then
                    printf '0' |sudo $(exe tee) "${items}/state" >/dev/null
                else
                    printf '1' |sudo $(exe tee) "${items}/state" >/dev/null
                fi
            ;;
        esac
    done
}

network.bandwidth() {
    local dev byte pckt err drop fifo frame zip mcast
    local byteT pcktT errT dropT fifoT coll carr zipT
    local item items=( "${DEVICE_NETWORK_ETH[@]}" "${DEVICE_NETWORK_WLAN[@]}" )
    local re strB strBT strP strPT IFS

    IFS='|'; for item in "${items[@]}"; do
        IFS=','; items=( ${item} ); unset IFS
        [[ ${re} ]] && re+='|'
        re+="${items[1]}"
    done; unset IFS
    while read dev byte pckt err drop fifo frame zip mcast \
            byteT pcktT errT dropT fifoT coll carr zipT; do
        [[ ${dev%:} != +(${re}|lo) ]] && continue
        if (( byte/1024**2 > 1 )); then  # Mega
            strB='M' byte=$(( byte/1024**2 ))
        elif (( byte/1024 > 1 )); then
            strB='K' byte=$(( byte/1024 ))
        fi
        if (( pckt/1024**2 > 1 )); then  # Mega
            strP='M' pckt=$(( pckt/1024**2 ))
        elif (( pckt/1024 > 1 )); then
            strP='K' pckt=$(( pckt/1024 ))
        fi
        if (( byteT/1024**2 > 1 )); then  # Mega
            strBT='M' byteT=$(( byteT/1024**2 ))
        elif (( byteT/1024 > 1 )); then
            strBT='K' byteT=$(( byteT/1024 ))
        fi
        if (( pcktT/1024**2 > 1 )); then  # Mega
            strPT='M' pcktT=$(( pcktT/1024**2 ))
        elif (( pcktT/1024 > 1 )); then
            strPT='K' pcktT=$(( pcktT/1024 ))
        fi

        printf "%6s %8s${strB} %7s${strP} %4s %5s${strBT} %7s${strPT}\n" \
            "${dev}" "${byte}" "${pckt}" '' "${byteT}" "${pcktT}"
    done <<<"$(grep . "${PROC_NET_DEV}")"
    printf '%5s  %8s %8s %4s %6s %8s\n' \
        'Int' 'Byte' 'Pckt' '<>' 'Byte' 'Pckt'

    #awk -v dc="date \"+%T\"" '/eth0/{i = $2 - oi; o = $10 - oo; oi = $2; oo = $10; dc|getline d; close(dc); if (a++) printf "%s %8.2f KiB/s in %8.2f KiB/s out\n", d, i/1024, o/1024}'
}

network.arguments() {
    local item
    unset ACTION ITEMS TYPE FORCE
    
    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                1|on) ACTION='on' ;;
                0|off) ACTION='off' ;;
                a|all) ALL=1 ;;
                c|con) ACTION='con' ;;
                d|dns) ACTION+=( 'dns' ) ;;
                e|eth) ACTION='con' TYPE='3' ;;  # 802.3
                h|host) ACTION+=( 'host' ) ;;
                l|list) ACTION='list' ;;
                w|wlan) ACTION='con' TYPE='11' ;;  # 802.11
                !|force) FORCE=1 ;;
                r|route) ACTION='route' ;;
                t|tog?(g)?(l)?(e)) ACTION='tog' ;;
            esac
        done
    done
    [[ ${ITEMS} == bound ]] && ACTION='route'
    [[ ${ACTION} ]]
}

source 'request'
source 'device_network'
return 2>/dev/null

_ARGS='(!)force (e)th (w)lan (l)ist (0|off) (1|on) (t)oggle (all) (d)ns (h)ost (r)oute <args>'
_DESC="(:!:sudo)
    List, list of integer args per device.
    Wireless, connect to wireless AP, request address.
    DNS, request Domain Name System address.
    Host, identify network hostnames.
    Route, used by udhcpc.
    Force, reconnect."

network.arguments "${@:-?}" || exit

for item in "${ACTION[@]}"; do
    case "${item}" in
        dns) network.dns ;;
        host) network.host ;;
        list) network.list "${ITEMS[@]}" ;;
        route) [[ ${ip} && ${router} ]] && network.udhcpc_route ;;
        deconfig) [[ ${interface} ]] && network.set "${interface}" ;;
        on|off|tog) network.mode "${ACTION}" "${ITEMS[@]}" ;;
        con)
            (( FORCE )) || { request.ping && exit; }

            if network.variables "${TYPE}"; then
                if network.set && network.auth "${TYPE}"; then
                    network.dns
                    network.host
                else
                    print_false '!Network Connection\n'
                    (( 0 ))
                fi
            else
                print_false '!Network Device\n'
                (( 0 ))
            fi
        ;;
    esac
done
