#!/usr/bin/env bash
source 'bash.sh'

cpu.variables() {
true
}

cpu.list() {
    local _ARGS='(cvs) (feh) (cel)'
    local _DESC="
        Print cpu_{id,temp,freq}."
    local item temp freq int
    local action type

    while [[ ${1} == +(-)* ]]; do
        case "${1##+(-)}" in
            \?) print_help; return 1 ;;
            cel) type='cel' ;;
            feh) type='feh' ;;
            cvs) action='cvs' ;;
        esac
        shift
    done

    for item in "${CPU_ID[@]}"; do
        (( int++ ))
        [[ ${item} ]] || continue

        if [[ ${action} == cvs ]]; then
            fmt='%s,%s,%s'
        else
            fmt='%s:%s@%s'
        fi
        if [[ ${type} == cel ]]; then
            temp="${CPU_TEMP[${item}]%000}"
            freq="${CPU_FREQ[${item}]%000}"
        else
            temp="${CPU_TEMP[${item}]%000}"
            freq="${CPU_FREQ[${item}]%000}"
        fi

        [[ ${temp} && ${freq} ]] || continue
        printf "${fmt}\n" $(( int-1 )) "${temp}" "${freq}"
    done
}

cpu.arguments() {
    local item 
    unset ACTION ITEM TYPE

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEM=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                l|list) ACTION='list' ;;
                c|celsius) TYPE='cel' ;;
                f|fahrenheit) TYPE='fah' ;;
            esac
        done
    done

    if [[ ${ACTION} == list ]]; then
        [[ ${TYPE} ]] || TYPE='cel'
    fi
}

cpu.test() {
    source 'test'
false
}

cpu.variables
source 'device_cpu'
return 2>/dev/null

_ARGS='(l)ist (c)elsius (f)ahrenheit'
_DESC=""

cpu.arguments "${@:-?}" || exit

case "${ACTION}" in
    list) cpu.list "--${TYPE}" ;;
esac

