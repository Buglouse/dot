#!/usr/bin/env bash
source 'bash.sh'

battery.print() {
    local item name id stat crnt chrgN chrgF chrgD volt diff perc hour min

    for item in "${!SYS_CLASS_POWERSUPPLY_*}"; do
        printf -v item "%s[@]" "${item}"; read -a item <<<"${!item}"
        name="${item[0]}" id="${item[0]##*[[:alpha:]]}"  
        stat="${item[1]}" crnt="${item[2]%000}"
        chrgN="${item[3]%000}" chrgF="${item[4]%000}"
        chrgD="${item[5]%000}" volt="${item[6]%000}"

        diff=$(( (10000*${chrgF})/${chrgD} )); diff=$(( 10000-diff ))
        perc=$(( (100*${chrgN})/${chrgF} ))

        case "${item[1]}" in
            Discharging)
                hour=$(( ${chrgN}/${crnt} ))
                min=$(( 60*${chrgN}/${crnt}-(${hour}*60) ))
            ;;
            Charging)
                hour=$(( (${chrgF}-${chrgN})/${crnt} ))
                min=$(( (1000*(${chrgF}-${chrgN})/${crnt})*60/60 ))
                (( ${#min} == 4 )) && min="${min:1}"
                min=$(( min*60)); min="${min:0:2}"
            ;;
            Full) ;;
        esac

        if [[ ${1} == cvs ]]; then
            printf '%s,%s,%d,%d,%d,%d,%d,%02d:%02d,%d\n' \
                "${id}" "${stat}" "${chrgN}" "${chrgF}" "${crnt}" \
                "${volt}" "${perc}" "${hour}" "${min}" "${diff}"

        else
            printf '%s%s %s %d%%[%02d:%02d] %dmAh/%dmAh[%%%d.%d] @ %dmA_%dV\n' \
                "${name}" "${id}" "${stat}" "${perc}" "${hour}" "${min}" \
                "${chrgN}" "${chrgF}" "${diff:0:1}" "${diff:1:1}" "${crnt}" "${volt}" 
        fi
    done
}

battery.print_state() {
    local item

    for item in "${!SYS_CLASS_POWERSUPPLY_*}"; do
        printf -v item "%s[@]" "${item}"; read -a item <<<"${!item}"

        if [[ ${TYPE} == cvs ]]; then
            printf '%s,%s\n' "${item[0]##*/}" "${item[1]}"
        else
            printf '%s#%s\n' "${item[0]##*/}" "${item[1]}"
        fi
    done
}

battery.print_percent() {
    local item

    for item in "${!SYS_CLASS_POWERSUPPLY_*}"; do
        printf -v item "%s[@]" "${item}"; read -a item <<<"${!item}"
        perc=$(( (100*${item[3]})/${item[4]} ))

        if [[ ${TYPE} == cvs ]]; then
            printf '%s,%02d\n' "${item[0]##*/}" "${perc}"
        else
            printf '%s%%%02d\n' "${item[0]##*/}" "${perc}"
        fi
    done
}

battery.loop() {
    local item=( $(battery.print cvs) )
    local IFS=','; item=( ${item} ); unset IFS
    [[ ${item[1]} == +(Dis)* ]] || return 3

    if (( ${item[6]} < 10 )); then
        print_ptmx "${RED}{RAM Suspend}"
        power.sh -M 
    elif (( ${item[6]} < 15 )); then
        print_ptmx "${RED}(%s)" "${item[6]}"
    else
        return 3
    fi
}

battery.arguments() {
    local item
    unset ACTION ITEMS ELEM TYPE

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                C|cvs) TYPE='cvs' ;;
                L|loop) ACTION='loop' ;;
                l|list) ACTION='print' ;;
                p|perc*) ELEM+=( 'perc' ) ;;
                s|state) ELEM+=( 'state' ) ;;
            esac
        done
    done
    [[ ${TYPE} && ! ${ACTION} ]] && ACTION='print'
    [[ ${ACTION} ]]
}

source 'device_power.sh'
return 2>/dev/null

_ARGS='(l)ist (L)oop (p)ercent (s)tate (C)vs'
_DESC="
    List, print battery information.
        - State
        - Percent
    CVS, comma deliminated values.
    Loop, self loop."

battery.arguments "${@:-?}" || exit

case "${ACTION}" in
    loop) battery.loop ;;
    print)
        case "${ELEM}" in
            perc) batter.print_percent ;;
            state) battery.print_state ;;
            *) battery.print "${TYPE}" ;;
        esac
    ;;
esac
