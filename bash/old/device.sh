#!/usr/bin/env bash
source 'bash.sh'

device.variables() {
    local item

    export PROC_LOADAVG '/proc/loadavg'  #:order:0
    export PROC_NET "${DIR_PROC}/net"
    export PROC_NET_DEV "${PROC_NET}/dev"

    export SYS_CLASS_NET '/sys/class/net' #:order:0
    export SYS_CLASS_BLOCK '/sys/class/block'
    export SYS_CLASS_RFKILL '/sys/class/rfkill' #:order:0
    item=( $(<"${PROC_LOADAVG}") )
    export LOADAVG "${item[@]::3}"
    export PROCESS "${item[@]:3:1}"

    export LIMIT_PROCESS_MAX '150'
    export LIMIT_PROCESS_MIN '75'
    export LIMIT_PROCESS_RUN_MAX '15'
    export LIMIT_PROCESS_RUN_MIN '1'
}

device.info() {
    local _DESC="
        Find drivers for /dev items."
    local item items itemsD itemsF

    #--UDEV--#
    items=( ${DIR_DEV}/* )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] && continue
        [[ ${item} == *+([[:digit:]]) ]] && continue

        itemsD+=( $($(exe udevadm) info --attribute-walk --name="${item}" 2>/dev/null |
            $(exe sed) -n -e 's/.*DRIVERS=="\([^"]*\)".*/\1/p') )
    done
    #$(exe udevadm) info --export-db

    #--BLKID--#
    itemsF=( $($(exe blkid) |$(exe sed) -n 's/.*TYPE="\([^"]*\)".*/\1/p') )

    printf '%s\n' "${itemsF[@]}" "${itemsD[@]}" |$(exe sort) -u
}

device.module() {
    local _DESC="
        Load modules for detected devices."
    local _ id bus dev ctrl items

    unset items
    while read bus _ ctrl id; do
        case "${id}" in
            '8086:27d8'|'8086:1c20') items+=( 'snd-hda-intel' ) ;;
            '8086:a011') items+=( 'i915' ) ;;
            '8086:a010') items+=( 'eeepc-laptop' ) ;;
            '14e4:4727') items+=( 'brcmsmac' ) ;;
	    '1274:5880') items+=( 'snd-ens1371' ) ;;
        esac
    done <<<"$($(exe lspci))"

    #while read _ bus _ dev _ id _; do
    #    case "${id}" in
    #        *) ;;
    #    esac
    #done <<<"$($(exe lsusb))"

    [[ ${items} ]] && modprobe "${items[@]}"
    (( 1 ))
}

device.arguments() {
    local item 
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

device.variables
device.module
return 2>/dev/null

_ARGS=''
_DESC="Device Administration."

device.arguments "${@:-?}" || exit

