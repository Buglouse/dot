#!/usr/bin/env bash
source 'bash.sh'

device.power.variables() {
    local item name chrg chrgD chrgF curr stat voltD volt

    export SYS_POWER_STATE '/sys/power/state'
    export SYS_CLASS_POWERSUPPLY '/sys/class/power_supply'

    items=( ${SYS_CLASS_POWERSUPPLY[@]}/* )
    for item in "${items[@]/%//uevent}"; do
        [[ -e ${item} ]] || continue
        (( $($(exe sed) -ne 's/.*_PRESENT=\(.*\)/\1/p' "${item}") )) || continue

        name=$($(exe sed) -ne 's/.*[^L]_NAME=\(.*\)/\1/p' "${item}")
        [[ ${name^^} == BAT* ]] || continue
        chrg=$($(exe sed) -ne 's/.*_CHARGE_NOW=\(.*\)/\1/p' "${item}")
        curr=$($(exe sed) -ne 's/.*_CURRENT_NOW=\(.*\)/\1/p' "${item}")
        stat=$($(exe sed) -ne 's/.*_STATUS=\(.*\)/\1/p' "${item}")
        volt=$($(exe sed) -ne 's/.*_VOLTAGE_NOW=\(.*\)/\1/p' "${item}")
        chrgF=$($(exe sed) -ne 's/.*_CHARGE_FULL=\(.*\)/\1/p' "${item}")
        chrgD=$($(exe sed) -ne 's/.*_CHARGE_FULL_DESIGN=\(.*\)/\1/p' "${item}")
        voltD=$($(exe sed) -ne 's/.*_VOLTAGE_MIN_DESIGN=\(.*\)/\1/p' "${item}")

        read -a "SYS_CLASS_POWERSUPPLY_${name^^}" <<< \
            "${name} ${stat} ${curr} ${chrg} ${chrgF} ${chrgD} ${volt} ${voltD}"
        export SYS_CLASS_POWERSUPPLY_${name^^}
    done
}

device.power.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

device.power.variables
return 2>/dev/null

_ARGS=''
_DESC="Power Device."

device.power.arguments "${@:-?}" || exit

case "${ACTION}" in
    *) ;;
esac
