#!/bin/env bash
battery() {
    [[ $(type -P acpi) ]] || return
    local colours items=( $(acpi -b |
	    sed -e 's/.*\([CDUF]\)[^0-9]*\([0-9]\{,3\}\)\(%\)[^0-9]*\([0-9:]\{8\}\)\?.*/\2 \3 \4 \1/') )

    
    if (( ${items[0]} >= 50 )); then
	colours[0]='#[fg=brightgreen]'
    elif (( ${items[0]} >= 30 )); then
	colours[0]='#[fg=brightyellow]'
    else
	colours[0]='#[fg=brightred]'
    fi
    case "${items[3]}" in
	C) colours[3]='#[fg=brightyellow]' ;;
	D) colours[3]='#[fg=yellow]' ;;
	U) colours[3]='#[fg=brightgreen]' ;;
	F) colours[3]='#[fg=brightgreen]' ;;
    esac
    colours[1]='#[fg=colour248]'
    colours[2]='#[fg=white]'
    
    printf '%s%s%s%s%s%s%s%s' \
    	"${colours[0]}" "${items[0]}" \
    	"${colours[1]}" "${items[1]}" \
    	"${colours[2]}" "${items[2]}" \
    	"${colours[3]}" "${items[3]}"
}
loadavg() {
    # Comment output of below.
    local i colours item items=( $(</proc/loadavg) )

    unset i
    for item in "${items[@]::3}"; do
	if (( b${item%.*} >= 1 )); then
	    colours[i]='#[fg=red]'
	elif (( b${item#*.} >= 50 )); then
	    colours[i]='#[fg=yellow]'
	else
	    colours[i]='#[fg=green]'
	fi
	(( i++ ))
    done
    colours[3]='#[fg=yellow]'
    
    printf '%s%s %s%s %s%s %s%s' \
	"${colours[0]}" "${items[0]}" \
	"${colours[1]}" "${items[1]}" \
	"${colours[2]}" "${items[2]}" \
	"${colours[3]}" "${items[3]}"
}
sensors() {
    local item colours path
    local paths=( ${DIR_SYSFS}/devices/virtual/thermal/thermal*/temp )

    for path in "${paths[@]}"; do
	[[ -e ${path} ]] || return
	# Read sensor value
	item="$(<"${path}")"
	# Remove zeros
	item="${item%000}"

	# Decimal
	if (( ${#item} == 3 )); then
	    item="${item::2}.${item:2}"
	fi
	# Temporary remove decimal
	if [[ ${item} == +(\.) ]]; then
	    item2="${item%.*}"
	else
	    item2="${item}"
	fi
	    
	# Set colour based on tempurature
	if (( item2 >= 59 )); then
	    colours[0]='#[fg=brightred]'
	elif (( item2 >= 50 )); then
	    colours[0]='#[fg=brightyellow]'
	else
	    colours[0]='#[fg=green]'
	fi
	
	printf '%s%s' \
	    "${colours[0]}" "${item#000}C"
    done
}
voltage() {
    local path='/sys/class/power_supply/BAT0/uevent'
    local volts=( $(<"${path}") )
    volts=( "${volts[6]#*=}" "${volts[7]#*=}" )
    volts=( "${volts[0]%000}" "${volts[1]%000}" )
    
    printf '#[fg=yellow]%s#[fg=white]%s#[fg=colour248]@#[fg=yellow]%s#[fg=white]%s' \
	"${volts[0]}" "mV" "${volts[1]}" "mW" 
}
fan_find() {
    local pathHwmon='/sys/class/hwmon'
    for item in ${pathHwmon}/hwmon*; do
	[[ -e ${item}/pwm1 ]] || continue
	pathHwmon="${item}"
    done
    
    local pwm="${pathHwmon}/pwm1"
    local fan="${pathHwmon}/fan1_input"
    local pwmStatus="${pathHwmon}/pwm1_enable"
    [[ -e ${pwm} && -e ${fan} && -e ${pwmStatus} ]] || return
    export FAN_PWM="${pwm}"
    export FAN_FAN="${fan}"
    export PWM_STATUS="${pwmStatus}"
}
fan() {
    [[ ${FAN_FAN} ]] || fan_find
    printf '#[fg=colour43]%s' \
	"$(<"${FAN_FAN}")"
}
fds() {
    local name eq alloc unused max
    sysctl fs.file-nr |
    while read name eq alloc unused max; do
	printf '#[fg=colour81]%s' \
	    "${alloc}"
    done
}
mem() {
    local title total used free shared buffers cached
    free -m |grep -i 'mem' |
    while read title total used free shared buffers cached; do
	printf '#[fg=colour46]%s#[fg=colour248]+#[fg=colour46]%s' \
	    "${free}" "${used}"
    done
}
net() {
    local dir int int_eg item ip_eg conn serv
    for dir in /sys/class/net/*; do
	if [[ $(<"${dir}/flags") == 0x+(1003|1103) ]]; then
	    # egress
	    int=( $(<"${dir}/uevent") )
	    for item in "${int[@]}"; do
		[[ ${item} == +(INTERFACE=*) ]] || continue
		int_eg="${item#*=}"
		break
	    done    
	fi
    done
    [[ ${int} ]] || return
	
    ip_eg=( $(ip -o -f inet addr show dev "${int_eg}" | awk '{printf "%s ", $4}') )
    [[ ${ip_eg} ]] || return
    conn="$(netstat -tnu |tail -n '+3' |wc -l)"
    serv="$(netstat -tnul |tail -n '+3' |wc -l)"
    
    printf '%s%s %s%s#[fg=colour248]^%s%s' \
	'#[fg=colour198]' "${ip_eg[*]}" \
	'#[fg=colour214]' "${serv}" \
	'#[fg=colour166]' "${conn}"
}

gmail() {
    local item='https://mail.google.com/mail/feed/atom/'

     curl --silent --insecure --url "${item}"|
     sed -n -e 's@<fullcount>\(.*\)</fullcount>@\1@p'
}
printf '#[fg=colour248][%s %s %s#[fg=colour248]@%s#[fg=colour248]] #[fg=colour248](%s#[fg=colour248]) #[fg=colour248]#%s#[fg=colour248]# #[fg=colour248]<%s#[fg=colour248]> #[fg=colour248]{%s#[fg=colour248]}' \
    "$(battery)" "$(voltage)" "$(sensors)" "$(fan)" \
    "$(loadavg)" \
    "$(mem)" \
    "$(fds)" \
    "$(net)" 
#gmail
