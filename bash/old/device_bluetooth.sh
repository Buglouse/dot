#!/usr/bin/env bash
source 'bash.sh'

device.bluetooth.variables() {
    local item items itemsB type name stat old
    unset DEVICE_RFKILL_BLUETOOTH

    items=( ${SYS_CLASS_RFKILL}/* )
    for item in "${items[@]}"; do
        item="${item}/uevent"
        [[ -e ${item} ]] || continue

        type=$($(exe sed) -ne 's/.*_TYPE=\(.*\)/\1/p' "${item}")
        [[ ${type} == bluetooth ]] || continue
        name=$($(exe sed) -ne 's/.*_NAME=\(.*\)/\1/p' "${item}")

        itemsB+=( "${item%/*},${name}" )
        export DEVICE_RFKILL_${type^^} "${itemsB[@]}"
    done
}

device.bluetooth.arguments() {
    local item
    unset ACTION ITEMS STATE

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

device.bluetooth.variables
return 2>/dev/null

_ARGS=''
_DESC="
    "

device.bluetooth.arguments "${@:-?}" || exit

case "${ACTION}" in
    *) ;;
esac
