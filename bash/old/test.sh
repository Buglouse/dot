#!/usr/bin/env bash
source 'bash.sh'

test.variables() {
true
}

test.false() {
    export TEST '1'

    local items=( "${@}" )

    printf '(%s)%40s\t\t' "${items}" ''
    if ${items[*]} &>/dev/null; then
        print_false 'True'
    else
        print_true 'False'
    fi
    print_true '/False\n'

    unset TEST
}

test.true() {
    export TEST '1'

    if (( ${#} )); then
        local items=( "${@}" )

        printf '(%s)%40s\t\t' "${items}" ''
        if ${items[*]} &>/dev/null; then
            print_true 'True'
        else
            print_false 'False'
        fi
        print_true '/True\n'

    else
        printf '(%s)%20s\t\t' "${FUNCNAME}" ''
        if (( ${?} )); then
            print_false 'False'
        else
            print_true 'True'
        fi
        print_true '/True\n'
    fi

    unset TEST
}

test.arguments() {
    local item 
    unset ACTION ITEM

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEM=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

test.variables
return 2>/dev/null

_ARGS=''
_DESC="Test."

test.arguments "${@:-?}" || exit

case "${ACTION}" in
    *) ;;
esac

