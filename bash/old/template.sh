#!/usr/bin/env bash
{
source 'bash.sh'
}  # Initiate environment.

template.variables() {
    local _DESC="
        Variables pertaining to file.

        '  #:order:<int>', restrict entries to follow order."
}

template.example() {
    local _ARGS='(optionalArgument(subArgument)) <mandatoryArgument|mandatoryOption2Argument>'
    local _DESC="
        Description(short) of FUNCNAME.

        Arguments, listed as (optional) or <mandatory> with '|' denoting alternatives.
            SubArgument, only relevent if parent selected."
    {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    }  # Ignore during debug(xtrace).
    {
    local item items action
    }  # All function locals.

    {
    while [[ ${1} == +(-)* ]]; do
        item="${1##*+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            *) action="${item}" ;;
        esac
        shift
    done
    }  # Function option block, significance to order and no long/short distinction.
}  # <file>.<method>, syntax.

template.arguments() {
    local item 
    unset ACTION ITEMS

    {
    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
            esac
        done
    done
    }  # Support long/short distinction.

    {
    [[ ${ACTION} ]]
    }  # Return conditions.
}

template.test() {
    local _DESC="
        File testing."
    source 'test'

    $(exe cmd)
}

{
template.variables
}  # Calls for 'source'.
{
source 'requirement'
}  # Dependence.
{
return 2>/dev/null
}  # Return if 'source'.

{
_ARGS='(l)ist[(L)ong] (digit) <items>'
_DESC="
    Template file.

    List, print.
        Long, print long output.
    Digit, multiply print.
    Items, parts to act.

    This is a longer description of this Template."
}  # File information.

{
template.arguments "${@:-?}" || exit
}  # Parse arguments, default '-?'.

{
parse.pid --lock "${PID_TEMPLATE}" || exit
trap '{ parse.pid --term "${PID_TEMPLATE}"; }' EXIT
}  # Single use restriction.

{
case "${ACTION}" in
    *) ;;
esac
}  # Perform.

