#!/usr/bin/env bash

source ~/.bash/bash.sh

library_austin() {
    local formData='accept="I Accept"'
    local url='http://192.168.168.1:8000'
    
    curl --data "${formData}" --url "${url}"
    }
    
####
return 2>/dev/null

case "${1}" in
    austin) library_austin ;;
    *) printf '%s\n' 'austin' ;;
esac
