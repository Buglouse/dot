#!/usr/bin/env bash
source 'bash.sh'

alsa.variables() {
    local item items PS3='Card: '

    [[ -e /proc/asound ]] || { print_false 'ALSA Module!\n'; return 1; }

    ALSA_CARD=( /proc/asound/card+([[:digit:]])*/{midi,codec}* )
    ALSA_CARD=( $(_uniq "${ALSA_CARD[@]%/*}") )

    if (( ${#ALSA_CARD[@]} )); then  # Select default.
        for item in "${ALSA_CARD[@]/%//id}"; do
            [[ -e ${item} ]] && items+=( $(<"${item}") )
        done
        ALSA_CARD=( "${items[@]}" )
        selects -1 "${ALSA_CARD[@]}"
        ALSA_CARD_DEFAULT="${SELECT}"
    fi

    #:todo: Fix, IFS subshell; handle space.
    local IFS=$'\n'
    ALSA_MIXER=( $(unset IFS; $(exe amixer) scontrols |
        $(exe sed) -ne "s/.*'\(.*\)'.*/\1/p") )
    unset IFS

    export CONF_ALSA "${HOME}/.alsa/alsa.conf"
    export TMP_CONF_ALSA "${DIR_TMP_CACHE}/alsa.conf"
    export ALSA_CARD
    export ALSA_CARD_DEFAULT
    export ALSA_MIXER
}

alsa.write_conf() {
    local item

    [[ ${ALSA_CARD} ]] || { print_false 'Device Modules!\n'; return 1; }

    { for item in "${ALSA_CARD[@]}"; do
        printf 'pcm.%s { type hw; card %s; }\n' "${item}" "${item}"
        printf 'ctl.%s { type hw; card %s; }\n' "${item}" "${item}"
    done
    printf 'pcm.mixer { type dmix; ipc_key 1024\n' 
    printf '\tslave { pcm "%s"; period_time 0; period_size 2048; buffer_size 8192; }\n' \
        "${ALSA_CARD_DEFAULT}"
    printf '\tbindings { 0 0; 1 1; }\n}\n'
    printf 'ctl.eq { type equal; }\n'
    printf 'pcm.eq { type equal\n\tslave { pcm "plug:mixer"; }\n}\n'
    printf 'pcm.!default { type plug\n\tslave { pcm "mixer"; }\n}\n'
    #printf 'pcm.!default { type plug; slave.pcm "eq"; }\n'
    #printf 'pcm.!default pcm.%s\n' "${ALSA_CARD_DEFAULT}"
    #printf 'ctl.!default ctl.%s\n' "${ALSA_CARD_DEFAULT}"
    } >"${TMP_CONF_ALSA}"
    $(exe ln) -sf "${TMP_CONF_ALSA}" "${HOME}/.asoundrc"

    for item in "${ALSA_MIXER[@]}"; do
        { case "${item}" in
            *Auto*) $(exe amixer) --quiet -- set "${item}" Enabled ;;
            PCM|Master) $(exe amixer) --quiet -- set "${item}" mute 50% ;;
            Speaker|Headphone|Front) $(exe amixer) --quiet -- set "${item}" unmute 100% ;;
            *) $(exe amixer) --quiet -- set "${item}" mute 0% ;;
        esac; } 2>/dev/null
    done
}

alsa.mode() {
    local item mode

    exist_elem 'Master' "${ALSA_MIXER[@]}" || return

    case "${1}" in
        toggle) mode='toggle' ;;
        decrease) item="${2:-3}" mode='-' ;;
        increase) item="${2:-3}" mode='+' ;;
    esac

    if [[ ${1} == list ]]; then
        $(exe amixer) get 'Master' |
            $(exe grep) '^[^[[:space:]]|dB' |
            $(exe sed) -n \
                -e "/mixer/{s/[^']*'\([^']*\)',\(.*\)/\1:\2/}" \
                -e '/Playback/{s/[^\[]*\(.*\)/\1/}p'
    else
        $(exe amixer) --quiet -- set 'Master' "${item}${mode}"
    fi
}

#alsa.aoss() {
#    modprobe 'snd-seq-oss' 'snd-pcm-oss' 'snd-mixer-oss'
#
#    if ! [[ -e /dev/dsp ]]; then
#        [[ -e /dev/dsp1 ]] && sudo $(exe ln) -s /dev/dsp1 /dev/dsp
#    fi
#
#    if [[ -d /proc/asound ]]; then
#        LD_PRELOAD="${PREFIX_LIB}/libaoss.so${LD_PRELOAD:+:${LD_PRELOAD}}" "${@}"
#    else
#        "${@}"
#    fi
#}

alsa.arguments() {
    local item 
    unset ACTION FORCE ITEMS ITEM_DIGIT

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                u|up) ACTION='increase' ;;
                d|down) ACTION='decrease' ;;
                l|list) ACTION='list' ;;
                w|write) ACTION='write' ;;
                t|toggle) ACTION='toggle' ;;
                !|f|force) FORCE='1' ;;
                +([[:digit:]])) ITEM_DIGIT="${ARG}" ;;
            esac
        done
    done

    [[ ${ITEM_DIGIT} ]] &&
        if (( ITEM_DIGIT > 100 )); then
            ITEM_DIGIT=100
        elif (( ITEM_DIGIT == 0 )); then
            ITEM_DIGIT=1
        fi
    [[ ${ACTION} ]]
}

alsa.variables || return 2>/dev/null
return 2>/dev/null

_ARGS='(w)rite (t)oggle (u)p (d)own (!)force (l)ist'
_DESC="
    (Up|Down), (increase|decrease) Mater volume by percent, default 3%."

alsa.arguments "${@:-?}" || exit

[[ -e /proc/asound ]] ||
    { $(exe amixer) --version >/dev/null; alsa.variables; }

case "${ACTION}" in
    write) [[ -e ${HOME}/.asoundrc && ! ${FORCE} ]] || alsa.write_conf ;;
    +(in|de)crease|toggle|list)
        [[ ${ALSA_CARD} ]] ||  alsa.variables  #:fix: TC package.
        alsa.mode "${ACTION}" "${ITEM_DIGIT}"
    ;;
esac

