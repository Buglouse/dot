#!/usr/bin/env bash
source 'bash.sh'

bluetooth.variables() {
true
}

bluetooth.mode() {
    local item items IFS

    for item in "${DEVICE_RFKILL_BLUETOOTH[@]}"; do
        IFS=',' items=( ${item} ); unset IFS
        item=$($(exe sed) -ne 's/.*_STATE=\(.*\)/\1/p' "${items}/uevent")

        case "${1}" in
            on)
                (( item )) && continue
                printf '1' |sudo $(exe tee) "${items}/state" >/dev/null
            ;;
            off)
                (( item )) || continue
                printf '0' |sudo $(exe tee) "${items}/state" >/dev/null
            ;;
            tog)
                if (( item )); then
                    printf '0' |sudo $(exe tee) "${items}/state" >/dev/null
                else
                    printf '1' |sudo $(exe tee) "${items}/state" >/dev/null
                fi
            ;;
        esac
    done
}

bluetooth.list() {
    local item items IFS

    for item in "${DEVICE_RFKILL_BLUETOOTH[@]}"; do
        IFS=','; items=( ${item} ); unset IFS

        printf '%s:%d\n' "${items[1]}" \
            $($(exe sed) -ne 's/.*_STATE=\(.*\)/\1/p' "${items}/uevent")
    done
}

bluetooth.arguments() {
    local item 
    unset ACTION ITEM

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEM=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                1|on) ACTION='on' ;;
                0|off) ACTION='off' ;;
                t|tog?(g)?(l)?(e)) ACTION='tog' ;;
                l|list) ACTION='list' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

bluetooth.test() {
    source 'test'
}

source 'device_bluetooth'
bluetooth.variables
return 2>/dev/null

_ARGS='(l)ist (0|off) (1|on) (t)oggle <args>'
_DESC="
    List, list of integer args per device."

bluetooth.arguments "${@:-?}" || exit

case "${ACTION}" in
    list) bluetooth.list "${ITEMS[@]}" ;;
    on|off|tog) bluetooth.mode "${ACTION}" ;;
esac

