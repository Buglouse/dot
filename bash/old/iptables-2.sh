#!/usr/bin/env bash
source 'bash.sh'

iptables.variables() {
    alias ipt iptables

    export CMD_IPTABLES "$(exe iptables)"
    export CMD_TC "$(exe tc)"
    export CMD_IP "$(exe ip)"
    export PID_IPTABLE "${DIR_TMP_PID}/iptable.pid"

    # ip_tables
    # nf_conntrack ip_conntrack
    # xt_conntrack ipt_conntrack
    # xl_limit ipt_limit
    # xt_state ipt_state
    # xt_multiport ipt_multiport
    # xt_TCPMSS ipt_TCPMSS
    # xt_DSCP ipt_DSCP ipt_TOs
    #export IPTABLE_MODULE 'xt_state' 'xt_limit' 'nf_nat' 'nf_defrag_ipv4' \
    #    'nf_conntrack_ipv4' 'nf_conntrack' 'iptable_nat' \
    #    'ipt_REJECT' 'ipt_LOG' 'xt_conntrack' 'xt_mark'
    export IPTABLE_MODULE 'iptable_nat' 'nf_conntrack_ipv4'
    export IPTABLE_SYSCTL 'net.netfilter.nf_conntrack_acct=1' \
        'net.ipv4.icmp_ignore_bogus_error_responses=1' \
        'net.ipv4.conf.all.accept_redirects=0' \
        'net.ipv4.conf.all.accept_source_route=0' \
        'net.ipv4.tcp_ecn=0' \
        'net.ipv4.conf.all.log_martians=1' \
        'net.ipv4.conf.all.rp_filter=2' \
        'net.ipv4.icmp_echo_ignore_all=1' \
        'net.ipv4.icmp_echo_ignore_broadcasts=1' \
        "net.ipv4.ip_forward=1" \
        'net.ipv4.conf.all.rp_filter=1'
    export IPTABLE_MOUDLE_TC 'ifb'

    export IPTABLE_TABLES 'filter' 'net' 'mangle' 'raw' 'security'

    declare -A IPTABLE_FILTER
    export IPTABLE_FILTER[chain]='input;forward;output'
    export IPTABLE_FILTER[input]='drop'
    export IPTABLE_FILTER[output]='accept'
    export IPTABLE_FILTER[forward]='drop'

    declare -A IPTABLE_FILTER_STATE
    IPTABLE_FILTER_STATE[policy]='accept;drop;reject'
    IPTABLE_FILTER_STATE[accept]='FORWARD:established,related'
    #IPTABLE_FILTER_STATE[drop]='INPUT:invalid;FORWARD:new,invalid;OUTPUT:invalid'

    export IPTABLE_SYSCTL 'net.nf_conntrack_max=8192'
        'net.netfilter.nf_conntrack_udp_timeout=60'
        'net.netfilter.nf_conntrack_acct=1'
        'send_redirects=0'
        'accept_source_route=1'
        'icmp_echo_ignore_bradcasts=1'
        'icmp_ignore_bogus_error_resonses=1'
        'ip_forard=1'
        'autoconf=0'
        'accept_ra=0'
        'tcp_window_scaling=1'
        'tcp_timestamps=1'
        'tcp_sack=1'
        'tcp_dsack=1'
        'tcp_fack=1'
        'tcp_low_latency=0'
        'tcp_fin_timeout=30'
        'tcp_keepalive_time=1800'
        'tcp_syn_retries=3'
        'tcp_synack_retries=2'
        'tcp_rfc1337=1'
        'ip_local_port_range'
        'tcp_syncookies=0'
        'rp_filter=1'
        'icmp_echo_ignore_all=0'
        'log_martians=0'
        'accept_redirects=1'
        'tcp_ecn=0'
        'ip_dynaddr=0'
        'ip_no_pmtu_disc=1'
        'ip_default_ttl'
        'ip_queue_maxlen=2048'
   
    export IPTABLE_CHAIN 'logger' 'flood' 'loop' 'lan' 'nat' 'shapeout:mangle'
    export IPTABLE_CHAIN_DEFAULT 'input' 'output' 'forward'
    export IPTABLE_CHAIN_FILTER 'input' 'output' 'forward' 'postrouting'
    export IPTABLE_CHAIN_FILTER_INPUT 'logger:all' 'lan:all' 'icmp' 'tcp' 'udp'
    export IPTABLE_CHAIN_FILTER_OUTPUT 'logger:all' 'lan:all' 'icmp' 'tcp' 'udp'
    export IPTABLE_CHAIN_FILTER_FORWARD 'logger:all' 'lan:all'
    export IPTABLE_CHAIN_POLICY_DEFAULT 'ACCEPT'
    export IPTABLE_CHAIN_FILTER_POSTROUTING 'mangle:shapeout'
    export IPTABLE_CHAIN_LAN 'lan'
    export IPTABLE_CHAIN_LOG 'log'
    export IPTABLE_CHAIN_NAT 'nat'
    export IPTABLE_CHAIN_ICMP 'icmp'
    export IPTABLE_CHAIN_LOOP 'loop'
    export IPTABLE_CHAIN_FLOOD 'flood'

    export IPTABLE_PROTOCOL 'icmp' 'tcp' 'udp'

    export IPTABLE_POLICY_DEFAULT 'accept' 'drop' 'reject'
    export IPTABLE_POLICY_ACCEPT 'input:established' 'related' 'output:established,related'
    export IPTABLE_POLICY_DROP 'forward:new,invalid' 'output:invalid' 'input:invalid'
    export IPTABLE_POLICY_ICMP 'REJECT --reject-with icmp-net-prohibited'

    export IPTABLE_LAN_ACCEPT
    export IPTABLE_ACCEPT

    export IPTABLE_CHAIN_CLASS 'shapeout -t mangle'
    export NETWORK_CONNECTION_LIMIT '250'
}

iptables.table_policy() {
    local _DESC="
        Policy insturctions for tables."
    local item item2 IFS

    iptables.table_flush
    
    IFS=';'; for item in ${IPTABLE_FILTER[chain]}; do
        IFS=';'; for item2 in ${IPTABLE_FILTER[${item}]}; do
            ipt -P "${item^^}" "${item2^^}"
        done
    done
}
iptables.table_flush() {
    local _DESC="
        Remove all rules from all chains of all tables."
    local item

    for item in "${IPTABLE_TABLES[@]}"; do
        ipt -t "${item}" -F
        ipt -t "${item}" -X
        ipt -t "${item}" -Z
    done
}

iptables.chain_create() {
    local item

    for item in "${IPTABLE_CHAIN[@]}"; do
        if [[ ${item} == +(*:*) ]]; then
            item="${item#*:}"

            ${CMD_IPT} -N -t "${item}"

        else
            ${CMD_IPT} -N "${item}"
        fi
    done

    # Flood
    for item in "${IPTABLE_PROTOCOL[@]}"; do
        ${CMD_IPT} -N "flood-${item}"
    done
}
iptables.chain_filter() {
    # Redirection
    local item item2 itemT itemC items

    for item in "${IPTABLE_STATE[@]}"; do
        item="IPTABLE_CHAIN_FILTER_${item}[@]" items=( "${!item}" )

        for item2 in "${items[@]}"; do
            if [[ ${item2} == +(*:*) ]]; then
                itemT="${item2#*:}" itemC="${item2%*:}"

                ${CMD_IPT} -A "${item}" -p "${itemT}" -j "${itemC}"

            else
                ${CMD_IPT} -A "${item}" -p "${item2}" -j "${item2}"
            fi
        done
    done
}
iptables.chain_remove() {
    local item items=( $(sudo $(exe grep) . "${DIR_PROC}/net/ip_tables_names") )

    for item in "${items[@]}"; do
        ${CMD_IPT} -Z -t "${item}" 
        ${CMD_IPT} -X -t "${item}"
    done
}

iptables.policy() {
    iptables.policy_filter_loop
    iptables.policy_filter_state
    iptables.policy_filter_log
}
iptables.policy_filter_loop() {
    ipt -A INPUT -i lo -j ACCEPT
}
iptables.policy_filter_state() {
    local item item2 itemL itemR IFS

    # Get targets.
    IFS=';'; for item in ${IPTABLE_FILTER_STATE[policy]}; do
        IFS=';'; for item2 in ${IPTABLE_FILTER_STATE[${item}]}; do
            itemL="${item2%:*}" itemR="${item2#*:}"

            iptables -I "${itemL}" \
                -m state --state "${itemR^^}" \
                -j "${item^^}"
        done
    done
}
iptables.policy_filter_accept() {
    epxort IPTABLE_PROTOCOL 'icmp' 'tcp' 'udp'
    IPTABLE_FILTER_DROP[icmp]='echo-request'
    IPTABLE_FILTER_ACCEPT[icmp]='echo-request'
    local item item2 IFS

    for item in "${IPTABLE_PROTOCOL[@]}"; do
        IFS=';'; for item2 in ${IPTABLE_FILTER_ACCEPT[${item}]}; do
            case "${item}" in
                icmp)
                    ipt -A INPUT -p "${item}" --icmp-type "${item2}" -j ACCEPT
                ;;
                tcp)
                ;;
                udp)
                ;;
            esac
        done
        IFS=';'; for item2 in ${IPTABLE_FILTER_ACCEPT[${item}]}; do
            case "${item}" in
                icmp)

                ;;
                tcp)
                ;;
                udp)
                ;;
            esac
        done
    done
}

iptables.policy_nat() {
    # Dynamic
    ${CMD_IPT} -T "${IPTABLE_CHAIN_NAT}" \
        -A POSTROUTING -o "${INT}" -j MASQUERADE

    # Static
    ${CMD_IPT} -T "${IPTABLE_CHAIN_NAT}" \
        -A POSTROUTING -o "${INT}" -j SNAT --to "${HOST_ADDRESS}"
}
iptables.policy_bridge() {
	#ip route add default equalize nexthop dev "${INT}" nexthop dev "${INT}"

    ${CMD_IPT} -t "${IPTABLE_CHAIN_NAT}" -A POSTROUTING \
        -m statistic --mode nth --every 2 --packet 0 \
        -j SNAT --to-source 0.0.0.0
    ${CMD_IPT} -t "${IPTABLE_CHAIN_NAT}" -A POSTROUTING \
        -m statistic --mode random --probability 0.5 \
        -j SNAT --to-source 0.0.0.0
}
iptables.policy_limit() {
    ${CMD_IPT} -I INPUT \
        -m connlimit --connlimit-above "${NETWORK_CONNECTION_LIMIT}" --connlimit-mask 0 \
        -j  "${IPTABLE_POLICY_ICMP}"
}
iptables.policy_classify() {
    ${CMD_IPT} -A "${IPTABLE_CHAIN_CLASS}" -j CLASSIFY --set-class 1:12
    ${CMD_IPT} -A "${IPTABLE_CHAIN_CLASS}" -m length --length :68 \
		-j CLASSIFY --set-class 1:10
    ${CMD_IPT} -A "${IPTABLE_CHAIN_CLASS}" -p ICMP \
		-j CLASSIFY --set-class 1:11
    ${CMD_IPT} -A "${IPTABLE_CHAIN_CLASS}" -p TCP --tcp-flags SYN,RST,ACK SYN \
		-j CLASSIFY --set-class 1:11
    ${CMD_IPT} -A "${IPTABLE_CHAIN_CLASS}" -p UDP \
		-j CLASSIFY --set-class 1:11

    #${CMD_IPT} -A "${IPTABLE_CHAIN_CLASS}" -p ICMP --icmp-type ${!s} \
    #    -j CLASSIFY --set-class 1:xx
    #${CMD_IPT} -A "${IPTABLE_CHAIN_CLASS}" -p  -m multiport --ports ${!s} \
    #    -j CLASSIFY --set-class 1:xx
}
iptables.policy_log() { 
    export IPTABLE_MSG_FRAG 
    export IPTABLE_MSG_FLOOD 
    export IPTABLE_MSG_LOOP 
    export IPTABLE_MSG_BCAST 
    export IPTABLE_MSG_LAN 
    export IPTABLE_MSG_ICMP 
    export IPTABLE_MSG_ICMP_ACCEPT 

    ${CMD_IPT} -A "${IPTABLE_CHAIN_LOG}" -f
		-j LOG --log-prefix "${IPTABLE_MSG_FRAG}"
    ${CMD_IPT} -A "${IPTABLE_CHAIN_LOG}" -m limit --limit 1/s --limit-burst 15 \
		-j LOG --log-prefix "${IPTABLE_MSG_FLOOD}"
    ${CMD_IPT} -A "${IPTABLE_CHAIN_LOG}" -i lo ! -s 127.0.0.1 \
		-j LOG --log-prefix "${IPTABLE_MSG_LOOP}"
    ${CMD_IPT} -A "${IPTABLE_CHAIN_LOG}" -i lo -d "${HOST_NETWORK}"
		-j LOG --log-prefix "${IPTABLE_MSG_BCAST}"
    ${CMD_IPT} -A "${IPTABLE_CHAIN_LOG}" -i lo ! -d 127.0.0.1 \
		-j LOG --log-prefix "${IPTABLE_MSG_LAN}"
    ${CMD_IPT} -A "${IPTABLE_CHAIN_LOG}" -i lo -m limit --limit 3/s --limit-burst 3 \
		-j LOG --log-prefix "${IPTABLE_MSG_ICMP}"
}
iptables.policy_frag() {
    ${CMD_IPT} -A INPUT -f -j DROP
}
iptables.policy_flood() {
    local item

    ${CMD_IPT} -A "${IPTABLE_CHAIN_FLOOD}" \
        -p ICMP -m limit --limit 4/s --limit-burst 4 \
        -j "${IPTABLE_CHAIN_FLOOD}-icmp"
    ${CMD_IPT} -A "${IPTABLE_CHAIN_FLOOD}" \
        -p TCP --tcp-flags SYN,RST,ACK SYN \
        -m limit --limit 1/s --limit-burst 3 \
        -j "${IPTABLE_CHAIN_FLOOD}-tcp"

    #for item in "${IPTABLE_PROTOCOL}"; do
    #    [[ ${item} == +(*/*) ]] && continue

    #${CMD_IPT} -A "${IPTABLE_CHAIN_FLOOD}" \
    #    -p "${item^^}" -m limit --limit 3/s --limit-burst 15 \
    #    -j "${IPTABLE_CHAIN_FLOOD}-${item}"

    #done
}
iptables.policy_lan() {
    local item itemT itemP

    for item in "${IPTABLE_LAN_ACCEPT[@]}"; do
        if [[ ${item} == +(*/*) ]]; then  # ICMP
            ${CMD_IPT} -A "${IPTABLE_CHAIN_LAN}" --icmp-type "${item}" \
                -i "${INT}" -d "${HOST_ADDRESS}" \
                -j ACCEPT
        else
            itemT="${item%:*}" itemP="${item#*:}"

            ${CMD_IPT} -A "${IPTABLE_CHAIN_LAN}" -p "${itemT}" \
                -i "${INT}" -d "${HOST_ADDRESS}" -m multiport --dport "${itemP}" \
                -j ACCEPT
        fi
    done

    #${CMD_IPT} -A "${IPTABLE_CHAIN_LAN}" -i "${INT}" -J "${IPTABLE_POLICY_ICMP}"
}
iptables.policy_accept() {
    local item itemC itemM itemP itemT

    for item in "${IPTABLE_ACCEPT[@]}"; do
        if [[ ${item} == +(*/*) ]]; then  # ICMP
            ${CMD_IPT} -A "${IPTABLE_CHAIN_ICMP}" -p ICMP \
                -i "${INT}" -d "${HOST_ADDRESS}" --icmp-type "${item}" \
                -j LOG --log-prefix "${IPTABLE_MSG_ICMP_ACCEPT}" 

            ${CMD_IPT} -A "${IPTABLE_CHAIN_ICMP}" -p ICMP \
                -i "${INT}" -d "${HOST_ADDRESS}" \
                --icmp-type "${item}" -m conntrack --ctstate NEW \
                -J ACCEPT

        else
            itemT="${item%:*}" itemP="${item#*:}"
            itemC="IPTABLE_CHAIN_${itemT}" itemM="IPTABLE_MSG_${itemT}"

            ${CMD_IPT} -A "${!itemC}" -p "${itemT}" \
                -i "${INT}" -d "${HOST_ADDRESS}" -m multiport --dport "${itemP}" \
                -j LOG --log-prefix "${!itemM}" 

            ${CMD_IPT} -A "${!itemC}" -p "${itemT}" \
                -i "${INT}" -d "${HOST_ADDRESS}" \
                -m multiport --dport "${itemP}" \
                -m conntrack --ctstate NEW \
                -J ACCEPT
        fi

        #print_msg '[%s:%s] (%s/%s)kbps' "$intIn" "$ipIn" "$limitUp" "$limitDown"
    done

    #${CMD_IPT} -A "${IPTABLE_CHAIN_ICMP}" -i "${INT}" -J "${IPTABLE_POLICY_ICMP}"
    #${CMD_IPT} -A "${IPTABLE_CHAIN_TCP}" -i "${INT}" -J "${IPTABLE_POLICY_TCP}"
    #${CMD_IPT} -A "${IPTABLE_CHAIN_UDP}" -i "${INT}" -J "${IPTABLE_POLICY_UDP}"
}
iptables.policy_list() {
    local item items=( $(sudo $(exe grep) . "${DIR_PROC}/net/ip_tables_names") )

    for item in "${items[@]}"; do
        if (( long )); then
            ${CMD_IPT} -xvn --list -t "${item}"
        else
            ${CMD_IPT} --list-rules -t "${item}"
        fi
    done
	#${CMD_IPT} -t raw -A OUTPUT -j TRACE
}

iptables.queue_remove() {
    export IPTABLE_QUEUE_COUNT '3'
    local int item

    ${CMD_TC} qdisc del dev "${INT}" ingress

    for item in (( int=0; int<IPTABLE_QUEUE_COUNT;int++ )); do
        ${CMD_TC} qdisc del dev "${INT}" root handle "${int}:0"
        #${CMD_TC} filter del dev "${INT}" pref "${int}"
        #${CMD_TC} filter del dev "${INT_TC}" pref "${int}"
    done
    ${CMD_TC} filter del dev "${INT}"
    ${CMD_TC} filter del dev "${INT_TC}"
}
iptables.queue_create() {
    local int item

    ${CMD_TC} qdisc add dev "${INT}" ingress

    for item in (( int=0; int<IPTABLE_QUEUE_COUNT;int++ )); do
        ${CMD_TC} qdisc add dev "${INT}" root handle "${int}:0" \
            htb default "${int}:${IPTABLE_QUEUE_COUNT}" r2q 10
        ${CMD_TC} class add dev "${INT}" parent "${int}:0" classid "${int}:1" \
            htb rate "${NETWORK_LIMIT_UP}kbps"
        #${CMD_TC} filter add dev "${INT}" pref ${int}
        #${CMD_TC} filter add dev "${INT_TC}" pref ${int}
    done

    itemR=$(( limitUp / IPTABLE_QUEUE_COUNT ))
    itemB=$(( rate * rateMultiply ))
    for item in (( int=0; int<IPTABLE_QUEUE_COUNT;int++ )); do
        ${CMD_TC} class add dev "${INT}" parent "1:1" classid "1:${int}" \
            htb rate "${itemR}kbps" ceil "${itemRkbps}" burst "${itemB}kb" prio "${int}"
        ${CMD_TC} qdisc add dev "${INT}" parent "1:${int}" handle "1:${int}" \
            sfq perturb 10
        ${CMD_TC} filter add dev "${INT}" parent "1:0" \
            protocol ip prio "${int}" \
            fw flowid "1:${int}"
    done

    #${CMD_TC} filter add dev "${INT}" parent 1:0 \
        protocol ip prio 0 u32 match u32 0 0 \
        flowid 1:1 action mirred egress redirect dev "${INT_TC}"
}
iptables.queue_list() {
    if (( long )); then
        ${CMD_TC} -s qdisc show dev "${INT}"
        ${CMD_TC} -s qdisc show dev "${INT_TC}"
        ${CMD_TC} -s class show dev "${INT}"
        ${CMD_TC} -s class show dev "${INT_TC}"
        ${CMD_TC} -s filter show dev "${INT}"
        ${CMD_TC} -s filter show dev "${INT_TC}"

    else
        ${CMD_TC} qdisc show dev "${INT}"
        ${CMD_TC} qdisc show dev "${INT_TC}"
        ${CMD_TC} class show dev "${INT}"
        ${CMD_TC} class show dev "${INT_TC}"
        ${CMD_TC} filter show dev "${INT}"
        ${CMD_TC} filter show dev "${INT_TC}"
    fi
}

iptables.module() {
    local item unload

    while [[ ${1} == +(-)* ]]; do
        item="${1##*+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            u|unload) (( unload++ )) ;;
        esac
        shift
    done

    if (( unload )); then
        modprobe -r "${IPTABLE_MODULE_TC[@]}"
        modprobe -r "${IPTABLE_MODULE[@]}"
    else
        modprobe "${IPTABLE_MODULE[@]}"
        modprobe "${IPTABLE_MODULE_TC[@]}"

        for item in "${IPTABLE_SYSCTL[@]}"; do
            $(exe sysctl) -w "${item}"
            #$(exe sysctl) -w "${IPTABLE_SYSTCL[@]}"
        done
    fi
}

#####

iptables() { sudo ${CMD_IPTABLES} "${@}"; }
iptables.drop() {
    ipt -A INPUT -i "${INT}" -s "${item}" -j DROP
}
iptables.accept() {
    ipt -A INPUT -i "${INT}" -s "${item}" -j ACCEPT
}

tc() { sudo ${CMD_TC} "${@}"; }
ip() { ${CMD_IP} "${@}"; }

#####

iptables.arguments() {
    local item 
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                i|interface) ITEMS=( "${ARG_OPT}" ) ;;
                I|init) ACTION='init' ;;
            esac
        done
    done

    [[ ${ACTION} && ${ITEMS} ]]
}

iptables.variables
source 'requirement'
return 2>/dev/null

_ARGS='(i)nterface'
_DESC="
    Interface, apply to interface."

iptables.arguments "${@:-?}" || exit

parse.pid --lock "${PID_IPTABLE}" || exit
trap '{ parse.pid --term "${PID_IPTABLE}"; }' EXIT

case "${ACTION}" in
    init)
        iptables.module

        for INT in "${ITEMS[@]}"; do
            # Flush rules, set default targets.
            iptables.table_policy

            iptables.chain_create

        done

        $(exe ip) link set "${INT_TC}" up
    ;;
    halt)
        iptables.chain_flush
        iptables.chain_remove

        iptables.module -unload
        $(exe ip) link set "${INT_TC}" down
    ;;
    list)
        (( queue )) && iptables.queue_list
        (( policy )) && iptables.policy_list
        #$(exe tcpdump) -n -i "${INT_TC} -x -e -t
    ;;
    stat)
        [[ -d ${DIR_SYS}/module/nf_conntrack_ipv4 &&
            -e ${DIR_SYS}/module/nf_conntrack_ipv4/refcnt ]] &&
            (( $(</sys/module/nf_conntrack_ipv4/refcnt) )) ||
                print_false 'Invalid!\n'
    ;;
esac

