#!/usr/bin/env bash
source 'bash.sh'

loop.variables() {
    export PID_LOOP "${DIR_TMP_PID}/loop.pid"
    export LOG_LOOP "${DIR_TMP_LOG}/loop.log"
}

loop.exit() {
    printf '[%.04d] (%s)  %-40s STOP\n' \
        $(( DIGIT*60 )) "$($(exe date) "+${FORMAT_DATE}")" "{${TASK[*]}}" \
        &>>"${LOG_LOOP}"
    parse.pid --term "${PID_LOOP}_${$}"
    #kill -term "${$}"
}

loop.arguments() {
    local item 
    unset ACTION TASK
    DIGIT=15
    FORMAT_DATE="${FORMAT_DATE_LOOP:-%Y.%m.%d %R:%S %s}"

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && TASK=( "${ARG_OPT[*]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                L|log) LOG_LOOP="${ARG_OPT}" ;;
                D|date) FORMAT_DATE="${ARG_OPT[*]}" ;;
                *([[:digit:]])) DIGIT="${ARG}" ;;
            esac
        done
    done

    (( DIGIT > 0 )) || return
    [[ ${TASK} ]]
}

loop.variables
return 2>/dev/null

_ARGS='(L)og (P)id (D)ate (d)ns (s)shd (b)attery (r)epository (f)an (interval)'
_DESC="Loop.
    :path:Log,Pid
    :fmt:Date
    Log, path for written stdout & stderr.
    Pid, path for written pid.
    Date, a 'date' format string.
    Interval, minute count of iteration; default 15."

loop.arguments "${@:-?}" || exit

case "${ACTION:-0}" in
    *)
        {
        parse.pid "${PID_LOOP}_${$}" "${$}"
        trap '{ loop.exit; }' EXIT 

        printf '[%.04d] (%s)  %-40s INIT\n' \
            $(( DIGIT*60 )) "$($(exe date) "+${FORMAT_DATE}")" "{${TASK[*]}}"
        while sleep $(( DIGIT*60 )); do
            for item in "${TASK[@]}"; do
                eval ${item} &>/dev/null; rtn="${?}"
                printf '[%.04d] (%s)  %-40s %s' \
                    "$(( DIGIT*60 ))" \
                    "$($(exe date) "+${FORMAT_DATE}")" \
                    "{${item}}" \
                    "$( if (( rtn == 0 )); then
                            print_true 'True\n'
                        elif (( rtn == 1 )); then
                            print_false 'False\n'
                        elif (( rtn == 3 )); then
                            print_warn 'Skip\n'
                        else
                            print_false 'Error\n'
                        fi 2>&1
                    )"
            done
        done
        } &>>"${LOG_LOOP}"
    ;;
esac
