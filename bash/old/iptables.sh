#!/bin/env bash

variables() {
    local item
    unset ip_lo ip_eg int_lo int_eg ip_gateway int_in ip_in
    
    modules=(
	'iptable_nat'
	'nf_nat_ipv4'
	'nf_nat'
	'iptable_raw'
	'iptable_mangle'
	'iptable_security'
	#'ip_tables'
	#'iptable_filter'
	#'x_tables'
	'xt_conntrack'
	'nf_conntrack_ipv4'
	'nf_conntrack'
	'tcp_htcp'
	'xt_set'
	#'nfnetlink'
	'ip_set_hash_net'
	'ip_set'
	#'tcp_cubic'
    )
    modules_tc=( 'ifb' )
    sysctls=(
	"net.ipv4.tcp_rmem='4096 87380 33554432'"
	"net.ipv4.tcp_wmem='4096 65536 33554432'"
	#"net.ipv4.tcp_mem='8388608 8388608 8388608'"
	'net.core.netdev_max_backlog=2000'
	'net.core.rmem_default=65536'
	'net.core.rmem_max=8388608'
	'net.core.wmem_default=65536'
	'net.core.wmem_max=8388608'
        'net.ipv4.conf.all.accept_redirects=0' 
	'net.ipv4.conf.all.accept_redirects=0'
        'net.ipv4.conf.all.accept_source_route=0' 
        'net.ipv4.conf.all.log_martians=1' 
        'net.ipv4.conf.all.rp_filter=1'
        'net.ipv4.icmp_echo_ignore_all=0' 
        'net.ipv4.icmp_echo_ignore_broadcasts=1' 
        'net.ipv4.icmp_ignore_bogus_error_responses=1'
	'net.ipv4.ip_forward=0'
	'net.ipv4.tcp_congestion_control=cubic'
	'net.ipv4.tcp_dsack=1'
        'net.ipv4.tcp_ecn=0' 
	'net.ipv4.tcp_fin_timeout=30'
        'net.ipv4.tcp_keepalive_intvl=30'
        'net.ipv4.tcp_keepalive_probes=5'
	'net.ipv4.tcp_max_orphans=8192'
	'net.ipv4.tcp_max_syn_backlog=1024'
        'net.ipv4.tcp_no_metrics_save=1'
	'net.ipv4.tcp_sack=1'
	'net.ipv4.tcp_synack_retries=1'
	'net.ipv4.tcp_syncookies=1'
        'net.ipv4.tcp_tw_recycle=1'
        'net.ipv4.tcp_tw_reuse=1'
	'net.ivp4.route.flush=1'
	'net.netfilter.nf_conntrack_acct=1'
        'tcp_timestamps=1'
	'tcp_window_scaling=1'

	#'accept_ra=0'
        #'accept_redirects=1'
        #'accept_source_route=1'
        #'autoconf=0'
        #'icmp_echo_ignore_all=0'
        #'icmp_echo_ignore_bradcasts=1'
        #'icmp_ignore_bogus_error_resonses=1'
        #'ip_default_ttl'
        #'ip_dynaddr=0'
        #'ip_forard=1'
        #'ip_local_port_range'
        #'ip_no_pmtu_disc=1'
        #'ip_queue_maxlen=2048'
        #'log_martians=0'
        #'net.netfilter.nf_conntrack_acct=1'
        #'net.netfilter.nf_conntrack_udp_timeout=60'
	#'net.nf_conntrack_max=8192'
        #'rp_filter=1'
        #'send_redirects=0'
        #'tcp_dsack=1'
        #'tcp_ecn=0'
        #'tcp_fack=1'
        #'tcp_low_latency=0'
        #'tcp_rfc1337=1'
        #'tcp_sack=1'
        #'tcp_syn_retries=3'
    )

    #$(<"${PROC}/net/ip_tables_names") )
    tables=(
	'filter'
	'nat'
	'mangle'
	'raw'
	'security'
    )
    
    # table:chain
    chains=(
	'log'
	'block'
	'tcp'
	'udp'
	'icmp'
	'loop'
	'lan'
	'mangle:shapeout'
    )

    # chain:policy
    policies=(
	'loop:ACCEPT'
	'nat:ACCEPT'
    )
    policies_filter=(
	'INPUT:DROP'
	'FORWARD:DROP'
	'OUTPUT:ACCEPT'
    )
    policy_nats=(
	10.0.0.0/8
	192.168.0.0/16
    )
    policy_input_tcp_src_ports=(
    )
    policy_input_tcp_dst_ports=(
	#53
	#80
	#443
	#587
	#993
	#13800:13850
    )
    policy_input_udp_src_ports=(
    )
    policy_input_udp_dst_ports=(
	#53
	#67
	#13780
    )
    policy_output_tcp_src_ports=(
    )
    policy_output_tcp_dst_ports=(
	#53
	#80
	#443
	#587
	#993
	8890
    )
    policy_output_udp_src_ports=(
	#53
    )
    policy_output_udp_dst_ports=(
	#53
    )
    policy_block_nets=(
	#10.0.0.0/8
	#192.168.0.0/16
	#'0.0.0.0/8:67'
	'0.0.0.0/8'
	127.0.0.0/8
	169.254.0.0/16
	172.16.0.0/12
	192.0.2.0/24
	224.0.0.0/4
	240.0.0.0/5
	248.0.0.0/5
	255.255.255.255/32
    )
    policy_block_ips=(
    )
    policy_block_tcp_ports=(
    )
    policy_block_udp_ports=(
	137
    )
    policy_block_exips=(
    )
    policy_block_macs=(
    )
    policy_block_icmp_types=(
	#'echo-reply'
	#'echo-request'
	#'destination-unreachable'
	#'fragmentation-needed'
	#'source-quench'
	#'time-exceeded'
	#'parameter-problem'
    )

    local dir int item
    for dir in /sys/class/net/*; do
	if [[ $(<"${dir}/flags") == 0x+(109|9) ]]; then
	    # loopback
	    int=( $(<"${dir}/uevent") )
	    for item in "${int[@]}"; do
		[[ ${item} == +(INTERFACE=*) ]] || continue
		int_lo="${item#*=}"
		break
	    done
	    
	elif [[ $(<"${dir}/flags") == 0x+(1003|1103) ]]; then
	    # egress
	    int=( $(<"${dir}/uevent") )
	    for item in "${int[@]}"; do
		[[ ${item} == +(INTERFACE=*) ]] || continue
		int_eg="${item#*=}"
		break
	    done    
	fi
    done
    [[ ${int_lo} && ${int_eg} ]] || error 'Invalid Network Interface'
    [[ ${int_in} ]] || int_in="${int_eg}"

    # Route
    #ip route show scope global |grep '^default' |
    #while read nil nil gateway nil; do
    #	ip_gateway="${gateway}"
    #done
    
    ip_lo=( $(ip -o -f inet addr show dev "${int_lo}" | awk '{printf "%s ", $4}') )
    ip_eg=( $(ip -o -f inet addr show dev "${int_eg}" | awk '{printf "%s ", $4}') )
    ip_in=( $(ip -o -f inet addr show dev "${int_in}" | awk '{printf "%s ", $4}') )
    [[ ${ip_lo} ]] || ip_lo='127.0.0.1/24'
    [[ ${ip_eg} ]] || ip_eg='0.0.0.0/24'
    [[ ${ip_in} ]] || ip_in="${ip_eg}"
}

modules_unload() {
    mp_r -a "${modules_tc[@]}" "${modules[@]}"
}
modules() {
    mp -a "${modules[@]}" "${modules_tc[@]}"
}
sysctls() {
    local item
    
    for item in "${sysctls[@]}"; do
        sctl -w "${item}"
    done
}

tables_flush() {
    local table

    for table in "${tables[@]}"; do
        ipt -t "${table}" -F
        ipt -t "${table}" -X
        ipt -t "${table}" -Z
    done
}
tables() {
    tables_flush
}
chains() {
    local chain table

    for chain in "${chains[@]}"; do
	unset table 
	[[ ${chain} == +(*:*) ]] && table="${chain%:*}" chain="${chain#*:}" 
        if [[ ${table} ]]; then
            ipt -t "${table}" -N "${chain}" 
        else
            ipt -N "${chain}"
        fi
    done
}

policy_default() {
    local chain 
    
    for chain in "${policies_filter[@]}"; do
	unset policy
	if (( accept )); then
	    policy='ACCEPT' chain="${chain%:*}"
	else
	    policy="${chain#*:}" chain="${chain%:*}"
	fi
	[[ ${policy} && ${chain} ]] || error 'No policy'
        ipt -P "${chain}" "${policy}"
    done
}
policy_accept() {
    (( accept++ ))
    policy_default
}
policy_log() {
    #ipt -A log -j LOG --log-prefix 'Log:'

    #ipt -A log -m limit --limit 3/s --limit-burst 10 -j LOG --log-prefix 'Flood:'
    
    ipt -A log -f -j LOG --log-prefix 'Frag:'
    ipt -A log -m state --state INVALID -j LOG --log-prefix 'Invalid:'
    
    ipt -A log -i "${int_lo}" ! -s "${ip_lo}" -j LOG --log-prefix 'Invalid_Loop:'
    ipt -A log -i "${int_lo}" ! -d "${ip_lo}" -j LOG --log-prefix 'Invalid_Loop:'
    ipt -A log -i "${int_lo}" -d "${ip_eg}" -j LOG --log-prefix 'Invalid_Nat:'
    ipt -A log -i "${int_lo}" -d "${ip_lo}" -j LOG --log-prefix 'Loop:'

    ipt -A log -p tcp -i "${int_eg}" \
	--tcp-flags SYN,RST,ACK SYN -m limit --limit 1/s --limit-burst 3 \
        -j LOG --log-prefix 'Flood_TCP:'
    
    ipt -A log -p icmp -m limit --limit 4/s --limit-burst 4 -j LOG --log-prefix 'Flood_ICMP:'
}
policy_forward() {
    if [[ ${int_in} != ${int_eg} ]]; then
	ipt -A FORWARD -i "${int_in}" -o "${int_eg}" -j ACCEPT
	ipt -A FORWARD -i "${int_in}" -o "${int_eg}" -m state --state ESTABLISHED,RELATED -j ACCEPT
    fi
}
policy_nat() {
    local nat
    #10.0.0.0/8
    #192.168.0.0/16
    # Nat addr?
    set -x
    if [[ ${ip_in} == +(192.168|10.)* ]]; then
	for nat in "${policy_nats[@]}"; do
	    # Only valid traffic
	    case "${nat#**/}" in
		16) [[ ${ip_in} == ${nat%.*.*} ]] || break ;;
		8) [[ ${ip_in} == ${nat%.*.*.*} ]] || break ;;
	    esac

    	    ipt -A nat -i "${int_in}" -s "${}"  -d "${ip_in}" -j DROP
    	    ipt -A nat -o "${int_eg}" ! -s "${ip_eg}" -j ACCEPT
	    
	    ipt -A nat -i "${int_in}" -s "${nat}" -j ACCEPT
	    ipt -A nat -o "${int_eg}" -d "${nat}" -j ACCEPT
	done
    fi
    set +x
}
policy_snat() {
    ipt -A POSTROUTING -t nat -s "${ip_in}" -o "${int_eg}" -j SNAT --to-source "${addr}"
}
policy_loop() {
    ipt -A loop -i "${int_lo}" ! -s "${ip_lo}" -j DROP
    ipt -A loop -i "${int_lo}" ! -d "${ip_lo}" -j DROP
    ipt -A loop -i "${int_lo}" -d "${ip_eg}" -j DROP
    
    ipt -A loop -i "${int_lo}" -o "${int_lo}" -j DROP
    ipt -A loop -s "${ip_lo}" -d "${ip_lo}" -j DROP
}
policy_block_lan() {
    local addr port ports

    ipt -A lan -p udp -i "${int_eg}" -s '0.0.0.0/32' -d '255.255.255.255/32' --dport 67 -j ACCEPT
    
    for addr in "${policy_block_nets[@]}"; do
	#unset port ports
	#[[ ${addr} == *+(:)* ]] && ports=( "${addr#*:}" ) addr="${addr%%:*}"
	#if [[ ${ports} ]]; then
	#    for port in "${ports}"; do
	#	ipt -A lan -p tcp -i "${int_eg}" -s "${addr}" --dport "${port}" -j ACCEPT
	#    done
	#fi
	ipt -A lan -i "${int_eg}" -s "${addr}" -j block
    done
}
policy_block_ip() {
    local addr

    for addr in "${policy_block_ips[@]}"; do
	ipt -I INPUT -s "${addr}" -j block
    done
}
policy_block_mac() {
    local addr port

    for addr in "${policy_block_macs[@]}"; do
	unset port
	port="${addr#*=}" addr="${addr%=*}"

	if [[ ${port} ]]; then
	    ipt -A tcp -p tcp --dport "${port}" -m mac --mac-source "${addr}" -j ACCEPT
	fi
	ipt -A block -m mac --mac-source "${addr}" -j block
    done
}
policy_block_icmp() {
    local item
    
    for item in "${policy_block_icmp_types[@]}"; do
	ipt -A icmp -p icmp -i "${int_in}" --icmp-type "${item}" -j block
    done
    
    ipt -A icmp -p icmp -i "${int_in}" -m limit --limit 4/s --limit-burst 4 -j ACCEPT
}
policy_block_tcp() {
    ipt -A tcp -p tcp -i "${int_in}" ! --syn -m state --state NEW -j block
    ipt -A tcp -p tcp -i "${int_in}" --tcp-flags ALL NONE -j block
    ipt -A tcp -p tcp -i "${int_in}" --tcp-flags ALL ALL -j block
    ipt -A tcp -p tcp -i "${int_in}" --tcp-flags ALL FIN,URG,PSH -j block
    ipt -A tcp -p tcp -i "${int_in}" --tcp-flags ALL SYN,RST,ACK,FIN,URG -j block
    ipt -A tcp -p tcp -i "${int_in}" --tcp-flags SYN,RST SYN,RST -j block
    ipt -A tcp -p tcp -i "${int_in}" --tcp-flags SYN,FIN SYN,FIN -j block
    ipt -A tcp -p tcp -i "${int_in}" -s 0/0 -d 0/0 --syn -j block
    ipt -A tcp -p tcp -i "${int_in}" --tcp-flags SYN,RST,ACK SYN \
	-m limit --limit 1/s --limit-burst 3 -j ACCEPT
    ipt -A tcp -p tcp -i "${int_in}" --tcp-flags SYN,ACK,FIN,RST RST \
	-m limit --limit 1/s -j ACCEPT

    local port
    for port in "${policy_block_tcp_ports[@]}"; do
	ipt -A tcp -p tcp -i "${int_eg}" --dport "${port}" -j block
    done
}
policy_block_udp() {
    local port
    
    for port in "${policy_block_udp_ports[@]}"; do
	ipt -A udp -p udp -i "${int_eg}" --dport "${port}" -j block
    done
}
policy_tcp() {
    local port

    for port in "${policy_input_tcp_src_ports[@]}"; do
	case "${port}" in
	    *) false ;;
	esac || ipt -A tcp -p tcp -o "${int_eg}" \
	    -m state --state NEW --sport "${port}" --syn -j ACCEPT
	    #--destination-port 1024:65535 --syn -j ACCEPT
    done
    for port in "${policy_input_tcp_dst_ports[@]}"; do
	case "${port}" in
	    22)
		ipt -A tcp -p tcp -i "${int_in}" --dport "{port}" \
		    -m state --state NEW -m recent --update --seconds 60 --hitcount 4 \
		    -j REJECT --reject-with tcp-reset
		ipt -A tcp -p tcp -i "${int_in}" --dport "${port}" \
		    -m state --state NEW -m recent --set
		;;
	    *) false ;;
	esac || ipt -A tcp -p tcp -i "${int_in}" \
	    -m state --state NEW --dport "${port}" --syn -j ACCEPT
    done

    # Output
    for port in "${policy_output_tcp_src_ports[@]}"; do
	ipt -A tcp -p tcp -o "${int_eg}" tcp --sport "${port}" -j ACCEPT
    done
    for port in "${policy_output_tcp_dst_ports[@]}"; do
	ipt -A tcp -p tcp -o "${int_eg}" --dport "${port}" -j ACCEPT
    done

}
policy_udp() {
    local port

    for port in "${policy_input_udp_src_ports[@]}"; do
	ipt -A udp -p udp -i "${int_in}" -m state --state NEW --sport "${port}" -j ACCEPT
	    #--destination-port 1024:65535 -j ACCEPT
    done
    for port in "${policy_input_udp_dst_ports[@]}"; do
	ipt -A udp -p udp -i "${int_in}" -m state --state NEW --dport "${port}" -j ACCEPT
	    #--destination-port 1024:65535 -j ACCEPT
    done

    # Output
    for port in "${policy_output_udp_src_ports[@]}"; do
	ipt -A udp -p udp -o "${int_eg}" --sport "${port}" -j ACCEPT
    done
    for port in "${policy_output_udp_dst_ports[@]}"; do
	ipt -A udp -p udp -i "${int_in}" --dport "${port}" -j ACCEPT
    done

}
policy_icmp() {
    local item
    
    for item in "${policy_input_icmp[@]}"; do
	ipt -A icmp -p icmp -i "${int_in}" --icmp-type "${item}" -j ACCEPT
    done
    
    # Output
    #for item in "${policy_output_icmp[@]}"; do
    #	ipt -A icmp -o "${int_eg}" --icmp-type "${item}" -j ACCEPT
    #done
}
policy_block() {
    policy_block_lan
    policy_block_ip
    policy_block_mac
    policy_block_icmp
    policy_block_tcp
    policy_block_udp
    
    ipt -A block -j LOG --log-prefix 'Block:'
    ipt -A block -j DROP
}
policy_input() {
    ipt -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
    ipt -A INPUT -m state --state INVALID -j DROP
    ipt -A INPUT --fragment -j DROP
    
    ipt -A INPUT -j log
    ipt -A INPUT -i "${int_lo}" -j loop
    ipt -A INPUT -i "${int_eg}" -j lan
    #[[ ${ip_in} == +(192.168|10.)* ]] && ipt -A INPUT -i "${int_in}" -j nat
    ipt -A INPUT -p tcp -j tcp
    ipt -A INPUT -p udp -j udp
    ipt -A INPUT -p icmp -j icmp
    
    ipt -A INPUT -i "${int_eg}" -j 'LOG' --log-prefix 'Drop:'
}
policy_output() {
    ipt -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
    ipt -A OUTPUT -m state --state INVALID -j DROP
    
    ipt -A OUTPUT -j log
    ipt -A OUTPUT -o "${int_lo}" -j loop
    ipt -A OUTPUT -o "${int_eg}" -j lan
    #[[ ${ip_in} == +(192.168|10.)* ]] && ipt -A OUTPUT -o "${int_eg}" -j nat
    ipt -A OUTPUT -p tcp -j tcp
    ipt -A OUTPUT -p udp -j udp
    ipt -A OUTPUT -p icmp -j icmp

    ipt -A OUTPUT -o "${int_eg}" -j 'LOG' --log-prefix 'New:'
}
policy() {
    policy_default
    policy_log
    policy_loop
    #policy_nat
    #policy_forward
    policy_input
    policy_output
    policy_block
    policy_tcp
    policy_udp
    policy_icmp
}
policy_ports() {
    policy_tcp
    policy_udp
}

level1() {
    level1_get && policy_level1
}
level1_get() {
    # Refresh on every call, grab list modifications.
    ips list LEVEL1 &>/dev/null || ips create LEVEL1 hash:net maxelem 262144
    
    printf 'Getting %s\n' 'list.iblocklist.com'
    curl -s -L "http://list.iblocklist.com/?list=bt_level1&fileformat=p2p&archiveformat=gz" |
    gunzip |
    cut -d':' -f2 |
    grep -E "^[-0-9.]+$" |
    gawk '{print "add LEVEL1 "$1}' |
    ips restore -exist && printf 'Got %s List\n' 'Level1'
}
policy_level1() {
    ipt -C INPUT -m set --match-set LEVEL1 src -j DROP && return
    
    ipt -I INPUT -m set --match-set LEVEL1 src -j DROP
    ipt -I OUTPUT -m set --match-set LEVEL1 dst -j DROP
}

ipt() {
    local item="$(type -P iptables)"
    [[ ${item} ]] || error "${item} not found"
    if (( PORTS )); then
	sudo ${item} "${@//-A/-C}" || sudo ${item} "${@}"
    else
	sudo ${item} "${@}"
    fi
}
ips() {
    local item="$(type -P ipset)"
    [[ ${item} ]] || error "${item} not found"
    sudo ${item} "${@}"
}
tc() {
    local item="$(type -P tc)"
    [[ ${item} ]] || error "${item} not found"
    sudo ${item} "${@}"
}
ip() {
    local item="$(type -P ip)"
    [[ ${item} ]] || error "${item} not found"
    ${item} "${@}"
}
mp_r() {
    mp -r "${@}"
}
mp() {
    local item="$(type -P modprobe)"
    [[ ${item} ]] || error "${item} not found"
    sudo ${item} "${@}"
}
sctl() {
    local item="$(type -P sysctl)"
    [[ ${item} ]] || error "${item} not found"
    sudo ${item} "${@}" &>/dev/null
}

status() {
    local table

    if (( long )); then
	for table in "${tables[@]}"; do
	    printf 'table: %s\n' "${table}"
	    ipt -xvnL -t "${table}"
	done
    elif (( short )); then
        ipt --list-rules
    else
	ipt -xvnL
    fi
}
halt() {
    tables_flush
    policy_accept
    
    modules_unload
    printf '%s\n' 'Iptables De-Initialized'
}
init_queue() {
    ip link set "${int_tc}" up
}
init() {
    modules
    sysctls
    
    tables
    chains

    policy
    printf '%s\n' 'Iptables Initialized'
}

error() {
    printf '%s\n' "${@}"
    exit 1
}

[[ ${DEBUG} ]] && set -x
variables
action="${1}"; shift
case "${action}" in
    init|start) init "${@}" ;;
    show|list|status)
	[[ ${1} == long ]] && (( long++ ))
	[[ ${1} == short ]] && (( short++ ))
	status
	;;
    stop|halt|clear|flush) halt "${@}" ;;
    restart)
	${0} halt
	${0} init
	;;
    level1) level1 ;;
    ports) PORTS=1 policy_ports ;;
    *)
	printf 'Usage: %s\n\t%s\n\t%s\n\t%s\n' \
	    '(init|show|halt|restart)' \
	    'init: Start script' \
	    'show: status' \
	    'halt: clear policies' \
	    'restart: clear and load policies'
	;;
esac
[[ ${DEBUG} ]] && set +x
    
# Modules
# Sysctl
:<<DOCU
 [-t table] -[A|C|D] chain rule
 .. -I chain [rule-num] rule
 .. -R chain rule-num rule
 .. -D chain rule-num
 .. -S [chain [rule-num]]
 .. -[F|L|Z] [chain [rule-num]] [options]
 .. -N chain
 .. -X [chain]
 .. -P chain target
 .. -E old-chain new-chain

 Targets
     ACCEPT
         Pass.
     DROP
         Drop.
     QUEUE
         Pass to userspace.
     NFQUEUE
     NOTRACK
     RETURN
         Stop traverse and resume at next rule.
         Default target is set by chain policy.
 
     Exensions
         AUDIT
             Record accept, drop, reject packets; auditd(8).
 
             type <accept|drop|reject>
                 Set type of record.
 
         CHECKSUM
             Valid table; mangle.
 
             checksum-fill
                 In a packet lacking a checksum, compte and fill.
 
         CLASSIFY
             Set skb->priority, CBQ class.
 
             set-class <major:minor>
                 Set MAJOR and MINOR value in hexadecimal.
 
         CLUSTERIP
             Statically distribute connections to nodes of IP/MAC address in a cluster.
 
             new
                 Create new cluster; required in first rule.
             hashmode <mode>
                 Set hashmode, valid; sourceip, sourceip-sourceport, sourceip-sourceport-destport.
             clustermac <mac>
                 Set MAC of cluster, link-layer multicast.
             total-nodes <num>
                 Set NUM of nodes for cluster.
             local-node <num>
                 Local NUM of node in cluster.
             hash-init <int>
                 Set random seed to INT for hashes.
 
         CONNMARK
             Set 32-bit mark of a connection.
 
             set-xmark <value[/mask]>
                 Set XOR VALUE and MASK bits as 0.
             save-mark [--nfmask <mask>] [--ctmask <mask>]
                 Copy nfmask mark to ctmask using MASK.
                 Default nfmask/ctmask, 0xFFFFFFFF.
                 ctmark=(ctmark&~ctmask)^(nfmark&nfmask)
             restore-mark [--nfmask <mask>] [--ctmask <mask>]
                 Valid table; mangle.
                 Copy ctmark mark to nfmask using MASK.
                 Default nfmask/ctmask, 0xFFFFFFFF.
                 nfmark=(nfmark&~nfmask)^(ctmark&ctmask)
 
             Mnemonics (set-xmark)
                 and-mark <bits>
                     Binary AND ctmark with BITS.
                 or-mark <bits>
                     Binary OR ctmark with BITS.
                 xor-mark <bits>
                     Binary XOR ctmark with BITS.
                 set-mark <value[/mask]>
                     Set mark to VALUE, restrain to bits of MASK.
                 save-mark [--mask <mask>]
                     Copy nfmark to ctmark, restrain to bits of MASK.
                 restore-mark [--mask <mask>]
                     Valid table, mangle.
                     Copy ctark to nfmark, restrain to bits of MASK.
 
         CONNSECMARK
             Copy mark of packets to connections and connections to packets, if unlabeled.
             Valid table, security, mangle.
 
             save
                 Copy packet mark to connection if not marked.
             restore
                 Copy connection mark to packet if not marked.
 
         CT
             Set connection tracking template.
             Valid table; raw.
 
             notrack
                 Disable connection tracking.
             helper <name>
                 Use helper of NAME.
             ctevents <event[,...]>
                 Generate EVENT.
 
                 Event
                     new
                     related
                     destroy
                     reply
                     assured
                     protoinfo
                     helper
                     mark (ctmark)
                     natsequinfo
                     secmark (ctsecmark)
             expevents <event[,...]>
                 Generate expectation EVENT.
 
                 Event
                     new
             zone <id>
                 Set zone ID; default 0.
             timeout <name>
                 Set timeout policy of NAME; default, /proc/sys/net/netfilter/nf_conntrack_*_timeout_*.
         DNAT
             Modify destination address and exempt rule matching.
             Valid table; nat.
             Valid chain; PREROUTING, OUTPUT, user-defined.
 
             to-destination [ipAddr[-ipAddr]][:port[-port]]
                 Set inclusive IPADDR and PORT ranges (TCP/UDP protocol) to be redirected.
             random
                 Randomize port mapping.
             persistent
                 Consitent address per connection; superseding SAME target.
 
         DSCP
             Modify DSCP bits of TOS in IPv4 packets.
             Valid table; mangle.
 
             set-dscp <int>
                 Set to INT, decimal or hexadecimal.
             set-dscp-class <class>
                 Set to DiffServ CLASS.
 
         ECN
             Avoid ECN blackholes
             Valid table; mangle.
             
             ecn-tcp-remove
                 Remove all ENC bits from TCP, protocol TCP.
 
         IDLETIMER
             Identify idle interfaces with labeled timers; remaining time available in sysfs, /sys/class/xt_idletimer/timers/LABEL; reset timer on match.
 
             timeout <int>
                 Trigger notification in seconds of INT.
             label <string>
                 Identify timer as STRING, maximum character length of 27.
 
         LOG
             Send to kernel log.
             Non-terminating target, only logs.
 
             log-level <lvl>
                 Log LVL, syslog.conf(5). 
             log-prefix <prefix>
                 Set message PREFIX, 29 character limit.
             log-tcp-sequence
                 Log TCP sequence numbers, security risk.
             log-tcp-options
                 Log TCP options.
             log-ip-options
                 Log IP options.
             log-uid
                 Log UID of generated packet.
 
         MARK
             Set 32-bit mark value of packets
             If routed, set in PREROUTING chain of mangle table.
 
             set-xmark <value[/mask]>
                 Set bits to 0 of MASK and XOR VALUE in nfmark, default 0xFFFFFFFF.
             set-mark <value[/mask]>
                 Set bits to 0 of MASK and OR VALUE in nfmark, default 0xFFFFFFFF.
 
             Mnemonics
                 and-mark <bits>
                     Binary AND nfmark with BITS.
                     Mnemonic for, set-xmark 0/invbits; invbits binary negation.
                 or-mark <bits>
                     Binary OR nfmark with BITS.
                     Mnemonic for, set-xmark bits/bits.
                 xor-mark <bits>
                     Binary XOR nfmark with BITS.
                     Mnemonic for, set-xmark bits/0.
 
         MAQUERADE
             Map an interface with outbound address; used for dynamic connections; static connections use SNAT.
             Valid table; nat.
             Valid chain; POSTROUTING.
 
             to-ports <port[-port]>
                 Set source port range, TCP/UDP protocol.
             random
                 Randomize source port map.
 
         MIRROR (experimental)
             Invert source and destination fields, retransmit.
             Valid chain; INPUT, FORWARD, PREROUTING, user-defined.
             Outbound packets bypass chains, connection tracking, and NAT.
             
         NETMAP
             Set static network topology.
 
             to <addr[/mask]>
             Map to ADDR; fill '1' bits in MASK from ADDR and '0' from orginal address.
 
         NFLOG
             Pass packet to loaded logging backend.
             Non-terminating traversal.
 
             nflog-group <int>
                 Netlink INT (0-2^16-1) group, default 0; valid only with nfnetlink_log.
             nflog-prefix <prefix>
                 Include PREFIX string in log message, 64 character limit.
             nflog-range <int>
                 Override byte INT to copy to userspace; valid only with nfnetlink_log.
             nflog-threshold <int>
                 Queue INT packets before kernel sends to userspace, default 1. 
 
         NFQUEUE (extends QUEUE)
             Set 16-bit queue.
 
             queue-num <int>
                 Set queue to INT (0-65536), default 0.
             queue-balance <int:int>
                 Balance between INT queue range.
             queue-bypass
                 Silently traverse past NFQUEUE; default is drop without an userspace capture.
 
         NOTRACK
             Disable connection tracking
             Valid table; raw.
 
         RATEEST
             Perform rate estimation calculations.
 
             rateest-name <counter>
                 Apply count to COUNTER.
             rateest-interval <interval<s|ms|us>>
                 Measure INTERVAL in, seconds, milliseconds, or microseconds.
             rateest-ewmalog <time>
                 Average TIME constant to apply to measurements.
 
         REDIRECT
             Mutate destination address to incomming interface; local to 127.0.0.1.
             Valid table; nat.
             Valid chain; PREROUTING, OUTPUT, sub-user-defined.
 
             to-ports <port[-port]>
                 Set destination PORT range, protocol TCP/UDP.
             random
                 Randomize port map.
 
         REJECT
             Drop with error packet.
             Valid chain; INPUT, FORWARD, OUTPUT, sub-user-defined.
 
             reject-with <type>
                 Default port-unreachable.
 
                 Type
                     icmp-net-unreachable
                     icmp-host-unreachable
                     icmp-port-unreachable
                     icmp-proto-unreachable
                     icmp-net-prohibited
                     icmp-host-prohibited
                     icmp-admin-prohibited
             tcp-reset
                 Return TCP RST, valid TCP protocol.
 
         SAME (DNAT persistent replaces SAME)
             Send same source/destination address.
 
             to <addr[-addr]>
                 Map source to multiple ADDR range.
             nodst
                 Ignore destination address in source calculations.
             random
                 Randomize port map.
 
         SECMARK
             Set 32-bit security mark.
             Valid table; security.
 
             selctx <security_context>
 
         SET
             Change, add or remove, blocks; ipset(8).
 
             add-set <name> <flag[,flag...]>
                 Append FLAG, src/dst specifications limited to 6, to NAME.
             del-set <name> <flag[,flag...]>
                 Remove FLAG, src/dst specifications limited to 6, from NAME.
             timeout <int>
                 Use INT as timeout value for add-set.
             exist
                 Reset timeout of exsisting sets.
 
         SNAT
             Modify source address and terminate traversal.
             Valid table; nat.
             Valid chain; POSTROUTING.
 
             to-source [addr[-addr]][:port[-port]]
                 Set source ADDR and PORT (TCP/UDP protocol) range.
             random
                 Randomize port map.
             persistent
                 Consistent address mapping; supersedes SAME target.
 
         TCPMSS
             Modity TCP SYN MSS values, TCP protocol.
             Used to circumvent missing 'ICMP Fragmentation Needed' and 'ICMPv6 Packet Too Big' packets.
             Options are exclusive.
 
             set-mss <int>
                 Set MSS to INT, reductions ignored.
             clam-nss-to-pmtu
                 Lock MSS value to 40(IPv4)/60(IPv6)
 
             Example
                 .. --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu ..
 
         TCPOPTSTRIP
             Replace TCP options with NO-OP, protocol TCP.
 
             strip-options <option[,option...]>
                 Select alnum OPTION; '-j TCPOPTSTRIP -h'.
             
         TEE
             Redirect clone.
 
             gateway <addr>
                 Send to ADDR.
 
             Example
                 .. -A PREROUTING -i int -j TEE --gateway 2001:db8::1 ..
 
         TOS
             Set ToS (Type of Service) field in IPv4 or Priority field in IPv6.
             Valid table; mangle.
 
             set-tos <value[/mask]>
                 Set bits to 0 of MASK and XOR; default 0xFF mask.
             set-tos <symb>
                 Set SYMB, mask of 0xFF; '-j TOS -h'.
 
             Mnemonic
                 and-tos <bits>
                     Binary AND the TOS with BITS; set-tos 0/invbits, negation of bits.
                 or-tos <bits>
                     Binary OR the TOS with BITS; set-tos bits/bits.
                 xor-tox <bits>
                     Binary XOR the TOS with BITS; set-tos bits/0.
 
         TPROXY
             Redirect to socket, optionally change mark.
             Valid table; mangle.
             Valid chain; PREROUTING, sub-user-defined.
             Requires on-port.
 
             on-port <port>
                 Set destination PORT, 0 is no change; TCP/UDP protocol.
             on-ip <addr>
                 Set destination ADDR, default incoming interface; TCP/UDP protocol.
             tproxy-mark <value[/mask]>
                 Mark with VALUE and MASK.
 
         TRACE
             Log traversed rules; prefix, 'TRACE: table:chain:type:nm'.
             Valid table; raw.
 
         TTL
             Modify IPv4 TTL which determines hop count in time.
             Valid table; mangle.
             Caution setting outbound packets.
 
             ttl-set <int>
                 Set TTL to INT.
             ttl-dec <int>
                 Decrement TTL INT times.
             ttl-inc <int>
                 Increment TTL INT times.
 
         ULOG
             Multicast to socket.
             Non-terminating traversal
 
             ulog-nlgroup <group>
                 Send to GROUP (1-32), default 1.
             ulog-prefix <prefix>
                 Set message PREFIX, 32 character limit.
             ulog-cprange <bytes>
                 Copy BYTES to userspace, entire if 0, default 0.
             ulog-qthreshold <num>
                 Queue NUM before transmit, default 1.
 
 Tables
     filter (default)
         Chains
             INPUT
                 Destined for local sockets.
             FORWARD
                 Routed packets.
             OUTPUT
                 Local generated packets.
     nat (connection)
         Chains
             PREROUTING
                 Pre-route.
             OUTPUT
                 Local generated packets pre-route.
             POSROUTING
                 Post-route.
     mangle (alteration)
         Chains
             PREROUTING
                 Pre-route.
             OUTPUT
                 Local generated packets pre-route.
             INPUT
                 Destined for local sockets.
             FORWARD
                 Routed packets.
             POSTROUTING
                 Post-route.
     raw (connection tracking exemptions)
         Chains
             PREROUTING
                 Pre-route.
             OUTPUT
                 Local generated packets pre-route.
     security (Mandatory Access Control or Discretionary Access Control)
         Chains
             INPUT
                 Inbound.
             OUPUT
                 Outbound.
             FORWARD
                 Routed.
 
 Commands
     One per rule.
 
     A|append <chain> <args>
         Append rule, per address, to CHAIN.
     D|delete <chain> <args|num>
         Delete from CHAIN maching NUM or ARGS.
     I|insert <chain> [num] <args>
         Insert rule in CHAIN at NUM, default 1.
     R|replace <chain> <num> <args>
         Replace NUM in CHAIN; failing if muliple address resolution.
     L|list [chain]
         List rules of CHAIN, default all.
     S|list-rules [chain]
         Print rules of CHAIN, default all.
     F|flush [chain]
         Flush, or remove all, rules of CHAIN.
     Z|zero [chain [num]]
         Zero packet and byte counters of CHAIN, default all, matching NUM.
     N|new-chain <chain>
         Create CHAIN.
     X|delete-chain [chain]
         Remove CHAIN, must be empty, default all non-builtin chains.
     P|policy <chain> <target>
         Define policy for CHAIN to TARGET; only builtin, no CHAIN can be TARGET.
     E|rename-chain <old> <new>
         Edit OLD chain to NEW.
 
     Arguments (Global)
         v|verbose
             Increase the verbosity of output.
             If used with; list, show interface; args; TOS; counters.
         n|numeric
             Numerical output, resolve; hostnames, aliases, services.
         x|exact (only list)
             Expand numbers.
         line-numbers
             Prefix each printed line with numerical equivilant.
         modprobe<=command>
             Command to load required modules.
 
     Arguments
         !, invert.
         Multiple address expand to multiple rules.
 
         p|protocol <proto>
             Proto; tcp, udp, udplite, icmp, esp, ah, sctp, all(0), integer, or from /etc/protocols; default all.
         s|src|source <addr[/mask][,...]>
             Address; alias, hostname, IP.
                 Hostname, resolve only on rule creation.
         d|dst|destination <address[/mask][,...]>
             See source.
         j|jump <target>
             Target of rule can be; user-defined, builtin, or extension.
             If no target to rule, only increment counter.
         g|goto <chain>
             Jump to CHAIN, but do not return.
         i|in-interface <int>
             If omited, match any interface.
             Wildchar; + (eth+).
             Limited to chains; INPUT, FORWARD, PREROUTING.
         o|out-interface <int>
             If omited, match any interface.
             Wildchar; + (eth+).
             Limited to chains; FORWARD, OUTPUT, POSTROUTING.
         f|fragment
             Match all packets, excluding first, of a series; ignores ports.
         c|set-counters
             Initialize counters.
             Limited to commands; insert, append, replace.
 
     Extension (used with protocol or match args)
         !, invert; [src|dst]-type.
         help, print documentation.
         
         addrtype
             Match address type.
 
             UNSPEC
             LOCAL
             BROADCAST
             ANYCAST
             MULTICAST
             BLACKHOLE
             UNREACHABLE
             PROHIBIT
             THROW
             NAT
             XRESOLVE
 
             src-type <type>
                 Match TYPE.
             dst-type <type>
                 Match TYPE.
             limit-iface-in
                 Limited to chains; PREROUTING, INPUT, FORWARD.
                 Match only if receiving.
             limit-iface-out
                 Limited to chains; POSTROUTING, OUTPUT, FORWARD.
                 Match only if sending.
 
         ah
             Match SPIs in authentication header of IPsec packets.
             !, invert.
 
             ahspi <spi[:spi]>
 
         cluster
             Load-sharing without load-balance.
             !, invert; local-[node[mask]].
 
             cluster-total-nodes <num>
                 Total NUM of nodes in cluster.
             cluster-local-node <num>
                 Define local node to NUM. 
             cluster-local-nodemask <mask>
                 Define local node to MASK.
             cluster-hash-seed <seed>
                 Set Jenkins hash SEED.
 
         comment
             Add description to any rule.
 
             comment <comment>
                 256 character limit.
 
         connbytes
             Match from byte, or average byte per packet, count.
             Return false if no data is available.
             sysctl; net.netfilter.nf_conntrack_acct.
             !, invert; connbytes.
 
             connbytes <from[:to]>
                 Match if size is more than FROM and less than TO.
 
             connbytes-dir <original|reply|both>
                 Match condition.
             connbytes-mode <packets|bytes|avgpkt>
                 Match condition.
 
         connlimit
             Parallel connection restrictions.
             !, invert; connlimit-above.
 
             connlimit-above <num>
                 Match if greater than NUM.
             connlimit-mask <prefix>
                 Group hosts using PREFIX.
 
             Example
                 .. -m connlimit --connlimit-above 2 -j ..
                 .. -m connlimit --connlimit-above 3 --connlimit-mask 24 -j ..
 
         connmark
             Match mark field of connection, set by CONNMARK target.
             !, invert; mark.
 
             mark <value[/mask]>
                 Match VALUE of mark.
 
         conntrack
             Match state of connection.
             !, invert; not ctdir.
 
             ctstate <states>
                 Match STATES, comma deliminated.
 
                 States
                     INVALID
                         Not associated with any known connection.
                     NEW
                         Initial connection.
                     ESTABLISHED
                         Successful send and reply.
                     RELATED
                         Initial connection with association.
                     UNTRACKED
                         Target of NOTRACK.
                     SNAT (Virtural) 
                         Differentiating source and destination address.
                     DNAT (Virtural) 
                         Differentiating destination and source address.
 
             ctproto <proto>
                 Match Layer-4 protocol; alnum.
             ctorigsrc <addr[/mask]>
                 Match original source ADDR.
             ctorigdst <addr[/mask]>
                 Match original destination ADDR.
             ctreplsrc <addr[/mask]>
                 Match reply source ADDR.
             ctrepldst <addr[/mask]>
                 Match reply destination ADDR.
             ctorigsrcport <port>
                 Match original source PORT or GRE key.
             ctorigdstport <port>
                 Match original destination PORT or GRE key.
             ctreplsrcport <port>
                 Match reply source PORT or GRE key.
             ctrepldstport <port>
                 Match reply destination PORT or GRE key.
             ctstatus <states>
                 Match STATES, comma deliminated.
 
                 States
                     NONE
                         Match fails all else.
                     EXPECTED
                         Condition matched.
                     SEEN_REPLY
                         Reply of sent packet.
                     ASSURED
                         Never early-expired.
                     CONFIRMED
                         Original packet sent.
 
             ctexpire <time[:time]>
                 Match expiration of TIME in seconds, inclusive.
             ctdir <ORIGINAL|REPLY>
                 Match flow direction, default both.
 
         dccp
             !, invert.
 
             sport|source-port <port[:port]>
             dport|destination-port <port[:port]>
             dccp-types <mask>
                 Mask, comma deliminated; REQUEST, RESPONSE, DATA, ACK, DATAACK, CLOSEREQ, CLOSE, RESET, SYNC, SYNCACK, INVALID.
             dccp-option <num>
                 If option of NUM is set.
 
         dscp
             Match 6-bit DSCP of TOX in IP header; superseeds TOS.
             !, invert.
 
             dscp <value>
                 Match VALUE, integer or hexidecimal.
             dscp-class <class>
                 Convert CLASS into numerical equivilant.
                 Match DiffServ CLASS; BE, EF, AFxx, CSxx.
 
         ecn
             ECN(Explicit Congestion Notification/RFC3168) bits of IPv4 and TCP header.
             CWR(Congestion Window Received)
             ECE(ECN Echo)
             ECT(ECN-Capable Transport)
             !, invert.
 
             ecn-tcp-cwr
                 Match TCP ECN CWR bit.
             ecn-tcp-ece
                 Match TCP ECN ECE bit.
             ecn-ip-ect <num>
                 Match IPv4 ECT of NUM(0-3).
 
         esp
             Match SPIs in ESP header of IPsec packets.
             !, invert.
 
             espspi <spi[:spi]>
 
         hashlimit
 
             hashlimit-upto <quant[</[sec|min|hour|day]>]>
                 Match if less than or equal to QUANT; default 3/hour.
             hashlimit-above <quant[</[sec|min|hour|day]>]>
                 Match if greater than QUANT.
             hashlimit-burst <quant>
                 Match no more than QUANT succession; default 5.
             hashlimit-mode <srcAddr|srcPort|dstAddr|dstPort>
                 Comma deliminated.
             hashlimit-srcmask <mask>
                 Used with 'hashlimit-mode srcAddr' to group by MASK.
             hashlimit-dstmask <mask>
                 Used with 'hashlimit-mode srcAddr' to group by MASK.
             hashlimit-name <string>
                 Identifier for /proc/net/ipt_hashlimit/STRING.
             hashlimit-htable-size <int>
                 Limit hash table to INT buckets.
             hashlimit-htable-expire <int>
                 Expire entries every INT in milliseconds.
             hashlimit-htable-gcinterval <int>
                 Garbage collection every INT in milliseconds.
 
         helper
             conntrack-helper
             !, invert.
 
             helper <proto[-port]>
                 PROTO; ftp.
                 PORT; integer.
 
         icmp
             Additional arguments for 'protocol icmp'.
             !, invert.
 
             icmp-type <int[/code]|name>
                 Print valid entries; -p icmp -h
 
         iprange
             Match IP address.
             !, invert.
 
             src-range <from[-to]>
                 Match source IP FROM to TO.
             dst-range <from[-to]>
                 Match destination IP FROM to TO.
 
         length
             Match layer-3 packet length.
             !, invert.
 
             length <len[:len]>
 
         limit
             Match rate as token bucket filter; valid until limit.
             !, invert.
 
             limit <rate[</[second|minute|hour|day]>]>
                 Match RATE of maximum average; default 3/hour.
             limit-burst <int>
                 Match a maximum of INT concecutive packets.
                 
         mac
             !, invert.
 
             mac-source <addr>
                 Match mac source ADDR, xx:xx:xx:xx:xx:xx(2*6).
                 Valid chains; PREROUTING, FORWARD, INPUT.
 
         mark
             Match mark field of packet, set with MARK target.
             !, invert.
 
             mark <value[/mask]>
                 Match VALUE of mark.
 
         multiport
             Match a maximum of 15 port groupings, ranges are two groups; only valid is protocols of tcp and udp.
             !, invert.
                 
             sports|source-ports <port[<,[port|port:port]>]>
                 Match source PORT, comma delimited and colon ranges.
             dports|destination-ports <port[<,[port|port:port]>]>
                 Match destination PORT, comma delimited and colon ranges.
             ports <port[<,[port|port:port]>]>
                 Match source or destination PORT, comma delimited and colon ranges.
 
         osf (Operating System Fingerprinting)
             Compares data (window size, MSS, options order, TTL, DF, etc) from SYN packets.
             Use 'nfnl_osf' to load fingerprint data.
             !, invert; genre.
 
             genre <string>
                 Match STRING of genre.
             ttl <int>
                 Level of INT TTL comparison; 0, true comparison of IP address and fingerprint TTL; 1, IP TTL is less than fingerprint; 2, disable comparison.
             log <int>
                 Log INT into dmesg; 0, all matching or unknown; 1, only first match; 2, all unknown.
 
         owner
             Match local packet metadata, their associated sockets.
             !, invert.
 
             uid-owner <uid[-uid]>
                 Match socket permissions.
             gid-owner <gid[-gid]>
                 Match socket permissions.
             socket-exists
                 Match on association of packet socket.
 
         physdev
             Bridge device matchin.
             !, invert.
 
             physdev-in <name>
                 Match NAME of bridge port received.
                 Valid chains; INPUT, FORWARD, PREROUTING.
                 Wildchar support; +.
             physdev-out <name>
                 Match NAME of bridge port sent.
                 Valid chains; FORWARD, OUTPUT, POSTROUTING.
                 Wildchar support; +.
             physdev-is-in
                 Match passed in.
             physdev-is-out
                 Match if passed out.
             physdev-is-bridged
                 Match if not routed, bridged.
                 Valid chains; FORWARD, POSTROUTING.
 
         pkttype
             Match link-layer type.
             !, invert.
 
             pkt-type <unicast|broadcast|multicast>
                 
         policy
             Match IPsec policy.
             !, invert; not dir, pol, strict, next.
 
             dir <in|out>
                 Match [de|en]capsulation.
                 Valid in chain; PREROUTING, INPUT, FORWARD
                 Valid out chain; POSTROUTING, OUTPUT, FORWARD.
             pol <none|ipsec>
                 Match if processed by IPsec.
             strict
                 Match exactly.
             reqid <id>
                 Match ID, specified by setkey(8) using unique:id as level.
             spi
                 Match SPI of SA.
             proto <ah|esp|ipcomp>
                 Match encapsulation protocol.
             mode <tunnel|transport>
                 Match encapsulation mode.
             tunnel-src <addr[/mask]>
                 Match source of SA tunnel.
             tunnel-dst
                 Match destination of SA tunnel.
             next
                 Start the next element in policy.
                 Valid arg; strict.
 
         quota
             Quota by decrementing a counter.
             !, invert.
 
             quota <bytes>
                 BYTES count for quota.
 
         ratees
             Rate estimator determined by RATEEST target; match absolute bps/pps values, difference of two rates.
             bps (bytes per second)
             pps (packets per second)
             !, invert; rateest-[lt|gt|eq].
 
             rateest1 <id>
                 First rate estimator ID.
             rateest2 <id>
                 Second rate estimator ID.
             rateest-delta
                 Compare difference.
             rateest-bps1 <value>
                 Compare bps.
             rateest-bps2 <value>
                 Compare bps.
             rateest-pps1 <value>
                 Compare pps.
             rateest-pps2 <value>
                 Compare pps.
             rateest-lt
                 Match if rate is less than rateest.
             rateest-gt
                 Match if rate is greater than rateest.
             rateest-eq
                 Match if rate is equal to rateest.
 
             Example
                 .. -j RATEEST --rateest-name eth0 --rateest-interval 250ms --rateest-ewma 0.5s
                 .. -m rateest --ratest-delta --rateest1 eth0 --rateest-bps1 2.5mbit --rateest-gt --rateest2 ppp0 --rateest-bps2 2mbit ..
 
         realm
             Match routing realm.
             !, invert.
 
             realm <value[/mask]>
                 Match VALUE, integer with mask or /etc/iproute2/rt_realms name.
 
         recent
             Variable matching of dynamic addresses.
             Mutually exclusive; set, rcheck, update, remove.
             !, invert; set, rcheck, update, remove.
 
             name <id>
                 ID for commands, default DEFAULT.
             set
                 Add/Update source to list.
             rsource
                 Match/Save source to recent table.
             rdest
                 Match/Save destination to recent table.
             rcheck
                 Check status of address in list.
             update
                 Update timestamp of address in list.
             remove
                 Remove source from address list.
             seconds <int>
                 Seen within INT in seconds.
                 Valid with; rcheck, update, hitcount.
             hitcount <hits>
                 Match if count in table is greater than or equal to HITS.
                 Maximum value; ip_pkt_list_tot of xt_recent module.
                 Valid with; rcheck, update, seconds.
             rttl
                 Match of if address found (/proc/net/xt_recent/) and TTL matches 'set' rule.
                 Valid with; rcheck, or udate.
 
                 Module Parameters
                     ip_list_tot=<INT>
                         Remember INT addresses per table.
                     ip_pkt_list_tot=<INT>
                         Remember INT packets per address.
                     ip_list_hash_size=<INT>
                         Set hash table size to INT; 0, calculate from ip_list_tot; default 512.
                     ip_list_perms=<mode>
                         Octal MODE for /proc/net/xt_recent/.
                     ip_list_uid=<uid>
                         UID for /proc/net/xt_recent/.
                     ip_list_gid=<gid>
                         GID for /proc/net/xt_recent/.
 
                 Echo commands to xt_recent/DEFAULT.
                     Commands
                         +addr
                             Append ADDR.
                         -addr
                             Remove ADDR.
                         /
                             Flush list.
 
                 Example
                     .. -m recent --name foo --rcheck --seconds 60 ..
                     .. -m recent --name foo --set ..
 
         sctp
             
             !, invert.
 
             sport|source-port <port[:port]>
             dport|destination-port <port[:port]>
             chunk-types <all|any|only> <type[:flags]>
                 Uppercase flags are set, lowercase flags are unset.
 
                 type
                     DATA
                     INIT
                     INIT_ACK
                     SACK
                     HEARTBEAT
                     HEARTBEAT_ACK
                     ABORT
                     SHUTDOWN
                     SHUTDOWN_ACK
                     ERROR
                     COOKIE_ECHO
                     COOKIE_ACK
                     ECN_ECNE
                     ECN_CWR
                     SHUTDOWN_COMPLETE
                     ASCONF
                     ASCONF_ACK
                     FOWARD_TSN
                 flag
                     DATA <I|U|B|E>
                     ABORT <T>
                     SHUTDOWN_COMPLETE <T>
 
             Example
                 .. -p sctp --chunk-types any DATA,INIT ..
                 .. -p sctp --chunk-types any DATA:Be ..
 
         set
             Match IP sets defined by ipset(8).
             !, invert.
 
             match-set <name> <flag[,flag]>
                 FLAG is a comma deliminated list of source/destination, limited to 6 items.
 
         sock
             Socket lookup on packet.
 
         state
             State connection tracking.
             !, invert.
 
             state <state>
                 Comma delminated list of states.
 
                 State
                     INVALID
                         Unable to be identified; memoroy allocation, ICMP.
                     ESTABLISHED
                         Packet transactions.
                     NEW
                         Initialized packet connection.
                     RELATED
                         Initialized associated connection.
                     UNTRACKED
                         Not tracked; NOTRACK target.
 
         statistic
             mode <mode>
                 Set MODE of rule.
             probability <int>
                 Set random match probability to INT; 0, 1.
                 Valid mode; random.
             every <int>
                 Match every INT packet.
                 Valid mode; nth.
             packet <int>
                 Define initial counter value to INT; default 0.
 
         string
             !, invert; string, hex-string.
 
             algo <bm|kmp>
                 Define pattern strategy; bm, Boyer-Moore; kmp, Knuth-Pratt-Morris.
             from <offset>
                 Beginning offset, default 0.
             to <offset>
                 Ending offset, default 0.
             string <expr>
                 Match EXPR.
             hex-string <expr>
                 Match hex EXPR.
 
         tcp
             Valid with; protocol TCP.
             !, invert.
 
             sport|source-port <port[:port]>
                 Source PORT; alnum, first:last, default 0 or 65535, swaps incomaptibilities.
             dport|destination-port <port[:port]>
                 Destination PORT; alnum, first:last, default 0 or 65535, swaps incomaptibilities.
             tcp-flags <mask> <comp>
                 MASK, comma deliminated list of flags to examine.
                 COMP, comma deliminated list of flags to be set.
 
                 Flags
                     SYN
                     ACK
                     FIN
                     RST
                     URG
                     PSH
                     ALL
                     NONE
 
             syn
                 Match TCP packets with SYN set and ACK,RST,FIN unset; equivalent to SYN,RST,ACK,FIN SYN.
             tcp-option <num>
                 Match if NUM set.
 
         tcpmss
             Match TCP MSS (maximum segment size), valid with SYN,ACK.
 
             mss <value[:value]>
                 Match TCP MSS VALUE.
 
         time
             !, invert.
 
             datestart <YYYY[-MM[-DD[Thh[:mm[:ss]]]]]>
                 Match time of 'ISO 8601 T'; valid, 1970-01-0T00:00:00 to 2038-01-19T04:17:07; default 1970-01-01 and 2038-01-19.
             datestop <YYYY[-MM[-DD[Thh[:mm[:ss]]]]]>
                 Match time of 'ISO 8601 T'; valid, 1970-01-0T00:00:00 to 2038-01-19T04:17:07; default 1970-01-01 and 2038-01-19.
             timestart <hh:mm[:ss]>
                 Match durring time; valid, 00:00:00 to 23:59:59; valid, prefix zero; base-10.
             timestop <hh:mm[:ss]>
                 Match durring time; valid, 00:00:00 to 23:59:59; valid, prefix zero; base-10.
             monthdays <day[,day]>
                 Match on DAY of month, skip non matched days; valid, 1 to 31.
             weekdays <day[,day]>
                 Match on weekday; valid, Mo[n], Tu[e], We[d], Th[u], Fr[i], Sa[t], Su[n]; valid, 1 to 7.
             utc
                 Interpret time as UTC; datestart, datestop, timestart, timestop.
             localtz
                 Interpret time as local kernel time, default; datestart, datestop, timestart, timestop.
 
         tos
             Match 8-bit ToS (Type of Service) field from IPv4 header and 8-bit Priority field in IPv6 header.
             !, invert.
 
             tos <value[/mask]|symbol>
                 Match TOS of VALUE.
                 Match TOS of SYMBOL, implies 0x3F mask (all except ECN); '-m tox -h'.
                 
         ttl
             Match TTL (Time to Live) field of IP header.
 
             ttl-eq <ttl>
                 Match TTL.
             ttl-gt <ttl>
                 Match if greater than TTL.
             ttl-lt <ttl>
                 Match if less than TTL.
 
         u32
             Extract maximum of 4bytes (skb->data,skb->end) from packet using language.
             !, invert.
 
             u32 <tests>
                 Length of IP header (IHL) is stored as 32-bit words in right half of byte 0.
                 A limit of 10; =, ranges, numbers.
                 tests:=location=value | tests&&location=value
                 value:=range | value,range
                 range:=number | number:number
                 location:=number | location operator number
                 operator:=& | << | >> | @
                     =, assign; @, 
 
                 Location
                     A is type char *, IP address header.
                     B and C, unsigned 32-bit integers, zero.
 
                     number B=number
                     C=(*(A+B)<<24)+(*(A+B+1)<<16)+(*(A+B+2)<<8)+*(A+B+3)
                     &number C=C&number
                     <<number C=C<<number
                     >>number C=C>>number
                     @number A=A+C
 
                 Example
                     Match >=256 length.
 
                         Read 0-3 bytes, 'AND' with '0xFFFF', test if range '0x100:0xFFFF'; .. --u32 "0&0xFFFF=0x100:0xFFFF" ..
 
                     Match ICMP of type 0.
                         ICMP test, true if 9 byte is '1'; read 6-9 bytes, discard with '&' and compare; .. --u32 "6&0xFF=1&&" ..
                         Fragment test, not a fragment if last 6 bits of byte 6 and entire byte 7 are '0' or last 5 bits of byte 6 are '0'; .. 4&0x3FFF=0&& ..
                         First byte after header (type) is '0', first '0' reads 0-3 bytes, shifting 22 bits, '&3C' removes two right bits and first four of first byte, shift 24 to right to compare first byte with '0'; .. 0>>22&0x3C@0>>24=0 ..
 
         udp
             Valid with; protocol UDP.
             !, invert.
 
             sport|source-port <port[:port]>
                 Source PORT; alnum, first:last, default 0 or 65535, swaps incomaptibilities.
             dport|destination-port <port[:port]>
                 Source PORT; alnum, first:last, default 0 or 65535, swaps incomaptibilities.
 
         unclean (experimental)
             Match malformed or unusual packets.
DOCU
