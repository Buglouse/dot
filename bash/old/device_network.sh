#!/usr/bin/env bash
source 'bash.sh'

device.network.variables() {
    device.network.module

    local item itemT items itemsT itemsB
    local type name 
    local reT='wlan|eth'
    unset DEVICE_NETWORK_WLAN DEVICE_NETWORK_ETH DEVICE_RFKILL_WLAN

    #--Interface--#
    items=( ${SYS_CLASS_NET}/* )
    for item in "${items[@]}"; do
        unset type name
        item="${item}/uevent"
        [[ -e ${item} ]] || continue

        if $(exe grep) -q 'DEVTYPE' "${item}"; then
            type=$($(exe sed) -ne 's/DEVTYPE=\(.*\)/\1/p' "${item}")
        fi
        name=$($(exe sed) -ne 's/INTERFACE=\(.*\)/\1/p' "${item}")
        [[ ${type} ]] && { [[ ${type} == +(${reT}) ]] || continue; }
        [[ ${name} == +(${reT})+([[:digit:]]) ]] || continue
        type="${type:-${name}}"
        type="${type%+([[:digit:]])}"

        itemsT+=( "${name}" )
        itemT="DEVICE_NETWORK_${type^^}[@]"
        export DEVICE_NETWORK_${type^^} "${!itemT}" "${item%/*},${name}"
    done
    export DEVICE_NETWORK "${itemsT[@]}"

    #--RFkill--#
    unset itemsT
    items=( ${SYS_CLASS_RFKILL}/* )
    for item in "${items[@]}"; do
        item="${item}/uevent"
        [[ -e ${item} ]] || continue

        type=$($(exe sed) -ne 's/.*_TYPE=\(.*\)/\1/p' "${item}")
        [[ ${type} == wlan ]] || continue
        name=$($(exe sed) -ne 's/.*_NAME=\(.*\)/\1/p' "${item}")

        itemsT+=( "${name}" )
        itemsB+=( "${item%/*},${name}" )
        export DEVICE_RFKILL_${type^^} "${itemsB[@]}"
    done
    export DEVICE_RFKILL "${items[@]}"

    [[ ${DEVICE_NETWORK} ]]
}

device.network.module() {
    local _ dev bus ctrl id items

    while read bus _ ctrl id; do
        case "${id}" in
            '8086:4232') ;; # iwlagn
            '14e4:4727') items+=( 'mac80211' 'brcmsmac' ) ;;
	    '8086:0885') items+=( 'iwlagn' ) ;;
        esac
    done <<<"$($(exe lspci))"

    while read _ bus _ dev _ id _; do
        case "${id}" in
            '0bda:8172') items+=( 'r8712u' ) ;;
        esac
    done <<<"$($(exe lsusb))"

    #modprobe -r "${items[@]}"
    [[ ${items} ]] && modprobe "${items[@]}"
}

device.network.arguments() {
    local item
    unset ACTION ITEMS STATE

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                M|module) ACTION='module' ;;
                R|module-remove) export MODPROBE_UNLOAD '1'; ACTION='module' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

device.network.variables
return 2>/dev/null

_ARGS='(M)odprobe(-(R)emove) (r)fkill (0|off) (1|on) (all)'
_DESC="
    Module, auto(load|remove) associated hardware modules."

device.network.arguments "${@:-?}" || exit

case "${ACTION}" in
    module) device.network.module ;;
esac
