#!/usr/bin/env bash
source 'bash.sh'

parse.variables() {
    true
}

parse.pid() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _ARGS='(signal) <file> <pids>'
    local _DESC="
        Read pids in FILEs or PIDS, write existing proc in FILE[0].

        Signal; LOCK, false if running; TERM, do not write PID."
    local item item2 items itemsR itemsF
    local IFS='|'; local re="${SIGNAL_EXIT[*],,}"; unset IFS

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) return 1 ;;
            +(${re})) signal="${item}" ;;
        esac
        shift
    done

    unset items itemsF
    for item in "${@}"; do
        if [[ ${item} == +(*/*) ]]; then
            [[ -e ${item} ]] && items+=( $(<"${item}") )
            itemsF+=( "${item}" )
        else
            items+=( "${item}" )
        fi
    done

    unset itemsR
    for item in "${items[@]}"; do
        [[ ${item} ]] || continue
        # Ignore terminating items.
        [[ ${signal} == term ]] && { [[ ${item} == ${$} ]] && continue; }
        exist_proc "${item}" || continue

        exist_elem "${item}" "${itemsR[@]}" || itemsR+=( "${item}" )
    done

    if [[ ${signal} == lock ]] && (( ${#itemsR[@]} > 1 )); then
        return 1
    else
        _rm "${itemsF[@]}"
        [[ ${1} == +(*/*) ]] || { print_false 'Invalid: %s' "${1}"; return 1; }
        [[ ${itemsR} ]] && printf '%s\n' "${itemsR[@]}" >"${1}"
    fi

    (( 1 ))
}

parse.arg() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _='
        @__snip__@  # Script
        local item items 
        while [[ ${1} ]]; do
            parse.arg "${@}"; shift ${SHIFT}
            [[ ! ${ARG} && ${ARG_OPT} ]] && items=( ${ARG_OPT[@]} )
            for item in "${ARG[@]}"; do
                case "${item}" in
                    \?) return 1 ;;
                esac
            done
        done
        @__snip__@  # Function
        local item
        while [[ ${1} == +(-)* ]]; do
            item="${1##*(-)}"
            case "${item}" in
                \?) print_help; return 1 ;;
                *) action="${item}" ;;
            esac
            shift
        done
        '
	local item int
	unset ARG ARG_OPT SHIFT
	
	while [[ ${1} ]]; do
		[[ ${ARG} ]] && break
		case "${1}" in
            ?(-)\?) ARG='?' ;;
			--+([[:alnum:]])=*)
				item="${1:2}"
                ARG=( "${item%=*}" )
				item="${item#*=}"; item="${item//${ARG_IFS:-;}/ }"
                ARG_OPT=( ${item[@]} )
                shift; break
			;;
            --) ARG_OPT=( "${@:2}" ) SHIFT=${#}; break ;;
			--+([[:print:]])) ARG=( "${1:2}" ) ;;
            -+([[:digit:]])) ARG="${1:1}" ;;
            -) ARG='-' ;;
			-+([[:print:]]))
                for (( int=1;int<${#1};int++ )); do
                    ARG+=( "${1:int:1}" )
                done
            ;;
			*) ARG_OPT+=( "${1}" ); (( SHIFT++ )) ;;
		esac
		shift
	done
	[[ ${ARG} ]] && (( SHIFT++ ))
    [[ ${ARG} == \? ]] && print_help
}

parse.conf() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _ARGS='<files>'
    local _DESC='
        Expand bash constructs in FILES, conditional to expansion (only if value of variable exists).

        @--snip--@
        local itemsC=( "${CONF_}" )
        profile.user.dot_relink "${itemsC%/*}";
        pathD=$($(exe realpath) "${itemsC}")
        parse.conf "${itemsC[@]}"
        if $(exe ); then
            for item in "${itemsC[@]}"; do
                parse.diff "${pathD%/*}/${item##*/}" "${item}"
            done
        fi
        '
    local item itemX line lineOld pass IFS
    shopt -q nocaseglob; SHOPT_NOCASEGLOB="${?}"
    #local reV='\$\{([[:alnum:]_]*(\[[[:digit:]@\*]+\]|[\^,]{,2}|[\#%]{,2}.*)?)\}'
    local reV='\$\{([_[:upper:][:digit:]]+(\[[[:digit:]@\*]+\]|[\^,]{,2})?)\}'
    local reE='\$\(([[:print:]]*)\)'
    local reT reS='#'


    for item in "${@}"; do
        [[ -e ${item} ]] || continue
        (( $($(exe stat) -c '%u' "$($(exe realpath) "${item}")") )) || continue
        itemX=$($(exe mktemp) -p "${DIR_TMP}" "${item##*/}.XXXXXX")

        set +C
        while IFS= read -r line; do
            shopt -u nocaseglob
            lineOld="${line}"

            while [[ ${line} =~ ${reV} ]]; do
                (( pass )) || (( pass++ ))
                [[ ${line} == *([[:space:]])+(${reS})* ]] && break
                reT="${BASH_REMATCH[1]//${BASH_REMATCH[2]}}"  # Remove fail expansions.
                [[ ${!reT} || ${!BASH_REMATCH[1]} ]] || break  # Keep if not value.

                case "${BASH_REMATCH[2]:-0}" in
                    ^^) line="${line//"${BASH_REMATCH[0]}"/${!reT^^}}" ;;
                    ,,) line="${line//"${BASH_REMATCH[0]}"/${!reT,,}}" ;;
                    ^) line="${line//"${BASH_REMATCH[0]}"/${!reT^}}" ;;
                    ,) line="${line//"${BASH_REMATCH[0]}"/${!reT,}}" ;;
                    *) line="${line//"${BASH_REMATCH[0]}"/${!BASH_REMATCH[1]}}" ;;
                esac
                #line="${line//"${BASH_REMATCH[0]}"/${!reT${BASH_REMATCH[2]}}}"
            done
            (( SHOPT_NOCASEGLOB )) || shopt -s nocaseglob
            #while [[ ${line} =~ ${reE} ]]; do
            #    (( pass )) || (( pass++ ))
            #    [[ ${line} == +(#)* ]] && break
            #    printf '%s\n' "${BASH_REMATCH[*]}" >&2
            #    #reT="${BASH_REMATCH[1]//${BASH_REMATCH[2]}}"
            #    #printf '%s\n' "${reT}" >&2
            #    line="${line/"${BASH_REMATCH[0]}"/$(${BASH_REMATCH[1]})}"
            #    printf '%s\n' "${line}" >&2
            #done
            printf '%s\n' "${line}"
        done <<<"$(<"${item}")" >"${itemX}"; set -C; unset IFS
        if (( pass )); then
            $(exe mv) -f "${itemX}" "${item}"
            $(exe chmod) 0644 "${item}"
        fi; $(exe rm) -f "${itemX}"
    done
}

parse.atom() {
true
}

parse.xml() {
    local IFS

    IFS='>'; read -d '<' -a REPLY
}

parse.diff() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _ARGS='<fileOld> <fileNew>'
    local _DESC="
        Keep space in OLD.
        If single item, assume OLD in repository.
        Deliminator, first match from left.
        Compare fileNew to fileOld; replace old Rvalues, append new, remove old."
    local int intF verb dry line lineL lineR
    local item item2 itemL itemR itemT itemsT itemsM itemsR item2L
    local re='${[[:upper:]]\|#$' reS='=|:' intD='@'
    # Prevent variable expansion; read -r item.
    # re, template values.
    # reS, variable${reS}value deliminator.

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            d|dry) (( dry++ )) ;;
            v|verb*) (( verb++ )) ;;
            =) reS="${1}" ;;
        esac
        shift
    done

    (( ${#} > 1 )) || return
    for item in "${@}"; do [[ -e ${item} ]] || return 1; done
    for item in "${@:2}"; do [[ ${item} == ${1} ]] && return 1; done

    # Generate diff.
    for item in "${@:2}"; do
        while read -r line; do  # Template entries.
            itemsT+=( "${line#[-+]}" )
        done <<<"$($(exe diff) -U 0 "${1}" "${item}" |
                $(exe grep) -- "-\+.*\(${re}\)")"

        while read -r line; do  # Modified and appended entries.
            line="${line#[+]}"
            lineL="${line%%*([[:space:]])+(${reS})*}"
            lineR="${line#*+(${reS})}"
            lineR="${lineR##*([[:space:]])}"

            [[ ${line} == *+(${reS})* && ${lineR} ]] || continue  # Value?
            itemT=$($(exe grep) "^${lineL}[[:space:]]*[${reS//\\|/}]\+" "${1}")
            [[ ${itemT} == "${line}" ]] && continue  # Diff?

            for item2 in "${itemsT[@]}"; do  # Template?
                item2L="${item2%%*([[:space:]])+(${reS})*}"

                [[ ${item2L} == ${lineL} ]] && continue 2
            done

            int=$($(exe grep) -n "^${lineL}[[:space:]]*[${reS//\\|/}]\+" "${item}")
            itemsM+=( "${int%%:*}${intD}${line}" )
        done <<<"$($(exe diff) -U 0 "${1}" "${item}" |
            $(exe grep) -v '^\+$\|^\(+#\|-\|+++\|@@\)')"

        while read -r line; do  # Removed entries.
            line="${line#[-]}"
            lineL="${line%%*([[:space:]])+(${reS})*}"
            lineR="${line#*+(${reS})}"
            lineR="${lineR##*([[:space:]])}"

            $(exe grep) -q "^${lineL}[[:space:]]*[${reS//\\|/}]\+" "${item}" && continue  # Moved?

            for item2 in "${itemsT[@]}" "${itemsM[@]/#+([[:digit:]])+(${intD})}"; do  # Template, Modified?
                read -r item2 <<<"${item2}"
                item2L="${item2%%*([[:space:]])+(${reS})*}"

                [[ ${lineL} == ${item2L} ]] && continue 2
            done
            itemsR+=( "${line}" )
        done <<<"$($(exe diff) -U 0 "${1}" "${item}" |
            $(exe grep) -v -- '^-$\|^\(+\|---\|@@\)')"
    done

    (( verb )) &&
        { printf '#--Template[%s]--#\n' "${1}"
        printf '\t%s\n' "${itemsT[@]}"
        printf '#--Merged[%s]--#\n' "${1}"
        [[ ${itemsR} ]] && printf -- '\t-%s\n' "${itemsR[@]}"
        [[ ${itemsM} ]] && printf -- '\t+%s\n' "${itemsM[@]}"; }

    (( dry )) && return

    for item in "${itemsR[@]}"; do  # Remove OLD if removed in NEW.
        [[ ${item} ]] || continue
        itemL="${item%%*([[:space:]])+(${reS})*}"
        itemL="${itemL//./\.}" itemL="${itemL//\//\/}"

        #-e '/^$/d'
        $(exe sed) -i -e "/^${itemL}[[:space:]]*[${reS//|/}]\+.*/d" "${1}"
    done

    for item in "${itemsM[@]}"; do  # Replace OLD with NEW.
        [[ ${item} ]] || continue
        item="${item#+([[:digit:]])+(${intD})}"
        itemL="${item%%*([[:space:]])+(${reS})*}"
        itemL="${itemL//./\.}" itemL="${itemL//\//\/}"
        itemR="${item#*+(${reS})}"

        $(exe grep) -q "^${itemL}[[:space:]]*[${reS//\\|/}]\+" "${1}" || continue  # PreExist?
        itemsM=( "${itemsM[@]/%+([[:digit:]])${intD}${item}/}" )
        $(exe sed) -i -e "s/\(^${itemL}[[:space:]]*[${reS//|/}]\+\).*/\1${itemR//\//\/}/" "${1}"
    done

    for item in "${itemsM[@]}"; do  # Empty elements from removal.
        [[ ${item} ]] || continue
        int="${item%%+(${intD})*}"
        item="${item#+([[:digit:]])+(${intD})}"
        intF=( $($(exe wc) -l "${1}") )

        if (( int >= intF )); then  # Past line count?
            printf '%s\n' "${item}" >> "${1}"  # Append.
        else
            $(exe sed) -i -e "${int}i${item}" "${1}"  # Insert @ int.
        fi
    done
}

parse.http() {
    local item action items
    local reC re='encode|decode|strip|find'

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) return 1 ;;
            +(${re})) action="${item}" ;;
        esac
        shift
    done

    unset items
    for item in "${@}"; do
        [[ ${item} ]] || continue

        if [[ ${item} == +(*://*) && ${item} == !(*magnet*) ]]; then
            items+=( "$($(exe curl) --silent --insecure "${item}")" ) 
        elif [[ -e ${item} ]]; then
            items+=( "$(<"${item}")" )
        else
            items+=( "${@}" )
            break
        fi
    done

    for item in "${items[@]}"; do
        case "${action}" in
            encode)
                [[ -e ${DIR_SED}/urlencode.sed ]] || continue
                printf '%s\n' "${item}" |$(exe sed) -f "${DIR_SED}/urlencode.sed"
            ;;
            decode)
                #[[ -e ${DIR_SED}/urldecode.sed ]] || continue
                #printf '%s\n' "${item}" |$(exe sed) -f "${DIR_SED}/urldecode.sed"
                printf '%b\n' "${item//\%/\x}" 
            ;;
            strip)
                printf '%s\n' "${item}" |
                    $(exe sed) \
                        -e ':a' \
                        -e 's/<[^>]*>//g;/</N;//a'
            ;;
            find)
                reC='s/%/\\x/g' re='s#.*video_url[^:]*:"\([^"]*\).*#\1#'

                # $(exe grep) '.flv' |
                item="$(printf '%s\n' "${item}" |
                    $(exe grep) 'var flashvars' |
                    $(exe sed) -e "${re}" -e "${reC}")"
                printf '%b\n' "${item//\%/\x}"
            ;;
            *)
                if [[ ${item} == +(*magnet:*) ]]; then
                    item="magnet:${item#*magnet:}" item="${item%%&*}"
                    printf '%s\n' "${item}"
                    printf '%s\n' "${item}" |xclip -i
                fi
            ;;
        esac
    done
}

parse.arguments() {
    local item items
    unset ACTION ITEM TYPE

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEM=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
                p|pid) ACTION='pid' ;;
                f|find) TYPE='find' ;;
                e|encode) TYPE='encode' ;;
                d|decode) TYPE='decode' ;;
                h|http) ACTION='http' ;;
                dry) (( DRY++ )) ;;
                v|verbose) (( VERBOSE++ )) ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

parse.test() {
    source 'test'
    local outO outN outT outTO outTN

    # parse.pid --lock
    read_true 'parse.pid' &&
        {
        test.true parse.pid --lock /dev/null
        test.false parse.pid --lock "${$}"
        }

    # parse.diff
    read_true 'parse.diff' &&
        {
        outTO=$($(exe mktemp) -p "${DIR_TMP}" "parse_diff.XXXXXX")
        outTN=$($(exe mktemp) -p "${DIR_TMP}" "parse_diff.XXXXXX")
        outO=( 'set connections = 3' 'set date = "${FORMAT_DATE}"' )
        outN=( 'set connections = 4' 'set date = "%Y"' )
        printf '%s\n' "${outO[@]}" >"${outTO}"
        printf '%s\n' "${outN[@]}" >"${outTN}"

        outT=( "$(parse.diff "${outTO}" "${outTN}")" )
        outR="
        "
        [[ ${outT} == $(printf '%s\n' \
            "--- ${outTO}" "+++ ${outTN}" '@@ -1,2 +1,2 @@' \
            '-set connections = 3' '+set connections = 4') ]]; test.true

        $(exe rm) -f "${outTO}" "${outTN}"
        }
}

parse.variables
return 2>/dev/null

_ARGS='(dry) (v)erbose (d)ecode (e)code (f)ind (h)tml'
_DESC="Parse.
    Dry, ignore writes.
    Http; encode, decode or find string in file."

parse.arguments "${@:-?}" || exit

case "${ACTION}" in
    pid) parse.pid "${ITEM[@]}" ;;
    http) parse.http "${TYPE}" "${ITEM[@]}" ;;
esac
