#!/usr/bin/env bash
source 'bash.sh'

display.variables() {
true
}

display.mode() {
    case "${1}" in
        on) $(exe xset) dpms force on ;;
        off) $(exe xset) dpms force suspend ;;
        toggle) ;;
    esac
}

display.arguments() {
    local item 
    unset ACTION ITEM

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEM=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                1|on) ACTION='on' ;;
                0|off) ACTION='off' ;;
                t|toggle) ACTION='toggle' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

display.test() {
    source 'test'
}

source 'test.sh'
display.variables
return 2>/dev/null

_ARGS='(1|on) (0|off) (t)oggle'
_DESC="
    "

display.arguments "${@:-?}" || exit

case "${ACTION}" in
    on|off|toggle) display.mode "${ACTION}" ;;
esac

