#!/usr/bin/env bash
source 'bash.sh'

list.variables() {
    export LIST_COLUMN_DATA 'type' 'cap' 'use' 'rem' 'perc' 'path'
    export LIST_COLUMN_MOUNT 'root' 'path' 'type' 'args'
    export LIST_COLUMN_PROCESS 'ppid' 'pid' 'rss' 'vsz' 'stat' 'user' 'comm' 'args'
    export LIST_COLUMN_FILE_DATE '%Y.%m.%d %H%M'

    export LIST_WARN_DATA ':'

    export LIST_ALRT_PROCESS 'root'
    export LIST_WARN_PROCESS '\[*\]'
    export LIST_INFO_PROCESS 

    export LIST_ALRT_DIGIT_PROCESS '50m'
    export LIST_WARN_DIGIT_PROCESS '10m'
    export LIST_INFO_DIGIT_PROCESS '1m'

    export LIST_COLOR_ALRT "${RED}"
    export LIST_COLOR_WARN "${YELLOW}"
    export LIST_COLOR_INFO "${CYAN}"

    export LIST_IGNORE_DATA 'loop[[:digit:]]'
    export LIST_IGNORE_MOUNT 'loop[[:digit:]]'
    export LIST_IGNORE_PROCESS '\['

    export LIST_LONG_DATA ':' 'uuid'
    export LIST_LONG_MOUNT ':' 'uuid'

    export LIST_FORMAT_PROCESS 'ppid,pid,rss,vsz,stat,user,comm,args'
}

list.data() {
    local _ int long item items reI reL IFS col=6 re='perc|path'

    for item in "${@}"; do
        if [[ ${item} == +([[:digit:]]) ]]; then
            col="${item}"
        else
            items+=( "${item}" )
        fi
    done

    IFS='|'; reI="${LIST_IGNORE_DATA[*]}"; reL="${LIST_LONG_DATA[*]}"; unset IFS
    local reIG="${reI//|/\\|}" reLG="${reL//|/\\|}"
    local "${LIST_COLUMN_DATA[@]}"
    local reLong=':' lenMax=30 len=( 24 10 6 12 5 4 ) lenLong=( 30 10 10 6 5 4 )

    while read ${LIST_COLUMN_DATA[@]} _; do
        long="${LIST_COLUMN_DATA[0]}"; long="${!long}"

        for (( int=0;int<${#LIST_COLUMN_DATA[@]};int++ )); do
            item="${LIST_COLUMN_DATA[${int}]}"

            if (( ${#long} >= ${lenMax} )); then
                printf "%-${lenLong[${int}]}b " "${!item}"

            elif [[ ${!item} == +(${LIST_ALRT_DATA}) ]]; then
                printf "${LIST_COLOR_ALRT}%-${len[${int}]}b${NORMAL} " "${!item}"

            elif [[ ${!item} == +(${LIST_WARN_DATA}) ]]; then
                printf "${LIST_COLOR_WARN}%-${len[${int}]}b${NORMAL} " "${!item}"

            elif [[ ${!item} == +(${LIST_INFO_DATA}) ]]; then
                printf "${LIST_COLOR_INFO}%-${len[${int}]}b${NORMAL} " "${!item}"

            else
                printf "%-${len[${int}]}b " "${!item}"
            fi
        done
        printf '\n'
    done <<<"$($(exe df) -Ph "${items[@]}" |
            $(exe grep) -i "${reLG}" |
            $(exe sort) -k ${col} |
            $(exe sed) -e 's/\([^ ]*\)\(.*\)/\1\n>> \2/'
            (( ${PIPESTATUS[1]} )) || printf '#\n'
            $(exe df) -Ph "${@}" |
            $(exe grep) -v "\(^Filesystem\|${reIG}\|${reLG}\)" |
            $(exe sort) -k ${col})"

    for (( int=0;int<${#LIST_COLUMN_DATA[@]};int++ )); do
        item="${LIST_COLUMN_DATA[${int}]}"
        [[ ${item} == +(${re}) ]] && continue

        printf "%-${len[${int}]}s " "${item^}"
    done
    printf '\n'
}

list.file() {
    local intD intF intA intR intL IFS
    local item items itemS itemsA itemsT itemsX
    local statArgs statCols
    local printfArgs printfArgsTS printfArgsTN
    local sort sortColN sortColS sortColT sortArgs=( '-f' '-i' '-t :' )
    local type mode uid gid byte time name size action pass reS='^'
    trap '{ shopt_default; }' RETURN

    unset items
    [[ ${1} ]] || items=( * )
    while [[ ${1} ]]; do
        if [[ ${1} == +(-)* ]]; then
            item="${1##*(-)}"
            case "${item}" in
                \?) print_help; return 1 ;;
                dir) action+=( 41ed ) ;;
                exe) action+=(  ) ;;
                file) action+=( 81a4 ) ;;

                dot) shopt -s dotglob ;;
                time) (( time++ )) ;;

                sort) sort+='DN' ;;
                sort-dir) sort+='D' ;;
                sort-case) sortArgs=( "${sortArgs[@]//-f/}" ) ;;
                sort-name) sort+='N' ;;
                sort-size) sort+='S' ;;
                sort-rev*) sortArgs+=( '-r' ) ;;
                sort-+(date|time)) sort+='T'; (( time++ )) ;;
            esac
        elif [[ -d ${1} ]]; then
            items+=( "${1%/}"/?(.)* )
        elif [[ -e ${1} ]]; then
            items+=( "${1}" )
        else
            return
        fi
        shift
    done
    [[ ${items} ]] || items=( * )
    [[ ${items} ]] || return
    statArgs="%f${reS}%a${reS}%u${reS}%g${reS}%s"
    statCols=( 'type' 'mode' 'uid' 'gid' 'byte' )
    printfArgs+="%s:%-20s "  #type${reS}mode
    printfArgs+="%13s:%-13s "  # uid${reS}gid
    printfArgs+="%'15d " # size
    printfArgsTS='32' printfArgsTN='1'
    sortColS='5' sortColT='6'
    if (( time )); then
        statArgs+="${reS}%Y" statCols+=( 'time' )
        printfArgs+='%s ' printfArgsTN=$(( printfArgsTN+16 ))
    fi
    sortColN=$(( ${#statCols[@]}+1 )) statCols+=( 'name' ) statArgs+="${reS}%N"
    printfArgs+='%s'

    #find "${PWD}" -maxdepth 1 \
    #    -printf '%.5m %10M %#9u:%-9g %#5U:%-5G  [%AD | %TD | %CD]  [%Y] %p\n'
    #$(exe stat) -c "${LIST_COLUMN_FILE}" "${items[@]}" |
    #    $(exe sed) -n \
    #        -e "s/\(.*\)41ed\(.*\)/\1$(printf "${RED}41ed${NORMAL}")\2/p"
    while IFS=${reS} read -r "${statCols[@]}"; do
        [[ ${name} ]] || continue
        unset pass IFS intL intD intF
        size=$(( byte+size ))

        [[ ${action} ]] &&  # Type conditional.
            { for item in "${action[@]}"; do
                [[ ${item} == +(${type}|${mode}) ]] && (( pass++ ))
            done; (( pass )) || continue; }

        case "${type}" in
            1*)  # FIFO
                (( intL++ ))
                type='I' type=$(printf "${RED}%s${NORMAL}" "${type}")
                name=$(printf "${RED}%s${NORMAL}" "${name}")
            ;;
            4*)  # Directory
                (( intD++ ))
                type='D' type=$(printf "${MAGENTA}%s${NORMAL}" "${type}")
                name="$(printf "${MAGENTA}%s${NORMAL}" "${name}")/"
            ;;
            8*)  # File
                (( intF++ ))
                type='F' type=$(printf "${ESC256}057m%s${NORMAL}" "${type}")
                case "${mode}" in
                    755) name=$(printf "${GREEN}%s${NORMAL}" "${name}") ;;
                    *) name=$(printf "${ESC256}15m%s${NORMAL}" "${name}") ;;
                esac
            ;;
            a*)  # Symlink
                (( intL++ ))
                type='L' type=$(printf "${CYAN}%s${NORMAL}" "${type}")
                name=$(printf "${CYAN}%s${NORMAL}" "${name}")
            ;;
            c*)  # Sock
                (( intL++ ))
                type='S' type=$(printf "${RED}%s${NORMAL}" "${type}")
                name=$(printf "${RED}%s${NORMAL}" "${name}")
            ;;
        esac
        case "${uid}" in
            0) (( intR++ )); uid=$(printf "${RED}%s${NORMAL}" "${uid}") ;;
            *) (( intA++ )); uid=$(printf "${GREEN}%s${NORMAL}" "${uid}") ;;
        esac
        case "${gid}" in
            *) gid=$(printf "${GREEN}%s${NORMAL}" "${gid}") ;;
        esac

        mode=$(printf "${ESC256}246m%s${NORMAL}" "${mode}")
        [[ ${time} ]] && time="$($(exe date) -d "@${time}" "+${LIST_COLUMN_FILE_DATE}")"

        itemsA+=( "${type}${reS}${mode}${reS}${uid}${reS}${gid}${reS}${byte}${time:+${reS}${time}}${reS}${name//\'/}" )
    done <<<"$($(exe stat) -c "${statArgs[*]}" "${items[@]}")"
    [[ ${itemsA} ]] || return
    items=( "${itemsA[@]}" )

    # Already sorted via bash expansion?
    if [[ ${sort} ]]; then
        [[ ${sort} == *+(D)* ]] &&
            {
            itemsT=( "$(
                { IFS=$'\n'; printf '%s\n' "${items[@]}"; unset IFS; } |
                $(exe grep) -e '/$')" )
                #$(exe sort) ${sortArgs[@]} -n -k "${sortColN}")" )
            itemsT+=( "$(
                { IFS=$'\n'; printf '%s\n' "${items[@]}"; unset IFS; } |
                $(exe grep) -e '[^/]$')" )
                #$(exe sort) ${sortArgs[@]} -n -k "${sortColN}")" )
            items=( "${itemsT[@]}" )
            }
        [[ ${sort} == *+(S)* ]] &&
            items=( "$(
                { IFS=$'\n'; printf '%s\n' "${items[@]}"; unset IFS; } |
                $(exe sort) ${sortArgs[@]} -n -k "${sortColS}")" )
        [[ ${sort} == *+(T)* ]] &&
            items=( "$(
                { IFS=$'\n'; printf '%s\n' "${items[@]}"; unset IFS; } |
                $(exe sort) ${sortArgs[@]} -k "${sortColT}")" )
        [[ ${sort} == *+(N)* ]] &&
            items=( "$(
                { IFS=$'\n'; printf '%s\n' "${items[@]}"; unset IFS; })" )
                #$(exe sort) ${sortArgs[@]} -k "${sortColN}")" )
    fi

    { set -f; IFS=$'\n'${reS}; printf "${printfArgs}\n" ${items[@]}; set +f
    (( ${#itemsA[@]} > 1 ))  &&
        printf "%'${printfArgsTS}d %${printfArgsTN}s%'d\n" "${size}" "${#items[@]}"; } |
        $(exe sed) -n -e 's/,/./g;p'
}

#list.find() {
#    _find() {
#        local item items action 
#
#        while [[ ${1} ]]; do
#            [[ ${1} == ?(-)+(\?|help) ]] && break
#            parse.arg "${@}"; shift ${SHIFT}
#            [[ ! ${ARG} && ${ARG_OPT} ]] && items=( "${ARG_OPT[@]}" )
#            for item in "${ARG[@]}"; do
#                case "${item}" in
#                    file) action='file' ;;
#                    p|path) action='path' ;;
#                    P|path-single) action='path-single' ;;
#                    a|alias) action='alias' ;;
#                    r|realpath) action='realpath' ;;
#                    f|function) action='function' ;;
#                    w|world) action='world' ;;
#                    *) return 1 ;;
#                esac
#            done
#        done
#        _find_action "${action:-file}" "${items[@]}"
#    }
#    _find_action() {
#        local item item2 item3 item4 items=( "${@:2}" )
#
#        case "${1}" in
#            path) builtin type -ap "${items[@]}" ;;
#            alias) builtin alias "${items[@]}" ;;
#            path-single) builtin type -P "${items[@]}" ;;
#            file)
#                if (( ${#items[@]} > 1 )); then
#                    $(exe find) "${items[0]}" -iname "*${items[@]:1}*"
#                else
#                    locate "${items[@]}"
#                fi
#            ;;
#            function)
#                for item2 in "${items[@]}"; do
#                    builtin type "${item2}" |tail -n +2
#                done
#            ;;
#            world) # mode 777, not sock, symlink, dir, tmp.
#                $(exe find) / \
#                    -perm 777 \
#                    -a \! -type s \
#                    -a \! -type l \
#                    -a \! \( -type d -a -perm 1777 \) 2>/dev/null
#            ;;
#            realpath)
#                for item2 in "${items[@]}"; do
#                    for item3 in $(type -ap "${item2}"); do
#                        builtin printf '%s:' "${item3}"
#                        for item4 in $($(exe realpath) "${item3}"); do
#                            [[ ${item3} != ${item4} ]] && builtin printf ' %s' "${item4}"
#                        done
#                        builtin printf '\n'
#                    done
#                done
#            ;;
#        esac
#    }
#    f() { _find --file "${@}"; }
#    fp() { _find --path "${@}"; }
#    fa() { _find --alias "${@}"; }
#    ff() { _find --function "${@}"; }
#    frp() { _find --realpath "${@}"; }
#    fP() { _find --path-single "${@}"; }
#}

list.mount() {
    local _ int long item items col=2
    for item in "${@}"; do
        if [[ ${item} == +([[:digit:]]) ]]; then
            col="${item}"
        else
            items+=( "${item}" )
        fi
    done

    local IFS='|'
    local re="${items[*]}" reI="${LIST_IGNORE_MOUNT[*]}" reL="${LIST_LONG_MOUNT[*]}"
    unset IFS
    local "${LIST_COLUMN_MOUNT[@]}"
    local lenMax=30 len=( 15 30 10 0 ) lenLong=( 15 30 10 0 )

    while read ${LIST_COLUMN_MOUNT[@]} _; do
        long="${LIST_COLUMN_MOUNT[0]}"; long="${!long}"

        for (( int=0;int<${#LIST_COLUMN_MOUNT[@]};int++ )); do
            item="${LIST_COLUMN_MOUNT[${int}]}"

            if (( ${#long} >= ${lenMax} )); then
                printf "%-${lenLong[${int}]}s " "${!item}"

            elif [[ ${!item} == +(${LIST_ALRT_MOUNT}) ]]; then
                printf "${LIST_COLOR_ALRT}%-${len[${int}]}s${NORMAL} " "${!item}"

            elif [[ ${!item} == +(${LIST_WARN_MOUNT}) ]]; then
                printf "${LIST_COLOR_WARN}%-${len[${int}]}s${NORMAL} " "${!item}"

            elif [[ ${!item} == +(${LIST_INFO_MOUNT}) ]]; then
                printf "${LIST_COLOR_INFO}%-${len[${int}]}s${NORMAL} " "${!item}"

            else
                printf "%-${len[${int}]}s " "${!item}"
            fi
        done
        printf '\n'
    done <<<"$({ $(exe grep) -i "${reL}" "${PROC_MOUNTS}" |
            $(exe sort) -s -k "${col}" |
            $(exe sed) -e 's/\([^ ]*\)\(.*\)/\1\n>> \2/'
            (( ${PIPESTATUS[0]} )) || printf '#\n'
            $(exe grep) -vi "\(${reI}\|${reL}\)" "${PROC_MOUNTS}" |
            $(exe sort) -s -k "${col}"; } |
            $(exe grep) -i "\(${re}\)")"

    for (( int=0;int<${#LIST_COLUMN_MOUNT[@]};int++ )); do
        item="${LIST_COLUMN_MOUNT[${int}]}"

        printf "%-${len[${int}]}s " "${item^}"
    done; printf '\n'
}

list.module() {
    $(exe lsmod) |$(exe tail) -n +2
    printf '%-22s %10s %s\n' 'Module' 'Byte' 'Use'
}

list.process() {
    local _ int item items pass long col=1 sort='n'

    unset items
    for item in "${@}"; do
        if [[ ${item} == +([[:digit:]]) ]]; then
            if (( item > 5 )); then
                items+=( "${item}" )
            elif [[ ${LIST_COLUMN_PROCESS[${item}-1]} == +(stat|comm|user|args) ]]; then
                sort='d'
            fi
            col="${item}"
        else
            items+=( "${item}" )
        fi
    done
    shift ${#}

    local IFS='|'; local re="${items[*]}" reI="${LIST_IGNORE_PROCESS[*]}"; unset IFS
    local "${LIST_COLUMN_PROCESS[@]}"
    local reLong=':' lenMax=30 len=( 6 5 5 5 5 7 16 ) lenLong=( 6 5 5 5 5 9 17 )

    while read ${LIST_COLUMN_PROCESS[@]}; do
        long="${LIST_COLUMN_PROCESS[0]}"; long="${!long}"
        [[ ${long} ]] || continue

        (( pass++ ))
        for (( int=0;int<${#LIST_COLUMN_PROCESS[@]};int++ )); do
            item="${LIST_COLUMN_PROCESS[${int}]}"


            if (( ${#long} >= ${lenMax} )); then
                printf "%-${lenLong[${int}]}s " "${!item}"

            elif [[ ${!item} == +(${LIST_ALRT_PROCESS}) ]]; then
                printf "${LIST_COLOR_ALRT}%-${len[${int}]}s${NORMAL} " "${!item}"

            elif [[ ${!item} == +(${LIST_WARN_PROCESS}) ]]; then
                printf "${LIST_COLOR_WARN}%-${len[${int}]}s${NORMAL} " "${!item}"

            elif [[ ${!item} == +(${LIST_INFO_PROCESS}) ]]; then
                printf "${LIST_COLOR_INFO}%-${len[${int}]}s${NORMAL} " "${!item}"

            else
                printf "%-${len[${int}]}s " "${!item}"
            fi
        done
        printf '\n'
    done <<<"$($(exe ps) -o "${LIST_FORMAT_PROCESS}" |
            $(exe grep) -i "\(${re//|/\\|}\)" | 
            $(exe grep) -v "\(^PPID\|${reI//|/\\|}\)" |
            $(exe sort) "-${sort}" -k "${col}")"

    (( pass )) &&
        { for (( int=0;int<${#LIST_COLUMN_PROCESS[@]};int++ )); do
            item="${LIST_COLUMN_PROCESS[${int}]}"

            printf "%-${len[${int}]}s " "${item^}"
        done; printf '\n'; }
}

list.network() {
    $(exe netstat) -tunl "${@}" 2>/dev/null |
        $(exe tail) -n +2
}
alias list.net 'list.network'

list.arguments() {
    local item 
    unset ACTION ITEMS SORT

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
                +(${FORMAT_LIST_DATA_COLUMN})) SORT="${ARG}" ;;
                +(${FORMAT_LIST_PROCESS_COLUMN})) SORT="${ARG}" ;;
                n|net) ACTION='net' ;;
                d|data) ACTION='data' ;;
                f|file) ACTION='file' ;;
                p|proc*) ACTION='proc' ;;
                m|module) ACTION='mod' ;;
            esac
        done
    done
    ACTION="${ACTION:-file}"
}

list.test() {
    source 'test'

    $(exe cmd)
}

list.variables
return 2>/dev/null

_ARGS='(p)rocess (f)ile (d)ata (n)et' 
_DESC="
    Data;
    File; 
    Proc; specify column [${FORMAT_LIST_PROCESS_COLUMN}] to sort, grep argument.
        STAT
            - D, unint-sleep
            - R, running
            - S, int-sleep
            - T, stop
            - Z, zombie"

list.arguments "${@:-?}" || exit

case "${ACTION}" in
    net) list.net "${ITEMS[@]}" ;;
    mod) list.module "${ITEMS[@]}" ;;
    data) list.data "${ITEMS[@]}" ;;
    file) list.file "${ITEMS[@]}" ;;
    proc) list.process "${ITEMS[@]}" ;;
esac

