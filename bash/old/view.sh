#!/usr/bin/env bash
source 'bash.sh' 

view.vim() {
    vim -R \
        -c 'let no_plugin_maps = 1' \
        -c 'set scrolloff=999' \
        -c 'runtime! macros/less.vim' \
        -c 'set foldlevel=999' \
        -c 'set mouse=h' \
        -c 'set nonu' \
        -c 'nmap <ESC>u :nohlsearch<cr>' \
        "${@:--}"
        #<"${TTY}" >"${TTY}"
}

view.file() { view.vim "${@}"; }
view.pdf() { $(exe "${CMD_PDF}") "${@}"; }

view.man() {
    local item 

    for item in "${@}"; do

        $(exe groff) -e -t -mandoc -Tascii "${item}" |
            $(exe col) -bx |${CMD_VIEW} -

        #$(exe grep) . "${item}" |
        #    $(exe tbl) |
        #    $(exe nroff) -mandoc |
        #    ${CMD_VIEW} -
    done
}
view.man_find() {
    local item items=( "${PATH_MAN//:/ }" ) 

    for item in "${@}"; do
        [[ ${item} == *+(/)* ]] && items=( "${item%/*}" )

        $(exe find) ${items[@]} -iname "${item##*/}"
    done
}

view.case() {
    local _ARGS='(man|stdin) <files>'
    local _DESC="
        View files through handle."
    local item items item2 items2 itemsP action

    while [[ ${1} == +(-)* ]]; do
        item="${1##*+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            man) action="${item}" ;;
            stdin) action="${item}" ;;
        esac
        shift
    done
    items=( "${@}" )

    for item in "${items[@]}"; do
        [[ ${item} ]] || continue

        if [[ -e ${item} ]]; then
            case "${item}" in
                *.pdf) view.pdf "${item}" & ;;
                *+(.[[:digit:]])|*+(.man)) view.man "${item}" ;;
                *) view.file "${item}" ;;
            esac

        else
            case "${action:-null}" in
                stdin) view.file ;;
                man|*)
                    selects "$(view.man_find "${item}")"
                    for item2 in "${SELECT[@]}"; do
                        [[ -e ${item2} ]] || continue

                        view.man "${item2}"
                    done 
                ;;
            esac
        fi

        (( ${#items[@]} > 1 )) && [[ ${item} != ${items[${#items[@]}-1]} ]] &&
            { read_true -1 --erase 'Continue' || break; }
    done
}

view.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                \-) ACTION='stdin' ;;
                m|man) ACTION='man' ;;
                p|pdf) ACTION='pdf' ;;
            esac
        done
    done
    ITEMS=( "${ITEMS[@]:-stdin}" )
}

return 2>/dev/null

_ARGS='(man) (pdf) <file|->'
_DESC="
    Print output through scroll mechanism.

    Man, parse format.
    Pdf, ${CMD_PDF}.
    File, files.
    -, stdin."

view.arguments "${@:-?}" || exit

view.case "${ACTION:+--${ACTION}}" "${ITEMS[@]}"
