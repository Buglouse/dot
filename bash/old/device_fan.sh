#!/usr/bin/env bash
source 'bash.sh'

device.fan.variables() {
    export SYS_CLASS_HWMON '/sys/class/hwmon'
    [[ -d ${SYS_CLASS_HWMON} ]] || return

    device.fan.id
}

device.fan.id() {
    local item
    unset FAN_ID FAN_IDS FAN_COUNT

    for item in ${SYS_CLASS_HWMON}/hwmon+([[:digit:]]); do
        if [[ -e ${item}/pwm1_enable ]]; then
            FAN_ID["${item##*hwmon}"]=$(<"${item}/pwm1_enable")
            FAN_IDS+=( "${item##*hwmon}" )
        else
            continue
        fi
        (( FAN_COUNT++ ))
    done

    export FAN_ID
    export FAN_IDS
    export FAN_COUNT
}
device.fan.rpm() {
    local item item2
    unset FAN_RPM

    for item in "${FAN_IDS[@]}"; do
        (( ${FAN_ID[${item}]} )) || continue

        for item2 in ${SYS_CLASS_HWMON}/hwmon${item}/fan+([[:digit:]])_input; do
            FAN_RPM[${item}]=$(<"${item2}")
        done
    done

    export FAN_RPM
}

device.fan.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                i|id) ACTION='id' ;;
                s|rpm) ACTION='rpm' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

device.fan.variables
return 2>/dev/null

_ARGS=''
_DESC="Fan Device"

device.fan.arguments "${@:-?}" || exit

source 'request.sh'
case "${ACTION}" in
    rpm) device.fan.rpm ;;
esac
