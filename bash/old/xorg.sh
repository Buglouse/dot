#!/bin/env bash
trap "exit 0" USR1
(
    trap "" USR1
    exec Xorg
) &
wait
exit 1
