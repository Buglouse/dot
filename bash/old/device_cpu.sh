#!/usr/bin/env bash
source 'bash.sh'

device.cpu.variables() {
    device.cpu.module

    export SYS_CLASS_THERMAL '/sys/class/thermal'
    export SYS_DEVICES_SYSTEM_CPU '/sys/devices/system/cpu'
    export SYS_DEVICES_PLATFORM '/sys/devices/platform'

    export LIMIT_CPU_TEMP_MAX '55'
    export LIMIT_CPU_TEMP_MIN '48'

    device.cpu.id
    device.cpu.frequency
    device.cpu.tempurature
}

device.cpu.module() {
    modprobe 'coretemp'
}

device.cpu.id() {
    local item
    unset CPU_ID CPU_COUNT

    for item in ${SYS_DEVICES_SYSTEM_CPU}/cpu+([[:digit:]]); do
        if [[ -e ${item}/online ]]; then
            CPU_ID[$(<"${item}/topology/core_id")]=$(<"${item}/online")
        elif [[ ${item} == *0 ]]; then
            CPU_ID[$(<"${item}/topology/core_id")]=0
        else
            continue
        fi
        (( CPU_COUNT++ ))
    done
    export CPU_COUNT
    export CPU_ID
}

device.cpu.tempurature() {
    local item int
    unset CPU_TEMP

    for (( int=0;int<CPU_COUNT;int++ )); do
        for item in "${SYS_DEVICES_PLATFORM}/coretemp.0"; do
            exist "${item}/temp${int}_label" || continue
            id=$(<"${item}/temp${int}_label")
            [[ ${id%% *} == Core ]] || continue
            CPU_TEMP[${id##* }]=$(<"${item}/temp${int}_input")
        done
    done
    export CPU_TEMP
}

device.cpu.frequency() {
    local int
    unset CPU_FREQ

    for (( int=0;int<CPU_COUNT;int++ )); do
        exist_dir "${SYS_DEVICES_SYSTEM_CPU}/cpu${int}/cpufreq" || continue
        CPU_FREQ[${int}]=$(<"${SYS_DEVICES_SYSTEM_CPU}/cpu${int}/cpufreq/scaling_cur_freq")
    done
    export CPU_FREQ
}

device.cpu.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                f|freq*) ACTION='freq' ;;
                i|id) ACTION='id' ;;
                t|temp*) ACTION='temp' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

device.cpu.variables
return 2>/dev/null

_ARGS=''
_DESC="CPU Device. (:!:sudo)"

device.cpu.arguments "${@:-?}" || exit

case "${ACTION}" in
    freq) device.cpu.frequency ;;
    temp) device.cpu.tempurature ;;
esac
