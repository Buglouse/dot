#!/usr/bin/env bash
source 'bash.sh'

repository.variables() {
    export DIR_REPO "${DIR_REPO:-${DIR_TMP}}"  #:order:0
    until [[ -d ${DIR_REPO} ]]; do _read 'Directory: ' DIR_REPO; done  #:order:1

    export DIR_DOT "${DIR_REPO}/dot"
    export MSG_COMMIT "${MSG_COMMIT:-Commit.}"
    export FILE_HG_COMMIT "${HOME}/.mercurial/commit.txt"
    export PID_REPO "${DIR_TMP_PID}/repository.pid"
}

repository.data() {
    local item dir srv repo pull commit msg="--message ${MSG_COMMIT}"
    [[ -e ${FILE_HG_COMMIT} ]] && msg="--logfile ${FILE_HG_COMMIT}"

    for item in "${@}"; do
        dir="${DIR_REPO}/${item##*-}"  #:order:0

        srv="ssh://${item}"
        pull="pull --update"
        repo="--repository ${dir}"
        commit="commit --addremove"

        print_true '(%s)' "${srv}"
        print_type ' <-%s-> ' "SSH"
        print_warn '(%s)\n' "${dir}"

        request.ping --fail "${srv}" || return

        [[ -d ${dir} ]] || { hg clone "${srv}" "${dir}"; continue; }
        if hg ${repo} ${commit} ${msg}; then
            { hg --quiet ${repo} ${pull} && hg ${repo} push; } ||
                if hg ${repo} merge; then
                    hg ${repo} ${commit} ${msg} &&
                        hg --quiet ${repo} ${pull}
                else
                    hg ${repo} resolve --list
                fi
        elif (( ${?} == 255 )); then
            return 1
        else
            hg ${repo} ${pull}
        fi
    done
}

repository.copy() {
    local path

    path=( ${@:1:${#}-1} )

    if [[ -e ${path} ]]; then
        [[ -d ${DIR_DOT}/${!#} ]] || mkdir "${DIR_DOT}/${!#}"
        $(exe cp) -vu "${path[@]}" "${DIR_DOT}/${!#}" 2>&1 |grep -v 'are the same file'
    else
        print_exit 301 ${path[@]}
        (( 0 ))
    fi
}

repository.arguments() {
    local item 
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        if [[ ! ${ARG} && ${ARG_OPT} ]]; then
            if [[ ${ACTION} == data ]]; then
                for item in "${ARG_OPT[@]}"; do
                    if [[ ${item} == */* ]]; then
                        ITEMS+=( "${item}" )
                    else
                        MSG_COMMIT="${item}"
                    fi
                done
            else
                ITEMS=( "${ARG_OPT[@]}" )
            fi
        fi
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                D|data) ACTION='data' ;;
                c|copy) ACTION='copy' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

repository.variables
source 'request'
return 2>/dev/null

_ARGS='(D)ata (c)opy'
_DESC=""

repository.arguments "${@:-?}" || exit

case "${ACTION}" in
    data)
        if parse.pid --lock "${PID_REPO}" "${$}"; then
            trap '{ parse.pid --term "${PID_REPO}"; }' EXIT
            repository.data "${ITEMS[@]}"
        else
            [[ -e ${PID_REPO} ]] || exit
            print_false 'Instance Active: '
            print_warn '%s\n' $(<"${PID_REPO}")
        fi
    ;;
    copy) repository.copy "${ITEMS[@]}" ;;
esac
