#!/usr/bin/env bash
source 'bash.sh'

power.variables() {
    export PID_POWER "${DIR_TMP_PID}/power.pid"
    export POWER_SUSPEND_TYPE $(<"${SYS_POWER_STATE}")
}

power.suspend() {
    local IFS='|'; local re="${POWER_SUSPEND_TYPE[*]}"; unset IFS
    local _ARGS="[${re}] [seconds]"
    local _DESC="
        ${re^}, suspend type.
        Seconds, time in epoc to stay suspended."
    local item action intT

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            +(${re})) action="${item}" ;;
            +([[:digit:]]))
                item=$(date -ud "${item}" '+%s')  # Epoch
                [[ ${item} ]] && intT="${item}"
            ;;
        esac
        shift
    done

    kill "${CMD_XORG}"
    if [[ ${intT} ]]; then
        $(exe rtcwake) -u -m "${action}" -t ${intT} || return
    else
        printf '%s\n' "${action}" |sudo $(exe tee) "${SYS_POWER_STATE}"
    fi
    [[ ${CMD_RECOVER} ]] && ${CMD_RECOVER}
}

power.ask() {
    read_false -3 'Reboot' && sudo $(exe reboot)
    read_true -3 'Poweroff' && sudo $(exe poweroff)
}
power.halt() { read_true -3 'Poweroff' && sudo $(exe poweroff); }
power.reboot() { read_true -3 'Reboot' && sudo $(exe reboot); }

power.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
                ask) ACTION='ask' ;;
                M|mem) ACTION='mem' ;;
                r|reboot) ACTION='reboot' ;;
                h|halt|off|power*|p) ACTION='halt' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

source 'device_power'
power.variables
return 2>/dev/null

_ARGS='(M|mem) (p)ower (r)eboot (a)sk (d)ate'
_DESC="
    :!:sudo
    Date, time for action [[[[[[YY]YY]MM]DD]hh]mm[.ss].
    Mem, suspend.
    ABRT signal, terminate process."

power.arguments "${@:-?}" || exit
parse.pid -lock "${PID_POWER}" || exit
trap '{ parse.pid -term "${PID_POWER}"; }' EXIT

case "${ACTION}" in
    mem) power.suspend "-${ACTION}" ;;
    halt) power.halt ;;
    reboot) power.reboot ;;
    ask|*) power.ask ;;
esac
