#!/usr/bin/env bash
source 'bash.sh'

cron.variables() {
    export DIR_CRON "${HOME}/.cron"
    export LOG_CROND "${DIR_TMP_LOG}/crond.log"
    export LOG_CRON "${DIR_TMP_LOG}/cron.log"
}

cron.daemon() {
    local crondArg=( -L "${LOG_CROND}" -c "${DIR_CRON}" )

    kill 'crond'
    sudo $(exe crond) -f -d 0 ${crondArg[@]}
}

cron.repository() {
    print_warn '    Repository: '
    if repository.sh -D &>/dev/null; then
        print_true 'True'
    else
        print_false 'False'
    fi
    printf '\n'
}

cron.battery() {
    local state=( $(battery.sh -s) )

    ! [[ ${state[0]##*#} == Discharging ]] && return

    local perc=( $(battery.sh -p) )

    if (( ${perc[0]##*%} < 10 )); then
        print_log "${RED}[ALERT] (RAM Suspend)"
        power.sh -M 

    elif (( ${perc[0]##*%} < 15 )); then
        print_log "${YELLOW}[WARN] (%s)" "${perc[0]}"

    elif (( ${perc[0]##*%} > 40 )); then
        print_log "${GREEN}[OK] (%s)" "${perc[0]}"
    fi
}

cron.arguments() {
    local v args=( "${@:-?}" )
    unset ACTION ARGS WRAP

    while [[ ${args} ]]; do
        parse.arg "${args[@]}"; args=( "${args[@]:${SHIFT}}" )
        [[ ! ${ARG} && ${ARG_OPT} ]] && ARGS=( ${ARG_OPT[@]} )
        for v in "${ARG[@]}"; do
            case "${v}" in
                \?) unset ACTION; break ;;
                D|daemon) ACTION='daemon' ;;
                W|wrap*) ACTION='wrap' ;;
                r|repo*) WRAP+=( 'repository' ) ;;
                b|bat*) WRAP+=( 'battery' ) ;;
            esac
        done
    done
    if [[ ${ACTION} ]]; then
        if [[ ${ACTION} == wrap ]]; then
            [[ ${WRAP} ]] || return
        else
            (( 1 )) 
        fi
        (( 1 )) 
    fi
}

cron.variables
return 2>/dev/null

_ARGS='(D)aemon'
_DESC="Cron."

cron.arguments "${@}" || exit

case "${ACTION}" in
    daemon) cron.daemon ;;
    wrap)
        { for item in "${WRAP[@]}"; do
            $(exe date) "+%Y.%m.%d.%s %R%z %s"
            case "${item}" in
                #repsitory) cron.repository ;;
                battery) cron.battery ;;
            esac
        done; } >>"${LOG_CRON}"
    ;;
esac

