#
set_default() {
    # histexpand nounset
    builtin export SET_DISABLE='xtrace:errexit'
    builtin export SET_ENABLE='notify:noclobber:ignoreeof:physical:hashall'

    builtin set -o ${SET_ENABLE//:/ }
    builtin set +o ${SET_DISABLE//:/ }
}

shopt_default() {
    builtin export SHOPT_DISABLE='extdebug:mailwarn'
    builtin export SHOPT_ENABLE='cdable_vars:cdspell:checkhash:checkjobs:checkwinsize:cmdhist:expand_aliases:extglob:extquote:globstar:histappend:histreedit:histverify:hostcomplete:interactive_comments:nocaseglob:no_empty_cmd_completion:nullglob:progcomp:promptvars:sourcepath'
        # 'failglob'

    builtin shopt -s ${SHOPT_ENABLE//:/ }
    builtin shopt -u ${SHOPT_DISABLE//:/ }
}
shopt_default; set_default

export() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _DESC="
        Export ${@:1:1} with value: ${*:2}."

    local item items IFS

    [[ ${1} ]] || return

    builtin export -- "${1}"
    if (( ${#} == 2 )); then
        #IFS=$'\n'; mapfile -t "${1}" <<<"${*:2}"; unset IFS
        read -r "${1}" <<<"${2}"

    elif (( ${#} > 2 )); then  # Array
        #IFS=$'\n'; mapfile -t "${1}" <<<"${*:2}"; unset IFS
        read -ra "${1}" <<<"${*:2}"

    elif [[ ${!1} ]]; then
        #printf -v item '%s[@]' "${1}"; item=( "${!item}" )
        #if [[ ${#item[@]} -gt 1 ]] ; then
        #    #IFS=$'\n'; mapfile -t "${1}" <<<"${item[*]}"; unset IFS
        #else
        #    #printf -v "${1}" '%s' "${item}"
        #    read -r "${1}" <<<"${item}"
        #fi
        true
    fi
    [[ ${!1} ]] && builtin export -- "${1}"

    (( VERBOSE_EXPORT )) &&
        if [[ ${!1} ]]; then
            print_true "%s${NORMAL}=" "${1}" >&2
            if (( ${#} > 2 )); then
                printf -v item '%s' "${1}[*]"
                print_warn '( %s )\n' "${!item}" >&2
            else
                print_warn '%s\n' "${!1}" >&2
            fi

        else
            print_false '%s\n' "${1}" >&2
        fi

    [[ ${!1} ]]
}

unset_global() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local item

    for item in "${@}"; do
        builtin unset "${item}"
        while [[ ${!item} ]]; do
            builtin unset "${item}"
        done
    done
}

source() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _DESC="
        Append path of file to ${PATH}.
        If not sourced add to ${SOURCES} and source."
    local item itemF rtn IFS

    item="${BASH_SOURCE[0]%/*}"
    [[ ${item} == /* ]] || item="${PWD}/${item}"
    exist_action elem "${item}" ${PATH//:/ } || PATH="${item}${PATH:+:${PATH}}"

    #shopt_default; set_default
    for item in "${@}"; do
        [[ ${item} == *profile || ${item} == *.sh ]] || item="${item}.sh"
        itemF="${item##*/}"
        exist_action elem "${itemF}" ${SOURCES//:/ } && continue
        SOURCES+="${SOURCES:+:}${itemF}"; export SOURCES
        #printf '%s[%s]\n' "${itemF}" "${SOURCES}"

        #time {
        if builtin source "${item}"; then
            (( VERBOSE_SOURCE )) && print_true '%s\n' "${itemF}"
        else
            (( VERBOSE_SOURCE )) && print_false '%s\n' "${itemF}"
            (( rtn++ ))
        fi
        #}
    done

    #export FUNCTIONS $(declare -Fp |cut -d' ' -f3)
    #for item in "${FUNCTIONS[@]}"; do declare -xf "${item}"; done
    ! (( rtn ))
}

pushd() {
    local item item2 items itemC IFS

    for item in "${@}"; do 
        if [[ ${item} == .?(/) ]]; then
            [[ ${STACK_DIRS} ]] && printf '%s\n' "${STACK_DIRS[*]}"
            return

        elif [[ ${item} == +(-) ]]; then
            item="${PWD%/*}"

        elif [[ ${item} == .. ]]; then
            item="${PWD%/*}"

        elif ! [[ ${item} == +(/)* ]]; then  # Relative
            itemC="${item^^}"

            if [[ -d ${item} ]]; then
                item="${PWD}/${item}"

            elif [[ ${!item} ]]; then
                item="${!item}"

            elif [[ ${!itemC} ]]; then
                item="${!itemC}"

            else # Find
                IFS=':'; for item2 in ${PATH_DIRS[*]} ""; do
                    [[ -d ${item2}/${item} ]] && { item="${item2}/${item}"; break; }
                done; unset IFS
            fi
        fi
        [[ -d ${item} ]] || continue

        for item2 in "${STACK_DIRS[@]}"; do
            [[ ${item2} == ${item} ]] && break 2
        done

        STACK_DIRS+=( "${item}" )
        export STACK_DIRS
    done
    [[ ${STACK_DIRS} ]] && printf '%s\n' "${STACK_DIRS[*]}"
    [[ ${STACK_DIRS} ]] && item="${STACK_DIRS[${#STACK_DIRS[@]}-1]}"
    [[ ! -d ${item} && -d ${DIR_DEFAULT} ]] && item="${DIR_DEFAULT}"
    builtin cd "${item}"
}
popd() {
    local item

    if (( ${#STACK_DIRS[@]} )); then
        STACK_DIRS=( "${STACK_DIRS[@]::${#STACK_DIRS[@]}-1}" )

        (( ${#STACK_DIRS[@]} > 1 )) &&
            while [[ ${PWD} == ${STACK_DIRS[${#STACK_DIRS[@]}-1]} ]]; do
                STACK_DIRS=( "${STACK_DIRS[@]::${#STACK_DIRS[@]}-2}" )
            done
        (( ${#STACK_DIRS[@]} > 1 )) &&
            until [[ -d ${STACK_DIRS[${#STACK_DIRS[@]}-1]} ]]; do 
                (( ${#STACK_DIRS[@]} > 1 )) || break
                STACK_DIRS=( "${STACK_DIRS[@]::${#STACK_DIRS[@]}-2}" )
            done
    fi

    if (( ${#STACK_DIRS[@]} )); then
        #printf "${YELLOW}%s\n${NORMAL}" "${STACK_DIRS[*]}"
        printf "%s\n" "${STACK_DIRS[*]}"
        item="${STACK_DIRS[${#STACK_DIRS[@]}-1]}"
    fi
    builtin cd "${item:-${DIR_DEFAULT}}"
}

selects() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _ARGS='(count) <items>'
    local _DESC="
        Count, is a digit of total selects.
        Items, supports expansion.
        Terminate if arg is space or ends in space."
    local item items min='1' max="${#}" limit="${#}"
    unset SELECT REPLY

    while [[ ${1} == +(-)* ]]; do
        item="${1##+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            +([[:digit:]])) limit="${item}" ;;
        esac
        shift
    done

    if ! [[ -t 0 && -t 1 ]] || (( ${#} == 1 )); then
        SELECT=( "${1}" ) REPLY=1
    else
        read_clear
        select item in "${@}"; do
            case "${REPLY}" in
                +([[:digit:][:space:]]))
                    if [[ ${REPLY//+([[:space:]])/} == +([[:digit:]]) ]]; then
                        (( REPLY < min || REPLY > max )) &&
                            { print_false 'Range(%s)\n' "${max}"; continue; }
                        SELECT+=( "${item}" )

                    else  # Default and terminate.
                        [[ ${SELECT} ]] && break
                        SELECT=( "${1}" ); break
                    fi
                    [[ ${REPLY##*([[:digit:]])} == +([[:space:]]) ]] && break
                ;;
                \*|\.) SELECT=( "${@}" ) REPLY="${#}"; break ;;
                q) break ;;
                *) continue ;;
            esac
            SELECT=( $(_uniq "${SELECT[@]}") )
            (( ${#SELECT[@]} < ${limit} )) || break
            read_clear
        done

        [[ ${REPLY//+([[:space:]])/} ]] || REPLY=1
        export REPLY
        export SELECT
    fi

    (( ${#SELECT[@]} ))
}

path_find() {
    local IFS='|'; local re="${*}"; unset IFS
    $(exe find) ${PATH//:/ } -iname "${re}"
}

history() {
    [[ -e ${HISTFILE} ]] || return
    local IFS='|'; local re="${*}"; unset IFS

    $(exe grep) -i "${re}" "${HISTFILE}"
}

exit() { builtin exit; }
export SIGNAL_EXIT 'TERM' 'KILL' 'INT' 'QUIT' 'LOCK'
exit_signal() {
    local item="${1,,}"; item="${item//sig}"

    unset SIGNAL; case "${item}" in
        7|bus) SIGNAL='BUS' ;;
        8|fpe) SIGNAL='FPE' ;;
        1|hup) SIGNAL='HUP' ;;
        4|ill) SIGNAL='ILL' ;;
        2|int) SIGNAL='INT' ;;
        30|pwr) SIGNAL='PWR' ;;
        31|sys) SIGNAL='SYS' ;;
        23|urg) SIGNAL='URG' ;;
        6|abrt) SIGNAL='ABRT' ;;
        9|kill) SIGNAL='KILL' ;;
        3|quit) SIGNAL='QUIT' ;;
        5|trap) SIGNAL='TRAP' ;;
        14|alrm) SIGNAL='ALRM' ;;
        17|chld) SIGNAL='CHLD' ;;
        18|cont) SIGNAL='CONT' ;;
        13|pipe) SIGNAL='PIPE' ;;
        29|poll) SIGNAL='POLL' ;;
        27|prof) SIGNAL='PROF' ;;
        11|segv) SIGNAL='SEGV' ;;
        19|stop) SIGNAL='STOP' ;;
        15|term) SIGNAL='TERM' ;;
        20|tstp) SIGNAL='TSTP' ;;
        21|ttin) SIGNAL='TTIN' ;;
        22|ttou) SIGNAL='TTOU' ;;
        10|usr1) SIGNAL='USR1' ;;
        12|usr2) SIGNAL='USR2' ;;
        24|xcpu) SIGNAL='XCPU' ;;
        25|xfsz) SIGNAL='XFSZ' ;;
        64|rtmax) SIGNAL='RTMAX' ;;
        32|rtmin) SIGNAL='RTMIN' ;;
        28|winch) SIGNAL='WINCH' ;;
        16|stkflt) SIGNAL='STKFLT' ;;
        26|vtalrm) SIGNAL='VTALRM' ;;
        #-----#
        47|lock) SIGNAL='LOCK' ;;
    esac; export SIGNAL
    [[ ${SIGNAL} ]]
}
exit_msg() {
    unset MSG_EXIT

    case "${1}" in
        0|EX_OK)             MSG_EXIT="Exit: ${0}" ;;
        1|EX_FAIL)           MSG_EXIT="Exit: ${0}" ;;
        2)                   MSG_EXIT='Misuse of Shell Builtin.' ;;
        4|EX_HALT)           MSG_EXIT="HALT: ${0}" ;;
        32|EX_PROC)          MSG_EXIT='Too many processes.' ;;
        45|EX_VARS)          MSG_EXIT='Invalid variable declaration.' ;;
        46|EX_ARGS)          MSG_EXIT='Invalid argument.' ;;
        64|EX_USAGE)         MSG_EXIT='Usage error.' ;;
        65|EX_DATAERR)       MSG_EXIT='Data format error.' ;;
        66|EX_NOINPUT)       MSG_EXIT='Failed to open STDIN.' ;;
        67|EX_NOUSER)        MSG_EXIT='Unknown addressee.' ;;
        68|EX_NOHOST)        MSG_EXIT='Unknown hostname.' ;;
        69|EX_UNAVAILABLE)   MSG_EXIT='Unavailable Service.' ;;
        70|EX_SOFTWARE)      MSG_EXIT='Internal error.' ;;
        71|EX_OSERR)         MSG_EXIT='Fork error.' ;;
        72|EX_OSFILE)        MSG_EXIT='Critical file missing.' ;; 
        72|EX_CANTCREAT)     MSG_EXIT='Unable to pipe STDOUT.' ;;
        73|EX_IOERR)         MSG_EXIT='I/O error.' ;;
        74|EX_TEMPFAIL)      MSG_EXIT='Temp error.' ;;
        75|EX_PROTOCOL)      MSG_EXIT='Protocol error.' ;;
        76|EX_NOPERM)        MSG_EXIT='Invalid permissions.' ;;
        77|EX_CONFIG)        MSG_EXIT='Configuration error.' ;;
        110|EX_PATH)
            MSG_EXIT=( 'Invalid path: %s\nFound: %s' "${1}"
                $($(exe find) ${PATH//:/ } -not -type d -name "${1}" 2>/dev/null) )
        ;;
        126)    MSG_EXIT=( 'Failure to execute: %s\nFound: %s' "${2}" "${paths[@]}" ) ;;
        127)    MSG_EXIT=( "Not found: ${YELLOW}%s\n" "${EXIT:-${2}}" ) ;;
        128)    MSG_EXIT="exit has invaild argument." ;;
        129)    exit_signal '1'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        130)    exit_signal '2'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        131)    exit_signal '3'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        132)    exit_signal '4'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        133)    exit_signal '5'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        134)    exit_signal '6'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        135)    exit_signal '7'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        136)    exit_signal '8'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        137)    exit_signal '9'; MSG_EIXT=( "${SIGNAL}" "${0}" ) ;;
        255)    MSG_EXIT='Exit code out of range (<255)' ;;
        301)    MSG_EXIT='Missing:' ;;
        300|*)  MSG_EXIT="Undefined Message: ${1}" ;;
    esac

    export MSG_EXIT
}

subshell() {
    local pid cmd state ppid pgrp sess tty tpgid _ 

    read pid cmd state ppid pgrp sess tty tpgid _ </proc/self/stat
    tpgid="${tpgid}"
    (( ${$} == tpgid ))
}

exist() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local action rtn

    case "${1##+(-)}" in
        \?) return 1 ;;
        mnt|elem|only|dir|exe|file|link|path|proc|size) action="${1#--}" ;;
    esac
    [[ ${action} ]] && shift

    exist_action "${action:-file}" "${@}"; rtn="${?}"
	(( rtn && VERBOSE )) && exist_verbose "${@}"

    ! (( rtn ))
}
exist_action() {
    local item item2 item3 items=( "${@:2}" )
    local IFS='|'; local re="${*:2}"; unset IFS

    for item in "${items[@]}"; do
        case "${1}" in
            dir) [[ -d ${item} ]] ;;
            exe) type -P "${item}" &>/dev/null ;;
            link) [[ -L ${item} ]] ;;
            file) [[ -f ${item} ]] ;;
            size) [[ -e ${item} && $($(exe stat) -c '%s' "${item}") -gt 1 ]] ;;
            mnt)
                local _ path
                
                while read _ path _; do
                    #:fix:Match optional '/' suffix.
                    [[ ${path} == +(${re}) ]] && break
                done <<<"$(<"${PROC_MOUNTS}")"
            ;;
            path)
                [[ -f ${item} ||
                    $($(exe find) ${PATH//:/ } -not -type d -name "${item}" 2>/dev/null) ]]
            ;;
            elem)
                (( ${#items[@]} > 1 )) || return
                for item2 in "${items[@]:1}"; do
                    [[ ${item} == ${item2} ]] && break
                done && break 
                (( 0 ))
            ;;
            only)
                printf -v item '%s[@]' "${items[@]:${#items[@]}-1}"
                for item2 in "${!item}"; do
                    for item3 in "${items[@]::${#items[@]}-1}"; do
                        [[ ${item2} == ${item3} ]] && break
                    done || return
                done && break
            ;;
            proc)
                if [[ ${item} == +([[:digit:]]) ]]; then  # PID
                    [[ -e ${DIR_PROC}/${item} ]] &&
                        ! $(exe grep) 'State' "${DIR_PROC}/${item}/status" |
                            $(exe grep) -q 'stopped'

                elif [[ -e ${item} ]]; then  # PID file
                    printf -v item $(<"${item}")  # Remove whitespace
                    [[ -e /proc/${item:-0} ]]

                elif [[ ${item} != */* ]]; then
                    $(exe pgrep) -f -- "(bin/${item}|^${item}|[ ]+${item})\b" >/dev/null

                else
                    (( 0 ))
                fi
            ;;
            wrap)  # TCP Wrapper (libwrap|tcp_wrapper) support.
                local _ lib pass

                while read lib _; do
                    [[ ${lib} == libwrap* ]] && (( pass++ ))
                done
                (( pass ))
            ;;
            *) [[ -e ${item} ]] ;;
        esac && continue
        return
    done
}
exist_verbose() {
    case "${1}" in
        exe) print_false 'Invalid Executable: ' ;;
        mnt) print_false 'Invalid Mount: ' ;;
        link) print_false 'Invalid Link: ' ;;
        proc) print_false 'Invalid Process: ' ;;
        elem|else) print_false 'Invalid Element: ' ;;
        dir|file|path|*) print_false 'Invalid Path: ' ;;
    esac; print_warn '%s\n' "${*}"
}
exist_mnt() { exist --mnt "${@}"; }
exist_exe() { exist --exe "${@}"; }
exist_dir() { exist --dir "${@}"; }
exist_elem() { exist --elem "${@}"; }
exist_file() { exist --file "${@}"; }
exist_size() { exist --size "${@}"; }
exist_link() { exist --link "${@}"; }
exist_only() { exist --only "${@}"; }
exist_path() { exist --path "${@}"; }
exist_proc() { exist --proc "${@}"; }
exist_wrap() { exist --wrap "${@}"; }

_read() {
    (( TEST )) || # TTY redirection.
        { [[ -t 0 && -t 1 ]] || return; }
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; unset TMOUT; }' RETURN EXIT
    local item items action erase rtn TMOUT

    while [[ ${1} == +(-)* ]]; do
        item="${1##+(-)}"
        case "${item}" in
            key) action='key' ;;
            true) action='true' ;;
            false) action='false' ;;
            silent) action='silent' ;;

            erase) (( erase++ )) ;;
            +([[:digit:]])) TMOUT="${item}" ;;
        esac
        shift
    done
    items=( "${@}" )

    read_clear
    case "${action}" in
        true)
            read -s -n 1 -p "$(printf "${BOLD}${GREEN}?${NORMAL}${items}")" rtn
            if (( erase )); then print_erase "?${items}"; else printf '\n'; fi
            [[ ! ${rtn} == +(n|N) || -z ${rtn} ]]
        ;;
        false)
            read -s -n 1 -p "$(printf "${BOLD}${RED}?${NORMAL}${items}")" rtn
            if (( erase )); then print_erase "?${items}"; else printf '\n'; fi
            [[ ${rtn} == +(y|Y) ]]
        ;;
        key)
            local key prompt min='8' max='8' char='*' var="${items:-KEY}"
            local prompt="${items[@]:2}"
            prompt="${prompt:-Key: }"

            until (( 0 )); do
                until (( ${#key} == max )); do
                    unset key; hold='.'

                    print_warn "${prompt}"
                    until [[ ${hold} == '' ]]; do
                        read -s -n 1 hold; key+="${hold}"
                        [[ ${hold} == @(''|$'\b') ]] || printf '%s' "${char}"
                    done

                    if (( ${#key} < min )); then
                        print_false '!Too short\n'

                    else
                        unset keyVerify; hold='.'

                        print_erase "${prompt}" "${#key}"
                        print_warn 'Verify: '
                        until [[ ${hold} == '' ]]; do
                            read -s -n 1 hold; keyVerify+="${hold}"
                            [[ ${hold} == '' ]] || printf '%s' "${char}"
                        done

                        if [[ ${key} != ${keyVerify} ]]; then
                            print_false '!Invalid\n'
                            unset key
                        else
                            printf '\n'
                            rtn=0
                            break 2
                        fi
                    fi
                done
            done
            export "${var}" "${key}"
        ;;
        silent)
            read -s -p "${items}" "${items[@]:2}"
            if (( erase )); then print_erase "?${items}"; else printf '\n'; fi
        ;;
        *) read -e -p "${items}" "${items[@]:2}" ;;
    esac; rtn=${?}

    ! (( rtn ))
}
read_key() { _read --key "${@}"; }
read_true() { _read --true "${@}"; }
read_clear() { builtin read -d'' -e -t 0.001 _; }
read_false() { _read --false "${@}"; }
read_silent() { _read --silent "${@}"; }

print() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local item action 
    local items=( 'exit' 'help' 'info' 'init' 'init_true' 'init_false'
        'link' 'mode' 'ptmx' 'rand' 'true' 'type' 'verb' 'warn'
        'false' 'erase' 'false_file' )
    local IFS='|'; local re="${items[*]}"; unset IFS

    while [[ ${1} == +(-)* ]]; do
        item="${1##+(-)}"
        case "${1##+(-)}" in
            +(${re})) action="${item}" ;;
            '') break ;;
        esac
        shift
    done

    print_action "${action:-null}" "${@}"
}
print_action() {
    local item items arg action="${1}"; shift
    unset items

    #$(exe gettext) --version >/dev/null &&
    type -P gettext >/dev/null &&
        for item in "${@}"; do
            items+=( "$($(exe gettext) "${item}")" )
        done

    arg=( "${items[@]:-${@}}" )

    case "${action}" in
        false) printf "${NORMAL}${RED}${arg}${NORMAL}" "${arg[@]:1}" >&2 ;;
        info) printf "${NORMAL}${BOLD}${BLACK}${arg}${NORMAL}" "${arg[@]:1}" >&2 ;;
        init) printf "${NORMAL}${YELLOW}%-$(( COLUMNS-40 ))s${NORMAL} \t" "${arg[*]}" ;;
        link) printf "${NORMAL}${GREEN}%s${NORMAL}->${CYAN}%s${NORMAL}\n" "${arg[@]}" ;;
        true) printf "${NORMAL}${GREEN}${arg}${NORMAL}" "${arg[@]:1}" ;;
        type) printf "${NORMAL}${BOLD}${WHITE}${arg}${NORMAL}" "${arg[@]:1}" >&2 ;;
        verb) (( VERBOSE )) && printf "${YELLOW}${arg}${NORMAL}" "${@:1}" ;;
        warn) printf "${NORMAL}${YELLOW}${arg}${NORMAL}" "${arg[@]:1}" >&2 ;;
        init_true) printf "${NORMAL}${GREEN}%s${NORMAL}\n" "${arg[*]}" ;;
        init_false) printf "${NORMAL}${RED}%s${NORMAL}\n" "${arg[*]}" ;;
        false_file) printf "${NORMAL}${RED}${arg}${YELLOW}%s${NORMAL}\n" "${arg[*]:1}" ;;
        exit)
            exit_msg "${arg:-301}"
            printf "${RED}${MSG_EXIT[0]}${NORMAL}" "${MSG_EXIT[@]:1}" >&2
        ;;
        ptmx)
            for item in /dev/pts/*([[:digit:]]); do
                printf "${NORMAL}${arg}${NORMAL}\n" "${arg[@]:1}" >>"${item}"
            done
        ;;
        rand)
            local item file int key
            if [[ -e ${arg} ]]; then file="${arg}"; shift; fi
            if [[ ${arg} == +([[:digit:]]) ]]; then int="${arg}"; shift; fi
            file="${file:-/dev/urandom}" int="${int:-8}"

            #key=$($(exe tr) -dc 'A-Za-z0-9' <"${file}" |$(exe head) -c "${int}")
            key=( $($(exe dd) if="${file}" bs=128 count=1 2>/dev/null |$(exe sha1sum)) )
            type -P "${CMD_XCLIP}" >/dev/null && printf '%s' "${key}" |xclip -i
            printf '%s\n' "${key}" 
        ;;
        mode)
            local mode modeU modeG modeO

            for item in "${arg[@]}"; do
                case "${item:0:1}" in
                    -|d)
                        mode=$(printf '%s' "${item}" |$(exe sed) \
                            -e 's/-/0/g;s/r/4/g;s/w/2/g;s/x/1/g')
                        modeU=$(( ${mode:0:1}+${mode:1:1}+${mode:2:1} ))
                        modeG=$(( ${mode:3:1}+${mode:4:1}+${mode:5:1} ))
                        modeO=$(( ${mode:6:1}+${mode:7:1}+${mode:8:1} ))
                        mode=$(printf '%s' "${item}" |$(exe sed) \
                            -e "s/${item:0:10}/${modeU}${modeG}${modeO}/")
                        printf '%s' "${mode}"
                    ;;
                    *) printf ' %s' "${item}" ;;
                esac
            done
        ;;
        erase) 
            local int

            for item in "${arg[@]}"; do
                [[ ${item} == +([[:digit:]]) ]] || item="${#item}"
                for (( int=0;int!=item;int++ )); do printf '\b \b'; done
            done
        ;;
        help)
            local name="${0##*/}"

            [[ ${0} == ?(-)bash ]] && name="${FUNCNAME[${#FUNCNAME[@]}-1]}"
            [[ ${_ARGS} || ${_DESC} ]] || local _ARGS='UNSET' _DESC="UNSET."
            _DESC="${_DESC//:\!:/${NORMAL}:${RED}!${NORMAL}:}"
            _DESC="${_DESC//:\*:/${NORMAL}:${YELLOW}*${NORMAL}:}"
            _DESC="${_DESC//\\n/\n        }"
            _DESC="${_DESC//\\t/    }"
            _DESC="${_DESC//$'\n'/\n        }"
            [[ ${_ARGS} ]] &&
                printf "\t${BOLD}${BLACK}Usage:${NORMAL} %b\n" "${_ARGS}"
            [[ ${_DESC} ]] &&
                printf "\t${BOLD}${BLACK}Description:${NORMAL} %s %b\n" "${name}" "${_DESC}"
        ;;
        *) printf "${NORMAL}${arg}${NORMAL}" "${arg[@]:1}" ;;
    esac
}
print_log() {
    print "${@}"
    print --ptmx "${@}"
}
print_color() {
    # /usr/lib/X11/rgb.txt
    local int

    for int in {1..8}; do
        printf "${ESC}05;%sm${BLACK}%2d${NORMAL} " \
            "${COLOR_CODE[${int}]/3/4}" "${COLOR_CODE[${int}]/3/4}"
    done; printf '\n'

    for int in {0..255}; do
        printf "${ESC256}%sm" "${int}"
        if (( int <= 15 )); then
            printf '%02d ' "${int}"
        else
            printf '%03d ' "${int}"
        fi

        if (( ${int} == 7 )); then
            printf '\n'
        elif (( ${int} == 15 )); then
            printf '\n\n'
        elif (( ${int} > 15 && 
            $(( ${int} % 3 )) == 0 &&
            $(( ${int} % 2 )) != 0 )); then
            printf '\n'
        fi
    done
}
print_exit() { print --exit "${@}"; }
print_help() { print --help "${@}"; }
print_info() { print --info "${@}"; }
print_init() { print --init "${@}"; }
print_link() { print --link "${@}"; }
print_mode() { print --mode -- "${@}"; }
print_ptmx() { print --ptmx "${@}"; }
print_rand() { print --rand "${@}"; }
print_true() { print --true "${@}"; }
print_type() { print --type "${@}"; }
print_verb() { print --verb "${@}"; }
print_warn() { print --warn "${@}"; }
print_erase() { print --erase "${@}"; }
print_false() { print --false "${@}"; }
print_init_true() { print --init_true "${@}"; }
print_init_false() { print --init_false "${@}"; }
print_false_file() { print --false_file "${@}"; }

clear() { printf '\033\143' >/dev/tty; }
command_not_found_handle() { $(exe "${1}") "${@:2}"; }
sudo() { $(exe sudo) -E "${@}"; }
#power() { sudo $(exe poweroff) "${@}"; }
#reboot() { sudo $(exe reboot) "${@}"; }

sleep() {
    local setOld="${-}"; set +m
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT

    { $(exe sleep) "${1}" & wait ${!} &>/dev/null; } &>/dev/null
}

alias() {
    (( ${#} > 1 )) || { builtin alias "${@}"; return; }

    builtin alias "${1}=${*:2}"
}

link() {
    local _ARGS="(H) (s) (r) <files> <path>"
    local _DESC="
        Multiple FILES into PATH.
        Default; force, symbolic; always force.

        Verbosity.
        If non-existant; write destination/source directory, write source file."

    (( ${#} > 1 )) || return
    local symlnk relativ recurse
    while [[ ${1} == +(-)* ]]; do
        item="${1##*+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            s) symlnk='s' ;;
            r) recurse='r' ;;
            H) relativ='H' ;;
        esac
        shift
    done
    local pathD="${@:(-1)}"  # Last element.
    local item item2 items=( "${@:1:(${#}-1)}" )  # Last element excluded.
    local args="-${symlnk}${relativ}"
    local argsD="${args//+(r|s)/}rs"
    local argsF="${args//+(r|s|H)/}H"
    local argsR="${args//+(r|d)/}d"

    mkdir "${pathD%/*}"
    for item in "${items[@]}"; do
        [[ ${item} ]] || continue

        #[[ -e ${item} ]] || { mkdir "${item%/*}"; >"${item}"; }

        (( VERBOSE_LINK )) && print_link "${item}" "${pathD}"

        if [[ -d ${item} && ! -d ${pathD} ]]; then  # First pass; subdirs.
            $(exe cp) ${argsD} "${item}" "${pathD}"
        elif ! [[ -d ${item} ]]; then  # symlink.
            $(exe cp) ${argsF} "${item}" "${pathD}"
        fi

        [[ -d ${item} ]] || continue  # Second pass.
        for item2 in ${item}/**; do  # Traverse.
            [[ ${item2} ]] || continue
            [[ -d ${item2} ]] && continue
            [[ ${item2} == +(*/.*) ]] && continue

            if [[ -L ${item2} ]]; then  # Relative symlink.
                $(exe cp) ${argsR} ${item2} "${pathD}${item2#${item}}"
            elif ! [[ -e ${item2} ]]; then
                $(exe cp) ${args} ${item2} "${pathD}${item2#${item}}"
            fi
        done
    done
}

mkdir() {
    local item

    for item in "${@}"; do
        [[ -f ${item} ]] && _rm "${item}"
        [[ -d ${item} ]] || $(exe mkdir) -p "${item}"
    done
}

_uniq() {
    unset IFS
    printf '%s\n' "${@}" |$(exe sort) -u
    #printf '%s\n' "${@}" |awk 'x[$0]++ ==0' |sort
}

sort_swap() {
    local t
    t=${SORT[${1}]}
    SORT[${1}]=${SORT[${2}]}
    SORT[${2}]=${t}
}
sort_part() {
    local c p x
    p=${SORT[${3}]}
    c=${1}
    
    sort_swap "${2}" "${3}"
    
    for (( x=${1};x<${2};x++ )); do
	if [[ ${SORT[${x}]}<${p} ]]; then
	    sort_swap "${x}" "${c}"
	    (( c++ ))
	fi
    done
    
    sort_swap "${2}" "${c}"
    n=${c}
}
sort_quick() {
    local i n
    (( ${1}>=${2} )) && return
    
    i=$(( (${1}+${2})/2 ))
    
    sort_part "${1}" "${2}" "${i}"
    sort_quick "${1}" "$(( n-1 ))"
    sort_quick "$(( n+1 ))" "${2}"
}

wait_exe() {
    local _ARGS='[command|pid]'
    local _DESC="
        Loop until command or pid returns or recieves TERM.
        If not available argument, use last background pid."
    local int item items itemS chr

    while [[ ${1} == +(-)* ]]; do
        case "${1##+(-)}" in
            \?) print_help; return 1 ;;
            rot) chr=( '-' '\' '|' '/' ) ;;
            dot) chr=( '.' '..' '...' '....' ) ;;
        esac
        shift
    done
    items=( "${@}" )
    [[ ${items} ]] || items=( "${!}" )
    [[ ${chr} ]] || chr=( '.' '..' '...' '....' )

    printf '%s' ' '
    while [[ -d ${DIR_PROC}/${items:-0} ]]; do
        itemS="${chr[$((int%4))]}"

        printf -- '%s' "${itemS}"
        sleep 0.2
        print_erase "${itemS}"
        (( int++ ))
    done
    print_erase ' '
}
wait_write() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _ARGS='<file:size>'
    local _DESC="
        Loop until file equals size."
    local item color action
    local size sizeN sizeT pass

    while [[ ${1} == +(-)* ]]; do
        item="${1##*+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            perc) action='perc' ;;
        esac
        shift
    done

    for item in "${@}"; do
        size="${item#*:}" item="${item%:*}"
        until [[ -e ${item} ]]; do continue; done
        [[ -f ${item} ]] || continue

        while (( sizeN < size )); do
            sizeN=$($(exe stat) -c '%s' "${item}")

            case "${action}" in
                perc)
                    if (( size == sizeN )); then
                        color="${GREEN}"
                    else
                        color="${YELLOW}"
                    fi
                    read_clear
                    sizeT=$(printf 'scale=2; 100*(%d/%d)\n' "${sizeN}" "${size}" |
                        $(exe bc))
                    (( pass )) && print_erase '3'
                    printf "${color}%02.f%%${NORMAL}" "${sizeT}"
                    (( pass++ ))
                ;;
            esac
            sleep 0.5
        done #; builtin wait; read_clear
    done
}

write() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local item items action 
    unset items

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && items+=( ${ARG_OPT[@]} )
        [[ ${action} ]] && break
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return ;;
                over) action='over' ;;
                a|append) action='append' ;;
            esac
        done
    done
    [[ ${action} ]] || action='over'
    [[ ${items} ]] || return

    for item in "${items[@]}"; do
        [[ -d ${item%/*} ]] || mkdir "${item%/*}"
    done

    if [[ ${action} == over ]]; then
        write_over "${items[@]}"
    fi

    if (( ${?} )); then
        print_false 'Invalid: '
    else
        print_true 'Wrote: '
    fi; print_warn '%s\n' "${items[*]}"
}
write_empty() {
    #devs=( '/dev/zero' '/dev/urandom' )
    #block=( 1{k,M} )
    #$(exe dd) if="${dev}" of="${out}" bs="${block}" count="${count}"
    #:>"${out}"
    true
}
write_over() { set +C
    local rtn
    trap '{ rm "${1}"; trap - EXIT; }' EXIT

    tee "${@}" >/dev/null
}
write_append() {
    local _ARGS='<strings> <file>'
    local _DESC="
        Append STRINGS to FILE."
    local item

    for item in "${@}"; do
        # Set file.
        [[ -e ${item} ]] && break
    done

    printf '%s\n' "${@}" >>"${item}"
}

rm_unresolved() {
    local _DESC="
        Remove unresolved symlinks."
    local item

    print_warn 'Unresolved symlinks'
    { sudo $(exe find) "${DIR_USR}" -not -type d -type l |
        while read item; do
            [[ -e ${item} ]] || sudo $(exe rm) -f "${item}"
        done & } 2>/dev/null
    wait_exe
    print_erase 'Unresolved symlinks'
}
_rm() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _ARGS='<path>'
    local _DESC=":!:rm
        Remove PATH."
    local item items

    for item in "${@}"; do
        # Ignore parent root, '.', '..'.
        [[ ${item} ]] || continue
        [[ -e ${item} ]] || continue
        [[ ${item} == *+(.|..) ]] && continue
        items=( ${item} )

        if (( VERBOSE )); then
            print_true 'Removed: '; print_warn '%s\n' "${items[0]}"
            (( ${#items[@]} > 1 )) && print_warn '\t\t%s\n' "${items[@]:1}"
        fi
        $(exe rm) -rf "${items[@]}"
    done
}

kill() {
    local _ARGS='(<signal>) (verbose) (list) <files> <pids>'
    local _DESC="
        Group PIDS from FILES or as arguments. If terminating SIGNAL, remove FILES.

        List, print valid signals.
        Verbose, print effected PIDS.
        Signal, send SIGNAL to PIDS."
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local IFS='|'; local reS="${SIGNAL_EXIT[*]}" reE="${SHELLS[*]}"; unset IFS
    local item items verb pass PS3
    unset SIGNAL

    while [[ ${1} == +(-)* ]]; do
        item="${1##+(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            list) $(exe kill) -l; return ;;
            verbose) (( verb++ )) ;;
            *) exit_signal "${item}" ;; # EXEC == SIGNAL (<26)?
        esac
        shift
    done
    SIGNAL="${SIGNAL:-TERM}"

    (( ${#} )) || PS3='PID: ' selects $($(exe ps) -o pid)
    for item in "${@:-${SELECT[@]}}"; do
        [[ ${item} ]] || continue

        if [[ ${item} == */* ]]; then  # path, prevent ./ capture.
            [[ -e ${item} ]] || return

            items=( $(<"${item}") )  # Grab PID
            [[ ${SIGNAL} == +(${reS}) ]] &&
                { sudo $(exe chown) ${USER_ID}:${GROUP_ID} "${item}"; rm -f "${item}"; }

        elif [[ ${item} != +([[:digit:]]) ]]; then  # exec
            items=( $($(exe pgrep) -f -- \
                "(^(${reE})+.*/${item}|^${item}|^/.*/${item})\b") )
                #"(^(${reE})+.*${item}|^${item}|[ ]+${item}|bin/${item})\b") )

        elif [[ -e /proc/${item} ]]; then
            items="${item}"
        fi
        [[ ${items} ]] || continue
    done

    unset pass
    for item in "${items[@]}"; do
        [[ ${item} ]] || continue

        (( verb || VERBOSE )) && print_warn '%s\n' "${item}"
        while :; do
            (( pass > 6 )) && break
            [[ -e /proc/${item} ]] || break 
            $(exe grep) -q 'stopped\|zombie\|dead' "${DIR_PROC}/${item}/status" && break

            if (( pass > 5 )); then  # Force.
                sudo $(exe kill) -9 "${item}" #>/dev/null
            else
                sudo $(exe kill) -${SIGNAL} "${item}" #>/dev/null
            fi

            [[ ${SIGNAL} == +(${reS}) ]] || break
            sleep 1
            (( pass++ ))
        done
    done
}

modprobe() {
    local _ARGS='<modules>'
    local _DESC="
        Act on MDOULES.
        set MODPROBE_UNLOAD to remove from stack."
    local item items blacklist unload
    #:todo: autoload, udev?

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            b) (( blacklist++ )) ;;
            r) (( unload++ )) ;;
            *) $(exe modprobe) "${@}"; return ;;
        esac
        shift
    done

    if (( ${#} )); then
        for item in "${@}"; do
            # Read modules.dep, depmod if missing.
            if ! $(exe grep) -qi "${item}\." <($(exe modprobe) -l); then
                depmod || { print_false 'Error! '; printf '%s\n' 'depmod'; return 1; }
                $(exe grep) -qi "${item}\." <($(exe modprobe) -l) || continue
                    #{ print_false 'Missing! '; printf '%s\n' "${item}"; continue; }
            fi

            if (( unload )); then 
                $(exe grep) -qi "^${item//-/_}\b" "${PROC_MODULES}" || continue

                sudo $(exe modprobe) -r "${item}"

            else
                $(exe grep) -qi "^${item//-/_}\b" "${PROC_MODULES}" && continue

                [[ ${CMD_MOD} ]] && ${CMD_MOD} "${item}"
                sudo $(exe modprobe) "${item}"
            fi
        done

    else
        if (( blacklist || unload )); then
            items=( $($(exe grep) -o -e '^[^ ]*' "${PROC_MODULES}") )
        else
            items=( $($(exe modprobe) -l) )
        fi

        selects "${items[@]##*/}" || return
        for item in "${SELECT[@]}"; do
            if (( blacklist )); then
                sudo $(exe modprobe) -b "${item}"
            fi
            if (( unload )); then
                sudo $(exe modprobe) -r "${item}"
            else
                sudo $(exe modprobe) "${item}"
            fi
        done

    fi
}
depmod() {
    local item

    for item in {/usr/local,}/lib/modules/*; do
        #[[ -e ${item}/modules.dep ]] && continue
    #    sudo $(exe depmod) -a -b "${item%%lib*}"
        sudo $(exe depmod) -a
    done
}

touch() {
    local item

    for item in "${@}"; do
        [[ -e ${item} ]] || $(exe touch) "${item}"
    done
}

exe(){
    local _ARGS='<executable>'
    local _DESC="
        Execute as busybox or using distro method."
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    #:fix: IFS scope of caller.
    local item force skip IFS 

    for item in busybox "${CMD_BUSYBOX_SKIP[@]}"; do
        [[ ${1} == ${item} ]] && { (( skip++ )); break; }
    done
    (( skip )) ||
        for item in "${CMD_BUSYBOX[@]}"; do
            [[ ${1} == ${item} ]] || continue

            printf '%s ' $(type -P busybox) "${1}"
            return
        done

    [[ ${CMD_EXE} ]] &&
        for item in "${CMD_EXE[@]}"; do ${item} "${1}" >&2; done
        #{ type -P "${1}" >/dev/null || ${CMD_EXE} "${1}" >&2; }

    type -P "${1}" || exe_false "${1}"
}
exe_false() {
    EXIT="${1}"

    # Permission
    [[ -x /usr/sbin/${1} || -x /sbin/${1} ]] && return 110

    print_exit '127' >&2
    [[ -t 1 ]] || printf '%s' 'false'

    (( 0 ))
}

available_bytes() {
    local _ARGS=':str:path :str:mbyte_limit'
    local bytes path='/' limit="${2:-100}"

    exist "${1}" && path="${1}"
    bytes=$($(exe df) -Pm "${path}" |tail -1 |awk '{printf $4}')
    if (( bytes > limit )); then
        return
    else
        print_false 'Available Bytes: %s\n' "${bytes}"
        return 1
    fi
}

shells() {
    local item="${DIR_ETC}/shells"

    exist "${item}" || return
    $(exe grep) -v '^#\|^$' "${item}"
}

terminal_title() { printf '\033]0;%s\007' "${1}"; }

_getty() {
    exec </dev/${1} >/dev/${1} 2>&1 || exit
    reset
    stty sane
    stty ispeed 38400
    stty ospeed 38400
    printf 'Login %s:' "${HOSTNAME}"
    read -r item
    exec /bin/login "${item}"
}

reverse() {
    local _ARGS='<array|elements>'
    local _DESC="
        Reverse elements."
    local int items

    unset items
    for (( int=${#};int>0;int-- )); do
        items+=( "${@:${int}:1}" )
    done
    printf '%s\n' "${items[*]}"
}

profile() {
    local item
    unset SOURCES_PROFILES

    for item in "${PROFILES[@]}"; do
        source "profile_${item}" && SOURCES_PROFILES+=( "${item}" )
    done

    export SOURCES_PROFILES
}
distro() {
    local item
    unset SOURCES_DISTROS

    for item in "${DISTROS[@]}"; do
        source "${item}" && SOURCES_DISTROS+=( "${item}" )
    done

    export SOURCES_DISTROS
}
source_force() {
    local item

    for item in ${SOURCES//:/ }; do
        [[ ${item} == +(bash.sh) ]] || builtin . "${item}"
    done
}

prompt() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    unset IFS TMOUT
    local item

    for item in "${PROMPTS[@]}"; do ${item}; done
    export PROMPT '1'
}
prompt_append() {
    exist_action elem "${1}" "${PROMPTS[@]}" || PROMPTS+=( "${1}" )
    export PROMPTS
}

bash_test() {
    source 'test'

    test.true read_true -1 'Test'
    test.false read_false -1 'Test'
}

_DESC="
    Verify ${BASH_VERSION}.
    Verbose output if login.
    Set ${SOURCE}, if not called as rc-file;
        - Prevent source execution.
        - Initialize if unset."

if [[ ${BASH_VERSION%.*} < 4.0 ]]; then
    print_false 'BASH version (%s)\n' "${BASH_VERSION}"
    exit
fi

if (( SHLVL == 1 )); then
    VERBOSE_SOURCE='1'

    if [[ ${SSH_CONNECTION} ]]; then
        item=( ${SSH_CONNECTION} )
        print_warn 'Remote: '
        printf '%s:%s\n' "${item[@]:2:1}" "${item[@]:3:1}"
    fi
fi

if [[ ${SSH_TTY} ]]; then
    export REMOTE '1'
fi

unset CMD_EXE CMD_MOD PROMPT
SOURCES='bash.sh'
source 'environment'
source 'parse'
profile
distro
export PROMPT_COMMAND 'prompt'

(( 1 )) 

