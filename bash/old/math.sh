#!/usr/bin/env bash
source 'bash.sh'

math.variables() {
true
}

math.conversion() {
    local item 

    for item in "${@:2}"; do
        [[ ${item} == +([[:digit:]]) ]] || continue
        case "${1}" in
            dec) printf '%s:%d\n' "${item}" "${item}" ;;
            hex) printf '%s:%x\n' "${item}" "${item}" ;;
            oct) printf '%s:%o\n' "${item}" "${item}" ;;
            fah) printf '%d:%d\n' "${item}" $(( (9/5)*item+32 )) ;;
            cel) printf '%d:%d\n' "${item}" $(( (item-32)*(5/9) )) ;;
        esac
    done
}
math.c2f() { math.conversion 'fah' "${@}"; }
math.f2c() { math.conversion 'cel' "${@}"; }

math.float() { [[ ${1} != *+([[:alpha:]]|+(.)*+(.))* && ${1} == *+(.)* ]]; }
math.number() { [[ ${1} == +([[:digit:].])* && ${1} != *+(.)*+(.)* ]] ;}
math.integer() { [[ ${1} == +([[:digit:]]) ]]; }
math.int() { math.integer "${@}"; }

math.arguments() {
    local item 
    unset ACTION ITEM

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEM=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
                f|fah*) action='fah' ;;
                c|cel*) action='cel' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

math.test() {
    source 'test'

    test.true   math.float 0.1
    test.false  math.float 1
    test.false  math.float 1.0.0
    test.false  math.float ' '

    test.true   math.number 1
    test.true   math.number 1.0
    test.false  math.number 0.1.0
    test.false  math.number one
    test.false  math.number ' '

    test.true   math.integer 1
    test.false  math.integer 1.0
    test.false  math.integer one
    test.false  math.integer ' '
}

(( SOURCE )) && return 2>/dev/null
math.variables
return 2>/dev/null

_ARGS=''
_DESC="Math."

math.arguments "${@:-?}" || exit

case "${ACTION}" in
    *) ;;
esac

