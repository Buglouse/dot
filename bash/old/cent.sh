#!/usr/bin/env bash
source bash.sh

cent.variables() {
    [[ -e /etc/arch-release ]] || return
    export VERSION_ARCH '/etc/arch-release'  #:order:0

    export PACKAGE_INSTALL 'curl' 'sudo' 'busybox' 'pkgtools' "${EDITOR}" 

    export LOG_MAKEPKG "${DIR_TMP_LOG}/makepkg.log"
    export LOG_PACMAN "${DIR_TMP_LOG}/pacman.log"

    #--Local(environment)--#

    export DIR_INSTALL '/'
    export USE_MLOCATE '0'

    #####

    export DIR_PACMAN "${HOME}/.pacman"  #:order:0
    export DIR_PACMAN_DB "${DIR_VAR_DATA}/pacman"
    export DIR_PACMAN_CACHE "${DIR_VAR_CACHE}/pacman"
    export DIR_PACMAN_AUR_CACHE "${DIR_VAR_CACHE}/pacman/aur"

    export CMD_EXE 'arch.execute'
    export CMD_MOD 'arch.module'
    export CMD_LOGIN 'arch.login'

    export CONF_ABS "${DIR_PACMAN}/abs.conf"
    export CONF_PACMAN "${DIR_PACMAN}/pacman.conf"
    export CONF_MAKEPKG "${DIR_PACMAN}/makepkg.conf"
    export CONF_PACMAN_MIRROR "${DIR_PACMAN}/mirror"

    export PACMAN_ARCHS 'x86_64' 'i686'
    export PACMAN_REPOS 'core' 'extra' 'community' '!testing'

    export PACMAN_EXT_ABS '.abs.tar.gz'

    #-----#

    export PACMAN 'pacman'
    export ABSROOT "${DIR_VAR_DATA}/abs"

    #####
}

cent.install() {
    local items

    # pacman
    items+=( "${DIR_PACMAN_CACHE}" "${DIR_PACMAN_DB}" )

    # abs
    items+=( "${ABSROOT}" )

    # makepkg
    items+=( "${DIR_VAR_DATA}/pacman/pkg" "${DIR_VAR_DATA}/pacman/src"
        "${DIR_VAR_CACHE}/pacman/build" "${DIR_VAR_DATA}/pacman/src" )

    for item in "${items[@]}"; do
        sudo $(exe mkdir) -m 755 -p "${item}"
        sudo $(exe chmod) -R 777 "${item}"
        sudo $(exe chown) "${USER_ID}:${GROUP_ID}" "${item}"
    done

    arch.update
    return
    # /arch/setup
    # network
    # format
    # packages
    # /etc/rc.d
    # /etc/fstab
    # adduser
    # passwd
    # /etc/sudoers
    # bootloader
}
cent.install_chroot() {
    local itemP="${DIR_DEV}${1}" itemD="${DIR_MNT}/root"

    [[ ${UID} == 0 ]] || return
    [[ -e ${itemP} ]] || return

    $(exe mkdir) -p "${itemD}"
    $(exe mount) "${itemP}" "${itemD}"
    $(exe mount) -t proc proc "${itemD}/proc"
    $(exe mount) -t sysfs sys "${itemD}/sys"
    $(exe mount) -o bind /dev "${itemD}/dev"
    $(exe cp) "${FILE_DNS}" "${itemD}/${FILE_DNS}"
    $(exe chroot) "${itemD}" "${SHELL}"
}

cent.login() {
    arch.update
}

cent.alias() {
    alias aur 'aurora'
    alias pkg 'pacman.load'
}

#--Package--#

cent.execute() {
    local items itemsU item="${1}"

    type -P "${item}" >/dev/null && return
    case "${item}" in
        dig) items+=( 'bind' ) ;;
        locate) items+=( 'mlocate' ) ;;
        7z) items+=( 'p7zip' ) ;;
        *) items+=( ${1} ) ;;
    esac
    arch.load "${items[@]}" ||
        { type -P 'aurora' && aurora "${items[@]}"; }
}

cent.find() { pacman --query --search "${@}"; }
cent.info() { pacman --info "${@}"; }
cent.locate() { pkgfile -s "${@}"; }
cent.update() {
    pacman --sync --refresh --refresh
    pkgfile --update
    aurora --update
}
cent.upgrade() { pacman --sync --refresh --sysupgrade; }

cent.load() {
    local item items=( "${@}" )

    for item in "${items[@]}"; do
        arch.loaded "${item}" || pacman --sync "${item}"
    done
}
cent.loaded() {
    local _ARGS='<packages>'
    local _DESC="
        Print loaded PACKAGES."
    local IFS='|'; local re="${*}"; unset IFS

    pacman --query --quiet |
        $(exe grep) -q "\(${re}\)" ||
            pacman --query --quiet --group |
                $(exe grep) -q "\(${re}\)"
}
cent.remove() { pacman --remove "${@}"; }
cent.provides() {
    local IFS='|'; local re="${*}"; unset IFS

    pacman --query --list |
        $(exe grep) "\(${re}\)"
}

cent.fetch() {
    local item repo arch host hosts items=( "${@}" )

    if type -P pacman; then 
        pacman --downloadonly "${items[@]}";

    #:todo: pkg-version
    #elif [[ -e ${DIR_PACKMAN} && -e ${CONF_PACMAN_MIRROR} ]]; then
    #    for item in "${items[@]}"; do
    #        for repo in "${PACMAN_REPOS[@]}"; do
    #            [[ ${repo} == +(!)* ]] && continue

    #            for arch in "${PACMAN_ARCHS[@]}"; do
    #                hosts=( $($(exe sed) -n -e 's/.*= \(.*\)/\1/p' "${CONF_PACMAN_MIRROR}") )

    #                for host in "${hosts[@]}"; do
    #                    request.ping "${host}" || continue

    #                    item="${item}-version-${arch}"

    #                    $(exe curl) --silent "${host}${item}${PACMAN_EXT_ABS}" 
    #                done
    #            done
    #        done
    #    done
    fi
}

cent.help() { pacman --help; }

cent.error() {
    printf 'Distro Release:\t\t %s\n' "${VERSION_ARCH}"
    printf 'Kernel:\t\t %s\n' $($(exe uname) -r)
    printf 'Architecture:\t\t %s\n' $($(exe uname) -m)
    printf '#--Report--#\n'
    printf '%s\n' "${out}"
}

cent.build() {
    arch.load 'abs' 'base-devel'
    #! type -P 'bcrypt' >/dev/null && return  #:fix: dependency
    #identity.define 'name'
    # abs <repo/pkg>
}

#-----#


#-----#

cent.arguments() {
    local item 
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                E|exe*) ACTION='execute' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

cent.variables || return 2>/dev/null
arch.alias
return 2>/dev/null

_ARGS='(e)xecute'
_DESC="
    Cent configuration.
    #----#
    $(arch.help)"

[[ ${VERSION_CENT} ]] || exit
cent.arguments "${@:-?}" || exit

case "${ACTION}" in
    execute) cent.execute "${ITEMS[@]}" ;;
esac
