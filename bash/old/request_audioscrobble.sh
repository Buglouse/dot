#!/usr/bin/env bash
source 'bash.sh'

request.audioscrobble.variables() {
    #ws.audioscrobbler.com
    export HTTP_LASTFM 'ws.audioscrobbler.com' #:order:0
    export HTTP_LIBREFM 'ws.audioscrobbler.com' #:order:0

    export URI_LASTFM 'lastfm'
    export URI_LIBREFM 'librefm'  #libre.fm

    export LASTFM_CONTROL 'loveTrack' 'unLoveTrack' 'banTrack' 'unBanTrack' \
        'addTrackToUserPlaylist' 'removeRecentlyListenedTrack'
    export LASTFM_STATION 'user' 'artist' 'globaltags'
    export LASTFM_STATION_USER 'loved' 'personal' 'playlist' 'neighbours' 'recommended'
    export LASTFM_API_KEY 'f76d5a9c96ba86c0b6c6c5a2af464bdd'

    export LIBREFM_STATION 'user' 'artist' 'community' 'globaltags'
    export LIBREFM_STATION_USER 'loved' 'personal' 'playlist' 'neighbours' 'recommended'

    export HTTP_AS_CMD 'http://ws.audioscrobbler.com/radio/control.php?debug=0'
    export HTTP_AS_AUTH 'http://ws.audioscrobbler.com/radio/handshake.php?version=1.1.1&platform=linux&debug=0&partner='
    export HTTP_AS_INFO 'http://ws.audioscrobbler.com/radio/np.php?debug=0'
    export HTTP_AS_XSPF 'http://ws.audioscrobbler.com/1.0/rw/xmlrpc.php'
    export HTTP_AS_TRACK 'http://ws.audioscrobbler.com/radio/adjust.php?debug=0'
    export HTTP_LASTFM_TAG "${HTTP_LASTFM}/2.0/?method=tag.gettoptags"
    export HTTP_LIBREFM_TAG "${HTTP_LIBREFM}/2.0/?method=tag.gettoptags&limit=1000"

    export FILE_TAG_LASTFM "${DIR_TMP_CACHE}/audioscrobble-lastfm.tags"
    export FILE_TAG_LIBREFM "${DIR_TMP_CACHE}/audioscrobble-librefm.tags"
}

request.audioscrobble.authenticate() {
    # http://ws.audioscrobbler.com/radio/handshake.php?version=1.1.1&platform=linux&debug=0&partner=&username=xxxx&passwordmd5=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    local pass user line items=( 'last.fm' )

    identity.define "${items[@]}" &>/dev/null
    user="${LAST_FM_USER}" pass="${LAST_FM_PASS}"
    pass=( $(printf '%s' "${pass}" |$(exe md5sum)) )

    while read line; do
        [[ ${line%%=*} == session ]] || continue
        printf '%s\n' "${line#*=}"
        break
    done <<<"$($(exe curl) --silent \
        "${HTTP_AS_AUTH}&username=${user}&passwordmd5=${pass}")"
    # session=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    # stream_url=http://195.24.233.49:80/last.mp3?Session=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    # subscriber=0
    # framehack=0..
    # base_url=ws.audioscrobbler.com
    # base_path=/radio
    # info_message=
    # fingerprint_upload_url=http://ws.audioscrobbler.com/fingerprint/upload.php
    # permit_bootstrap=0
}

request.audioscrobble.play() {
    local _DESC="
        Generate play uri."
    local item item2 uri tag serv servS servC servCS PS3='Station: '

    [[ ${1} ]] || return

    case "${1}" in
        lastfm)
            identity.define 'last.fm' || return
	    servC="${LAST_FM_USER}"

            tag="${URI_LASTFM}"
            serv="${URI_LASTFM}"
            servS=( "${LASTFM_STATION[@]}" )
            servCS=( "${LASTFM_STATION_USER[@]}" )
        ;;
        librefm)
            identity.define 'libre.fm' || return
            servC="${LIBRE_FM_USER}"

            tag="${URI_LIBREFM}"
            serv='lastfm'  #:fix: MPD
            servS=( "${LIBREFM_STATION[@]}" )
            servCS=( "${LIBREFM_STATION_USER[@]}" )
        ;;
    esac
    shift
    [[ ${serv} && ${servS} ]] || return

    selects -1 "${servC}" "${servS[@]}"
    for item in "${SELECT[@]}"; do
        case "${item}" in
            globaltags)
                IFS=$'\n'
                PS3='Tag: ' selects $(request.audioscrobble.tags "${tag}" "${@}")
                unset IFS
                [[ ${SELECT} ]] || read -a SELECT -p 'Tag: ' 
                for item2 in "${SELECT[@]}"; do
                    uri+=( "${serv}://${item}/${item2}" )
                done
            ;;
            user)
                read -e -p "${item^}: " item2
                selects -1 "${servCS[@]}"
                uri+=( "${serv}://${item}/${item2}/${SELECT}" )
            ;;
            artist)
                read -e -p "${item^}: " item2
                uri+=( "${serv}://${item}/${item2}/" )
            ;;
            +(${servC}))
                selects -1 "${servCS[@]}"
                uri+=( "${serv}://user/${item}/${SELECT}" )
            ;;
        esac
    done

    [[ ${uri} ]] && export URI "${uri[@]// /+}"
}

request.audioscrobble.command() {
    local IFS='|'; local re="${LASTFM_CONTROL[*]}"; unset IFS
    local _ARGS='<command> <artist> <track>'
    local _DESC="
        Command, [${re}].
        Call command with ARTIST and TRACK."
    local item itemS action PS3='Command: '

    [[ ${1} == -\? ]] && { print_help; return 1; }

    identity.define 'last.fm'
    itemS=$(request.audioscrobble.authenticate)
    [[ ${itemS} ]] || return

    case "${1}" in
        add) action='addTrackToUserPlaylist' ;;
        ban) action='banTrack' ;;
        love) action='loveTrack' ;;
        unban) action='unBanTrack' ;;
        remove) action='removeRecentlyListenedTrack' ;;
        unlove) action='unLoveTrack' ;;
        +(${re})) action="${1}" ;;
    esac; shift
    [[ ${#} == 2 ]] || return

    [[ ${action} ]] || selects -1 "${LASTFM_CONTROL[@]}"

    # method,user,challenge,auth,artist,title
    $(exe curl) --request POST --header 'Content-Type: text/xml' \
        --data "$(printf '<?xml version="1.0" encoding="UTF-8"?><methodCall><methodName>%s</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>' \
            "${action:-${SELECT}}" "${LAST_FM_USER}" \
            $($(exe date) '+%s') "${itemS}" "${@}")" \
        "${HTTP_AS_XSPF}"
}

request.audioscrobble.tags() {
    local IFS='|'; local arg_re="${*:2}"; unset IFS
    local items itemU itemF re

    case "${1}" in
        lastfm)
            itemU="${HTTP_LASTFM_TAG}&api_key=${LASTFM_API_KEY}"
	    itemF="${FILE_TAG_LASTFM}"
            re='/<url>/{s@.*/tag/\([^<]*\).*@\1@p}'
            # 's@[[:space:]]@\+@g'
        ;;
        librefm)
            itemU="${HTTP_LIBREFM_TAG}" itemF="${FILE_TAG_LIBREFM}"
            re='\@<url>@{s@.*tag/\([^<]*\).*@\1@;p}'
        ;;
    esac

    [[ -e ${itemF} && $($(exe stat) -c '%s' "${itemF}") > 10 ]] || {
	$(exe rm) -f "${itemF}"
	items=( $($(exe curl) --silent --location "${itemU}" |
            $(exe sed) -ne "${re}" |sort) )
	printf '%b\n' "${items[@]//\%/\x}" >"${itemF}"; }
    [[ -e ${itemF} && $($(exe stat) -c '%s' "${itemF}") > 10 ]] || return

    $(exe grep) -i -e "\(${arg_re//|/\|}\)" "${itemF}" || $(exe grep) . "${itemF}"
}

request.audioscrobble.arguments() {
    local item 
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                #-----#
                info) ACTION='info' ;;
                play) ACTION='play' ;;
                command) ACTION='command' ;;
            esac
        done
    done
    [[ ${ACTION} && ${ITEMS} ]]
}

request.audioscrobble.test() {
    source 'test'

    $(exe cmd)
}

request.audioscrobble.variables
return 2>/dev/null

_ARGS='(c)ommand (p)lay <type>'
_DESC="
    Type, [Last.FM]
    Play, generate play url.
    Command, [$(IFS='|';printf '%s\n' "${LASTFM_CONTROL[*]}")]"

request.audioscrobble.arguments "${@:-?}" || exit

# 128 kbit/s@44.1kHz_MPEG-1_Layer-3;Spliced with 'SYNC'.
#lastfmSession=$(request.audioscrobble.authenticate)
#[[ ${lastfmSession} ]] || return

case "${ACTION}" in
    info) request.audioscrobble.info "${ITEMS[@]}" ;;
    play) request.audioscrobble.play "${ITEMS[@]}" ;;
    command) request.audioscrobble.command "${ITEMS[@]}" ;;
esac
