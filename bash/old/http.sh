#!/usr/bin/env bash
source 'bash.sh'

http.variables() {
    export HTTP_DOCTYPE "
        <?xml version='1.0' encoding='UTF-8'?>
        <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml'>
        <head>
        <meta http-equiv='Content-Type' content='text/html'; charset='UTF-8' />
        </head>"

    export HTTP_ROOT "${HTTP_ROOT:-${DIR_TMP_DATA}/www}"
}

http.link_path() {
    local item items

    if (( ${#} )); then
        items=( ${@/%/*} )
    else
        items=( * )
    fi

    for item in "${items[@]}"; do
        if [[ -d ${item} ]]; then
            printf '<br>%s<br>' "${item}"
        else
            printf '<br><a href="%s">%s</a>' "${item}" "${item}"
        fi
    done
}
