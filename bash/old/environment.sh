#!/usr/bin/env bash
source 'bash.sh'

environment.variables() {
    local _DESC="
        DIR_VAR, permanent application data.
        DIR_VAR_LIB, per-application data.
        DIR_VAR_DATA, per-application user data.
        DIR_VAR_CACHE, per-application cache.
        DIR_CHROOT, chroot.
        DIR_TMP, temporary application data.
        DIR_i18n, locale/charmap.
        DIR_REPO_SRC_BASH, relative repository path.
        DIR_REPO, root of repository.
        PROFILES, per user customizations.
        DISTROS, distribution specific functions."
    local item items

    #[[ ${HOME} ]] || { print_false 'Missing[HOME]: %s' "${HOME}"; return 1; }
    mkdir "${HOME}"

    environment.variables_verbose
    environment.variables_color
    environment.variables_path

    export DIR_ETC "${DIR_ETC:-/etc}"  #:order:0
    export DIR_LIB "${DIR_LIB:-/lib}"  #:order:0
    export DIR_MNT "${DIR_MNT:-/mnt}"  #:order:0
    export DIR_OPT "${DIR_OPT:-/opt}"  #:order:0
    export DIR_SHM "${DIR_SHM:-/dev/shm}"  #:order:0
    export DIR_TMP "${DIR_TMP:-/tmp}"  #:order:0
    export DIR_USR "${DIR_USR:-/usr}"  #:order:0
    export DIR_DEV "${DIR_DEV:-/dev}"  #:order:0
    export DIR_VAR "${DIR_VAR:-/var}"  #:order:0
    export DIR_PROC "${DIR_PROC:-/proc}"  #:order:0
    export DIR_REPO_SRC_BASH 'src/bash'  #:order:0

    item=$($(exe realpath) "${BASH_SOURCE}")
    export DIR_REPO "${item%/${DIR_REPO_SRC_BASH}*}" #:order:1
    export DIR_INSTALL "${DIR_USR}/local"  #:order:1
    export DIR_VAR_DATA "${DIR_VAR}/data"  #:order:1
    export DIR_VAR_CACHE "${DIR_VAR}/cache"  #:order:1

    if (( REMOTE )); then
        unset DISPLAY
        # Save logs and credentials.
        #export DIR_TMP "${DIR_VAR}"

        export TZ 'UTC'
        export TIMEZONE_OFFSET '0'
    else
        unset TZ
        item=( /tmp/.+(X*lock) )
        [[ -e ${item} ]] && export DISPLAY ':0.0'
    fi 

    export PROFILES 'user'
    export DISTROS 'tc' 'arch' 'debian'

    export DIR_GPG "${HOME}/.gnupg"
    export DIR_PAM "${DIR_ETC}/pam.d"
    export DIR_I18N "${HOME}/.i18n"
    export DIR_REPO "${DIR_REPO:-${DIR_TMP}}" 
    export DIR_UDEV "${DIR_ETC}/udev/rules.d"
    export DIR_CHROOT "${DIR_TMP}/chroot"
    export DIR_SHM_TMP "${DIR_SHM}/tmp"
    export DIR_TMP_LOG "${DIR_TMP}/log"
    export DIR_TMP_NFS "${DIR_TMP}/nfs"
    export DIR_TMP_PID "${DIR_TMP}/pid"
    export DIR_USR_BIN "${DIR_USR}/bin"
    export DIR_MODULES "${DIR_LIB}/modules/${VERSION_KERNEL}"
    export DIR_MODPROBE "${DIR_ETC}/modprobe.d"
    export DIR_REPO_DOT "${DIR_REPO}/dot" 
    export DIR_REPO_SRC "${DIR_REPO}/src" 
    export DIR_TMP_DATA "${DIR_TMP}/data"
    export DIR_TMP_SOCK "${DIR_TMP}/sock"
    export DIR_TMP_BUILD "${DIR_TMP}/build"
    export DIR_TMP_CACHE "${DIR_TMP}/cache"
    export DIR_HOME_LOCAL "${HOME}/.local"
    export DIR_TMP_EXPORT "${DIR_TMP}/export"
    export DIR_HOME_LOCAL_BIN "${HOME}/.local/bin"


    #"${DIR_VAR}"
    export DIRS_VAR "${DIR_VAR_CACHE}" "${DIR_VAR_DATA}"
    #"${DIR_TMP}" "${DIR_TMP_NFS}" "${DIR_TMP_EXPORT}"
    export DIRS_TMP "${DIR_TMP_LOG}" "${DIR_TMP_PID}" \
        "${DIR_TMP_SOCK}" "${DIR_TMP_DATA}" "${DIR_TMP_CACHE}" \
        "${DIR_CHROOT}"

    #:fix: FILE_HOST
    export FILE_DNS "${DIR_ETC}/resolv.conf"
    export FILE_MOTD "${DIR_ETC}/motd" 
    export FILE_HOSTS "${DIR_ETC}/hosts" 
    export FILE_PASSWD "${DIR_ETC}/passwd"
    export FILE_GROUP "${DIR_ETC}/group"
    export PROC_ACPI "${DIR_PROC}/acpi"
    export PROC_MOUNTS "${DIR_PROC}/mounts"
    export PROC_MEMINFO "${DIR_PROC}/meminfo"
    export PROC_MODULES "${DIR_PROC}/modules"
    export PROC_DISKSTATS "${DIR_PROC}/diskstats"

    # Application
    #export CMD_EXE 'type -P'
    # LS; colors, grouping.
    # IP; compile with
    export CMD_BUSYBOX ${CMD_BUSYBOX[@]:-$(busybox --list)}
    export CMD_BUSYBOX_SKIP 'ls' 'dpkg' 'modprobe' 'depmod'

    export VERSION_BUSYBOX $($(exe busybox) |$(exe head) -1 |$(exe sed) 's/.*v\([^ ]*\).*/\1/')
    export VERSION_BUSYBOX_MIN '1.19.3'

    # Environment
    unset SHELL_BASH
    unset POSIXLY_CORRECT
    export TY '1'
    export TERM "${TERM:-linux}"
    export USER_ID $($(exe id) -u)
    export GROUP_ID $($(exe id) -G)
    export GROUP $($(exe id) -gn)
    export HISTFILE "${DIR_TMP_CACHE}/bash_history"  #:order:3
    export HISTSIZE '10000'
    export USECOLOR 'yes'
    export HISTCONTROL 'erasedups'
    export TIME_POOL 'nist1-chi.ustiming.org'
    export HISTIGNORE '[ \t]+:??:exit:rm .*'  # Common commands.
    export XDG_CACHE_HOME "${DIR_TMP_CACHE}"  #:order:3
    export XDG_DATA_HOME "${HOME}/.local/share"
    export HISTTIMEFORMAT '%H:%M > '
    export XDG_CONFIG_HOME "${HOME}"
    export XDG_RUNTIME_DIR "${DIR_TMP_PID}"  #:order:3

    item=$($(exe hostname))
    export HOST "${item,,}"
    export HOSTNAME "${item,,}"

    item=$(tty)
    if [[ ${item} == *+(/)* ]]; then
        [[ ${item} == /dev/tty+([[:digit:]]) ]] && item="${item%+([[:digit:]])}"
        export TTY "${item}"
    fi
    item=( $(shells) )
    export SHELLS "${item[@]##*/}"

    export SHELL_BASH $($(exe pgrep) -f -- '-bash\b' |$(exe grep) -v "${$}")
    prompt_append 'environment.prompt'

    # Application
    export GROFF_NO_SGR '0'
    LS_COLORS='di=35:ln=36:cd=01;33:su=01;31:sg=01;31:tw=31:ex=32:mi=37:so=01;30'
    LS_COLORS+=':*.txt=222'
    LS_COLORS+=':*.log=33:*.pwd=31:*lock=01;30:*.pid=33'
    LS_COLORS+=':*.trt=033:*.torrent=033'
    LS_COLORS+=':*.pdf=027'
    LS_COLORS+=':*.srt=116:*.flv=114'
    LS_COLORS+=':*.bfe=040:*.gpg=028'
    export LS_COLORS
            #rs=0:mh=00:pi=40;33:so=01;35:do=01;35:bd=33:or=40;31;01:ca=30;41:ow=34;42:st=37;44:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:

    #export KERNEL "${KERNEL:-$($(exe uname) -r)}"  #:obsolete:
    export VERSION_KERNEL "${KERNEL:-$($(exe uname) -r)}"

    #-----#
    export TEMP "${DIR_TMP_CACHE}"
    export TMPDIR "${DIR_TMP_CACHE}"
    export LOCALEDIR "${DIR_TMP_CACHE}"
    #export I18NDIR "${DIR_I18N}"

    environment.variables_locale
    environment.variables_build
}
environment.variables_build() {
    local _DESC="
        Build (make/configure) specific."

    export PREFIX "${DIR_USR}"
    export PREFIX_ETC "${DIR_ETC}"
}
environment.variables_verbose() {
    case "${VERBOSE}" in
        0) unset VERBOSE_EXPORT VERBOSE_LINK VERBOSE_SOURCE VERBOSE_FUNC ;;
    esac

    export VERBOSE "${VERBOSE:-0}"
    export VERBOSE_FUNC "${VERBOSE_FUNC:-${VERBOSE}}"
    export VERBOSE_LINK "${VERBOSE_LINK:-${VERBOSE}}"
    export VERBOSE_EXPORT "${VERBOSE_EXPORT:-${VERBOSE}}"
    export VERBOSE_SOURCE "${VERBOSE_SOURCE:-${VERBOSE}}"
}
environment.variables_locale() {
    local item items itemD pass
    local itemT="${DIR_TMP_CACHE}/locales" itemL="${DIR_I18N}"
    local itemsA itemsS items=( ${itemL}/locales/* )
    unset LANGUAGE
    export DEFAULT_LOCALE C
    type -P 'locale' >/dev/null || return

    mkdir "${itemT}"
    [[ -d ${DIR_USR}/lib/locale ]] || sudo mkdir "${DIR_USR}/lib/locale"

    {
    [[ -e ${itemL}/charmap/UTF-8.gz ]] || return  #:todo: request.charmap()
    [[ -e ${itemT}/UTF-8 ]] ||
        $(exe gzip) -dc "${itemL}/charmap/UTF-8.gz" >"${itemT}/UTF-8"
    } # Charmap

    for item in "${items[@]}"; do
        itemD="${item##*/}"
        for item2 in $($(exe locale) -a); do
            [[ ${itemD} == ${item2%.*} ]] && continue 2
        done

        print_warn 'Locale: %s' "${itemD}"
        if $(exe localedef) --no-archive \
            --charmap="${itemT}/UTF-8" \
            --inputfile="${item}" \
            "${itemT}/${itemD}.UTF-8"; then

            print_erase "${itemD}"
            print_true "${itemD}\n"
            itemsA+=( "${itemT}/${itemD}.UTF-8" )
            (( pass++ ))
        else
            print_erase "${itemD}"
            print_false "${itemD}\n"
        fi
    done
    (( pass )) && sudo $(exe localedef) --quiet --add-to-archive ${itemsA[@]}

    while read item; do
        [[ ${item} == +(en_US)* ]] || continue
        LANGUAGE="${item}"
        break
    done <<<"$($(exe locale) -a)"

    export LANGUAGE "${LANGUAGE:-${DEFAULT_LOCALE}}"  #:order:0
    export LANG "${LANGUAGE}"
    export LC_ALL "${LANGUAGE}"
    export LOCALE "${LANGUAGE}"
}
environment.variables_path() {
    local item items

    unset PATH
    items=( ${HOME}/.{bash,perl,python}
        {/usr/local,/usr,}/{bin,sbin} )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] && PATH+="${PATH:+:}${item}"
    done
    export PATH

    unset PATH_MAN
    items=( "${DIR_TMP_DATA}"
        /usr/{local,local/share,share}/man )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] && PATH_MAN+="${PATH_MAN:+:}${item}"
    done
    export PATH_MAN

    unset LD_LIBRARY_PATH
    items=( "/usr/local/lib" "/usr/lib" "/lib" )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] && LD_LIBRARY_PATH+="${LD_LIBRARY_PATH:+:}${item}"
    done
    export LD_LIBRARY_PATH
    
    #-----#
    unset I18NPATH  # Locale definitions.
    items=( "${DIR_TMP_CACHE}/" "${DIR_I18N}/" "/usr/share/i18n/" )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] && I18NPATH+="${I18NPATH:+:}${item}"
    done
    export I18NPATH

    #unset LOCPATH  # Locale definitions.
    #items=( "${DIR_USR}/share/i18n/locales" )
    #for item in "${items[@]}"; do
    #    [[ -d ${item} ]] && LOCPATH+="${LOCPATH:+:}${item}"
    #done
    #export LOCPATH

    unset TERMINFO  # Terminfo
    items=( "${DIR_TMP_CACHE}/terminfo"
        "${DIR_USR}/local/terminfo" "${DIR_USR}/share/terminfo" )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] && TERMINFO+="${TERMINFO:+:}${item}"
    done
    export TERMINFO

    unset XDG_DATA_DIRS
    items=( {/usr/local,/usr,}/share )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] && XDG_DATA_DIRS+="${XDG_DATA_DIRS:+:}${item}"
    done
    export XDG_DATA_DIRS
} 
environment.variables_color() {
    local name code
    local int

    # Color
    export NL '\012'
    export CRE '\r\033[K'
    export ESC '\e['
    export BOLD '\e[1m'
    export ESC256 '\e[38;05;'
    export ESCAPE '\e['
    export COLOR_CODE '0' '30' '31' '32' '33' '34' '35' '36' '37'
    export COLOR_NAME 'NORMAL' 'BLACK' 'RED' \
        'GREEN' 'YELLOW' 'BLUE' 'MAGENTA' 'CYAN' 'WHITE'
    for (( int=0;int<${#COLOR_NAME[@]};int++ )); do
        name="${COLOR_NAME[${int}]}"
        code="${COLOR_CODE[${int}]}"
        export "${name}" "${ESC}${code}m"
        export "${name}_BACK" "${ESC}${code/3/4}m"
    done
    
    # Dircolors (LS_COLORS)
    #if exist_exe 'dircolors' && [[ -e ${CONF_DIRCOLORS} ]]; then
    #    eval "$($(exe dircolors) "${CONF_DIRCOLORS}")"
    #fi
}
environment.variables_prompt() {
    local item

    #item=( $(stty size) )
    #export ROW "${item[0]}"
    #export COLUMN "${item[1]}"

    PS1="\[${NORMAL}\]"
    if (( REMOTE )); then
        item=( ${SSH_CONNECTION} )
        export PS1 "[${item[2]}]"
    fi
    PS1+='> '
    export PS1
}
environment.prompt() {
    if (( SHLVL > 1 )); then
        environment.variables_verbose
    elif ! (( PROMPT )); then
        $(exe uname) -rv
    fi
    environment.variables_prompt
}

environment.login() {
    environment.mkdirs
    environment.chmods
}

environment.alias() {
    alias cp '$(exe cp) -i'
    alias df '$(exe df) -h'
    alias rm '$(exe rm) -i'
    alias du '$(exe du) -hc'
    alias which 'builtin type -a'

    alias ls '$(exe ls) -hF --color=auto --group-directories-first --time-style="+${FORMAT_DATE_LS}"'
    #alias ll 'ls -l'
    #alias la 'll -A'  # Show hidden
    #alias lx 'll -xB'  # Sort extention
    #alias lk 'll -St'  # Sort size, largest last
    #alias lc 'll -tcr'  # Sort change, recent last
    #alias lu 'll -tur'  # Sort access, recent last
    #alias lt 'll -gotr'  # Sort date, recent last
    #alias lr '$(exe l)s -R'  # Recursive
}

environment.mkdirs() {
    local item items

    items=( "${HOME}" "${DIRS_TMP[@]}" )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] || sudo $(exe mkdir) -p -m 1777 "${item}"
    done

    items=( "${DIRS_VAR[@]}" )
    for item in "${items[@]}"; do
        [[ -d ${item} ]] || sudo $(exe mkdir) -p -m 0755 "${item}"
    done
}
environment.chmods() {
    local item items

    items=( "${DIRS_VAR[@]}" )
    for item in "${items[@]}"; do
        sudo $(exe chgrp) -R $($(exe id) -g) "${item}"
        sudo $(exe chmod) -R 0775 "${item}"
    done
}

if [[ ${VERSION_BUSYBOX} < ${VERSION_BUSYBOX_MIN} ]]; then
    print_false 'BUSYBOX (%s < %s)\n' \
        "${VERSION_BUSYBOX}" "${VERSION_BUSYBOX_MIN}"
fi

environment.variables
environment.alias
return 2>/dev/null

