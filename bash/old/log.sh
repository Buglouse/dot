#!/usr/bin/env bash
source 'bash.sh'

log.variables() {
true
}

log.move() {
    local item

    for item in "${@}"; do
        [[ -e ${item} ]] || continue

        sudo $(exe chown) ${USER_ID}:${GROUP_ID} "${item}"
        $(exe mv) "${item}" "${item}_$($(exe stat) -c '%Y' "${item}")"
    done
}

log.save() {
    local _ARGS="[path]"
    local _DESC="
        Archive logs in PATH."
    local item items itemsN pathO pathI="${1:-${DIR_TMP_LOG}}"
    until [[ -d ${pathO} ]]; do
        read -e -i "${DIR_TMP_DATA}" -p 'Destination Path: ' pathO
    done

    items=( ${pathI}/* )
    unset itemsN
    for item in "${items[@]}"; do
        item="${item%%[_.]*}"
        exist_elem "${item##*/}" "${itemsN[@]##*/}" || itemsN+=( "${item##*/}" )
    done

    for item in "${itemsN[@]}"; do
        items=( ${pathI}/${item}* )
        [[ ${items} ]] &&
            $(exe tar) -cz \
                -C "${pathI}" \
                -f "${DIR_TMP_DATA}/${item}.tar.gz" \
                "${items[@]##*/}"
    done
}

log.remove() { _rm ${DIR_TMP_LOG}/*.*_*; }

log.print() {
    local item items
    local IFS='|'; local re="${*}"; unset IFS

    if [[ ${1} == +(*/*) ]]; then
        for item in "${@}"; do
            items+=( $($(exe find) "${@}" -regex ".*\.log") )
        done
    elif (( ${#} )); then
        items=( $($(exe find) "${DIR_TMP_LOG}" -regex ".*\(${re//|/\\|}\).*\.log") )
    else
        items=( ${DIR_TMP_LOG}/*.log )
    fi
    [[ ${items} ]] || return
    selects -1 "${items[@]#${DIR_TMP_LOG}/}"
    if [[ ${SELECT} == +(/*) ]]; then
        item="${SELECT}"
    else
        item="${DIR_TMP_LOG}/${SELECT}"
    fi
    [[ -f ${item} ]] || return

    printf -- '--%s--\n' "${item}"
    #--unbuffered \
    $(exe tail) -f "${item}" |
        $(exe sed) \
                -e 's/!\( : \)\(.*\)/\1\n\t\2/' \
                -e 's/ [:;] /\n\t/g'
        #$(exe less)
            #-e 's/  \{2,\}//' \
            #-e 's/\(.*\)[ ]\{2,\}\(.*\)/\1\n\t \2/p' \
}

log.redirect() {
    local _ARGS='<commands> <file>'
    local _DESC="
        Execute COMMANDS, append output into FILE."
    local item out="${@:${#}}" items=( "${*:1:${#}-1}" )
    local IFS=';'; items=( ${items[*]} ); unset IFS

    for item in "${items[@]}"; do
        printf '\n#> %s\n' "${item}"
        eval "${item}" 2>&1 |while read; do
            printf '\t%s\n' "${REPLY}"
        done
    done &>>"${out}"
}


log.arguments() {
    local item 
    unset ACTION FORCE

    while [[ ${args} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

log.variables
return 2>/dev/null

_ARGS=''
_DESC="Log."

log.arguments "${@:-?}" || exit

case "${ACTION}" in
    *) ;;
esac

