#/usr/bin/env bash
fan_find() {
    local pathHwmon='/sys/class/hwmon'
    for item in ${pathHwmon}/hwmon*; do
	[[ -e ${item}/pwm1 ]] || continue
	pathHwmon="${item}"
    done
    
    local pwm="${pathHwmon}/pwm1"
    local fan="${pathHwmon}/fan1_input"
    local pwmStatus="${pathHwmon}/pwm1_enable"
    [[ -e ${pwm} && -e ${fan} && -e ${pwmStatus} ]] || return
}
fan_set() {
    local item status
    
    if [[ ${1} == 1 ]]; then
	status=1
    elif [[ ${1} == 0 ]]; then
	status=2
    else
	status=1
    fi
    printf '%s' "${status}" |sudo tee "${pwmStatus}"

    read -p 'PWM Value (0-255)? '
    [[ ${REPLY} == *([0-9]) ]] || return
    echo "${REPLY}" |sudo tee "${pwm}"
}
fan_status() {
    while sleep 5; do
    printf '%s\n' "$(<"${fan}")"
    done
    #printf '%s' "${action}" |sudo tee "${dir}"
}

#fan.variables() {
#    export LIMIT_FAN_TEMP_MAX "${LIMIT_FAN_TEMP_MAX:-62}"
#    export LIMIT_FAN_TEMP_MIN "${LIMIT_FAN_TEMP_MIN:-40}"
#    export MULTI_FAN '21'
#    export OFFSET_FAN '40'
#}
#
#fan.set() {
#    local item id path type="${1}"
#    
#    for item in "${@:2}"; do
#        (( ${FAN_ID[${item}]} )) || continue
#        path="${SYS_CLASS_HWMON}/hwmon${item}/pwm1_enable"
#
#        (( $(<"${path}") == type )) && continue
#        printf '%s' "${type}" |sudo $(exe tee) "${path}" >/dev/null
#    done
#}
#fan.set_rpm() {
#    local item id path count="${1:-1}"
#
#    if (( count > 10 )); then
#        count=10
#    elif (( count < 0 )); then
#        count=0
#    fi
#    count=$(( (MULTI_FAN*count)+OFFSET_FAN ))
#
#    fan.set 1 "${FAN_IDS[@]}"
#
#    for item in "${@:2}"; do
#        (( ${FAN_ID[${item}]} )) || continue
#
#        path="${SYS_CLASS_HWMON}/hwmon${item}/pwm1"
#        printf '%s' "${count}" |sudo $(exe tee) "${path}" >/dev/null
#    done
#}
#
#fan.get() {
#    local item type
#
#    for item in "${@}"; do
#        type="$(<"${SYS_CLASS_HWMON}/hwmon${item}/pwm1_enable")"
#
#        printf '%s:%s' "${item}" "${type}"
#        case "${type}" in
#            1) printf '[user]\n' ;;
#            2) printf '[sensor]\n' ;;
#            3) printf '[sensor+user]\n' ;;
#            *) printf '[unknown]\n' ;;
#        esac
#    done
#}
#fan.list() {
#    local item rpm
#
#    for item in "${@:-${FAN_IDS[@]}}"; do 
#        (( ${FAN_ID[${item}]} )) || return
#        rpm="$(<"${SYS_CLASS_HWMON}/hwmon${item}/fan1_input")"
#
#        if [[ ${1} == cvs ]]; then
#            printf '%s,%s\n' "${item}" "${rpm}"
#        else
#            printf '%s:%s\n' "${item}" "${rpm}"
#        fi
#    done
#}
#
#fan.calibrate() {
#    local item count
#
#    for item in "${FAN_IDS[@]}"; do
#        (( ${FAN_ID[${item}]} )) || continue
#
#        for (( count=255; count!=0; count-- )); do
#            printf '%s' "${count}" |sudo $(exe tee) "${SYS_CLASS_HWMON}/hwmon${item}/pwm1"
#            sleep 0.5
#            printf ':%s\n' "$(<"${SYS_CLASS_HWMON}/hwmon${item}/fan1_input")"
#        done
#        for (( count=0; count!=255; count++ )); do
#            printf '%s' "${count}" |sudo $(exe tee) "${SYS_CLASS_HWMON}/hwmon${item}/pwm1"
#            sleep 0.5
#            printf ':%s\n' "$(<"${SYS_CLASS_HWMON}/hwmon${item}/fan1_input")"
#        done
#    done
#}
#
#fan.auto() {
#    local item temp items=( $(cpu.list --cvs) )
#
#    for item in "${items[@]}"; do
#        [[ ${item} ]] || continue
#        IFS=','; item=( ${item} ); unset IFS
#        item="${item[1]}"
#
#        [[ ${temp} ]] && { (( item >= temp )) && continue; }  # Find max temp.
#        temp="${item}"
#    done
#
#    if (( temp >= LIMIT_FAN_TEMP_MAX )); then
#        fan.set_rpm 9 "${FAN_IDS[@]}"
#    elif (( temp >= LIMIT_FAN_TEMP_MIN )); then
#        fan.set_rpm 5 "${FAN_IDS[@]}"
#    else
#        fan.set_rpm 2 "${FAN_IDS[@]}"
#    fi
#}
#
#fan.arguments() {
#    local item 
#    unset ACTION ITEMS ITEM_DIGIT FAN
#
#    while [[ ${1} ]]; do
#        parse.arg "${@}"; shift ${SHIFT}
#        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
#        for item in "${ARG[@]}"; do
#            case "${item}" in
#                \?) unset ACTION; break ;;
#                *([[:digit:]])) ITEM_DIGIT="${ARG}" ;;
#                A|auto) ACTION='auto' ;;
#                u|user) ACTION='set' TYPE=1 ;;
#                c|cali*) ACTION='calibrate' ;;
#                s|sensor) ACTION='set' TYPE=2 ;;
#                l|list) ACTION='list' ;;
#                t|st*|get) ACTION='get' ;;
#            esac
#        done
#    done
#
#    [[ ${ITEMS} ]] || ITEMS=( "${FAN_IDS[@]}" )
#    if [[ ${ITEM_DIGIT} ]]; then
#        ACTION='set' TYPE=1
#
#    elif [[ ${ACTION} == set ]]; then
#        [[ ${TYPE} ]] || TYPE='2'
#
#    else
#        [[ ${ACTION} ]]
#    fi
#}
#
#source 'cpu'
#source 'device_fan' || return 2>/dev/null
#fan.variables
#return 2>/dev/null
#
#_ARGS='(A)uto (c)alibrate (digit) s(t)atus (l)ist (u)ser (s)ensor'
#_DESC="Fan.
#    Status, print rpm of FAN.
#    Auto, set rpm from cpu_temp between ${LIMIT_FAN_TEMP_MAX} & ${LIMIT_FAN_TEMP_MIN}
#    Calibrate, spin up and down FANs, output rpm.
#    User|Sensor, FAN rpm determined by (USER) or (SENSOR).
#    Digit[0-10], set FAN to rpm (255-${OFFSET_FAN}/10).
#    Additional arguments are FAN (0 offset)."
#
#fan.arguments "${@:-?}" || exit
#
#[[ ${FAN_IDS} ]] || exit
#case "${ACTION}" in
#    auto) fan.auto ;;
#    list) fan.list "${ITEMS[@]}" ;;
#    calibrate) fan.calibrate ;;
#    set)
#        fan.set "${TYPE}" "${ITEMS[@]}"
#        [[ ${ITEM_DIGIT} ]] && fan.set_rpm "${ITEM_DIGIT}" "${ITEMS[@]}"
#    ;;
#    get) fan.get "${ITEMS[@]}" ;;
#esac
#
