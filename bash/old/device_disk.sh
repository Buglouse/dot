#!/usr/bin/env bash
source 'bash.sh'

device.disk.variables() {
    export 'DEVICE_DISK' $(find "${SYS_CLASS_BLOCK}" -name '[hs]d[[:alpha:]]')
}

device.disk.state() {
    local item

    for item in "${@}"; do
        $(exe hdparm) -C "/dev/${item}"
    done
}

device.disk.suspend() {
    local item

    for item in "${@}"; do
        $(exe lsof) |$(exe grep) -wi "/dev/${item}" && continue
        $(exe grep) -wi "/dev/${item}" 

        $(exe hdparm) -Y "/dev/${item}"
    done
}

device.disk.arguments() {
    local v items args=( "${@:-?}" )
    unset ACTION

    while [[ ${args} ]]; do
        parse.arg "${args[@]}"; args=( "${args[@]:${SHIFT}}" )
        [[ ! ${ARG} && ${ARG_OPT} ]] && items=( ${ARG_OPT[@]} )
        for v in "${ARG[@]}"; do
            case "${v}" in
                \?) unset ACTION; break ;;
                Y) ACTION='suspend' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

device.disk.test() {
    # SMART
    # badblocks

    dd if='/dev/zero' of="${item}" bs=8M

    smartctl -a "${item}"
    #
    smartctl -x "${item}"
    smartctl -c "${item}"
    smartctl -t (short|long) "${item}"
    smartctl -H "${item}"
    smartctl -l selftest "${item}"
    smartctl -a "${item}"

    badblocks -v "${item}"
    if ${found}; then
	badblocks "${item}" >"${temp}"
	fsck -l "${temp}" "${item}"
    fi

    hdparm -t "${item}"
    dd count=25 bs=8M if=/dev/zero of="${item}" oflag=sync
}

device.disk.variables
return 2>/dev/null

_ARGS=''
_DESC="ACPI."

device.disk.arguments "${@}" || exit

case "${ACTION}" in
    suspend)
    ;;
esac
