#!/usr/bin/env bash
source 'bash.sh'

x.variables() {
    export DIR_XORG "${HOME}/.xorg"  #:order:0

    export DIR_XKB "${DIR_XORG}/xkb"  #:order:1

    export CONF_XKB "${DIR_XKB}/105_us.xkb"
    export CONF_XAUTHORITY "${HOME}/.xorg/auth.xorg"  #:order:0
    export CONF_XRESOURCES "${HOME}/.xorg/xresources.conf"

    export LOG_XORG "${DIR_TMP_LOG}/xorg.log"
    export LOG_WM "${DIR_TMP_LOG}/${CMD_WM}.log"
    export LOG_XKB "${DIR_TMP_LOG}/xkb.log"

    export PID_XORG "${DIR_TMP}/.X0-lock"
    export PID_WM "${DIR_TMP_PID}/${CMD_WM}.pid"
    export DISPLAY ':0.0' # HOSTNAME:DISPLAY:SCREEN

    #-----#
    export XAUTHORITY "${CONF_XAUTHORITY}"  #:order:1
}

x.authority() {
    local item hash=$(mcookie)

    for item in "${DISPLAY%.*}"; do
        if ! [[ -e ${XAUTHORITY} ]] || $(exe xauth) list "${item}" |
                $(exe grep) -q "${item} "; then
            $(exe xauth) -q add ${item} . ${hash} &>/dev/null
            (( VERBOSE )) && print_true 'XAuth: %s\n' "${item}"
        fi
    done
}

x.xorg_conf() {
 true
 # Synaptic condiional.
}
x.xorg() {
    local count

    $(exe fc-cache) -f
    exist_proc "${CMD_XORG}" && kill "${CMD_XORG}"

    x.xorg_conf &&
	$(exe ${CMD_XORG}) +xinerama 2>/dev/null & sleep 0.3
    #-logfile "${LOG_XORG}" -novtswitch -sharevts -allowMouseOpenFail

    until exist_proc "${PID_XORG}"; do sleep 0.3; done

    $(exe xset) s blank
    $(exe xset) s 240
    $(exe xset) dpms 300 480 600
}

x.manager() {
    set +C
    $(exe ${CMD_WM}) -display "${DISPLAY}" &>>"${LOG_WM}" &
    printf "${!}" >"${PID_WM}"
    set -C

    until exist_proc "${PID_WM}"; do sleep 0.3; done
}

x.resources() {
    [[ -e ${CONF_XRESOURCES} ]] || return

    print_warn "xresources"
    if $(exe xrdb) -override "${CONF_XRESOURCES}"; then
        print_erase 'xresources'; print_true 'xresources\n'
    else
        print_erase 'xresources'; print_false 'xresources!\n'
        (( 0 ))
    fi
}

x.keyboard() {
    [[ -e ${CONF_XKB} ]] || return

    print_warn "xkb"
    if $(exe xkbcomp) \
            -I"${DIR_XKB}" "${CONF_XKB}" "${DISPLAY}" \
            &>>"${LOG_XKB}"; then
        print_erase 'xkb'; print_true 'xkb\n'
    else
        print_erase 'xkb'; print_false 'xkb!\n'
        (( 0 ))
    fi
}

x.arguments() {
    local item
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
                I|init) ACTION='init' ;;
                k|keyboard) ACTION='keyboard' ;;
                r|resources) ACTION='resources' ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

x.variables
return 2>/dev/null

_ARGS='(I)nit (r)esources (k)board (m)anager'
_DESC=""

x.arguments "${@:-?}" || exit

case "${ACTION}" in
    init)
        type -P "${CMD_WM}" >/dev/null ||
            { print_false 'Missing: %s\n' "${CMD_WM}"; exit 1; }

        available_bytes "${DIR_TMP}" '55' || exit
        #x.authority
        x.xorg && x.manager || exit
        x.resources
        x.keyboard
    ;;
    manager) x.manager ;;
    keyboard) x.keyboard ;;
    resources) x.resources ;;
esac
