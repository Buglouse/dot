#!/usr/bin/env bash
source 'bash.sh'

acpi.variables() {
    export SYS_CLASS_THERMAL '/sys/class/thermal'
    export SYS_DEVICES_SYSTEM_CPU '/sys/devices/system/cpu'
}

acpi.arguments() {
    local item
    unset ACTION ITEM

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS=( ${ARG_OPT[@]} )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) unset ACTION; break ;;
            esac
        done
    done
    [[ ${ACTION} ]]
}

(( SOURCE )) && return 2>/dev/null
acpi.variables
return 2>/dev/null

_ARGS=''
_DESC="ACPI."

acpi.arguments "${@:-?}" || exit

