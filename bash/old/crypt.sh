#!/usr/bin/env bash
source 'bash.sh'

crypt.variables() {
    export DIR_AUTH "${DIR_AUTH:-${HOME}/.auth}"
    export DIR_AUTH_TMP "${DIR_AUTH_TMP:-${DIR_TMP_CACHE}}"
    export CRYPT_SCHEME 'gpg' 'bfe'

    crypt.mkdirs
}

crypt.mkdirs() {
    [[ -d ${DIR_AUTH_TMP} ]] || mkdir "${DIR_AUTH_TMP}"
    [[ -d ${DIR_AUTH} ]] || print_false 'Missing: %s\n' "${DIR_AUTH}"
    [[ -d ${DIR_AUTH} ]]
}

crypt.gpg_prv_list() {
    $(exe gpg) -K |$(exe grep) '^\(sec\|uid\)' |$(exe sed) -e 's/  */ /g'
}
crypt.gpg_pub_list() {
    #:todo: Sed
    #$(exe sed) -n -e '/^pub/{N;/[^\n].*Key/{s/[^=]*=//;s/ //g;};h;};p'
    $(exe gpg) --list-keys --fingerprint |$(exe tail) -n +3
}

crypt.import() {
    local item="${1##*.}"
    [[ -t 0 && -t 1 ]] || return

    case "${item}" in
        gpg) crypt.import_gpg "${1}" ;;
        bfe) crypt.key ;;
        *) return 1 ;;
    esac
}
crypt.import_gpg() {
    local item item2 items PS3 IFS
    IFS='|'; local reS="${CRYPT_SCHEME[*]}" reE='gpg'; unset IFS
    local key pub prv=( $($(exe gpg) --list-secret-keys |
            $(exe grep) '^\(prv\|ssb\)' |
            $(exe sed) -e 's#[^/]*/\(.*\) .*#\1#') )

    if [[ -e ${1} ]]; then  # Decrypt.
        key=( $($(exe gpg) -v --list-only "${1}" 2>&1 |
            $(exe sed) -e 's/.* \(\w\)/\1/') )

        for item in "${prv[@]}"; do  # Private imported?
            for item2 in "${key[@]}"; do
                [[ ${item2} == ${item} ]] && return
            done
        done

        crypt.gpg_prv_list
        items=( ${DIR_AUTH}/*.+(prv).* )

    else  # Encrypt
        crypt.gpg_pub_list
        items=( ${DIR_AUTH}/*.+(pub).* )
    fi
    [[ ${items} ]] || return

    PS3='Import: ' selects -1 "${items[@]}"
    for item in "${SELECT[@]}"; do
        if [[ ${item} == *.+(${reS}) ]]; then  # Decrypt.
            crypt.decrypt "${item}" || return
            item="${DIR_AUTH_TMP}/${item##*/}"; item="${item%.*}"
        fi
        [[ -e ${item} ]] || continue
        $(exe gpg) --status-fd 1 --import "${item}" 2>/dev/null |
            $(exe grep) -iv 'res'
    done

    if [[ -e ${1} ]]; then
        [[ $($(exe gpg) --list-secret-keys |
            $(exe grep) '^\(prv\|ssb\)' |
            $(exe sed) -e 's#[^/]*/\(.*\) .*#\1#') ]] || return
    else
        PS3='ID: ' selects -1 $($(exe gpg) --list-keys |
            $(exe grep) '^\(pub\|sub\)' |
            $(exe sed) -e 's#[^/]*/\([^ ]*\).*#\1#')
        [[ ${SELECT} ]] && export CRYPT_KEY "${SELECT}"
    fi
}
crypt.key() {
    unset CRYPT_KEY
    read -s -p 'Crypt Key: ' CRYPT_KEY || return
    printf '\n'
    export CRYPT_KEY
    [[ ${CRYPT_KEY} ]]
}

crypt.decrypt() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local _ARGS="<dir> <files>"
    local _DESC="
        Dir, last argument as directory.
        Write FILES to DIR."
    local item items itemX itemE itemP re='gpg|bfe'
    for item in "${@}"; do
        [[ ${item} == *+(/)* || -d ${item} ]] && itemP="${itemP}"
    done
    [[ ${itemP} ]] || itemP="${DIR_AUTH_TMP}"

    for item in "${@}"; do
        [[ -e ${item} && ${item##*.} == +(${re}) ]] || continue
        itemE="${item##*.}"  # Extension
        itemX="${item##*/}"  # File
        itemX="${itemP%/}/${itemX%.${itemE}}"  # Extracted 
        [[ -e ${itemX} && $($(exe stat) -c '%s' "${itemX}") -gt 1 ]] && continue

        print_true '%-20s' "[${itemE^^}]"
        print_warn '%s\n' "${item}"

        while :; do
            crypt.import "${item}" || break

            set +C
            case "${itemE}" in
                gpg)
                    crypt.key
                    $(exe gpg) --decrypt --batch --yes \
                        --passphrase "${CRYPT_KEY}" \
                        --output "${itemX}" \
                        "${item}" 2>/dev/null && break
                ;;
                bfe)
                    printf '%s\n' "${CRYPT_KEY}" |
                        $(exe bcrypt) -o $($(exe realpath) \
                            "${item}") \
                            >"${itemX}" 2>/dev/null && break
                ;;
            esac
            set -C
        done
        [[ -e ${itemX} ]] && chmod 0600 "${itemX}"
    done
    unset CRYPT_KEY
}

crypt.encrypt() {
    local setOld="${-}"; set +x
    trap '{ [[ ${setOld} ]] && builtin set -${setOld}; }' RETURN EXIT
    local PS3 IFS='|'; re="${CRYPT_SCHEME[*]}"; unset IFS
    local _ARGS="(${re}) <dir> <files>"
    local _DESC="
        Dir, last argument as directory.
        Write FILES to DIR.

        Use same scheme as FILES in DIR."
    local item items itemX itemE itemP re='gpg|bfe'

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            +(${re})) action="${item}" ;;
        esac
        shift
    done
    for item in "${@}"; do
        [[ -d ${item} ]] && { itemP="${item}"; continue; }
        items+=( "${item}" )
    done
    itemP="${itemP:-${DIR_AUTH}}"; itemP="${itemP%/}"

    for item in "${items[@]}"; do
        [[ -e ${item} ]] || continue
        itemX=( ${itemP}/${item##*/}.+(${re}) )  # Find existing.
        #:todo: (( ${#itemX[@]} > 1 )) ; uniq
        if [[ ${action} ]]; then
            itemE="${action}"
        elif [[ ${itemX} ]]; then
            itemE="${itemX##*.}"
        else
            PS3='Scheme: ' selects -1 "${CRYPT_SCHEME[@]}"
            itemE="${SELECT}"
        fi
        itemX="${itemP}/${item##*/}.${itemE}"
        [[ ${itemE} == +(${re}) ]] || return

        print_true '%-20s' "[${itemE^^}]"
        print_warn '%s\n' "${itemX}"

        while :; do
            crypt.import "${itemE}" || break

            set +C
            case "${itemE}" in
                gpg)
                    $(exe gpg) --trust-model always --yes --encrypt \
                        --recipient "${CRYPT_KEY}" \
                        --output "${itemX}" "${item}" && break
                ;;
                bfe)
                    printf '%s\n' "${CRYPT_KEY}" "${CRYPT_KEY}" |
                        $(exe bcrypt) -o "${item}" \
                            >"${itemX}" 2>/dev/null && break
                ;;
            esac
            set -C
        done
        [[ -e ${itemX} ]] && chmod 0644 "${itemX}"
        unset CRYPT_KEY
    done
}

crypt.pem() {
    local item 

    for item in "${@:-${HOSTNAME}}"; do
        [[ -e ${DIR_AUTH}/${item}.pub.rsa ]] ||
            { print_false 'Hostname! '; print_warn '%s\n' "${item}"; return 1; }

        crypt.decrypt "${DIR_AUTH}/${item}.prv.rsa.gpg" &&
            { set +C
            $(exe cat) "${DIR_AUTH_TMP}/${item}.prv.rsa" "${DIR_AUTH}/${item}.pub.rsa" \
                >"${DIR_AUTH_TMP}/${item}.pem.rsa"
            set -C; }
    done
}

crypt.generate_ca() {
    local _ARGS='<path> <file>'
    local _DESC="
        File, set file.
        Path, set output directory."
    local item itemF itemP itemC="${DIR_TMP_CACHE}/${HOSTNAME}_${CERT_SIZE}.dh"

    while [[ ${1} == +(-)* ]]; do
        item="${1##*(-)}"
        case "${item}" in
            \?) print_help; return 1 ;;
            
        esac
        shift
    done
    for item in "${@}"; do
        if [[ ${item} == *+(/)* || -d ${item} ]]; then
            itemP="${item%/}"
        else
            itemF="${item}"
        fi
    done
    itemP="${itemP:-${DIR_AUTH}}/${itemF:-${HOSTNAME}}"

    # Diffie-Hellman parameters
    #$(exe openssl) dhparam \
    #    -out "${itemC}" "${CERT_SIZE}"

    #$(exe openssl) genpkey \
    #    -algorithm rsa \
    #    -outform pem \
    #    -out "${itemP}.prv.rsa" || return
    #$(exe openssl) pkey \
    #    -inform pem -outform pem \
    #    -pubout \
    #    -in "${itemP}.prv.rsa" \
    #    -out "${itemP}.pub.rsa" || return

    #$(exe openssl) ca -config "${itemC}" \
    #    -days 180 -extensions v3_ca \
    #    -out "${itemP}.crt.rsa" \
    #    -in "${itempP}.crt.rsa"

    # Root
    [[ -e ${itemP}.prv.rsa || -e ${itemP}.pub.rsa ]] &&
        { $(exe openssl) x509 -noout -subject -dates -fingerprint \
                -in "${itemP}.pub.rsa"
        read_false "Overwrite: ${itemP}.prv.rsa" || return; }

    if $(exe openssl) req -x509 -nodes -days 3560 \
        -newkey rsa:${CERT_SIZE} \
        -config "${CONF_OPENSSL}" \
        -keyout "${itemP}.prv.rsa" \
        -out "${itemP}.pub.rsa"; then
        $(exe openssl) x509 -noout -subject -dates -fingerprint \
                -in "${itemP}.pub.rsa"
        chmod 0644 "${itemP}.prv.rsa"
    else
        print_false 'Invalid!\n'
        return 1
    fi
            
    $(exe rm) -f "${itemP}.pub.rsa.ssh"
    read_true 'SSH' &&
        { chmod 0600 "${itemP}.prv.rsa"
        $(exe ssh-keygen) -y \
            -f "${itemP}.prv.rsa"  \
            >"${itemP}.pub.rsa.ssh"; }
}

crypt.md5() {
    local _ARGS='<files>'
    local _DESC="
        Print fingerprint of FILES."
    local item

    for item in "${@}"; do
        [[ -e ${item} ]] || return

        item="$($(exe openssl) x509 -noout -fingerprint \
            -in "${item}")"
        item="${item//:/}" item="${item#*=}"
        printf '%s\n' "${item,,}"
    done
}

crypt.list() {
    $(exe gpg) --list-keys --keyid-format long
    $(exe gpg) --list-secret-keys --keyid-format long
}

crypt.help() {
printf '%s\n' 'GPG
    Primary
        gpg --gen-key
        gpg --edit ${keyId} && { key n; delkey; }
        gpg --keyserver ${keySrv} --send-keys ${keyId}

        Export
            gpg -export ${keyId} > ${file}.pub.enc
            gpg -export-secret-key ${keyId} > ${file}.prv.enc

    SubKey
        gpg --edit ${keyId} && { addkey; save; }

        Export
        gpg --export-secret-subkeys ${keyId} > ${file2}.prv.enc'
}

crypt.arguments() {
    local item 
    SCHEME='gpg'
    unset ACTION ITEMS

    while [[ ${1} ]]; do
        parse.arg "${@}"; shift ${SHIFT}
        [[ ! ${ARG} && ${ARG_OPT} ]] && ITEMS+=( "${ARG_OPT[@]}" )
        for item in "${ARG[@]}"; do
            case "${item}" in
                \?) return 1 ;;
                R|rsa) ACTION='rsa' ;;
                M|md5) ACTION='md5' ;;
                l|list) ACTION='list' ;;
                h|help) ACTION='help' ;;
                I|import) ACTION='import' ;;
                r|random) ACTION='random' ;;
                d|decrypt) ACTION='decrypt' ;;
                e|encrypt) ACTION='encrypt' ;;

                G|gpg) TYPE='gpg' ;;
                p|pub) TYPE='encrypt' ;;
                s|prv) TYPE='decrypt' ;;
                B|blowfish) TYPE='bfe' ;;

                path) DIR_AUTH_TMP="${ARG_OPT}" ;;
            esac
        done
    done

    if [[ ${ACTION} == import ]]; then
        [[ ${TYPE} ]] || { selects 'decrypt' 'encrypt' && TYPE="${SELECT}"; }
        [[ ${SCHEME} ]] || { selects "${CRYPT_SCHEME[@]}" && SCHEME="${SELECT}"; }

    elif [[ ${ACTION} == +(+(de|en)crypt) ]]; then
        [[ ${TYPE} && ${ITEMS} ]]
    else
        [[ ${ACTION} ]]
    fi
}

crypt.variables
return 2>/dev/null

_ARGS='(h)elp (l)ist (I)mport (p)ub (s)ec (d)ecrypt (e)ncrypt (r)andom (R)sa (M)d5 (G)pg (B)fe (path)'
_DESC="Crypt.
    Path, extract path.
    Help, print gpg usage.
    Scheme: GPG|Blowfish.
    Rsa, produce certificate using this algorithm.
    Md5, print md5 hash."

crypt.arguments "${@:-?}" || exit

case "${ACTION}" in
    md5) crypt.md5 "${ITEMS[@]}" ;;
    rsa) crypt.rsa "${ITEMS[@]}" ;;
    help) crypt.help ;;
    list) crypt.list ;;
    import) crypt.import "${TYPE}" "${SCHEME}" ;;
    encrypt) crypt.encrypt "${ITEMS[@]}" ;;
    decrypt) crypt.decrypt "${ITEMS[@]}" ;;
esac
