#!/usr/bin/env bash

[[ ${INSIDE_EMACS} ]] && return

login.login() {
    local _DESC="
        :!:REMOVE ${HOME}

        Execute if running instances of shell are equal to 1 and there are no forks.
        Clean ${HOME} directory."
    local items

    read_true -3 'Login' || return

    #--Dot--#
    [[ ${SSH_TTY} ]] ||
        { read_true --erase 'Dot: Home' &&
            { print_warn '[HOME]'; print_init "(${DIR_REPO})"
            items=( ${HOME}/.* )
            [[ ${items} ]] && sudo $(exe rm) -rf "${items[@]}" 2>/dev/null
            if profile.user.dot; then
                print_init_true 'True'
            else
                print_init_false 'False'
            fi; }; }

    environment.login
    [[ ${CMD_LOGIN} ]] && ${CMD_LOGIN}  # Distro.
    profile.user.login
    source bash.sh
}

login.source() {
    local _DESC="
        Prompt, file or directory for 'bash.sh'."
    local item items

    items=( "${HOME}/.bash" "${BASH_SOURCE%/*}" )
    for item in "${items[@]}"; do
        item="${item}/bash.sh"
        [[ -e ${item} ]] && break
    done
    if [[ -e ${item} ]]; then
        source "${item}"
    else
        until [[ -e ${item} ]]; do
            read -erp 'Source: ' item
        done
        [[ -e ${item} ]] && item="${item%/*}"
        [[ -d ${item} ]] && source "${item}/bash.sh"
    fi
}

_ARGS=''
_DESC="
    Using ${BASH_SOURCE} to locate and source 'bash.sh'."

login.source || { print_false 'Source!\n'; exit 1; }
(( SHLVL == 1 && ${#SHELL_BASH[@]} <= 1 )) && login.login
