#!/bin/env bash
#:TODO: Verify not busybox (ldd)
shopt -s extglob
#set -x

# Partition
# Encrypt
# Filesystem
# Bootstrap
# Packages
# Users/Credentials
# Hard Customizations
# User Customizations
# Kernel
# Bootloader

PARAMS=""
DISTRO=""
IMAGE=""
DISK=""
MNTPRFX=""
MNTROOT=""
MNTBOOT=""
STRAP=""
SWAP=""
CRYPTTMP=""
CRYPTKEY=""
CRPYTKEYSWAP=""
USB=""
FAT=""
DISKBOOT=""
DISKSWAP=""
DISKROOT=""
DISKLABELBOOT=""
DISKLABELROOT=""
DISKLABELSWAP=""
UUIDBOOT=""
UUIDSWAP=""
UUIDROOT=""
UUIDCRYPTROOT=""
LABELBOOT=""
LABELROOT=""
LABELSWAP=""
LABELCRYPTSWAP=""
LABELCRYPTROOT=""
BOOTTYPE=""
PASS=""
SSH=""
SSHKEY=""
GITHOST=""
POST_SCRIPT=""

ONLY_PKGS=""
SKIP_PKGS=""
SKIP_PKGS_MINIMAL=""
SKIP_PKGS_FULL=""
SKIP_PKGS_KERNEL=""

BINS=(
    debootstrap
    syslinux
)
BIN_DIRS=(
    "/usr/lib/syslinux"
)

PKGS_STRAP_DEB=(
    ca-certificates
    locales
    cryptmount
    cryptsetup
    dosfstools
    btrfs-progs
    e2fsck-static
    exfat-utils
    extlinux syslinux
    sudo
    openssh-server
    readline-common
    console-setup
    systemd
)
PKGS_MINIMAL_DEB=(
    pciutils
    usbutils
    firmware-linux
    firmware-linux-nonfree
    firmware-realtek
    firmware-iwlwifi
    fontconfig
    network-manager
    git
    curl
    rsync
    elinks
    ntp
    acpi-support
)
PKGS_FULL_DEB=(
    wget
    nmap
    tcpdump
    tmux
    rxvt-unicode
    ttf-anonymous-pro
    tightvncserver
    xorg
    xfce4
    font-manager
    firefox-esr
    emacs
    blueman
    xserver-xorg-input-evdev
    xserver-xorg-input-mouse
    claws-mail
    claws-mail-extra-plugins
    mplayer
    xsel
    nm-tray
    adb
    x11vnc
)
PKGS_KERNEL_DEB=(
    linux-image-amd64
    linux-headers-amd64
)

ssh-key() {
    local d dt f

    if (( SSHKEY )); then
        [[ -r ${SSHKEY} ]]
    else
        dt="$(mktemp -d)"
        mount -t ramfs ramfs "${dt}"
        SSHKEY="${dt}/${UUIDROOT}"
        ssh-keygen -f "${SSHKEY}"
        mv "${SSHKEY}" "${SSHKEY}.prv.ssh"
        mv "${SSHKEY}.pub" "${SSHKEY}.pub.ssh"
        SSHKEY="${SSHKEY}.prv.ssh"
        f="${SSHKEY##*/}"
        chmod 400 "${SSHKEY}"
        read -ep "Copy ${SSHKEY//prv/pub} to: " -i "${PWD}/${f//prv/pub}" d
        echo
        [[ -n ${d} ]] && { printf "${SSHKEY//prv/pub} > ${d}\n"; cp "${SSHKEY//prv/pub}" "${d}"; }
        d=""
        read -ep "Copy ${SSHKEY} to: " -i "${PWD}/${f}" d
        echo
        [[ -n ${d} ]] && { printf "${SSHKEY} > ${d}\n"; mkdir -p "${d%/*}" &>/dev/null; cp "${SSHKEY}" "${d}"; }
        SSHKEY="${d}"
    fi
}
crypt-key() {
    local pass2="0" d

    while ! [[ ${PASS} == ${pass2} ]]; do
        read -esp "Crypt: " PASS
        echo
        read -esp "Verify: " pass2
        echo
    done

    if [[ -e ${CRYPTKEY} ]]; then
        [[ -r ${CRYPTKEY} ]]
    else
        CRYPTTMP="$(mktemp -d)"
        CRYPTKEY="$(mktemp -p ${CRYPTTMP}).luks"
        mount -t ramfs ramfs "${CRYPTKEY%/*}"
        dd bs=512 count=4 if=/dev/urandom of="${CRYPTKEY}" iflag=fullblock &>/dev/null
        chmod 400 "${CRYPTKEY}"
        read -ep "Copy ${CRYPTKEY} to: " -i "${PWD}/${CRYPTKEY##*/}" d
        printf "${CRYPTKEY} > ${d}\n"
        [[ -n ${d} ]] && { mkdir -p "${d%/*}" &>/dev/null; cp "${CRYPTKEY}" "${d}"; }
        CRYPTKEY="${d}"
    fi

    d=""
    pass2="0"
    while ! [[ ${PASS} == ${pass2} ]]; do
        read -esp "Crypt Swap: " PASS
        echo
        read -esp "Verify Swap: " pass2
        echo
    done
    if [[ -e ${CRYPTKEYSWAP} ]]; then
        [[ -r ${CRYPTKEYSWAP} ]]
    else
        [[ -d ${CRYPTTMP} ]] || CRYPTTMP="$(mktemp -d)"
        CRYPTKEYSWAP="$(mktemp -p ${CRYPTTMP}).luks"
        mount -t ramfs ramfs "${CRYPTKEYSWAP%/*}"
        dd bs=512 count=4 if=/dev/urandom of="${CRYPTKEYSWAP}" iflag=fullblock &>/dev/null
        chmod 400 "${CRYPTKEYSWAP}"
        read -ep "Copy ${CRYPTKEYSWAP} to: " -i "${PWD}/${CRYPTKEYSWAP##*/}" d
        printf "${CRYPTKEYSWAP} > ${d}\n"
        [[ -n ${d} ]] && { mkdir -p "${d%/*}" &>/dev/null; cp "${CRYPTKEYSWAP}" "${d}"; }
        CRYPTKEYSWAP="${d}"
    fi
}
crypt() {
    if (( SWAP )); then
        printf "Crypt: ${DISKSWAP}:${CRYPTKEYSWAP}\n"
        echo -n "${PASS}" |cryptsetup --use-urandom --batch-mode -y --label="${DISKLABELSWAP}" luksFormat "${DISKSWAP}" 2>/dev/null &&
            echo -n "${PASS}" |cryptsetup luksAddKey "${DISKSWAP}" "${CRYPTKEYSWAP}" &&
            cryptsetup open "${DISKSWAP}" "${DISKSWAP##*/}" --key-file "${CRYPTKEYSWAP}" ||
                { printf "Error: Crypt ${DISKSWAP}"; exit 1; }
    fi
    printf "Crypt: ${DISKROOT}:${CRYPTKEY}\n"
    echo -n "${PASS}" |cryptsetup --batch-mode -y --use-urandom --label="${DISKLABELROOT}" luksFormat "${DISKROOT}" 2>/dev/null &&
        echo -n "${PASS}" |cryptsetup luksAddKey "${DISKROOT}" "${CRYPTKEY}" &&
        cryptsetup open "${DISKROOT}" "${DISKROOT##*/}" --key-file "${CRYPTKEY}" ||
            { printf "Error: crypt ${DISKROOT}"; exit 1; }
}

partition() { # device (/dev/hdX)
    #Type Code = Linux:8300 Swap:8200 EFI:ef00 BIOS:ef02
    # Remove existing files
    printf "Wipe: Remove existing data\n"
    #dd if=/dev/zero of="${1}" bs=1M count=501 status=progress
    wipefs -a "${1}"
    echo
    if (( SWAP )); then
        sgdisk --zap-all --clear \
               --new 1::+1M --typecode=1:ef02 --change-name=1:'bios' \
               --new 2::+500M --typecode=2:ef00 --change-name=2:'efi' \
               --new 3::+4G --typecode=3:8200 --change-name=3:"${DISKLABELSWAP}" \
               --new 4::0 --typecode=4:8300 --change-name=4:"${DISKLABELROOT}" \
               --attributes=2:set:2 \
               "${1}" >/dev/null
    else
        sgdisk --zap-all --clear \
               --new 1::+1M --typecode=1:ef02 --change-name=1:'bios' \
               --new 2::+500M --typecode=2:ef00 --change-name=2:'efi' \
               --new 3::0 --typecode=3:8300 --change-name=3:"${DISKLABELROOT}" \
               --attributes=2:set:2 \
               "${1}" >/dev/null
    fi
    sgdisk --print "${1}"
    partprobe
    echo
    sleep 2
}
format() {
    local dev

    # Boot
    dev=${1}2
    if (( FAT )); then
        printf "Format: ${dev} FAT32\n"
        mkfs.fat -F -F32 -n "${DISKLABELBOOT}" ${dev}
    else
        printf "Format: ${dev} EXT2\n"
        mkfs.ext2 -F -L "${DISKLABELBOOT}" "${dev}"
    fi
    (( ${?} )) && exit 1
    # Swap
    if (( SWAP )); then
        (( CRYPT )) && dev=/dev/mapper/${DISKSWAP##*/} || dev=${1}3
        printf "Format: ${dev} SWAP\n"
        mkswap -L "${DISKLABELSWAP}" "${dev}"
    fi
    # Root
    (( SWAP )) && dev=${1}4 || dev=${1}3
    (( CRYPT )) && dev=/dev/mapper/${DISKROOT##*/}
    printf "Format: ${dev} EXT4\n"
    (( USB )) && mkfs.ext4 -F -L "${DISKLABELROOT}" -O^has_journal "${dev}" || mkfs.ext4 -F -L "${DISKLABELROOT}" "${dev}"
    (( ${?} )) && exit 1
}
mount-parts() { # MOUNTROOT MOUNTBOOT
    local disk

    (( CRYPT )) && disk=/dev/mapper/${DISKROOT##*/} || disk=${DISKROOT}
    mkdir -p ${1}
    mount ${disk} ${1}
    mkdir -p ${2}
    mount ${DISKBOOT} ${2}
}
mkimg() { # directory block-device
    local ans

    [[ -b ${1} ]] || { printf "Error: ${2} is not a block device"; exit 1; }

    printf "Create: ${1} > ${2}\n"
    [[ -e ${2} ]] && read -ep "${2} exists, overwrite? (y) " ans
    [[ -z ${ans} || ${ans} == y ]] && dd if="${1}" of="${2}" status=progress
}
uuid() { # DISK
    (( SWAP )) && UUIDSWAP="$(blkid -s UUID -o value ${DISKSWAP})" LABELSWAP="$(blkid -s LABEL -o value ${DISKSWAP})"

    BOOTTYPE="$(blkid -s TYPE -o value ${DISKBOOT})"
    UUIDBOOT="$(blkid -s UUID -o value ${DISKBOOT})"
    LABELBOOT="$(blkid -s LABEL -o value ${DISKBOOT})"

    UUIDROOT="$(blkid -s UUID -o value ${DISKROOT})"
    LABELROOT="$(blkid -s LABEL -o value ${DISKROOT})"
    UUIDCRYPTROOT="$(blkid -s UUID -o value /dev/mapper/${DISKROOT##*/})"
    LABELCRYPTROOT="$(blkid -s LABEL -o value /dev/mapper/${DISKROOT##*/})"
}

boot-syslinux() { #MNTBOOT
    local memdisk syslinux boot=${1} efi=${1}/EFI/BOOT dev
    local boots=${boot}/syslinux
    local dev=$(findmnt -n -o SOURCE --target ${1})
    local syslinux="/usr/lib/syslinux"
    local memdisk="${syslinux}/memdisk"
    local cryptparm="" swapparm="" rootparm=""

    printf "Installing Syslinux\n"

    # cryptdevice=/dev/sdX#:root
    # cryptkey=/dev/sdX#:fstype:path
    # resume=/dev/mapper/sdX#
    # root=/dev/mapper/sdX#
    #cryptparm="cryptdevice=UUID=${UUIDROOT}:${DISKLABELROOT} cryptkey=UUID=${UUIDBOOT}:${BOOTTYPE}:${MNTBOOT#${MNTPRFX}}/${CRYPTKEY}"
    (( CRYPT )) && cryptparm="cryptdevice=UUID=${UUIDROOT}:${DISKLABELROOT}" rootparm="rw root=UUID=${UUIDCRYPTROOT}"
    (( SWAP )) && swapparm="resume=UUID=${UUIDSWAP}"

    mkdir -p ${boots}/32 || { printf "Error: Create directory ${boots}\n"; exit 1; }
    mkdir -p ${efi}/{32,64}

    [[ -d ${syslinux} ]] || { printf "Error: Syslinux %s not found" "${syslinux}"; exit 1; }
    [[ -e ${memdisk} ]] || { printf "Warning: Syslinux %s not found" "${memdisk}"; }

    cp ${memdisk} ${boots}/32/
    cp ${memdisk} ${efi}/

    cp ${syslinux}/modules/bios/*.c32 ${boots}/32/
    cp ${syslinux}/modules/efi32/* ${efi}/32/
    cp ${syslinux}/modules/efi64/* ${efi}/64/
    cp ${efi}/32/ldlinux.e32 ${efi}/BOOTIA32.EFI
    cp ${efi}/64/ldlinux.e64 ${efi}/BOOTX64.EFI

    cat <<EOF >${boots}/syslinux.cfg
PROMPT 0
TIMEOUT 1
PATH ./32
PATH ./64

UI vesamenu.c32

MENU TITLE Boot Menu
#MENU BACKGROUND splash.png
DEFAULT linux

LABEL ${DISTRO}
MENU LABEL ${DISTRO}
LINUX ../vmlinuz
APPEND ${cryptparm} ${swapparm} ${rootparm}
INITRD ../${DISTRO}

LABEL hdt
MENU LABEL HDT
COM32 hdt.32

LABEL memdisk
MENU LABEL Memdisk
LINUX memdisk

LABEL reboot
MENU LABEL Reboot
COM32 reboot.c32

LABEL poweroff
MENU LABEL Poweroff
COM32 poweroff.c32

LABEL disk
MENU LABEL Disk
COM32 disk.c32
APPEND hd0 0

LABEL win
MENU LABEL Windows
COM32 chain.c32
APPEND label=win ntldr=/bootmgr

EOF
    cp ${boots}/syslinux.cfg ${efi}
    extlinux --install ${boot} >/dev/null

    dd bs=440 count=1 conv=notrunc if=${syslinux}/mbr/gptmbr.bin of=${DISK} 2>/dev/null
}
boot-grub() {
    grub-install --target=i386-pc --verbose /dev/sda
    grub-install --target=x86_64-efi --efi-directory=boot --bootloader-id=GRUB --verbose /dev/sda
    grub-mkconfig -o /boot/grub/grub.cfg
}

copy-root() { # STRAP {root@1.1.1.1:/*} MOUNTROOT
    printf "Rsync: ${1} > ${2}/\n"
    stty -echo -icanon time 0 min 0
    rsync -aHx --numeric-ids --no-inc-recursive --info=progress2 "${1}" "${2}"/ \
          --exclude=/dev --exclude=/proc --exclude=/sys --exclude=/tmp --exclude=/mnt --exclude=/media --exclude=/run --exclude=/home \
          --exclude=/boot --exclude=/etc/locale.gen --exclude=/etc/apt/sources.list
    mkdir -p ${2}/run &>/dev/null
    ln -s /run /var/run &>/dev/null
    while read _; do _=''; done
    stty sane
    mkdir ${2}/tmp
    chmod 777 ${2}/tmp
}
chroot-bind() {
    local d

    for d in sys dev proc; do
        mkdir -p ${1}/${d}
        mount --bind /${d} ${1}/${d}
    done
}
chroot-init() { # MNTROOT
    local user="" pass="0" passr="" passu="" hostname="" crypttab timezone="" locale=""

    (( CRYPT )) && {
        printf "Crypt Swap Key: ${MNTROOT}/.${CRYPTKEYSWAP##*/}\n"
        cp "${CRYPTKEYSWAP}" "${MNTROOT}/.${CRYPTKEYSWAP##*/}"
        printf "Crypt Key: ${MNTROOT}/.${CRYPTKEY##*/}\n"
        cp "${CRYPTKEY}" "${MNTROOT}/.${CRYPTKEY##*/}"
        rm -f "${CRYPTKEY}"
    }
    (( SSH )) && {
        ssh-key
        printf "SSH Key: ${MNTROOT}/.${SSHKEY##*/}\n"
        cp "${SSHKEY}" "${MNTROOT}/.${SSHKEY##*/}"
        rm -f "${SSHKEY}"
    }

    while ! [[ ${pass} == ${passr} ]]; do
        read -esp "Root Pass: " pass
        echo
        read -esp "Verify: " passr
        echo
    done
    #read -ep "User: " user
    #while ! [[ ${pass} == ${passu} ]]; do
    #    read -esp "${user} Pass: " pass
    #    read -esp "Verify: " passu
    #done
    read -ep "Timezone: " -i "America/Chicago" timezone
    read -ep "Hostname: " hostname
    printf "en_US ISO-8859-1\nen_US.ISO-8895-15 ISO-8859-15\nen_US.UTF-8 UTF-8\n" >${1}/etc/locale.gen
    printf "LANG=en_US.UTF-8\n" >${1}/etc/default/locale
    printf "${hostname}" >${1}/etc/hostname
    printf "LC_ALL=en_US.UTF-8" >${1}/etc/enviornment

    # crypttab: <target> <source> <key> <options>
    # fstab:
    >/etc/fstab
    >/etc/crypttab
    echo "UUID=${UUIDBOOT} /boot ${BOOTTYPE} noatime,defaults,ro 0 0" >>${1}/etc/fstab
    if (( CRYPT )); then
        (( SWAP )) && echo "${DISKLABELSWAP} UUID=${UUIDSWAP} /.${CRYPTKEYSWAP##*/} luks,swap" >>${1}/etc/crypttab
        echo "${DISKLABELROOT} UUID=${UUIDROOT} /.${CRYPTKEYSWAP##*/} luks" >>${1}/etc/crypttab
        (( SWAP )) && echo "${DISKSWAP#**/} UUID=${UUIDSWAP} /.${CRYPTKEYSWAP##*/} luks,swap" >>${1}/etc/crypttab
        echo "${DISKROOT##*/} UUID=${UUIDROOT} /.${CRYPTKEYSWAP##*/} luks" >>${1}/etc/crypttab
        (( SWAP )) && echo "/dev/mapper/${DISKLABELSWAP} none swap sw 0 0" >>${1}/etc/fstab
        echo "UUID=${UUIDCRYPTROOT} / auto errors=remount-ro,noatime,defaults 0 1" >>${1}/etc/fstab
        echo "KEYFILE_PATTERN=\"/.*.luks\"" >>${1}/etc/cryptsetup-initramfs/conf-hook
    else
        (( SWAP )) && echo "UUID=${UUIDSWAP} swap swap default 0 0" >>${1}/etc/fstab
        echo "UUID=${UUIDROOT} / ext4 errors=remount-ro,noatime,defaults 0 1" >>${1}/etc/fstab
    fi
    echo "UMASK=0077" >>${1}/etc/initramfs-tools/initramfs.conf

    # SSHD
    printf "HostKey ${SSHKEY}\nPermitRootLogin no\nPasswordAuthentication no\n" >>${1}/etc/ssh/sshd_config

    cat <<EOF |chroot ${1}
ln -sf /usr/share/zoneinfo/$timezone /etc/localtime
locale-gen
echo -n "root:${passr}"|chpasswd
#useradd -m -G sudo -s /bin/bash "${user}"
#echo "${user}:${passu}"|chpasswd

EOF

    rm -rf ${1}/usr/share/man 2>/dev/null
}
clean() {
    local d

    stty sane

    # Key temps
    [[ -e ${CRYPTTMP} ]] && {
        rm -rf "${CRYPTTMP}/*"
        [[ -d ${CRYPTTMP} ]] && umount "${CRYPTTMP}"
        rm -rf "${CRYPTTMP}"
    } 2>/dev/null
    [[ -e ${SSHKEY} ]] && {
        rm -f "${SSHKEY}"
        [[ -d ${SSHKEY%/*} ]] && umount "${SSHKEY%/*}"
        rm -rf "${SSHKEY#/*}"
    } 2>/dev/null

    while grep ${MNTROOT} /proc/mounts &>/dev/null; do
        for d in $(grep ${MNTROOT} /proc/mounts|cut -d' ' -f2); do
            umount -Rf ${d} &>/dev/null 2>/dev/null
        done
    done

    for d in /dev/mapper/*; do
        cryptsetup close "${d}" 2>/dev/null
    done
    [[ -n ${IMAGE} ]] && losetup -d ${DISK}
}

debian-strap() { # STRAP
    mkdir -p "${1}"
    printf "Debian: ${1}\n"
    while (( $? )); do
        debootstrap --verbose --include="$(echo ${PKGS_STRAP_DEB[@]}|tr ' ' ',')" stable "${1}" || sleep 5
    done
}
debian-sources() { # MNTROOT
    cat <<EOF >${1}/etc/apt/sources.list
deb https://deb.debian.org/debian stable main contrib non-free
deb-src https://deb.debian.org/debian stable main contrib non-free

deb https://deb.debian.org/debian-security stable/updates main contrib non-free
deb-src https://deb.debian.org/debian-security stable/updates main contrib non-free

# deb https://deb.debian.org/debian testing main
# deb https://security.debian.org/debian-security testing/updates main
# deb http://security.debian.org/debian-security stretch/updates main
# deb https://deb.debian.org/debian unstable non-free contrib main

EOF
}
debian-pkgs() { # MNTROOT MNTBOOT
    local f

    cat <<EOF |chroot ${1}
dpkg --configure -a
apt-get update

EOF
    (( SKIP_PKGS_MINIMAL )) || cat <<EOF |chroot ${1}
apt-get -y install ${PKGS_MINIMAL_DEB[@]}

EOF
    (( SKIP_PKGS_FULL )) || cat <<EOF |chroot ${1}
apt-get -y install ${PKGS_FULL_DEB[@]}

EOF
    if ! (( SKIP_PKGS_KERNEL )); then
        images=( $(apt-cache search linux-image-.*-amd64$|cut -d' ' -f1) )
        select image in ${images[@]}; do (( ${#image[@]} )) && break; done
        cat <<EOF |chroot ${1} /bin/bash
headers=${image//image/headers}
apt-get -y install --reinstall ${PKGS_KERNEL_DEB[@]} ${image} ${headers} && update-initramfs -cv -k all
apt-get -y dist-upgrade
apt autoremove
apt-get clean

EOF
    fi

    f=( ${1}/boot/vmlinuz-* )
    [[ -e ${f[0]} ]] && {
        printf "Kernel: ${f[0]} > ${2}/vmlinuz \n"
        cp ${f[0]} ${2}/vmlinuz 2>/dev/null || { printf "Error: Kernel not found\n"; exit 1; }
    }
    f=( ${1}/boot/initrd.img-* )
    [[ -e ${f[0]} ]] && {
        printf "Initrd: ${f[0]} > ${2}/debian \n"
        cp ${f[0]} ${2}/${DISTRO} 2>/dev/null || { printf "Error: Initramfs not found\n"; exit 1; }
    }
}
debian-conf() {
    cat <<EOF |chroot ${1}

EOF
}

arch-pkgs() {
    # /etc/pacman.d/mirrorlist
    # server; development; desktop
    pacstrap /mnt base efibootmgr grub sudo rxvt-unicode git tmux emacs fvwm xorg elinks wpa_supplicant firefox openssh tigervnc xsel alsa-utils alsa-tools mplayer feh evince sysstat x11vnc qemu p7zip connman cmst
    # NetworkMager lxqt sddm gimp libreoffice
    # (AUR) wpa_cute tkpacman

    mkinitcpio -p linux
}

network() {
    local rtn

    while ! [[ -z ${rtn} && $(ping 8.8.8.8 -c 1) ]]; do
        read -e -s -n1 -p "Error: Network Connection. Retry? (non-blank)" rtn
    done
}
sanity() {
    local e missing=()

    for e in ${BINS[@]}; do
        [[ -n $(type -p ${e}) ]] || missing+=( "${e}" )
    done

    (( ${#missing[@]} )) && { printf "Error: Missing %s" "${missing[@]}"; exit 1; }

    missing=()
    e=""
    for e in ${BIN_DIRS[@]}; do
        [[ -d ${e} ]] || missing+=( "${e}" )
    done
    (( ${#missing[@]} )) && { printf "Error: Missing " "${missing[@]}"; exit 1; }
}
args() {
    local a
    local help="
Parameters
<block-device> (strap-path) (mount-path) (arguments)
Use existing contents of strap or receive strap. Optional mount-path

Init Sets
* Timezone
* Hostname
* DDNS Url (Cron)
* Root Password
- Locale (en_US.UTF-8)
- fstab (boot,swap,root)
- crypttab
- Configure SSHD
- Copy Crypt Key
- Copy SSH Key

Distributions
- Debian|apt

Arguments
-x|--crypt) dm-crypt/LUKS, default generate Keyfile.
-z|--swap) Install swap partition.
--key <path>) Keyfile.
--swap-key <path>) Swap Keyfile.
-I|--mkimg <directory> <block-device>) DD directory to block-device.
-P|--packages) Skip to package install.
-S=|--skip-packages=minimal,full,kernel) Skip listed package install or all.
--ssh <key>) Enable ssh with key or generate one.
-d=|--distro=<distro>) Install Distribution, default Debian.
--script=) Post install script. Pass <Root Mount> <Boot Mount> <Device>.
-s|--strap <dir>) Strap path.
"
    while (( ${#} )); do
        case "$1" in
            -h|--help) printf "${help}"; exit ;;
            -d=*|--distro=*)
                if [[ ${1} == *=* ]]; then
                    case ${a#*=} in
                        [dD]eb*|apt) DISTRO='Debian' ;;
                        pkg|[aA]rch) DISTRO='Arch' ;;
                        *) printf "Error: Unsupported distribution."; exit 1 ;;
                    esac
                else
                    printf "Error: Distribution required"; exit 1
                fi
                shift
                ;;
            -x|--crypt) CRYPT=1; shift ;;
            -z|--swap) SWAP=1; shift ;;
            -s|--strap)
                if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
                    STRAPDIR="${2}"
                    shift 2
                else
                    echo "Error: Argument for ${1} is missing" >&2
                    exit 1
                fi
                ;;
            --key) [[ -n ${2} && ${2:0:1} != "-" ]] && { CRYPTKEY="${2}"; shift 2; } || shift ;;
            --swap-key) [[ -n ${2} && ${2:0:1} != "-" ]] && { CRYPTKEYSWAP="${2}"; shift 2; } || shift ;;
            --ssh) SSH=1; [[ -n ${2} && ${2:0:1} != "-" ]] && { SSHKEY="${2}"; shift; } || shift ;;
            --fat) FAT=1; shift ;;
            --usb) USB=1; shift ;;
            -I|--mkimg) mkimg ${PARAMS}; exit ;;
            #-m|--minimal) MINIMAL=1; shift ;;
            -P|--packages) ONLY_PKGS=1; shift ;;
            -S=?*|--skip-packages=?*)
                if [[ ${1} == *=* ]]; then
                    a=${1#*=}
                    for a in ${a//,/ }; do
                        case ${a} in
                            min*) SKIP_PKGS_MINIMAL=1 ;;
                            full) SKIP_PKGS_FULL=1 ;;
                            kern*) SKIP_PKGS_KERNEL=1 ;;
                        esac
                    done
                else
                    SKIP_PKGS=1
                fi
                shift
                ;;
            --script=*) [[ ${1} == *=* ]] && POST_SCRIPT=${1#*=}; shift ;;
            -*|--*=) # unsupported flags
            echo "Error: Unsupported flag ${1}" >&2
            printf "${help}"
            exit 1
            ;;
            *) # preserve positional arguments
                PARAMS="${PARAMS} ${1}"
                shift
                ;;
        esac
    done # set positional arguments in their proper place
}

(( $(id -u) )) && { printf "Root Permissions required"; exit 1; }

args ${@}
eval set -- "${PARAMS}"
(( ${#} > 0 )) || { printf "Error: Too few arguments"; args -h; exit 1; }
[[ -f ${1} ]] && DISK=$(losetup --partscan --show --find ${1}) || DISK=${1}
DISTRO=${DISTRO:-Debian}
MNTPRFX="/mnt"
MNTROOT=${MNTPRFX}/${3:-${DISK##*/}}
MNTBOOT=${MNTROOT}/boot
STRAP=${STRAPDIR:-${MNTROOT}}
[[ ${STRAP} =~ .*/ ]] || STARP="${STRAP}/"
[[ ${DISK} =~ *(mmcblk|loop)* ]] && DISK=${DISK}p
DISKBOOT="${DISK}2"
(( SWAP )) && DISKSWAP="${DISK}3" DISKROOT="${DISK}4" || DISKROOT="${DISK}3"
DISKLABELBOOT="boot"
DISKLABELSWAP="swap"
DISKLABELROOT="root"
LABELBOOT="${DISKLABELBOOT}"
LABELSWAP="${DISKLABELSWAP}"
LABELROOT="${DISKLABELROOT}"
LABELCRYPTROOT="${DISKLABELROOT}"
LABELCRYPTSWAP="${DISKLABELSWAP}"

[[ -b ${DISK} ]] || { printf "Error: ${DISK} is not a block device"; exit 1; }
[[ -d ${STRAP} ]] && { [[ -e $(type -p rsync) ]] || { printf "Error: rsync missing"; exit 1; }; }

trap -- 'clean; exit;' EXIT
#trap -- 'clean; exit;' SIGINT
trap -- 'clean; exit;' SIGQUIT
trap -- 'clean; exit;' SIGTERM

clean

# Jump Operation
if [[ ${ONLY_PKGS} ]]; then
    uuid ${DISK}
    mount-parts ${MNTROOT} ${MNTBOOT}

    network

    debian-sources ${MNTROOT}
    debian-pkgs ${MNTROOT} ${MNTBOOT}

    pkgs-config ${MNTROOT}

    exit
fi

# Normal Operation
# Check binaries. Prepare Disk. Partition. Encrypt. Format. Mount. Identify
sanity
partition ${DISK}
clean
if (( CRYPT )); then
    crypt-key || { printf "Error: Key"; exit 1; }
    crypt || { printf "Error: Crypt"; exit 1; }
fi
format ${DISK}
mount-parts ${MNTROOT} ${MNTBOOT}
uuid ${DISK}

# Bootloader
boot-syslinux ${MNTBOOT}

network
# Bootstrap
case ${DISTRO} in
    Debian) debian-strap ${MNTROOT} || { printf "Unable to create strap-path"; exit 1; } ;;
esac

[[ ${MNTROOT} == ${STRAP} ]] || copy-root ${STRAP} ${MNTROOT}
chroot-bind ${MNTROOT}

# Configure
chroot-init ${MNTROOT}

(( SKIP_PKGS )) || {
    debian-sources ${MNTROOT}
    debian-pkgs ${MNTROOT} ${MNTBOOT}
}
[[ -e ${POST_SCRIPT} ]] && ${DISTRO} ${POST_SCRIPT} ${MNTROOT} ${MNTBOOT} ${DISK}

exit
# systemd-nspawn -bD /mnt/root
