#!/bin/bash
# DISTRO MNTROOT MNTBOOT DISK
set -x

DISTRO="${1}"
MNTROOT="${2}"
MNTBOOT="${3}"
DISK="${4}"

HOST=""

PKGS=(
    wget
    nmap
    tcpdump
    tmux
    rxvt-unicode
    ttf-anonymous-pro
    tightvncserver
    xorg
    xfce4
    font-manager
    firefox-esr
    emacs
    blueman
    xserver-xorg-input-evdev
    xserver-xorg-input-mouse
    claws-mail
    claws-mail-extra-plugins
    mplayer
    xsel
    nm-tray
    adb
    x11vnc
)

clone() { # MNTROOT
    local dir optdir

    dir="${HOST##*/}"
    dir="${dir%.git}"
    optdir="${1}opt/${dir}"

    if [[ -e ${optdir} ]]; then
        git -C ${optdir} reset --hard origin/master
        git -C ${optdir} clean -f -d
        git -C ${optdir} pull
    else
        git clone ${HOST} ${optdir} || return
        chmod 700 ${optdir}
    fi
    [[ -f ${optdir}/install.sh ]] || return
    chroot ${1} /bin/bash -c "cd /opt/${dir}; /opt/${dir}/install.sh"
}

pkgs() {
    cat <<EOF |chroot ${1}
apt-get -y install ${PKGS[@]}
apt-get -y dist-upgrade
apt autoremove
apt-get clean

EOF
}


eval set -- "${MNTROOT%/}/"

[[ -d ${MNTROOT} ]] || { printf "Error: ${MNTROOT} is not a directory.\n"; exit 1; }

read -ep "Git Url: " HOST
[[ -n ${HOST}  ]] && clone ${MNTROOT}


(( ${#PKGS[@]} )) &&
    case ${DISTRO} in
        Debian) pkgs ${MNTROOT} ;;
    esac
