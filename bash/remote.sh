#!/bin/bash

vnc-install() {
    cat <<EOF >/etc/X11/xorg.conf.d/10-vnc.conf
Section "Module"
Locad "vnc"
EndSection

Section "Screen"
Identifier "Screen0"
EndSection

EOF
}

vnc-control() {
    x0vncserver ${@}
}