#!/bin/env bash
source ~/.profile

logger -t "${UID}" -- "${1}"

${@}
