alsa_default() {
    {
        amixer -c 0 sset 'Master' on
        amixer -c 0 sset 'Master' 70%
        amixer -c 0 sset 'Headphone' on
        amixer -c 0 sset 'Headphone' 95%
        amixer -c 0 sset 'Speaker' off
        amixer -c 0 sset 'Speaker' 0%
        amixer -c 0 sset 'PCM' 95%
    } >/dev/null
}
