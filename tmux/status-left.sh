#!/bin/bash

mem=($(free -m|grep "Mem:"))

printf "%s@%s    [%s] {%s} <%s>" \
       "${USER}" "${HOSTNAME}" \
       "$(cat /proc/loadavg)" \
       "${mem[2]}+${mem[3]}" \
       "$(ping -c 2 8.8.8.8|tail -1|cut -d'/' -f5)"
