#!/usr/bin/sh
#|
exec csi -setup-mode -s "$0" "$@"
|#

(import scheme chicken)
(use data-structures posix files utils extras)
(use srfi-1)

(define null-dir "Non-Existent Directory")
(define sysfs-pw (directory-exists? "/sys/class/power_supply"))
(define sysfs-hwmon (directory-exists? "/sys/class/hwmon"))
(define proc-load (file-exists? "/proc/loadavg"))

(define (input x)
  (handle-exceptions exn
      #f
    (and (not (null? x))
         (file-exists? x)
         (file-read-access? x)
         (read-lines x))))

(define (battery)
  ;; power_supply.h: voltage (uV), current (uA), charge (uAh), energy (uWh), time (seconds), temperature (1/10DCelsius)
  (let ((ds (glob (conc sysfs-pw "/"))))
    (map (lambda (d) ;;/sys/class/power_supply/(AC0|BAT0)
           (map (lambda (l) ;;<(AC0|BAT0)/uevent
                  (let ((s (string-split l "=")))
                    (define-syntax setv
                      (syntax-rules ()
                        ((_ k)
                         (set! k (cadr s)))))
                    (case (string->symbol (car s))
                      ((POWER_SUPPLY_NAME) (setv name))
                      ((POWER_SUPPLY_ONLINE) (setv online))
                      ((POWER_SUPPLY_STATUS) (setv status))
                      ((POWER_SUPPLY_VOLTAGE_MIN_DESIGN) (setv voltage-design))
                      ((POWER_SUPPLY_VOLTAGE_NOW) (setv voltage-now))
                      ((POWER_SUPPLY_CURRENT_NOW) (setv current-now))
                      ((POWER_SUPPLY_POWER_NOW) (setv current-now))
                      ((POWER_SUPPLY_CHARGE_FULL_DESIGN) (setv charge-design))
                      ((POWER_SUPPLY_CHARGE_FULL) (setv charge-full))
                      ((POWER_SUPPLY_ENERGY_FULL_DESIGN) (setv charge-design))
                      ((POWER_SUPPLY_ENERGY_FULL) (setv charge-full))
                      ((POWER_SUPPLY_CHARGE_NOW) (setv charge-now))
                      ((POWER_SUPPLY_CAPACITY) (setv capacity))
                      ((POWER_SUPPLY_MODEL_NAME) (setv model-name))
                      ((POWER_SUPPLY_MANUFACTURER) (setv manufacturer)))))
                (let ((f (make-pathname d "uevent")))
                  (input f)))
           (cond
            ((irregex-search (string->sre "^AC.*") name)
             (case (string->number online)
               ((1) (list name))
               (else '())))
            ((irregex-search (string->sre "^BAT.*") name)
             (list name
                   (string->symbol status)
                   capacity
                   (/ (round (* (/ (string->number charge-full)
                                   (string->number charge-design)) 10000)) 100)
                   (/ (string->number voltage-now) 1000000)
                   (/ (string->number current-now) 1000000.)
                   ))))
         ds)))
(define (hwmon)
  ;; temp*_input, temp*_label, fan*_input
  (map (lambda (d) ;;/sys/class/hwmon/hwmon#
         (map (lambda (f) ;;/sys/class/hwmon/hwmon#/(fan,temp)#_(label,input)
                (map (lambda (f)
                       (let* ((m (irregex-search (string->sre ".*/[^0-9]*([0-9]*)_.*") f))
                              (i (input f)))
                         (if (and i (> (string-length (car i)) 4))
                             (/ (string->number (car i)) 1000)
                             '())))
                     f))
              (list (glob (conc d "/temp[0-9]*_input"))
                    (glob (conc d "/fan[0-9]*_input")))))
       (glob (conc sysfs-hwmon "/hwmon[0-9]*"))))
(define (network-addr)
  ;; (device ip bcast)
  (let ((cmd-route "ip route"))
    (map (lambda (m)
           (if (irregex-match-data? m)
               (list
                (irregex-match-substring m 2)
                (irregex-match-substring m 3)
                (irregex-match-substring m 1))
               '()))
         (map (lambda (x)
                (irregex-search (string->sre "^default[^[:numeric:]]*([[:numeric:]\\.]*) dev (\\w*) src ([[:numeric:]\\.]*).*") x))
              (call-with-input-pipe cmd-route read-lines)))))
(define (network-conn)
  ;; (listen established)
  ;;(let ((net-listen "sudo lsof -i |grep -ie '\\(listen\\|udp\\)' |wc -l")
  ;;      (net-established "sudo lsof -i |grep -vie '\\(pid\\|listen\\|udp\\)' |wc -l"))
  (let ((net-listen "ss -l -f inet|wc -l")
        (net-established "ss -f inet state connected|wc -l"))
    (list (car (call-with-input-pipe net-listen read-lines))
          (car (call-with-input-pipe net-established read-lines)))))
(define (loadavg)
  (let ((f (input proc-load)))
    (butlast (string-split (car f)))))
(define (mem)
  ;;total, used, free, shared, buff/cache, available
  (let* ((mem "free -m|grep -i mem")
         (l (cdr (string-split (car (call-with-input-pipe mem read-lines))))))
    (list (caddr l) (car (cddddr l)) (cadr l))))
(define (mail)
  (let ((cmd-imap (conc "curl --silent --url 'imaps://imap.gmail.com' "
                        "--user " (conc (get-environment-variable "AUTH_GMAIL_USER")
                                        ":"
                                        (get-environment-variable "AUTH_GMAIL_PASS"))
                        " --request 'EXAMINE INBOX'"
                        )))
    (remove (lambda (x) (not x))
            (map (lambda (m)
                   (if (irregex-match-data? m)
                       (irregex-match-substring m 1)
                       '#f))
                 (map (lambda (x)
                        (irregex-search (string->sre "^\\* ([[:digit:]]*) EXISTS$") x))
                      (call-with-input-pipe cmd-imap read-lines))))))

(define c-sep "#[fg=colour7]")
(define c-sep2 "#[fg=colour6]")
(define c-dev "#[fg=colour2]")
(define c-active "#[fg=colour10]")
(define c-inactive "#[fg=colour3]")
(define c-disable "#[fg=colour9]")
(define c-open "#[fg=colour14]")

(print* (string-translate
         (conc
          ;; Battery
          (let ((v (intersperse (remove null? (battery))
                                `(,(conc c-sep "|")))))
            ;; name.status.%.capacity.<.charge-less.@.voltage.V.current.A
            (if (not (null? v))
                (conc
                 c-sep "["
                 (string-intersperse
                  (map (lambda (b)
                         (if (> (length b) 1)
                             (conc
                              c-dev (car b)
                              (case (cadr b)
                                ((Discharging) (conc c-disable "-"))
                                ((Charging) (conc c-active "+"))
                                ((Full) (conc c-inactive "!")))
                              c-sep2 "%"
                              (let ((v (string->number (caddr b))))
                                (conc (cond
                                       ((> v 80) c-active)
                                       ((< v 20) c-disable)
                                       (else c-inactive))
                                      v))
                              c-sep2 "<"
                              c-inactive (cadddr b)
                              c-sep2 "@"
                              c-inactive (cadr (cdddr b))
                              c-sep2 "V"
                              c-inactive (caddr (cdddr b))
                              c-sep2 "A")
                             (conc
                              c-dev (car b))))
                       v) "")
                 c-sep "]")
                ""))
          ;; Tempurature|Fan
          (let ((v (flatten (hwmon))))
            (if v
                (conc
                 c-sep "("
                 c-dev (string-intersperse
                        (map (lambda (e)
                               (conc
                                (if (number? e)
                                    (if (> e 70)
                                        c-disable
                                        c-inactive)
                                    "")
                                e))
                             v)
                        (conc c-sep "|" c-dev))
                 c-sep ")")
                ""))
          ;; LoadAvg
          (let ((v (loadavg)))
            (if v
                (conc c-sep "{"
                      c-inactive (car v)
                      c-sep "_"
                      c-inactive (cadr v)
                      c-sep "_"
                      c-inactive (caddr v)
                      c-sep2 "+"
                      c-active (cadddr v)
                      c-sep "}")
                ""))
          ;; Memory
          (let ((v (mem)))
            (if v
                (conc
                 c-sep "{"
                 c-active (car v)
                 c-sep "-"
                 c-inactive (cadr v)
                 c-sep2 "+"
                 c-inactive (caddr v)
                 c-sep "}")
                ""))
          ;; Network-Interface
          (if (not (null? (network-addr)))
          (let ((v (remove null? (network-addr))))
            (if (not (null? v))
                (conc
                 c-sep "<"
                 (string-intersperse
                  (map (lambda (e)
                         (if e
                             (conc
                              c-dev (car e)
                              c-sep ":"
                              c-active (cadr e)
                              c-sep "/"
                              c-inactive (caddr e))
                             ""))
                       v)
                  (if (> (length v) 1)
                      "|"
                      ""))
                 c-sep ">")
                "")
          ;; Network-TCP
          (let ((v (network-conn)))
            (if v
                (conc
                 c-sep "{"
                 c-open
                 (car v)
                 c-sep "/"
                 c-inactive
                 (cadr v)
                 c-sep "}")
                ""))
          ;; Mail
          (let ((v (mail)))
            (if v
                (conc
                 c-sep "##"
                 c-open (car v))
                "")))
          ""))
         " "))
