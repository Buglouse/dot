export CONF_TMUX="${HOME}/.tmux/tmux.conf"
export TMUX_SCRIPT="tmux_script.scm"
export SOCK_TMUX="${DIR_SOCK}/tmux.sock"
export TMUX_SCRIPT="${HOME}/.local/bin/tmux-status"

tmux() {
    local cmd="$(type -P tmux)" i
    if [[ -S ${SOCK_TMUX} && $(ss |grep tmux) ]]; then
        ${cmd} -S "${SOCK_TMUX}" ${@:-attach-session}
    else
        ${cmd} -S "${SOCK_TMUX}" -f "${CONF_TMUX}" ${@}
    fi
}
