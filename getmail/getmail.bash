getmail() {
    local rc files=( ${UCS_INSTALL}/.getmail/getmail.* )

    mkdir -p ${UCS_INSTALL}/.mail/{gmail,cn}/{cur,new,tmp} &>/dev/null

    for file in ${files[@]}; do
        rc+=" --rcfile ${file}"
    done
    $(type -P getmail) ${rc[@]} "${@}"
}
