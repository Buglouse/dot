# Pre
echo 1 > pass
truncate --size 2g /tmp/disk
mount loop
LOOP_DEVICE="$(losetup --show -f /tmp/disk)"

# Build
image-bootstrap --hostname one arch --password pass "${LOOP_DEVICE}"
convert -p -f raw -O qcow2 "${LOOP_DEVICE}" ./arch-$(date -I).qcow2
losetup -d "${LOOP_DEVICE}"

# Post
# https://github.com/PacBSD/mkarchiso/raw/master/files/pacstrap
git clone https://git.archlinux.org/arch-install-scripts.git
cd /root
git clone https://bitbucket.org/Rocklyn/dot.git
git remote add bdot https://bitbucket.org/Buglouse/dot.git
git fetch bdot master
git checkout bdot/master -- bash readline ssh git
git pull bdot master --allow-unrelated-histories -f

ucs bash readline git ssh auth
cp -f ~/.ssh/ssh.conf /etc/ssh/ssh_conf
cp -f ~/.ssh/authorized_keys /etc/ssh/

#git clone https://bitbucket.org/Buglouse/identity.git

git clone https://bitbucket.org/Buglouse/ucs.git
./ucs/run.sh
.local/bin/run.sh bash readline

git checkout bdot/master -- ssh

ucs readline bash ssh

# Packages
make
m4

# Container
pacstrap -icMGd <path> <name> git --ignore linux --ignore linux-firmware --ignore nano --ignore man-db --ignore man-pages --ignore licenses
systemd-nspawn -b -D /path
OR
systemd-nspawn -UD name
pacman-key --init
pacman-key --populate
passwd
logout
systemd-nspawn -bUD name
