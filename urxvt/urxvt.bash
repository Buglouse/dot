export DIR_PERL="${HOME}/.perl"
export RXVT_SOCKET="${DIR_SOCK}/urxvt.sock"
export SOCK_URXVT="${RXVT_SOCKET}"

urxvt() {
    #--perl-lib "${DIR_PERL}/urxvt" "${@}"
    [[ -s ${SOCK_URXVT} ]] || urxvtd -q -f --opendisplay &>/dev/null
    urxvtc "${@}"
}
