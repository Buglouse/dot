;; Single semicolon represent column-wall.
;; Double semicolon provides inline-documentation or comments function block.
;; Triple semicolon identifies heading or comments inline-code (w/ two space).
;; Quadruple semicolon denotes heading.

;; βuglouse

;;(require 'benchmark-init)

(progn ;; Debug
  (setq debug-on-error 't
        debug-on-event 't
        debug-on-signal nil
        debug-on-quit nil
        ;;debug-ignored-errors nil
        stack-trace-on-error nil
        ;;load-suffixes '(".el" ".elc") ;;'(".elc" ".el")
        )
  )
(progn ;; Builtin
  ;; buffer-local
  (setq-default indicate-empty-lines t
                case-fold-search t
                cursor-in-non-selected-windows nil
                scroll-up-aggressively .5
                scroll-down-aggressively .5
                buffer-file-coding-system 'utf-8
                cursor-type 'hbar
                truncate-lines nil
                fill-column 80
                window-system nil
                indent-tabs-mode nil
                tab-width 4
                frame-title-format '("")
                load-prefer-newer nil
                )
  
  (setq auto-save-interval 1000
        force-load-messages nil
        default-process-coding-system '(utf-8 . utf-8)
        ;;shell-file-name "/usr/bin/bash"
        x-alt-keysym 'alt
        print-level nil
        print-length nil
        eval-expression-print-length nil
        eval-expression-print-level nil
        ;;x-hyper-keysym 'hyper
        ;;x-meta-keysym 'meta
        ;;Z-super-keysym 'super
        x-select-enable-clipboard-manager nil
        select-active-regions nil
        inhibit-eol-conversion nil
        mode-line-in-non-selected-windows t
        delete-by-moving-to-trash nil
        ;;gc-cons-threshold 400000
        history-delete-duplicates t
        history-length 300
        line-number-display-limit 3000000
        line-number-display-limit-width 200
        message-log-max 2000
        minibuffer-scroll-window t
        next-screen-context-lines 1
        scroll-conservatively 10
        scroll-error-top-bottom nil
        scroll-margin 1
        max-mini-window-height 0.25
        scroll-step 0
        undo-limit 80000
        echo-keystrokes 0.35
        undo-outer-limit 12000000
        undo-strong-limit 120000
        visible-bell nil)
  (when (boundp 'fringe-indicator-alist)
    (define-fringe-bitmap 'light-down-arrow [32 32 32 32 32 32 168 112 32] nil nil 'bottom)
    (define-fringe-bitmap 'light-up-arrow [32 112 168 32 32 32 32 32 32] nil nil 'top)
    (define-fringe-bitmap 'light-top-left-angle [254 254 128 128 128] nil nil 'top)
    (define-fringe-bitmap 'light-bottom-left-angle [128 128 128 254 254] nil  nil 'bottom)
    (define-fringe-bitmap 'light-left-bracket [254 254 128 128 128 0 0 0 0 128 128 128 254 254] nil nil 'center)
    (define-fringe-bitmap 'light-right-curly-arrow [96 16 8 8 72 80 96 120] nil nil 'bottom)
    (define-fringe-bitmap 'light-left-curly-arrow [8 16 16 16 18 10 6 30] nil nil 'top)
    (define-fringe-bitmap 'light-right-arrow [16 8 252 8 16] nil 11 'center)
    (define-fringe-bitmap 'light-left-arrow [32 64 254 64 32] nil nil 'center)
    (setq-default fringe-indicator-alist
                  '((truncation . (light-left-arrow light-right-arrow))
                    (continuation . (light-left-curly-arrow light-right-curly-arrow))
                    (overlay-arrow . right-triangle)
                    (up . light-up-arrow)
                    (down . light-down-arrow)
                    (top . (light-top-left-angle top-right-angle))
                    (bottom . (light-bottom-left-angle bottom-right-angle top-right-angle light-top-left-angle))
                    (top-bottom . (light-left-bracket right-bracket top-right-angle light-top-left-angle))
                    (empty-line . empty-line)
                    (unknown . question-mark))))
  )
(progn ;; Dependencies
  ;; Break -batch processing?
  ;; (when noninteractive (throw 'return t))

  ;;(unless (require 'el-get nil t)
  ;;  (url-retrieve
  ;;   "https://github.com/dimitri/el-get/raw/master/el-get-install.el"
  ;;   (lambda (s)
  ;;    (let (el-get-master-branch)
  ;;	 (goto-char (point-max))
  ;;	 (eval-print-last-sexp)))))

  (defun user-pkg (pkgs)
    (unless (featurep 'package)
      (require 'package))
    (package-initialize)
    (when (not package-archive-contents)
      (package-refresh-contents))
    (when (not (listp pkgs))
      (setq pkgs (list pkgs)))
    (dolist (lib pkgs)
      (unless (or (featurep lib)
                  (package-installed-p lib)
                  (not (assoc lib package-archive-contents)))
        (package-install lib))))
  (defun user-load (keys list)
    "Load using KEYS as KEY to LIST of libraries."
    (dolist (key keys)
      (dolist (lib (cdr (assoc key list)))
        (unless (require lib nil t)
          (user-pkg lib)))))
  (defun user-autoload (list)
    (dolist (pair list)
      (let ((lib (prin1-to-string (car pair)))
            (funs (cdr pair)))
        (cond ((listp funs)
               (dolist (fun funs)
                 (autoload fun lib)))
              (t
               (autoload lib lib))))))
  (defun user-unload (keys list)
    (dolist (key keys)
      (dolist (lib (cdr (assoc key list)))
        (when (featurep lib)
          (unload-feature lib)))))
  (defun user-disable (list)
    (dolist (fun list)
      (put fun 'disabled nil)))

  (defvar user-pkgs '(flx-ido
                      magit
                      ;;clojure-mode
                      ;;cider
                      ;;benchmark-init
                      ;;rainbow-delimiters
                      ;;yasnippet
                      ;;hideshow-org
                      ;;geiser
                      ;;chicken-scheme
                      ;;auto-complete

                      ;; Python
                      python-mode
                      elpy
                      flycheck
                      jedi
                      py-autopep8
                      smartparens
                      ))
  (defvar user-loads '((default . (cl-lib
                                   server
                                   package
                                   view
                                   magit
                                   ;;info-look
                                   ))
                       (optional . (hideshow
                                    ;;company
                                    auto-complete
                                    ;;notmuch
                                    ido
                                    smartparens
                                    paren
                                    ;;rainbow-delimiters
                                    ;;savehist
                                    ;;chicken-scheme
                                    ;;yasnippet
                                    ))
                       (clojure . (cider
                                   clojure-mode
                                   clojure-test-mode
                                   ac-nrepl
                                   clojure-mode-extra-font-locking
                                   ))))
  (defvar user-autoloads '((flyspell . (flyspell-mode))
                           (hl-lock . (highlight-lines-matching-regexp
                                       highlight-regexp))
                           (rect . (rectangle-number-lines
                                    insert-rectangle))
                           (smerge-mode . (smerge-mode))
                           (scheme-complete . (scheme-smart-complete))
                           (auto-complete . (auto-complete-mode))
                           (python-mode . (python-mode))
                           ;;(erc . (erc))
                           (sort . (reverse-region
                                    sort-columns
                                    sort-fields
                                    sort-lines
                                    sort-numeric-fields
                                    sort-pages
                                    sort-paragraphs
                                    sort-regexp-fields
                                    ;; string-insert-rectangle
                                    string-rectangle))))
  (defvar user-unloads '((default . (
                                     ;;scroll-bar
                                     ;;vc
                                     ;;ibuf-ext
                                     ;;ibuffer
                                     ;;time
                                     ;;easymenu
                                     ;;ps-print
                                     ))))
  (defvar user-disables '(narrow-to-page
                          narrow-to-defun
                          narrow-to-region
                          narrow-to-region
                          downcase-region
                          upcase-region
                          dired-find-alternate-file
                          ))
  )


(defvar emacs-extention ".el")
(defvar emacs-extention-compiled ".elc")

;;		(normal-top-level-add-to-load-path '("elpa" "elisp"))))
;;(normal-top-level-add-subdirs-to-load-path)))))
(defvar user-libs '(user-env user-func user-conf user-kmap user-face))
(when (mapcar (lambda (ref)
                (add-to-list 'load-path (substitute-in-file-name "$HOME/.emacs"))
                (require ref) (load (symbol-name ref)))
              user-libs)
  (provide 'user '(env func conf face keymap)))

(user-pkg user-pkgs)
(user-load '(default optional) user-loads)
(user-autoload user-autoloads)
(user-unload '(default) user-unloads)
(user-disable user-disables)
(xterm-mouse-mode 1)
(mouse-wheel-mode 1)


(with-eval-after-load 'user
  ;;Layout
  ;; (when (null (or
  ;;       (window-combined-p (selected-window))
  ;;       (window-combined-p (selected-window) t)))
  ;;   (split-window-horizontally))
  ;; Useful information.
  ;; (assoc-default 'defun (car load-history))
  (server-start)
  (message
   (format "Face: %s" (frame-terminal-default-bg-mode nil)))
  (message
   (format "Features: %d" (length features)))
  (message
   (format "Init-Time: %f" (float-time (time-subtract after-init-time before-init-time))))
  (with-eval-after-load 'bytecomp
    (defun user-update-compile ()
      (let ((byte-compile-verbose nil)
            file file-b)
        (dolist (lib user-libs)
          (setq file (locate-file (concat (symbol-name lib) emacs-extention) load-path))
          (and (file-newer-than-file-p file (byte-compile-dest-file file))
               (byte-compile-file file)))))
    (add-hook 'kill-emacs-hook 'user-update-compile)
    )
  )
