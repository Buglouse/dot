;;toggle point highlight line

(progn ;; Sort
  (defun benchmark-1 (body)
    (let (e
          (s (current-time)))
      (unwind-protect
          (eval body)
        (setq e (current-time)))
      (require 'timeclock)
      (- (time-to-seconds e)
         (seconds-to-time s))))
  (defmacro benchmark (&rest body)
    "Benchmark."
    `(benchmark-1 '(progn ,@body)))
  (defun remove-nil-until-match (sentinel xs)
    "Remove 'nil from XS until SENTINEL is found, then remove SENTINEL;
error if SENTINEL not found."
    ;;Example:
    ;;(setq xs '(nil a b nil c nil XX d nil e f))
    ;;=> (nil a b nil c nil XX d nil e f)
    ;;(remove-nil-until-match 'XX xs)
    ;;=> (a b c d nil e f) xs => (nil a b c d nil e f)"
    ;;(while (and (consp xs) (null (car xs)))
    (setq xs (cdr xs))
    (let* ((head xs)
           (tail (cdr head)))
      (while (progn (while (and (consp tail) (null (car tail)))
                      (setcdr head (setq tail (cdr tail))))
                    (if (and (consp tail) (not (eq (car tail) sentinel)))
                        (setq head tail
                              tail (cdr head)))))
      (if (null tail)
          (error "%s not found" sentinel))
      (setcdr head (cdr tail)))
    xs)
  (defun yes-or-no-p (x)
    (y-or-n-p x))
  (defun case-invert-char ()
    "Replace invert case of character at point."
    (let ((char (following-char)))
      (delete-char 1)
      (if (= (upcase char) char)
          (insert-char (downcase char) 1)
        (insert-char (upcase char) 1))))
  (defun case-invert (&optional beg end)
    "Invert mark or point character(s)."
    (interactive "r")
    (if mark-active
        (case-invert-rectangle beg end)
      (case-invert-char)))
  (defun region-comment (beg end)
    "`comment-region' with PREFIX."
    (interactive "r")
    (comment-region beg end -1))
  (defun event-repeat
      (command)
    "Repeat COMMAND."
    (interactive)
    (let ((repeat-previous-repeated-command command)
          (last-repeatable-command 'repeat))
      (repeat nil)))
  (defun buffer-exchange ()
    "Transpose buffer."
    (interactive)
    (let ((opt (if current-prefix-arg
                   'previous-window
                 'next-window)))
      (let ((window-focus (window-buffer))
            (window-next (window-buffer (funcall opt))))
        (set-window-buffer (selected-window) window-next)
        (set-window-buffer (funcall opt) window-focus)
        (select-window (funcall opt)))))
  (defun buffer-exchange-other ()
    (interactive)
    (let ((current-prefix-arg t))
      (buffer-exchange)))
  (defun rename-buffer_file (nameNew)
    "Set buffer and file to NAME."
    (interactive "sName: ")
    (let ((nameFile (buffer-name))
          (nameBuffer (buffer-file-name)))
      (if (equal "" nameNew)
          (message "Abort!")
        (if (not nameFile)
            (message "Buffer '%s' is not visiting a file!" nameBuffer)
          (if (file-exists-p nameNew)
              (message "File exists!")
            (if (get-buffer nameNew)
                (message "Buffer '%s' exists!" nameNew)
              (progn
                (rename-file nameFile nameNew 1)
                (rename-buffer nameNew)
                (set-visited-file-name nameNew)
                (set-buffer-modified-p nil))))))))
  (defun kill-buffer-fade (&optional buffer)
    ;; (interactive "bFade kill buffer: ")
    (interactive)
    (let ((buffer (if buffer buffer (current-buffer))))
      (with-current-buffer buffer
        (let ((str (buffer-substring
                    (progn (move-to-window-line 0)
                           (point))
                    (progn (move-to-window-line -1)
                           (point-at-eol)))))

          (if (string= "" str)
              (kill-buffer buffer)
            (when (kill-buffer buffer)
              (with-temp-buffer
                (insert str)
                (switch-to-buffer (current-buffer))
                (goto-char (point-min))
                (setq cursor-type nil)
                (dotimes (i 20)
                  (put-text-property (point-min) (point-max)
                                     'face (list :foreground
                                                 (format "gray%d"
                                        ;(* 5  (1+ i))))) ; for white background
                                                         (- 100 (* 5 (1+ i))))))
                  (sit-for 0)
                  (sleep-for .01)))))))))
  (defun buffer-file-name-print ()
    "`message` `buffer-file-name'."
    (interactive)
    (message "%S" (buffer-file-name)))
  (defmacro undo-boundary-skip (&rest body)
    "Wrap BODY in single undo-boundary; strip boundaries from BODY."
    ;; [http://lists.gnu.org/archive/html/emacs-devel/2007-02/msg01136.html]
    (let ((sentinel (make-symbol "sentinel"))
          (cb (make-symbol "cb")))
      `(let ((,cb (current-buffer)))
         (undo-boundary)
         (push ',sentinel buffer-undo-list)
         (unwind-protect
             (progn ,@body)
           (with-current-buffer ,cb
             (setq buffer-undo-list
                   (remove-nil-until-match ',sentinel
                                           buffer-undo-list)))))))
  (defun forward-char-repeat ()
    (interactive)
    (event-repeat 'forward-char))
  (defun compare-buffer-substr-lexi (f1 f2)
    (< (string-to-number (buffer-substring-no-properties (car f1) (cdr f1)))
       (string-to-number (buffer-substring-no-properties (car f2) (cdr f2)))))
  (defun insert-date ()
    (interactive)
    (insert (format-time-string "%Y%m%d")))
  (defun insert-time ()
    (interactive)
    (insert (format-time-string "%H%M")))
  (defun insert-date-file ()
    (interactive)
    (insert (format-time-string "%Y.%m.%d")))
  (defun list-functions ()
    (interactive)
    (let ((buffer (get-buffer-create "*Function List*")))
      (with-current-buffer buffer
        (unless buffer-read-only
          (let ((funclist (list)))
            (mapatoms
             (lambda (x)
               (and (fboundp x)
                    (not (commandp (symbol-function x)))
                    (subrp (symbol-function x))
                    (add-to-list 'funclist
                                 (concat (symbol-name x) ": "
                                         (let ((text (documentation x)))
                                           (string-match "\\(.*[^\n]\\)" text)
                                           (match-string 0 text))
                                         "\n"))
                    )))
            (dolist (item (sort funclist 'string<))
              (insert item))))
        (set-buffer-modified-p t)
        (setq buffer-read-only t
              view-mode t))
      (switch-to-buffer buffer)
      (goto-char (point-min))))
  (defun regexp-make-extension (strings)
    (concat "\\." (regexp-opt strings) "$"))
  (defun sort-block ()
    (interactive)
    (save-excursion
      (save-restriction
        (narrow-to-region (region-beginning) (region-end))
        (goto-char (point-min))
        (sort-subr nil
                   'beginning-of-defun
                   'end-of-defun
                   ;;(function
                   ;; (lambda ()
                   ;;   (while (and (not (eobp))
                   ;;               (looking-at paragraph-separate))
                   ;;     (forward-line 1))))
                   ;;(function
                   ;; (lambda ()
                   ;;   (when (eq (point)
                   ;;             (progn
                   ;;               (end-of-defun)
                   ;;               (point)))
                   ;;     (forward-line 1))))))))
                   ))))
  (progn ;; List
    (defun alist-concat-uniq (sym alist)
      "Concatenat ALIST to SYM alist, removing duplicates."
      ;; Iterate key lists in alist.
      (let (ret)
        (dolist (alist-list alist ret)
          (let* ((sym-alist (symbol-value sym))
                 (alist-key (car alist-list))
                 ;;Find matching key in sym.
                 (sym-val (assoc alist-key sym-alist))
                 (alist-val (cdr (assoc alist-key alist))))
            (cond (sym-val ;;Found alist key in sym.
                   (setq ret
                         (append
                          (list (delete-dups (append sym-val alist-val)))
                          (delq (assoc alist-key ret) ret))))
                  (t
                   (setq ret (append ret (list alist-list)))))))
        (eval `(setq ,sym ret))))
    (defun alist-keys (alist)
      (let ((copy alist)
            ret)
        (while copy
          (push (car (pop copy)) ret))
        ret))
    (defun alist-vals (alist)
      (let ((copy alist)
            ret)
        (while copy
          (push (cdr (pop copy)) ret))
        ret))
    )
  (progn ;; String
    (defun string-remove-space (str)
      "Remove leading and trailing whitespace."
      (replace-regexp-in-string (rx (or (: bos (* (any " \t\n")))
                                        (: (* (any " \t\n")) eos)))
                                ""
                                str))
    (defun string-remove-space-beg (str)
      "Remove leading whitespace."
      (replace-regexp-in-string (rx (: bos (* (any " \t\n"))))
                                ""
                                str))
    (defun string-remove-space-end (str)
      "Remove trailing whitespace."
      (replace-regexp-in-string (rx (* (any " \t\n")) eos)
                                ""
                                str))
    (defun string-match-regex-p (string regexp)
      "Return 't if string matches regexp. `same-window-p'"
      (when (stringp string)
        (catch 'found
          (dolist (exp regexp)
            ;;The elements of regexp can be exps or cons cells whose cars are exps.
            (when (or (and (stringp exp)
                           (string-match-p exp string))
                      (and (consp exp) (stringp (car exp))
                           (string-match-p (car exp) string)))
              (throw 'found t))))))
    )
  (progn ;; Window
    (defun window-dedicated-toggle (&optional window)
      "Toggle WINDOW dedication."
      (interactive)
      (set-window-dedicated-p nil (not (window-dedicated-p))))
    )
  (provide 'user-func)
  )
(progn ;; Error
  (defun valid-buffer (buffer)
    (or (bufferp buffer)
        (signal 'wrong-type-argument (list 'bufferp buffer))))
  )
(progn ;; User
  (progn ;; C
    (defun user-buffer-name()
      (interactive) (message (buffer-file-name)))
    (defun user-kill-buffer()
      (interactive)
      (kill-buffer))
    (defun user-buffer-readonly()
      (interactive)
      (unless (or buffer-read-only
                  ;; update-directory-autoloads creates a <pkg>-autoloads.el buffer
                  ;;(eq this-command 'package-install)
                  (string-match-p "\\-autoloads\\.el" (buffer-name))
                  (< (buffer-size) 1))
        (setq buffer-read-only t)))
    (defun user-mode-active ()
      (interactive)
      (message "Active modes: %s"
               (mapcar (lambda (e) (and (boundp e)
                                        (cons e (symbol-value e))))
                       minor-mode-list)))
    )
  (with-eval-after-load 'hideshow
    (defun hs-overlay-line-count (ov)
      (let (tf ts)
        (cond
         ((eq 'comment (overlay-get ov 'hs))
          (setq tf 'hs-overlay-comment
                ts hs-overlay-comment-string))
         ;; ((eq 'code (overlay-get ov 'hs))
         (t
          (setq tf 'hs-overlay-code
                ts hs-overlay-code-string)))
        (when (and tf ts (overlayp ov))
          (overlay-put ov 'display
                       (propertize
                        (format ts
                                (count-lines (overlay-start ov)
                                             (overlay-end ov)))
                        'face tf))
          ;; (message "1:%S" (overlay-properties ov))
          ;; (message "2:%S" (overlays-in (overlay-get ov 'hs-b-offset)
          ;;            (overlay-get ov 'hs-e-offset)))
          ;; (message "3:%S" (overlays-at (overlay-get ov 'hs-e-offset)))
          )))
    (defun user-hideshow ()
      (cond ((featurep 'hideshow-org)
             (when (not hs-org/minor-mode)
               (hs-org/minor-mode t)
               (hs-hide-all)))
            ((featurep 'hideshow)
             (hs-minor-mode t)
             (hs-hide-all))))
    )
  (progn ;; user-mode-name
    (defvar user-mode-name '()
      "alist of major-mode names to change for mode-line-format string.")

    ;;(setcar (cdr (assq '-minor-mode minor-mode-alist)) "|")

    (defun user-mode-name ()
      (dolist (elm user-mode-name)
        (let ((mode (car elm))
              (string (cdr elm)))
          (when (eq major-mode mode)
            (setq mode-name string)))))
    (add-hook 'after-change-major-mode-hook 'user-mode-name)
    )
  (progn ;; mark
    (defun user-mark-get ()
      (alist-keys user-mark-assocs))
    (defun user-mark-open (path)
      (interactive
       (list (alist-get (intern
                         (completing-read "Alias: " (user-mark-get)))
                        user-mark-assocs)))
      (when (and path (file-exists-p (expand-file-name path)))
        (find-file path)))
    (defun user-unpop-to-mark ()
      "Unpop off mark ring. Does nothing if mark ring is empty."
      (interactive)
      (when mark-ring
        (let ((pos (marker-position (car (last mark-ring)))))
          (if (not (= (point) pos))
              (goto-char pos)
            (setq mark-ring (cons (copy-marker (mark-marker)) mark-ring))
            (set-marker (mark-marker) pos)
            (setq mark-ring (nbutlast mark-ring))
            (goto-char (marker-position (car (last mark-ring))))))))

    (provide 'user-mark))
  (progn ;; Frame
    (defun user-frames-parameter (parameter)
      "Return PARAMETER list of `frame-list'."
      (let ((do (lambda (frame) (frame-parameter frame parameter))))
        (mapcar do (frame-list))))
    (defun user-frame-match-name (name &optional single)
      "Return list of frames matching NAME from 'name parameter; return first found if SINGLE non-nil."
      (let (ret)
        (catch 't
          (dolist (frame (frame-list))
            (when (string-equal name (frame-parameter frame 'name))
              (if single
                  (throw 't frame)
                (setq ret (list (append ret frame))))))
          ret)))
    (defun user-frame-buffer-list ()
      (interactive)
      (princ (frame-parameter (selected-frame) 'buffer-list)))

    ;; Sticky frames
    (defun user-frame-buffer-stick-check (buffer)
      "`buffer-predicate', ignore non-visited buffers."
      (when (and
             (buffer-live-p buffer)
             (not (string-match-p user-window-never-regexp (buffer-name buffer))))
        (let ((match (user-window-buffer-match (buffer-name (current-buffer)) nil))
              (match2 (user-window-buffer-match (buffer-name buffer) nil) ))
          (if (or (and match match2)
                  (and (not match) (not match2)))
              (car (memq buffer (frame-parameter (selected-frame) 'buffer-list)))))))
    (defun user-frame-buffer-stick-filter (buffers)
      (delq nil
            (mapcar #'(lambda (x) (and (not (string-match "^ " (buffer-name x)))
                                       x))
                    buffers)))
    (defun user-frame-buffer-stick-add (buffer frame)
      "http://www.emacswiki.org/emacs/frame-bufs.el"
      (valid-buffer buffer)
      (let ((buffers (user-frame-buffer-stick-filter (frame-parameter frame 'buffer-list))))
        (unless (or (not (buffer-live-p buffer))
                    (memq buffer buffers))
          (set-frame-parameter frame 'buffer-list (cons buffer buffers)))))
    (defun user-frame-buffer-stick-hook ()
      (let ((frame (selected-frame)))
        (dolist (window (window-list frame 'no-minibuf))
          (let ((buffer (window-buffer window)))
            ;;(when (or t
            ;;(memq buffer (frame-parameter frame 'buffer-list))
            ;;(memq buffer (frame-parameter frame 'buried-buffer-list)))
            (user-frame-buffer-stick-add buffer frame)))))
    (defadvice select-window (after frame-bufs)
      (user-frame-buffer-stick-add (window-buffer) (window-frame)))
    (add-hook 'window-configuration-change-hook 'user-frame-buffer-stick-hook)
    )
  (progn ;; Kmacro
    (with-eval-after-load 'kmacro
      (fset 'km-face
            (lambda (&optional arg)
              (interactive "p")
              (setq kill-ring nil)
              (kmacro-exec-ring-item
               (quote ("//(deffacermpLrcryptpjrmpbpjrcrypk" 0 "%d"))
               arg)))
      (fset 'km-keys
            (lambda (&optional arg)
              (interactive "p")
              (setq kill-ring nil)
              (kmacro-exec-ring-item
               (quote ("//(define-key.*'rm//)/?)rcry//$rm//^rcry" 0 "%d"))
               arg)))
      )
    )
  (progn ;; Scheme
    (defun user-scheme-indent-module (state indent-point normal-indent) 0)
    (defun scheme-compile-current-file (&optional switch)
      (interactive "P")
      (let ((file (buffer-file-name)))
        (comint-check-source file)
        (setq scheme-prev-l/c-dir/file (cons (file-name-directory file)
                                             (file-name-nondirectory file)))
        (message "compiling \"%s\" ..." file)
        (comint-send-string (scheme-proc) (concat "(compile-file \"" file "\"\)\n"))
        (if switch
            (switch-to-scheme t)
          (message "\"%s\" compiled and loaded." file))))

    (defvar ac-chicken-symbols-candidates 'nil)

    (defun user-indent-sxml ()
      (interactive "P")
      (let ((pos (- (point-max) (point)))
            (beg (progn (beginning-of-line) (point))))
        (skip-chars-forward " \t")
        (indent)
        ))
    (define-derived-mode sxml-mode scheme-mode "SXML"
      (scheme-mode-variables)
      (setq-local indent-line-function 'indent-relative)
      (setq-local standard-indent 2)
      (setq-local lisp-indent-function ())
      ;;(setq-local indent-line-function 'sxml-indent-line)
      ;;(setq-local lisp-indent-function 'scheme-indent-function)
      (user-define-keys sxml-mode-map
        [remap indent-for-tab-command] tab-to-tab-stop
        [remap newline-and-indent] newline
        [remap insert-line-indent] insert-line)
      )
    )
  (progn ;; rcirc
    (defun user-rcirc-ido()
      (interactive)
      (switch-to-buffer
       (ido-completing-read "Channel:"
                            (save-excursion
                              (delq
                               nil
                               (mapcar (lambda (buf)
                                         (when (buffer-live-p buf)
                                           (with-current-buffer buf
                                             (and (eq major-mode 'rcirc-mode)
                                                  (buffer-name buf)))))
                                       (buffer-list)))))))
    (defun user-rcirc ()
      (interactive)
      (let ((fr (or (cdr (assoc "rcirc" (make-frame-names-alist)))
                    (make-frame '((name . "rcirc"))))))
        (when (and fr (select-frame fr))
          (rcirc nil)
          (split-window-vertically)))
      )
    (defun irc()
      (interactive)
      (user-rcirc))
    )
  (progn ;; CSS
    (defun hexcolor (c)
      (let* ((vs (color-values c))
             (r (car vs))
             (g (cadr vs))
             (b (caddr vs)))
        (floor (+ (* .3 r) (* .59 g) (* .11 b)) 256)))
    (defun user-css ()
      ;; Fontify color names
      `((,(concat "#[0-9a-fA-F]\\{6\\}\\|" (regexp-opt (defined-colors) 'words))
         (0 (let ((color (match-string-no-properties 0)))
              (put-text-property
               (match-beginning 0) (match-end 0)
               'face (list :foreground (if (> 128.0 (hexcolor color)) "white" "black")
                           :background color)))))))
    ;;(add-hook 'css-mode-hook 'user-css-fontity)
    )
  (progn ;; Mail
    (defun user-sendmail ()
      (if (message-mail-p)
          (save-excursion
            (let* ((from (save-restriction
                           (message-narrow-to-headers)
                           (message-fetch-field "from")))
                   (account (cond
                             ((string-match ".*@gmail" from) "gmail.com")
                             ((string-match ".*@creationnetworks.net" from) "creationnetworks.net"))))
              (setq message-sendmail-extra-arguments (list '"-a" account))))))
    (add-hook 'message-send-mail-hook 'user-sendmail)
    )
  (progn ;; Emacs
    (require 'rect)
    (defun whitespace-single (beg end)
      (interactive "r")
      (save-excursion
        (save-restriction
          (narrow-to-region beg end)
          (goto-char (point-min))
          (while (re-search-forward "\\s-+" nil t)
            (replace-match " ")))))
    (defun whitespace-single-rectangle (beg end)
      (interactive "r")
      (apply-on-rectangle 'whitespace-single-rectangle-1 beg end))
    (defun whitespace-single-rectangle-1 (beg end)
      (save-restriction
        (save-match-data
          (narrow-to-region (+ (point) beg)
                            (+ (point) end))
          (while (re-search-forward "\\s-+" nil t)
            (replace-match " ")))))
    ))
(progn ;; Internal
  (with-eval-after-load "indent"
    (defun user-indent-buffer ()
      (interactive)
      (save-excursion
        (indent-region (point-min) (point-max) nil)))
    )
  (with-eval-after-load 'shr
    (advice-add #'shr-colorize-region :around
                (defun shr-no-colorize-region (&rest ignore)))
    )
  (with-eval-after-load "window"
    (defvar user-window-same-regexp nil
      "Regexp to match buffers using the same window as the match.")
    (defvar user-window-never-regexp nil
      "Regxp of buffers to never match.")
    (defvar user-window-same-sizes nil
      "(percentage . buffer-regexp) of widow sizes to set for buffers matching regexp.")

    (defun user-window-split-disable ()
      nil)
    (defun user-window-scroll-buffer-toggle (&optional window)
      "Toggle `other-window-scroll-buffer' for WINDOW."
      (interactive)
      (setq other-window-scroll-buffer
            (if (eq (current-buffer) other-window-scroll-buffer)
                (other-buffer (current-buffer) t)
              (current-buffer))))

    ;; Skip buffers on switch.
    (defun user-window-set-split-size (window)
      "Resize window matching buffer from `user-window-same-sizes' as a percentage of frame."
      (let ((buffer (buffer-name (window-buffer window)))
            (frame-height (frame-height (window-frame window)))
            (window-height (window-total-height window t))
            (copy user-window-same-sizes)
            elm)
        (catch 't
          (while (setq elm (pop copy))
            (when (string-match-p (car (cdr elm)) buffer)
              (window-resize-no-error window
                                      (- (round (* frame-height (/ (car elm) 100.0)))
                                         window-height))
              (throw 't t))))))
    (defun user-window-buffer-match (buffer alist)
      "Display in other window if (current-buffer) matches `user-window-same-regexp'."
      (string-match-p user-window-same-regexp buffer))
    (defun user-display-buffer-same (buffer alist)
      "Called from `display-buffer-alist'"
      ;; There is only one window, skip to next function (`display-buffer-at-bottom').
      (unless (one-window-p)
        (let ((match (lambda (window)
                       (string-match-p user-window-same-regexp
                                       (buffer-name (window-buffer window)))
                       )))
          (window--display-buffer buffer
                                  (get-window-with-predicate match)
                                  'window alist t)
          ;;(selected-window)
          )))

    (defun user-other-window-prev ()
      (interactive)
      (other-window -1))

    (defun user-display-buffer-current ()
      (interactive)
      (display-buffer (current-buffer)))
    )
  (with-eval-after-load "bindings"
    (defvar user-modeline-path-char "+")
    (defvar user-modeline-unignore-buffer-regexp "\\(\\*\\(Help\\)\\*\\)")
    (defvar user-modeline-ignore-buffer-regexp "^[ ]*\\*.*\\*$")
    (defvar user-modeline-window-active nil)

    (defun user-modeline-read ()
      (when buffer-read-only "R"))
    (defun user-modeline-modified ()
      (when (buffer-modified-p) "M"))
    (defun user-modeline-narrow ()
      (when (> (point-min) 1) "N"))
    (defun user-modeline-dedicated ()
      (when (window-dedicated-p) "D"))
    (defun user-modeline-size ()
      (file-size-human-readable (buffer-size) 'si))
    (defun user-modeline-process ()
      ;;user-modeline-process
      (let* ((buffer (current-buffer))
             (process (get-buffer-process buffer)))
        (when process
          (when (eq buffer (process-buffer process))
            (concat
             (process-name process)
             (when (not (process-live-p process))
               "*"))))))
    (defun user-modeline-frame ()
      (let ((c (length (frame-list))))
        (when (> c 1)
          (number-to-string c))))
    (defun user-modeline-scroll ()
      (when other-window-scroll-buffer
        (when (eq (current-buffer) (get-buffer other-window-scroll-buffer))
          "@")))
    (defun user-modeline-point ()
      (let* ((column column-number-mode)
             (region mark-active)
             (line line-number-mode)
             buffer-size point region-t region-b region-diff column-total column-point line-total window-total page page-total)
        (when region
          (setq region-t (count-lines 1 (region-beginning))
                region-b (count-lines 1 (region-end)))
          (setq region-diff (number-to-string (- region-t region-b)))
          (setq region-t (number-to-string region-t)
                region-b (number-to-string region-b)))
        (when column
          (setq column-point (number-to-string (current-column))
                column-total (number-to-string (window-total-width))))
        (when line
          (setq window-total (window-total-size)
                buffer-size (max (buffer-size) 1)
                point (max (point) 1)
                line-total (number-to-string (+ 1 (count-lines 1 buffer-size)))
                page (number-to-string (/ (count-lines 1 point) window-total))
                page-total (number-to-string (/ (count-lines 1 buffer-size) window-total))))

        `(""
          ,@(when region
              `(""
                ;;(,region-t user-modeline-region)
                ;;"/"
                ;;(,region-b user-modeline-region)
                ;;,@(when (/= region-diff 0)
                (,region-diff user-modeline-region)
                "-"
                ))
          ,@(when line
              `(("%l" user-modeline-line)
                "/"
                (,line-total user-modeline-line-total)
                ;;"("
                ;;(,page user-modeline-page)
                ;;"/"
                ;;(,page-total user-modeline-page-total)
                ;;")"
                ))
          ,@(when column
              `("_"
                (,column-point user-modeline-column)
                ;;"/"
                ;;(,column-total user-modeline-column-total)
                ;;"_"
                ))
          )))
    (defun user-modeline-path ()
      (let ((path (buffer-file-name))
            (name (buffer-name)))
        (cond ((and (featurep 'magit) path (magit-toplevel))
               (concat "["
                       ;;(:propertize  'face 'user-modeline-frame)
                       (file-name-nondirectory (directory-file-name (expand-file-name (magit-toplevel))))
                       ":"
                       (magit-get-current-branch)
                       "]"
                       name))
              (dired-directory
               (concat user-modeline-path-char name))
              (t
               name))))
    (defun user-modeline-align (size)
      (number-to-string (- (window-total-size nil t) (length size))))
    (defun user-modeline-active ()
      ;;(message (format "%sFrame:%s.Window:%s.Buffer:%s" (current-time) (frame-selected-window) (selected-window) (buffer-name)))
      ;;(with-current-buffer (window-buffer user-modeline-window-active)
      ;;(eq (current-buffer) (window-buffer (selected-window))))
      ;;(eq (frame-selected-window) user-modeline-window-active)
      ;;(eq (current-buffer) (window-buffer user-modeline-window-active)))
      (setq user-modeline-window-active (selected-window))
      )
    (defun user-modeline-active-set ()
      ;; (minibuffer-window)
      ;;(setq user-modeline-window-active (or (minibuffer-selected-window)
      ;;                                          (frame-selected-window)))
      (force-mode-line-update t)
      )
    (defun user-modeline-faces ()
      "Create faces for `user-modeline-format'."
      (let ((faces '(user-modeline
                     user-modeline-separator
                     user-modeline-line
                     user-modeline-line-total
                     user-modeline-page
                     user-modeline-page-total
                     user-modeline-column-total
                     user-modeline-column
                     user-modeline-region
                     user-modeline-frame
                     user-modeline-directory
                     user-modeline-buffer
                     user-modeline-readonly
                     user-modeline-modified
                     user-modeline-narrow
                     user-modeline-dedicated
                     user-modeline-scroll
                     user-modeline-byte
                     user-modeline-mode
                     user-modeline-process
                     user-modeline-modes
                     user-modeline-misc
                     user-modeline-vc
                     user-modeline-inactive
                     user-modeline-point))
            face)
        (while (setq face (pop faces))
          (when (not (facep face))
            (eval (nconc `(defface ,face nil "")))))))
    (defun user-modeline-func (elm &optional disable)
      (let* ((active (eq user-modeline-window-active (selected-window)))
             (elm (or (and (listp elm) elm) (list elm)))
             (string (car elm))
             (face (if active
                       (or (cadr elm) 'user-modeline-separator)
                     'user-modeline-inactive)))
        (if (and disable active)
            string
          (and (not (null string)) (symbolp face)
               `(:propertize ,string face ,face)))))

    (defun user-modeline-rcirc ()
      (let ((ml-target rcirc-target)
            (ml-server (with-rcirc-server-buffer rcirc-server-name))
            (ml-nick (rcirc-buffer-nick))
            (ml-count (length (rcirc-channel-nicks (rcirc-buffer-process) rcirc-target)))
            (joins (let ((server-plist
                          (cdr (assoc-string rcirc-server rcirc-server-alist))))
                     (when server-plist (plist-get server-plist :channels)))))
        (mapcar (lambda (e) (funcall 'user-modeline-func e))
                `(""
                  (,ml-target user-modeline-buffer) "@"
                  (,ml-server user-modeline-frame)
                  "{" (,(number-to-string ml-count) user-modeline-page) "}"
                  "(" (,ml-nick user-modeline-mode) ")"
                  (,(mapconcat 'identity joins "|") user-modeline-directory)
                  ,@(when global-mode-string
                      `("[" ,global-mode-string "]"))
                  ))))
    (defun user-modeline-notmuch ()
      (let ((ml-frame "%F")
            (ml-frame-count (user-modeline-frame))
            (ml-readonly (user-modeline-read))
            )
        (mapcar (lambda (e) (funcall 'user-modeline-func e))
                `(""
                  (,ml-frame user-modeline-frame)
                  ,@(when ml-frame-count
                      `("/"
                        (,ml-frame-count user-modeline-frame)
                        ))
                  "|"
                  (,(buffer-name) user-modeline-buffer)
                  ,@(when ml-readonly
                      `("("
                        (,ml-readonly user-modeline-readonly)))
                  "%[["
                  (,mode-name user-modeline-mode)
                  (,minor-mode-alist user-modeline-modes)
                  "%]]"
                  ("+" user-modeline)
                  ))))
    (defun user-modeline ()
      (let ((ml-fill "%- ")
            (ml-vbar "|")
            (ml-frame "%F")
            (ml-recursive-beg "%[[")
            (ml-recursive-end "%]]")
            (ml-path (user-modeline-path))
            (ml-frame-count (user-modeline-frame))
            (ml-readonly (user-modeline-read))
            (ml-modified (user-modeline-modified))
            (ml-narrow (user-modeline-narrow))
            (ml-dedicated (user-modeline-dedicated))
            (ml-scroll (user-modeline-scroll))
            (ml-byte (user-modeline-size))
            (ml-process (user-modeline-process))
            (ml-mode mode-name)
            (ml-mode-minor minor-mode-alist)
            ;;(ml-align (user-modeline-align))
            ;;(ml-misc user-modeline-misc-info)
            (ml-vc (when vc-mode (string-remove-space (substring-no-properties vc-mode))))
            (ml-point (user-modeline-point)))
        (mapcar (lambda (e) (funcall 'user-modeline-func e))
                `(""
                  (,ml-frame user-modeline-frame)
                  ,@(when ml-frame-count
                      `("/"
                        (,ml-frame-count user-modeline-frame)
                        ;;")"
                        ))
                  ,ml-vbar
                  (,ml-path user-modeline-buffer)
                  "@"
                  (,ml-byte user-modeline-byte)
                  ,@(when (or ml-readonly ml-modified ml-narrow ml-dedicated ml-scroll)
                      `("("
                        (,ml-readonly user-modeline-readonly)
                        (,ml-modified user-modeline-modified)
                        (,ml-narrow user-modeline-narrow)
                        (,ml-dedicated user-modeline-dedicated)
                        (,ml-scroll user-modeline-scroll)
                        ")"
                        ))
                  ,ml-recursive-beg
                  (,ml-mode user-modeline-mode)
                  (,ml-mode-minor user-modeline-modes)
                  ,ml-recursive-end
                  ;;ml-align
                  ,@(when (or ml-vc ml-process)
                      `("{"
                        (,ml-vc user-modeline-vc)
                        ,@(when (and ml-vc ml-process)
                            "|")
                        (,ml-process user-modeline-process)
                        "}"))
                  "<"
                  ,@ml-point
                  ">"
                  ("+" user-modeline)
                  global-mode-string
                  ;;,ml-misc
                  ;;,ml-fill
                  ))))

    (add-hook 'post-command-hook 'user-modeline-active)
    (add-hook 'buffer-list-update-hook 'user-modeline-active-set)
    (add-hook 'window-configuration-change-hook 'user-modeline-active-set)
    ;;(add-hook 'focus-in-hook 'user-modeline-active-set)
    ;;(add-hook 'focus-out-hook 'user-modeline-active-set)

    (user-modeline-faces)
    )
  (with-eval-after-load 'simple
    (defun user-yank ()
      (interactive)
      ;;(beginning-of-line)
      (when current-prefix-arg
        (forward-line -1))
      (yank)
      (call-interactively 'indent-region)
      )
    (defun user-yank-above ()
      (interactive)
      (let ((current-prefix-arg t))
        (user-yank)))
    (defun user-yank-pop ()
      (interactive)
      (yank-pop)
      (call-interactively 'indent-region))

    (defun user-message-mode-hook ()
      (setq scroll-up-aggressively nil
            scroll-down-aggressively nil)
      (use-local-map user-point-map)
      (goto-char (point-max)))
    (defun move-beginning-of-line-or-indentation (arg)
      ;; http://read.gwene.org/thread/gwene.org.emacsen.planet/1695
      "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
      (interactive "^p")
      (setq arg (or arg 1))

      ;; Move lines first
      (when (/= arg 1)
        (let ((line-move-visual nil))
          (forward-line (-1 arg))))

      (let ((orig-point (point)))
        (back-to-indentation)
        (when (= orig-point (point))
          (move-beginning-of-line 1))))
    (defun insert-line ()
      "Insert newline below point or above with PREFIX."
      (interactive)
      (if current-prefix-arg
          (beginning-of-visual-line)
        (end-of-visual-line))
      (newline 1)
      (when current-prefix-arg
        (forward-line -1)))
    (defun insert-line-indent ()
      "Insert line and indent."
      (interactive)
      (insert-line)
      (indent-according-to-mode))
    (defun insert-line-indent-above ()
      (interactive)
      (let ((current-prefix-arg t))
        (insert-line-indent)))
    (defun kill-line-insert ()
      "Kill line and insert below point or above with PREFIX."
      (interactive)
      (kill-ring-save (line-beginning-position) (line-beginning-position 2))
      (beginning-of-line)
      (yank)
      (when current-prefix-arg
        (forward-line -1))
      (beginning-of-line-text))
    (defun kill-line-insert-above ()
      (interactive)
      (let ((current-prefix-arg t))
        (kill-line-insert)))
    (defun mark-line ()
      "Mark line."
      (interactive)
      (push-mark (line-beginning-position) nil t)
      (goto-char (line-end-position)))

    (defvar command-clipboard "xsel"
      "Command for X-Clipboard actions.")
    (defvar command-clipboard-copy '("--input" "--clipboard")
      "Arguments for `command-clipboard', used with `clipboard-copy'.")
    (defvar command-clipboard-paste '("--output" "--clipboard")
      "Arguments for `command-clipboard', used with `clipboard-paste'.")
    (defun clipboard-copy (&optional text)
      "Copy TEXT into X-Clipboard."
      ;; What causes this limitation; `command-clipboard', `call-process-region'?
      (interactive)
      (if (region-active-p)
          (progn
            (shell-command-on-region (region-beginning) (region-end) "xsel -i")
            (message "Yank")
            (deactivate-mark)))
      ;; (with-temp-buffer
      ;;   ;; (when (and (region-active-p)
      ;;   ;;       (or (< (count-lines (region-beginning) (region-end)) 100)
      ;;   ;;      (> (count-lines (region-beginning) (region-end)) 300)))
      ;;   (insert text)
      ;;
      ;;   (apply 'call-process-region
      ;;    (point-min) (point-max)
      ;;    command-clipboard
      ;;    nil 0 nil
      ;;    command-clipboard-copy)))
      )
    (defun clipboard-paste ()
      "Insert X-Clipboard contents.
interprogram uses `yank-pop' for multiple return strings."
      (let* ((cmd (concat command-clipboard " "
                          (mapconcat 'identity
                                     command-clipboard-paste
                                     " ")))
             (text (shell-command-to-string cmd)))
        ;; Is XC is from external application?
        (unless (string= (car kill-ring) text)
          text)))

    (defun selective-overlay-toggle ()
      (interactive)
      (let ((col (1+ (current-column)))
            (sel selective-display))
        (set-selective-display
         (unless (and sel (= col sel))
           col))
        ))

    (defvar blink-kill-delay nil
      "Seconds to keep point at point exchange; nil to disable.")
    (defvar message-kill-limit 40
      "Limit `kill-ring-save' message.
Integer, character maximum to print.
Function, return integer maximum. (window-width).")
    (defvar message-kill "'%s'"
      "Message for `kill-ring-save'.")
    (defun kill-ring-save (beg end)
      "Save the region as if killed, but don't kill it.
In Transient Mark mode, deactivate the mark.
If `interprogram-cut-function' is non-nil, also save the text for a window
system cut and paste.

If you want to append the killed line to the last killed text,
use \\[append-next-kill] before \\[kill-ring-save].

This command is similar to `copy-region-as-kill', except that it gives
visual feedback indicating the extent of the region being copied."
      (interactive "r")
      (copy-region-as-kill beg end)
      ;; This use of called-interactively-p is correct
      ;; because the code it controls just gives the user visual feedback.
      (if (called-interactively-p 'interactive)
          (let ((other-end (if (= (point) beg) end beg))
                (opoint (point))
                ;; Inhibit quitting so we can make a quit here
                ;; look like a C-g typed as a command.
                (inhibit-quit t))
            ;; (if (pos-visible-in-window-p other-end (selected-window))
            ;;   ;; Swap point-and-mark quickly so as to show the region that
            ;;   ;; was selected.  Don't do it if the region is highlighted.
            ;; ;;    (unless (or (null blink-kill-delay)
            ;;         (and (region-active-p)
            ;;        (face-background 'region)))
            ;;     ;; Swap point and mark.
            ;;     (set-marker (mark-marker) (point) (current-buffer))
            ;;     (goto-char other-end)
            ;;     (sit-for blink-matching-delay)
            ;;     ;; Swap back.
            ;;     (set-marker (mark-marker) other-end (current-buffer))
            ;;     (goto-char opoint)
            ;;     ;; If user quit, deactivate the mark
            ;;     ;; as C-g would as a command.
            ;;     (and quit-flag mark-active
            ;;    (deactivate-mark)))
            (let* ((tc (current-kill 0))
                   (message-len (min (length tc)
                                     ;; message-kill-limit
                                     (frame-width)
                                     )))
              ;; Visual confirmation already shown.
              (unless (region-active-p)
                ;; Show exchange, when visibility is optional.
                (if (= (point) beg)
                    (message message-kill
                             (substring tc (- message-len)))
                  (message message-kill
                           (substring tc 0 message-len)))))
            )))
    )
  (with-eval-after-load 'gud
    (defadvice pdb (before gud-query-cmdline activate)
      (interactive
       (list (gud-query-cmdline pdb-path
                                (file-name-nondirectory buffer-file-name)))))
    )
  (with-eval-after-load 'dired
    (defun dired-kill-buffers ()
      "Call `kill-buffer' on all `dired-mode' buffers."
      (interactive)
      (save-excursion
        (let ((count 0))
          (dolist (buffer (buffer-list))
            (set-buffer buffer)
            (when (equal major-mode 'dired-mode)
              (setq count (1+ count))
              (kill-buffer buffer))))))

    (defvar user-dired-mode-name "Dired")
    (defvar user-dired-mode-sort-name "%n")
    (defvar user-dired-mode-sort-date "%d")
    (defun dired-sort-set-mode-line ()
      "Define mode-name."
      (when (eq major-mode 'dired-mode)
        (setq mode-name
              (concat user-dired-mode-name
                      ;;(let ((case-fold-search))
                      ;;  (cond ((string-match-p dired-sort-by-name-regexp dired-actual-switches)
                      ;;         user-dired-mode-sort-name)
                      ;;        ((string-match-p dired-sort-by-date-regexp dired-actual-switches)
                      ;;         user-dired-mode-sort-date)
                      ;;        (t
                      ;;         dired-actual-switches)))))
                      ))
        (force-mode-line-update)))

    (defun user-dired-up-directory (&optional other-window)
      ;;dired-up-directory
      (interactive)
      (if other-window
          (dired-other-window up)
        (find-alternate-file "..")))
    )
  (with-eval-after-load 'paren
    (defadvice show-paren-function
        (after show-matching-paren-offscreen activate)
      "Show matching paren in echo."
      (interactive)
      (let* ((cb (char-before (point)))
             (matching-text (and cb
                                 (char-equal (char-syntax cb) ?\) )
                                 (blink-matching-open)))))))
  (with-eval-after-load 'help-mode
    (define-button-type 'help-function-def
      :supertype 'help-xref
      'help-function (lambda (fun file)
                       (require 'find-func)
                       (when (eq file 'C-source)
                         (setq file
                               (help-C-file-name (indirect-function fun) 'fun)))
                       ;; Don't use find-function-noselect because it follows
                       ;; aliases (which fails for built-in functions).
                       (let ((location
                              (find-function-search-for-symbol fun nil file)))
                         (pop-to-buffer (car location))
                         (if (cdr location)
                             (progn
                               (goto-char (cdr location))
                               (recenter-top-bottom))
                           (message "Unable to find location in file"))))
      'help-echo (purecopy "mouse-2, RET: find function's definition"))
    )
  (with-eval-after-load 'button
    (define-button-type 'url
      'follow-link t
      'action #'buttonize-url-open)
    (define-button-type 'file
      'follow-link t
      'action #'buttonize-file-open)
    (defun buttonize-url-open (button)
      (browse-url (buffer-substring (button-start button) (button-end button)))
      )
    (defun buttonize-file-open (button)
      (message (buffer-substring (button-start button) (button-end button)))
      (browse-url-file-url (buffer-substring (button-start button) (button-end button))))
    (defun buttonize-buffer-url ()
      (interactive)
      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward "https?:[\d\w/.]*[^\s\n[]*" nil t)
          (make-button (match-beginning 0) (match-end 0) :type 'url))))
    (defun buttonize-buffer-file (regexp)
      (interactive)
      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward regexp nil t)
          (make-button (match-beginning 0) (match-end 0) :type 'file))))

    (defun user-button-get-action ()
      (interactive)
      (message "%s" (button-get (button-at (point)) 'action)))
    (defun user-button-get-type ()
      (interactive)
      (message "%s" (button-get (button-at (point)) 'type)))
    (defun user-button-get-function ()
      (interactive)
      (message "%s" (button-get (button-at (point)) 'help-function)))
    (defun user-button-get-args ()
      (interactive)
      (message "%s" (button-get (button-at (point)) 'help-args)))
    (defun user-button-get-follow ()
      (interactive)
      (message "%s" (button-get (button-at (point)) 'follow-link)))

    (provide 'user-button)
    )
  (with-eval-after-load 'smerge-mode
    (defun user-smerge-try ()
      (save-excursion
        (goto-char (point-min))
        (when (re-search-forward "^<<<<<<< " nil t)
          (smerge-mode 1)
          (hs-show-all))))

    (add-hook 'find-file-hook 'user-smerge-try t))
  )
(progn ;; External
  (with-eval-after-load 'geiser
    (with-eval-after-load 'geiser-mode
      (defun user-geiser-mode-hook ()
        (setq geiser-autodoc-mode-string "_D"
              geiser-smart-tab-mode-string "_T")
        )
      )
    (with-eval-after-load 'geiser-doc
      ;;(defun user-geiser-doc-mode ()
      ;;  (with-current-buffer "*Geiser documentation*"
      ;;    (message "foo")
      ;;    (setq mode-name "Geiser_Doc")
      ;;    (setq geiser-popup--registry (delete "*Geiser documentation*" geiser-popup--registry))))
      (defun geiser-doc--pop-to-buffer ()
        ;;(geiser-popup--define doc "*Geiser documentation*" geiser-doc-mode)
        ;;(defmacro geiser-popup--define (base name mode)
        (defun geiser-doc--get-buffer ()
          (or (get-buffer "*Geiser documentation*")
              (with-current-buffer (get-buffer-create "*Geiser documentation*")
                (funcall 'geiser-doc-mode)
                (geiser-popup--setup-view-mode)
                (current-buffer))))
        (let ((buffer (funcall 'geiser-doc--get-buffer)))
          (unless (eq buffer (current-buffer))
            (display-buffer buffer))))
      )
    (with-eval-after-load 'geiser-chicken
      (defun geiser-chicken--external-help (id module)
        "Loads chicken doc into a buffer"
        (browse-url (format user-geiser-chicken-external-url id))))
    )
  (with-eval-after-load 'notmuch
    (defun user-notmuch-hello-insert-header ()
      (widget-insert "Messages: ")
      (widget-create 'link
                     :notify (lambda (&rest ignore)
                               (notmuch-hello-update))
                     :help-echo "Refresh"
                     (notmuch-hello-nice-number
                      (string-to-number (car (process-lines notmuch-command "count"))))))
    (defun user-notmuch-hello-insert-saved-searches ()
      "Insert the saved-searches section."
      (let ((searches (notmuch-hello-query-counts
                       (if notmuch-saved-search-sort-function
                           (funcall notmuch-saved-search-sort-function
                                    notmuch-saved-searches)
                         notmuch-saved-searches)
                       :show-empty-searches notmuch-show-empty-saved-searches)))
        (when searches
          (widget-insert "Saved searches: ")
          (widget-create 'push-button
                         :notify (lambda (&rest ignore)
                                   (customize-variable 'notmuch-saved-searches))
                         "edit")
          (widget-insert "\n\n")
          (let ((start (point)))
            (notmuch-hello-insert-buttons searches)
            (indent-rigidly start (point) notmuch-hello-indent)))))
    (defun user-notmuch-hello-insert-saved-searches ()
      "Insert the saved-searches section."
      (let ((searches (notmuch-hello-query-counts
                       (if notmuch-saved-search-sort-function
                           (funcall notmuch-saved-search-sort-function
                                    notmuch-saved-searches)
                         notmuch-saved-searches)
                       :show-empty-searches notmuch-show-empty-saved-searches)))
        (when searches
          ;;(widget-insert "Saved searches: ")
          (widget-create 'push-button
                         :notify (lambda (&rest ignore)
                                   (customize-variable 'notmuch-saved-searches))
                         "edit")
          (widget-insert "\t")
          (let ((start (point)))
            (notmuch-hello-insert-buttons searches)
            (indent-rigidly start (point) notmuch-hello-indent)))))
    ;; Remove insert
    (defun notmuch-search-process-sentinel (proc msg)
      "Add a message to let user know when \"notmuch search\" exits"
      (let ((buffer (process-buffer proc))
            (status (process-status proc))
            (exit-status (process-exit-status proc))
            (never-found-target-thread nil))
        (when (memq status '(exit signal))
          (catch 'return
            (kill-buffer (process-get proc 'parse-buf))
            (if (buffer-live-p buffer)
                (with-current-buffer buffer
                  (save-excursion
                    (let ((inhibit-read-only t)
                          (atbob (bobp)))
                      (goto-char (point-max))
                      (if (eq status 'signal)
                          (insert "Incomplete search results (search process was killed).\n"))
                      (when (eq status 'exit)
                        ;;(insert "End of search results.\n")
                        ;; For version mismatch, there's no point in
                        ;; showing the search buffer
                        (when (or (= exit-status 20) (= exit-status 21))
                          (kill-buffer)
                          (throw 'return nil))
                        (if (and atbob
                                 (not (string= notmuch-search-target-thread "found")))
                            (set 'never-found-target-thread t)))))
                  (when (and never-found-target-thread
                             notmuch-search-target-line)
                    (goto-char (point-min))
                    (forward-line (1- notmuch-search-target-line)))))))))
    (defun notmuch-tree-process-sentinel (proc msg)
      "Add a message to let user know when \"notmuch tree\" exits"
      (let ((buffer (process-buffer proc))
            (status (process-status proc))
            (exit-status (process-exit-status proc))
            (never-found-target-thread nil))
        (when (memq status '(exit signal))
          (kill-buffer (process-get proc 'parse-buf))
          (if (buffer-live-p buffer)
              (with-current-buffer buffer
                (save-excursion
                  (let ((inhibit-read-only t)
                        (atbob (bobp)))
                    (goto-char (point-max))
                    (if (eq status 'signal)
                        (insert "Incomplete search results (tree view process was killed).\n"))
                    (when (eq status 'exit)
                      ;;(insert "End of search results.")
                      (unless (= exit-status 0)
                        (insert (format " (process returned %d)" exit-status)))
                      (insert "\n")))))))))
    )
  )

(provide 'user-func)
