#!/bin/bash
export SOCK_EMACS="${DIR_SOCK}/emacs.sock"
export CONF_EMACS="${HOME}/.emacs/user.el"

emacs_client() {
    $(type -P emacsclient) --tty --socket-name="${SOCK_EMACS}" "${@}"
}
emacsclient() {
    $(type -P emacsclient) --socket-name="${SOCK_EMACS}" "${@}"
}
emacs_session() {
    if [[ -S ${SOCK_EMACS} ]]; then
	      emacs_client "${@}"; else
        $(type -P emacs) --directory=${CONF_EMACS%/*} --load=${CONF_EMACS##*/} --no-window-system --no-init "${@}"
    fi
}
emacs() {
    [[ -s ${SOCK_EMACS} ]] && emacs_client "${@}" || emacs_session "${@}"
}

[[ "${0}" = "${BASH_SOURCE}" ]] || return 0
emacs ${@}
