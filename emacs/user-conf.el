(progn ;; ;User
  ;;(add-to-list 'user-mode-name '(lisp-interaction-mode "ILisp"))
  ;;(add-to-list 'user-mode-name '(emacs-lisp-mode "ELisp"))
  ;;(add-to-list 'user-mode-name '(vc-git-log-edit-mode "LogE-Git"))
  ;;;;(add-to-list 'user-mode-name '(log-edit-mode "LogE"))
  ;;(add-to-list 'user-mode-name '(notmuch-search-mode "Search"))
  ;;(add-to-list 'user-mode-name '(notmuch-hello-mode "Notmuch"))
  ;;(add-to-list 'user-mode-name '(notmuch-show-mode "Show"))
  ;;(add-to-list 'user-mode-name '(notmuch-message-mode "Message"))
  (setq
   small-temporary-file-directory user-path-cache
   temporary-file-directory user-path-cache
   )
  )
(progn ;; Internal
  (with-eval-after-load 'rcirc
    (setq rcirc-server-alist
          '(("" :port 9999 :encryption tls :nick "user" :password :channels ("*status"))))
    )
  (with-eval-after-load 'user
    (defun user-prog-mode-hooks()
      (interactive)
      (user-buffer-readonly)
      (user-hideshow)
      (hs-minor-mode t)
      ;;(turn-on-smartparens-strict-mode)
      (auto-complete-mode t)
      ;;(electric-pair-mode t)
      ;;(visual-line-mode t)
      )
    (add-hook 'prog-mode-hook 'user-prog-mode-hooks)

    (add-hook 'pre-command-hook 'hl-line-reposition-end)
    ;;(add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
    ;;(add-hook 'emacs-lisp-mode-hook 'turn-off-smartparens-strict-mode)
    (add-hook 'emacs-lisp-mode-hook 'hs-hide-all)

    (defun user-python-mode-hooks()
      (interactive)
      (when (functionp 'jedi:setup)
        (jedi:setup)
        (setq jedi:complete-on-dot t))
      (highlight-indentation-mode 0)
      (setq compile-command "python -m unittest")
      )
    (add-hook 'python-mode-hook 'user-python-mode-hooks)

    (add-hook 'scheme-mode-hook 'rainbow-delimiters-mode-enable)
    ;;(add-hook 'comint-output-filter-functions 'comint-truncate-buffer)
    ;;(add-hook 'comint-output-filter-functions 'user-comint-output-ro)
    ;;(add-hook 'shell-mode-ho 'user-comint-scroll-conservatively)
    ;;(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
    (add-hook 'before-save-hook 'delete-trailing-whitespace)
    (add-hook 'before-save-hook 'whitespace-cleanup)
    ;;(add-hook 'messages-buffer-mode-hook user-message-mode-hook)
    (add-hook 'isearch-mode-hook 'user-isearch-mode-name)
    (add-hook 'debugger-mode-hook 'debugger-mode-user)
    (add-hook 'diff-mode-hook 'diff-mode-hook-init)
    (add-hook 'elpy-mode-hook 'flycheck-mode)
    (elpy-enable)
    )
  (with-eval-after-load 'gud
    (setq pdb-path '/usr/lib/python3.7/pdb.py
          gud-pdb-command-name (symbol-name pdb-path)
          )

    )
  (with-eval-after-load "startup"
    (defalias 'startup-echo-area-message "")

    (setq inhibit-startup-screen t
          ;;inhibit-startup-echo-area-message user-name
          initial-major-mode 'text-mode
          inhibit-default-init t
          initial-scratch-message nil
          auto-save-list-file-prefix (concat temporary-file-directory ".save-")
          initial-buffer-choice
          (lambda () (kill-buffer "*scratch*") (get-buffer-create "*scratch*"))))
  (with-eval-after-load "bindings"
    ;;(setq-default mode-line-format "%-")
    (setq-default mode-line-format
                  `((:propertize "-" face mode-line)
                    (:eval
                     (cond
                      ((eq major-mode 'erc-mode) (user-modeline-erc))
                      ((string-match "^notmuch-.*" (symbol-name major-mode)) (user-modeline-notmuch))
                      ((string-match "rcirc*" (symbol-name major-mode)) (user-modeline-rcirc))
                      (t (user-modeline))))
                    (:eval (user-modeline-func '(global-mode-string modeline-highlight) 't)))))
  (with-eval-after-load "subr"
    (defun backward-same-syntax ()
      "Call `forward-same-syntax' with negative ARG."
      (interactive)
      (forward-same-syntax -1))

    ;;(setq user-emacs-directory user-path-emacs)
    )
  (with-eval-after-load 'proced
    (setq proced-filter 'all-running
          proced-format 'short
          proced-sort 'STime
          proced-descend t
          proced-goal-attribute 'args

          proced-format-alist
          '((short user pid tree pcpu pmem start time (args comm))
            (medium user pid tree pcpu pmem vsize rss ttname state start time (args comm))
            (long user euid group pid tree pri nice pcpu pmem vsize rss ttname state
                  start time (args comm))
            (verbose user euid group egid pid ppid tree pgrp sess pri nice pcpu pmem
                     state thcount vsize rss ttname tpgid minflt majflt cminflt cmajflt
                     start time utime stime ctime cutime cstime etime (args comm)))

          proced-filter-alist
          `((user (user . ,(concat "\\`" (regexp-quote (user-real-login-name)) "\\'")))
            (user-running (user . ,(concat "\\`" (regexp-quote (user-real-login-name)) "\\'"))
                          (state . "\\`[Rr]\\'"))
            (all)
            (all-running (state . "\\`[Rr]\\'"))
            (emacs (fun-all . (lambda (list)
                                (proced-filter-children list ,(emacs-pid))))))
          ))
  (with-eval-after-load 'gud
    (with-eval-after-load gdb-mi
      (setq gdb-show-main t
            gdb-many-windows t)
      ;;gdb-display-<buffertype>-buffer
      ;;gdb-frame-<buffertype>-buffer

      )
    )
  (with-eval-after-load 'outline
    (setcar (cdr (assq 'outline-minor-mode minor-mode-alist)) "|Ol")
    )
  (with-eval-after-load 'dired
    (require 'openwith nil t)
    ;;(require 'ls-lisp nil t)
    ;;(require 'dired-x nil t)

    (defvar dired-re-extension-image '(".png" ".jpg"))
    (defvar dired-re-extension-image-related '(".pdf"))
    (defvar dired-re-extension-compressed '(".gz" ".tar"))

    (setq dired-recursive-deletes 'top
          dired-deletion-confirmer 'y-or-n-p
          dired-no-confirm nil
          dired-recursive-deletes nil
          dired-copy-preserve-time t
          dired-recursive-copies 'top
          dired-auto-revert-buffer t
          dired-listing-switches "-alt --group-directories-first"
          )
    )
  (with-eval-after-load 'log-edit
    (setq log-edit-keep-buffer nil
          log-edit-require-final-newline nil
          log-edit-strip-single-file-name nil)
    )
  (with-eval-after-load 'scheme
    (setq scheme-program-name "csi -:c")
    (put 'module 'scheme-indent-function 'user-scheme-indent-module)
    (put 'with-test-prefix 'scheme-indent-function 1)
    (put 'and-let* 'scheme-indent-function 1)
    (put 'parameterize 'scheme-indent-function 1)
    (put 'handle-exceptions 'scheme-indent-function 1)
    (put 'when 'scheme-indent-function 1)
    (put 'unless 'scheme-indent-function 1)
    (put 'match 'scheme-indent-function 1)
    )
  (with-eval-after-load 'url-cache
    (setq url-cache-directory
          (expand-file-name "cache" url-configuration-directory))
    )
  (with-eval-after-load 'url
    (setq url-configuration-directory
          (expand-file-name "url" temporary-file-directory))
    )
  (with-eval-after-load 're-builder
    (setq reb-re-syntax 'read
          reb-auto-match-limit 200))
  (with-eval-after-load 'menu-bar
    (menu-bar-mode 0)
    )
  (with-eval-after-load 'conf-mode
    (setq conf-assignment-column 24
          conf-colon-assignment-column (- (abs conf-assignment-column))
          conf-assignment-space t)
    )
  (with-eval-after-load 'eshell
    (with-eval-after-load 'em-banner
      (setq eshell-banner-message ""))
    (with-eval-after-load 'em-prompt
      (setq eshell-prompt-function (function
                                    (lambda ()
                                      (if eshell-mode ">" "?> ")))
            eshell-prompt-regexp "^[^#$\n]*>"))
    )
  (with-eval-after-load 'mouse
    (setq mouse-drag-copy-region t)
    )
  (with-eval-after-load 'tool-bar
    (tool-bar-mode 0))
  (with-eval-after-load 'tooltip
    (tooltip-mode 0))
  (with-eval-after-load 'time
    (setq display-time-24hr-format t)
    (display-time-mode 0)
    )
  (with-eval-after-load 'ido
    (require 'flx-ido nil t)
    (setq ido-enable-flex-matching 1
          ido-use-faces nil
          ido-ignore-buffers '("\\` " "^\*")
          ido-save-directory-list-file (locate-user-emacs-file "ido.last")
          ;;(with-current-buffer name (derived-mode-p 'c-mode))
          ;;(and (string-match "^\*" name) (not (string= name "*foo*")))
          )

    (ido-mode 1)
    (ido-everywhere 1)
    )
  (with-eval-after-load 'info
    (add-to-list 'Info-directory-list "~/data/info")
    )
  (with-eval-after-load 'minibuffer
    (setq completion-ignore-case t
          read-file-name-completion-ignore-case t
          read-buffer-completion-ignore-case t)
    )
  (with-eval-after-load 'compile
    (setq compilation-ask-about-save nil
          compilation-scroll-output t)
    )
  (with-eval-after-load 'thingatpt
    (defun eval-region-sexp-defun (&optional arg)
      "Determine which evaluation to use."
      (interactive "P")
      (cond
       ((use-region-p)
        (eval-region (region-beginning) (region-end)))
       ((thing-at-point 'sexp)
        (end-of-thing 'sexp) (eval-last-sexp arg))
       ((thing-at-point 'list)
        (end-of-thing 'list) (eval-defun arg))))
    )
  (with-eval-after-load 'package
    (setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                             ("marmalade" . "https://marmalade-repo.org/packages/")
                             ("melpa" . "https://melpa.org/packages/"))
          package-user-dir (locate-user-emacs-file "elpa"))

    (package-initialize)
    (when (not package-archive-contents)
      (package-refresh-contents)))
  (with-eval-after-load 'electric
    (setq electric-pair-skip-self t
          electric-pair-pairs '((?\" . ?\")))

    (defadvice electric-pair-mode (before electric-pair-mode-local activate)
      (make-local-variable 'electric-pair-mode))
    )
  (with-eval-after-load 'smerge-mode
    (setcar (cdr (assq 'smerge-mode minor-mode-alist)) "|D")
    )
  (with-eval-after-load 'comint
    (setq comint-scroll-to-bottom-on-input t
          comint-scroll-to-bottom-on-output nil
          comint-scroll-show-maximum-output t
          ;;comint-completion-autolist t
          comint-input-ignoredups t
          comint-completion-addsuffix t
          comint-buffer-maximum-size 20000
          comint-prompt-read-only t
          comint-get-old-input 'comint-get-old-input-default ;;(lambda () ""))
          comint-input-ring-size 5000

          comint-password-prompt-regexp
          (concat
           "\\(^ *\\|"
           (regexp-opt
            '("Enter" "enter" "Enter same" "enter same" "Enter the" "enter the"
              "Old" "old" "New" "new" "'s" "login"
              "Kerberos" "CVS" "UNIX" " SMB" "LDAP" "[sudo]" "Repeat" "Bad") t)
           " +\\)"
           "\\(?:" (regexp-opt password-word-equivalents) "\\|Response\\)"
           "\\(?:\\(?:, try\\)? *again\\| (empty for no passphrase)\\| (again)\\)?\
\\(?: for [^:：៖]+\\)?[:：៖]\\s *\\'")
          )


    (defun user-comint-output-ro (text)
      "Read only stdout."
      (if (member (buffer-name) user-shells)
          (let ((inhibit-read-only t)
                (out-end (process-mark (get-buffer-process (current-buffer)))))
            (put-text-property comint-last-output-start output-end 'read-only t))))

    (defun user-comint-scroll-conservatively ()
      (set (make-local-variable 'scroll-conservatively) 10))
    (defun user-comint-modeline ()
      )
    )
  (with-eval-after-load 'shell
    (let ((pager "cat")
          (shell (or
                  explicit-shell-file-name
                  (getenv "ESHELL")
                  shell-file-name)))
      (setenv "PAGER" pager)
      (setenv "SHELL" shell)
      (setenv "PS1" (concat "#" (file-name-nondirectory shell) "> ")))
    (setq ;;explicit-shell-file-name "/bin/sh"
     shell-prompt-pattern "^#[^#$%>\n]*[>] *")
    (shell-dirtrack-mode 0)
    )
  (with-eval-after-load 'ansi-color
    (setq ansi-color-for-comint-mode t)

    )
  (with-eval-after-load 'find-func

    ;; This breaks when library in subdir of `emacs-default-directory' and named '.emacs'.
    (defun find-function-search-for-symbol (symbol type library)
      "Search for SYMBOL's definition of type TYPE in LIBRARY.
Visit the library in a buffer, and return a cons cell (BUFFER . POSITION),
or just (BUFFER . nil) if the definition can't be found in the file.

If TYPE is nil, look for a function definition.
Otherwise, TYPE specifies the kind of definition,
and it is interpreted via `find-function-regexp-alist'.
The search is done in the source for library LIBRARY."
      (if (null library)
          (error "Don't know where `%s' is defined" symbol))
      ;; Some functions are defined as part of the construct
      ;; that defines something else.
      (while (and (symbolp symbol) (get symbol 'definition-name))
        (setq symbol (get symbol 'definition-name)))
      (if (string-match "\\`src/\\(.*\\.\\(c\\|m\\)\\)\\'" library)
          (find-function-C-source symbol (match-string 1 library) type)
        (when (string-match "\\.el\\(c\\)\\'" library)
          (setq library (substring library 0 (match-beginning 1))))
        ;; Strip extension from .emacs.el to make sure symbol is searched in
        ;; .emacs too.
        (when (string-match "\\.emacs\\(.el\\)" library)
          ;; library is directory; what does this not work for?
          (when (not (file-directory-p (substring library 0 (match-beginning 1))))
            (setq library (substring library 0 (match-beginning 1)))))
        (let* ((filename (find-library-name library))
               (regexp-symbol (cdr (assq type find-function-regexp-alist))))
          (with-current-buffer (find-file-noselect filename)
            (let ((regexp (format (symbol-value regexp-symbol)
                                  ;; Entry for ` (backquote) macro in loaddefs.el,
                                  ;; (defalias (quote \`)..., has a \ but
                                  ;; (symbol-name symbol) doesn't.  Add an
                                  ;; optional \ to catch this.
                                  (concat "\\\\?"
                                          (regexp-quote (symbol-name symbol)))))
                  (case-fold-search))
              (with-syntax-table emacs-lisp-mode-syntax-table
                (goto-char (point-min))
                (if (or (re-search-forward regexp nil t)
                        ;; `regexp' matches definitions using known forms like
                        ;; `defun', or `defvar'.  But some functions/variables
                        ;; are defined using special macros (or functions), so
                        ;; if `regexp' can't find the definition, we look for
                        ;; something of the form "(SOMETHING <symbol> ...)".
                        ;; This fails to distinguish function definitions from
                        ;; variable declarations (or even uses thereof), but is
                        ;; a good pragmatic fallback.
                        (re-search-forward
                         (concat "^([^ ]+" find-function-space-re "['(]?"
                                 (regexp-quote (symbol-name symbol))
                                 "\\_>")
                         nil t))
                    (progn
                      (beginning-of-line)
                      (cons (current-buffer) (point)))
                  (cons (current-buffer) nil))))))))

    )
  (with-eval-after-load 'view
    (setcar (cdr (assq 'view-mode minor-mode-alist)) "|V")
    (setq view-scroll-auto-exit nil
          view-inhibit-help-message t
          view-try-extend-at-buffer-end nil)
    )
  (with-eval-after-load 'repeat
    (setq repeat-message-function 'ignore)
    )
  (with-eval-after-load 'autorevert
    (global-auto-revert-mode 0)
    (setq auto-revert-mode-text "|Ar")
    )
  (with-eval-after-load 'server
    (defun server-ensure-safe-dir (dir) "Noop" t)
                                        ;(setcar (cdr (assq 'server-buffer-clients minor-mode-alist)) "|$")
    (setq server-use-tcp nil
          server-host nil
          server-port nil
          server-window 'pop-to-buffer
          server-log nil
          server-auth-dir (concat temporary-file-directory "/server")
          server-socket-dir (getenv "XDG_RUNTIME_DIR")
          server-name "emacs.sock")
    ;;(unless server-process
    ;;(server-running-p server-name)
    ;;(server-start))
    )
  (with-eval-after-load 'lisp-mode
    (defun lambda-char ()
      (mapc (lambda (major-mode)
              (font-lock-add-keywords
               major-mode
               '(("(\\(lambda\\)\\>"
                  (0 (ignore
                      (compose-region (match-beginning 1)
                                      ;;(match-end 1) ?λ))))
                                      (match-end 1) λ))))
                 ;;("(\\|" . 'lisp-paren-face))))
                 )))
            '(emacs-lisp-mode
              lisp-mode
              inferior-emacs-lisp-mode
              lisp-interaction-mode
              inferior-lisp-mode
              slime-repl-mode)))
    ;;(lambda-char)
    )
  (with-eval-after-load "replace"
    (setq case-replace t
          query-replace-highlight t
          query-replace-lazy-highlight t
          list-matching-lines-default-context-lines 0
          list-matching-lines-face 'match
          list-matching-lines-buffer-name-face nil)

    ;;(defun user-query-replace-symbol ()
    ;;  (interactive)
    ;;  (call-interactively 'query-replace-regexp)
    ;;  (symbol-name (symbol-at-point)))
    )
  (with-eval-after-load 'savehist
    (setq savehist-file (concat temporary-file-directory "/history")
          savehist-autosave-interval (* 5 60))

    (savehist-mode t))
  (with-eval-after-load 'hideshow
    ;;(require 'hideshow-org nil t)
    (setcar (cdr (assq 'hs-minor-mode minor-mode-alist)) "|+")
    (setq hs-hide-comments-when-hiding-all t
          hs-isearch-open t
          ;; hs-hide-all-non-comment-function '
          hs-set-up-overlay 'hs-overlay-line-count
          hs-allow-nesting nil)

    (defface hs-overlay-code nil
      "")
    (defface hs-overlay-comment nil
      "")
    (defvar hs-overlay-code-string " <%d>"
      "Format string for `hs-overlay-line-count' code overlays.")
    (defvar hs-overlay-comment-string " <%d>"
      "Format string for `hs-overlay-line-count' comment overlays.")

    ;;(add-to-list 'hs-special-modes-alist '(adoc-mode ("\\(^#)") nil nil ))
    ;;(add-to-list 'hs-special-modes-alist '(adoc-mode "^#*" "^#*" ".." nil nil ))
    (add-to-list 'hs-special-modes-alist
                 '(nxml-mode
                   "<!--]\\|<[^/>]*[^/]>"
                   "-->\\|</[^/>]*[^/]>"
                   "<!--"
                   sgml-skip-tag-forward
                   nil))
    )
  (with-eval-after-load 'paren
    (setq show-paren-style 'parenthesis
          show-paren-delay 0)

    (show-paren-mode t)
    )
  (with-eval-after-load "window"
    ;; user-window
    (setq user-window-same-regexp
          ;;"[ ]?[*]\\([^*]+\\|Completions\\)[*]"
          "^\\*\\(Faces\\|scratch\\|Man.*\\|compilation\\|Compile.*\\|Info\\|[mM]essages\\|Help\\|Colors\\|Backtrace\\|eshell\\|Shell.*\\|magit.*\\|[gG]eiser.*\\|Occur\\|Proced\\|Ido.*\\|.*REPL.*\\|Register.*\\|jedi:.*\\|Warnings\\|Python.*\\|gud.*\\)\\*"
          user-window-never-regexp "^\\*\\(Completions\\)\\*"
          user-window-same-sizes
          '((15 . ("^\\*\\(Messages\\|Help\\)\\*$"))
            (20 . (".*")))
          ;;(20 . ("\\*\\([Gg]eiser.*\\|[e]?[sS]hell.*\\|Info\\|Compile.*\\|.*REPL.*\\|Colors\\|Backtrace\\|Occur\\|Proced\\)\\*")))
          )
    (setq recenter-positions '(top middle bottom)
          split-height-threshold 2
          split-width-threshold 2
          window-min-height 1
          pop-up-windows nil
          split-window-preferred-function (lambda() nil)
          same-window-buffer-names nil
          same-window-regexps nil ;;user-window-same-regexp
          display-buffer-base-action '(nil)
          ;;'((display-buffer-pop-up-window display-buffer-same-window))
          ;;display-buffer-reuse-window display-buffer-use-some-window))
          display-buffer-alist
          `(;;`display-buffer-fallback-action'
            ;;display-buffer--maybe-same-window
            ;;display-buffer-reuse-window
            ;;display-buffer--maybe-pop-up-frame-or-window
            ;;display-buffer-in-previous-window
            ;;display-buffer-use-some-window
            ;;`display-buffer-same-window'
            ;;`display-buffer-reuse-window'
            ;;`display-buffer-pop-up-frame'
            ;;`display-buffer-pop-up-window'
            ;;`display-buffer-in-previous-window'
            ;;`display-buffer-use-some-window'
            ;;(user-display-add-window-frame)
            ;;Bottom window
            (,user-window-same-regexp
             (user-display-buffer-same display-buffer-at-bottom)
             (display-buffer-reuse-window display-buffer-at-bottom)
             (window-height . user-window-set-split-size)
                                        ;(display-buffer-use-some-window)
             (inhibit-same-window . t)
             ;;Ignored
             ("\\*.*\\*")
             )))
    )
  (with-eval-after-load 'winner
    (defcustom windowmove-select-hook nil
      "Hook after `select-window'."
      :type 'hook
      :group 'windowmove)
    (defun windmove-do-window-select (dir &optional arg window)
      "Move to the window at direction DIR.
DIR, ARG, and WINDOW are handled as by `windmove-other-window-loc'.
If no window is at direction DIR, an error is signaled."
      (let ((other-window (windmove-find-other-window dir arg window)))
        (cond ((null other-window)
               (error "No window %s from selected window" dir))
              ((and (window-minibuffer-p other-window)
                    (not (minibuffer-window-active-p other-window)))
               (error "Minibuffer is inactive"))
              (t
               (select-window other-window)
               (run-hooks 'windowmove-select-hook)))))

    (winner-mode t)
    ;; (defun window-only-undo ()
    ;;   "Delete other windows, unless one window, then undo window deletion."
    ;;   (interactive)
    ;;    (if (not (eq 0 (ring-length (winner-ring (selected-frame)))))
    ;;     (winner-set (ring-remove (winner-ring (selected-frame))))
    ;;   (winner-save-conditionally)
    ;;   (delete-other-windows (selected-window))))
    )
  (with-eval-after-load "files"
    (setq-default version-control 'never
                  kept-new-versions 2
                  kept-old-versions 2
                  require-final-newline t
                  )
    (setq save-abbrevs nil
          find-file-visit-truename t
          auto-save-default t
          backup-by-copying t
          break-hardlink-on-save nil
          confirm-kill-emacs nil
          delete-auto-save-files t
          delete-old-versions t
          dired-kept-versions 2
          make-backup-files t
          revert-without-query nil
          view-read-only t

          backup-directory-alist `(("." . ,temporary-file-directory))
          auto-save-file-name-transforms
          `(("\\`/[^/]*:\\([^/]*/\\)*\\([^/]*\\)\\'" ,temporary-file-directory t)
            (".*" ,temporary-file-directory t))
          )
    (setq auto-mode-alist
          (append `(("\\.org\\'" . org-mode)
                    ("\\.py\\'" . python-mode)
                    ;;("\\.\\(sh\\|bash\\)\\'" .
                    ;;("\\.\\(txt\\|md\\)\\'" . markdown-mode)
                    ("\\.\\(scm\\|xmls\\|scss\\)\\'" . scheme-mode)
                                        ;("\\.\\(sxml\\)\\'" . sxml-mode)
                    ("\\.\\(sxml\\)\\'" . scheme-mode)
                    ("\\.\\(?:gemspec\\|irbrc\\|gemrc\\|rake\\|rb\\|ru\\|thor\\)\\'" . ruby-mode)
                    ("\\(Capfile\\|Gemfile\\(?:\\.[a-zA-Z0-9._-]+\\)?\\|[rR]akefile\\)\\'" . ruby-mode)
                    ,(cons (concat "\\."
                                   (regexp-opt '("rest"
                                                 "rst") t)
                                   "\\'") 'rst-mode)
                    ;; ("\\.info\\'" . Info-mode)
                    ,(cons (concat "\\."
                                   (regexp-opt '("xml"
                                                 "xsd"
                                                 "sch"
                                                 "rng"
                                                 "xslt"
                                                 "svg"
                                                 "rss") t)
                                   "\\'") 'nxml-mode)
                    ("\\.ucs\\'" . sh-mode))
                  auto-mode-alist))
    (delq (assoc "[acjkwz]sh" interpreter-mode-alist) interpreter-mode-alist)
    (delq (assoc "sh5?" interpreter-mode-alist) interpreter-mode-alist)
    (add-to-list 'interpreter-mode-alist
                 '("python" . python-mode))

    (make-directory temporary-file-directory t)
    ;;(make-directory small-temporary-file-directory t)
    )
  (with-eval-after-load "indent"
    ;; (defadvice beginning-of-line-text (after return-point)
    ;;   "Return `point'."
    ;;   (setq ad-return-value (point))
    ;;   ad-return-value
    ;;   )
    (defun beginning-of-line-text (&optional n)
      "Move to the beginning of the text on this line.
With optional argument, move forward N-1 lines first.
From the beginning of the line, moves past the left-margin indentation, the
fill-prefix, and any indentation used for centering or right-justifying the
line, but does not move past any whitespace that was explicitly inserted
\(such as a tab used to indent the first line of a paragraph)."
      (interactive "p")
      (beginning-of-line n)
      (skip-chars-forward " \t")
      ;; Skip over fill-prefix.
      (if (and fill-prefix
               (not (string-equal fill-prefix "")))
          (if (equal fill-prefix
                     (buffer-substring
                      (point) (min (point-max) (+ (length fill-prefix) (point)))))
              (forward-char (length fill-prefix)))
        (if (and adaptive-fill-mode adaptive-fill-regexp
                 (looking-at adaptive-fill-regexp))
            (goto-char (match-end 0))))
      ;; Skip centering or flushright indentation
      (if (memq (current-justification) '(center right))
          (skip-chars-forward " \t"))
      (point)
      )
    )
  (with-eval-after-load 'jka-compr
    (setq jka-compr-temp-name-template temporary-file-directory))
  (with-eval-after-load 'jka-cmpr-hook
    ;; (setcar (cdr (assq 'jka-compr-compression-info-list)) "|M")
    (auto-compression-mode t)
    (custom-set-variables
     '(jka-compr-compression-info-list
       '(["\\.Z\\(~\\|\\.~[0-9]+~\\)?\\'"
          "compressing" "compress" ("-c")
          "uncompressing" "gzip" ("-c" "-d")
          nil t "\235"]
         ["\\.bz2\\(~\\|\\.~[0-9]+~\\)?\\'"
          "bzip2ing" "bzip2" nil
          "bunzip2ing" "bzip2" ("-d")
          nil t "BZh"]
         ["\\.tbz2?\\'"
          "bzip2ing" "bzip2" nil
          "bunzip2ing" "bzip2" ("-d")
          nil nil "BZh"]
         ["\\.\\(?:tgz\\|svgz\\|sifz\\)\\(~\\|\\.~[0-9]+~\\)?\\'"
          "compressing" "gzip" ("-c")
          "uncompressing" "gzip" ("-c" "-d")
          t nil "\213"]
         ["\\.g?z\\(~\\|\\.~[0-9]+~\\)?\\'"
          "compressing" "gzip" ("-c" )
          "uncompressing" "gzip" ("-c" "-d")
          t t "\213"]
         ["\\.xz\\(~\\|\\.~[0-9]+~\\)?\\'"
          "XZ compressing" "xz" ("-c" "-q")
          "XZ uncompressing" "xz" ("-c" "-d")
          t t "\3757zXZ�"]
         ["\\.dz\\'"
          nil nil nil
          "uncompressing" "gzip" ("-c" "-d")
          nil t "\213"])
       ))
    )
  (with-eval-after-load 'select
    (setq select-enable-primary nil
          select-enable-clipboard t
          x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)
          interprogram-cut-function 'clipboard-copy
          )
    )
  (with-eval-after-load 'auth-source
    (setq auth-source-cache-expiry nil
          auth-source-do-cache t
          auth-source-debug t
          auth-source-save-behavior nil
          )
    )
  (with-eval-after-load 'password-cache
    (setq password-cache-expiry nil))
  (with-eval-after-load 'smtpmail
    (require 'smtpmail-multi)

    (setq smtpmail-stream-type 'starttls
          smtpmail-debug-info t
          smtpmail-debug-verb t
          smtpmail-queue-dir (expand-file-name "smtp" user-path-emacs)
          smtpmail-queue-index-file "queue")

    )
  (with-eval-after-load 'frame
    (defun frame-list-name ()
      (interactive)
      (reverse (mapcar
                (lambda (x)
                  (frame-parameter x 'name))
                (frame-list))))
    ;;(defun frame-change ()
    ;;  (interactive "SParameter: \nxValue: ")
    ;;  (set-frame-parameter nil var val))
    (defun frame-other-neg ()
      (interactive)
      (other-frame -1))

    (setq frame-background-mode nil
          initial-frame-alist
          '(;(name . "F1")
            (minibuffer . t))
          default-frame-alist
          '((buffer-predicate . user-frame-buffer-stick-check)))

    (blink-cursor-mode 0)
    )
  (with-eval-after-load 'font-lock
    ;; font-lock-add-keywords
    (setq font-lock-maximum-decoration t
          font-lock-support-mode 'jit-lock-mode)

    (global-font-lock-mode t)
    )
  (with-eval-after-load "isearch"
    (setq search-whitespace-regexp "\\s-+"
          search-highlight t
          regexp-search-ring-max 40
          search-ring-max 40)

    (defun user-isearch-mode-name ()
      (setq isearch-mode "|x"))

    (defun isearch-yank-point-symbol ()
      "Yank word at point into search"
      (interactive)
      (call-interactively 'isearch-forward-regexp)
      (let ((string (symbol-name (symbol-at-point))))
        (when (not (null string))
          (isearch-yank-string string))))
    (defun isearch-kill ()
      (interactive)
      (kill-new (buffer-substring (point) isearch-other-end))
      (iserach-done))
    )
  (with-eval-after-load 'hi-lock
    ;;(setcar (cdr (assq 'hi-lock-mode minor-mode-alist)) "|Hl")
    (setq hi-lock-file-patterns-range 10000
          hi-lock-highlight-range 200000
          hi-lock-file-patterns-policy 'ask)
    )
  (with-eval-after-load 'hl-line
    (defvar hl-line-reposition nil
      "Save state of `hl-line-mode'; used by `hl-line-reposition'.")
    ;;(make-variable-buffer-local 'hl-line-reposition)

    (defun hl-line-reposition ()
      "When `move-to-window-line-top-bottom', highlight point line."
      (interactive)
      (setq hl-line-reposition hl-line-mode)
      (move-to-window-line-top-bottom)
      (unless hl-line-reposition
        (hl-line-mode t)
        ))
    (defun hl-line-reposition-end ()
      "Undo `hl-line-reposition'."
      (unless hl-line-reposition
        (hl-line-mode 0)
        (remove-hook 'pre-command-hook 'hl-line-reposition-end)))
    )
  (with-eval-after-load 'kmacro
    (setcar (cdr (assq 'defining-kbd-macro minor-mode-alist)) "|K")
    (setq kmacro-ring-max 10)

    (defun kbd-macro-undo-wrapper (x)
      "Wrap `kbd-macro-undo-wrapper' in `undo-boundary-skip'; execute default behaviour with '-' argument."
      (interactive "P")
      (if (eq x '-)
          (call-last-kbd-macro 1)
        (undo-boundary-skip call-last-kbd-macro x)))

    (defadvice kmacro-exec-ring-item (around undo-boundary activate compile)
      "Wrap `undo-boundary-skip' around kbd-macro."
      (undo-boundary-skip
       ad-do-it))
    )
  (with-eval-after-load 'company
    (setcar (cdr (assq 'company-mode minor-mode-alist)) "|C")
    (setcar (cdr (assq 'company-search-mode minor-mode-alist))
            (concat (car (cdr (assq 'company-mode minor-mode-alist))) "_"))

    (setq company-tooltip-limit 30
          company-tooltip-margin 1
          company-tooltip-offset-display 'lines
          company-tooltip-flip-when-above nil
          company-minimum-prefix-length 2
          company-idle-delay nil
          company-show-numbers nil
          company-selection-wrap-around t
          ;;company-global-modes t
          )
    )
  (with-eval-after-load 'debug
    (defun debugger-mode-user ()
      "`debugger-mode-hook'."
      (with-current-buffer "*Backtrace*"
        (when truncate-lines
          (setq truncate-lines nil))
        (when (not word-wrap)
          (setq word-wrap t))))
    ;; (defadvice debug (after pp-debug deactivated compile)
    ;;   "`debugger-mode-hook'."
    ;;   (with-current-buffer "*Backtrace*"
    ;;   (let ((inhibit-read-only t)
    ;;         (indent-line-function 'lisp-indent-line))
    ;;     (pp-buffer)
    ;;     (indent-region (point-min) (point-max))))
    ;;   ad-return-value)

    )
  (with-eval-after-load 'abbrev
    (setcar (cdr (assq 'abbrev-mode minor-mode-alist)) "|Av")
    ;;(setq
    ;;abbrev-file-name nil
    ;;abbrev-table-name-list '(fundamental-mode-abbrev-table
    ;;global-abbrev-table)
    ;;global-abbrev-table nil
    ;;     )
    (defun user-abbrev-expand-remove-space-end ()
      (backward-char 1)
      (delete-char -1)
      (forward-char)
      (execute-kbd-macro (this-command-keys-vector))
      t)
    (defun user-abbrev-expand-remove-space-beg ()
      ;;(when (looking-back ""))
      (backward-char -1)
      t)

    (put 'user-abbrev-expand-remove-space-beg 'no-self-insert t)
    (put 'user-abbrev-expand-remove-space-end 'no-self-insert t)

    (define-abbrev-table 'global-abbrev-table
      '(("0b" "〔" user-abbrev-expand-remove-space-beg) ;;LEFT TORTISE SHELL BRACKET
        ("0B" "〕" user-abbrev-expand-remove-space-end) ;;RIGHT TORTISE SHELL BRACKET
        ("0q" "“" user-abbrev-expand-remove-space-beg) ;;LEFT DOUBLE QUOTATION MARK
        ("0Q" "”" user-abbrev-expand-remove-space-end) ;;RIGHT DOUBLE QUOTATION MARK
        ("0c" "「" user-abbrev-expand-remove-space-beg) ;;LEFT CORNER BRACKET
        ("0C" "」" user-abbrev-expand-remove-space-end) ;;RIGHT CORNER BRACKET
        ))

    (abbrev-mode 1)
    )
  (with-eval-after-load 'sort
    (defun sort-lexi (begin end)
      (interactive "r")
      (sort-regexp-fields nil "^[^0-9]*\\([0-9]+\\).*$" "\\1" begin end))
    (defadvice sort-subr (before sort-subr-compare-lexi activate)
      (when (eq this-command 'sort-lexi)
        (ad-set-arg 5 'compare-buffer-substr-lexi)))

    ;;(setq sort-fold-case t)
    )
  (with-eval-after-load 'browse-url
    (setq browse-url-browser-function 'browse-url-elinks
          browse-url-new-window-flag nil
          browse-url-text-input-field 'avoid
          browse-url-text-input-attempts 10
          browse-url-text-input-delay 0.2
          browse-url-elinks-wrapper '("urxvtc" "-e")
          browse-url-text-browser "elinks")
    )
  (with-eval-after-load 'flyspell
    (setq flyspell-highlight-flag t
          flyspell-mark-duplications-flag t
          flyspell-sort-corrections nil
          flyspell-duplicate-distance -1
          flyspell-delay 3
          flyspell-persistent-highlight t
          flyspell-mode-line-string "|Fy"
          flyspell-before-incorrect-word-string nil
          flyspell-after-incorrect-word-string nil
          flyspell-use-meta-tab nil
          flyspell-auto-correct-binding nil

          flyspell-delayed-commands nil
          flyspell-deplacement-commands nil
          flyspell-issue-welcome-flag t

          flyspell-default-dictionary nil
          flyspell-mark-duplications-exceptions '((nil . ("that" "had"))
                                                  ("\\`francais" . ("nous" "vous")))
          )
    )
  (with-eval-after-load 'mwheel
    (mouse-wheel-mode 0))
  (with-eval-after-load 'ispell
    (setq ispell-program-name "aspell"
          ispell-list-command "list"
          ;;ispell-extra-args '("--sug-mode=ultra")
          )
    )
  (with-eval-after-load 'align
    (setq ;align-text-modes (cons 'conf-mode align-text-modes)
     align-indent-before-aligning t)
    ;;(add-to-list 'align-rules-list
    ;;             '(text-column-equals
    ;;               (regexp . "\\(\\s-*\\)=")
    ;;               (group . 1)
    ;;               (modes . align-text-modes)
    ;;               (repeat . t)))
    )
  (with-eval-after-load 'diff
    ;; (setcar (cdr (assq 'diff-minor-mode minor-mode-alist)) "|D")

    (defun diff-mode-hook-init ()
      (diff-auto-refine-mode)
      ;; (diff-context->unified)
      )
    )
  (with-eval-after-load 'vc-dispatcher
    (defadvice vc-setup-buffer (after set-buffer-name activate)
      (setq vc-parent-buffer-name (buffer-name camefrom)))

    )
  (with-eval-after-load 'vc-hooks
    (setq vc-follow-symlinks 't
          vc-handled-backends nil
          vc-ignore-dir-regexp (format "\\(%s\\)\\|\\(%s\\)"
                                       vc-ignore-dir-regexp
                                       tramp-file-name-regexp)))
  (with-eval-after-load 'simple
    (setcar (cdr (assq 'visual-line-mode minor-mode-alist)) "|W")
    (setcar (cdr (assq 'auto-fill-function minor-mode-alist)) "|F")
    ;;(setcar (cdr (assq 'global-visual-line-mode minor-mode-alist)) "")
    (setq next-line-add-newlines nil
          kill-ring-max 1500
          set-mark-command-repeat-pop t
          line-move-visual t
          global-mark-ring-max 30
          suggest-key-bindings t
          yank-pop-change-selection t
          save-interprogram-paste-before-kill t
          mark-ring-max global-mark-ring-max
          ;;interprogram-paste-function 'nil
          ;;interprogram-cut-function 'nil
          interprogram-paste-function 'x-selection-value

          visual-line-fringe-indicators '(light-left-curly-arrow light-right-curly-arrow)
          blink-matching-delay 0
          blink-matching-paren-dont-ignore-comments nil
          )

    (line-number-mode t)
    (column-number-mode t)
    (size-indication-mode t)
    (transient-mark-mode t)
    (blink-matching-open)

    ;;(global-set-key [remap move-beginning-of-line] 'smarter-move-beginning-of-line)
    )
  (with-eval-after-load "mule-cmds"
    (setq default-input-method "UTF-8")
    )
  (with-eval-after-load 'calc
    (setq calc-settings-file nil)
    )
  (with-eval-after-load 'face-remap
    (setcar (cdr (assq 'buffer-face-mode minor-mode-alist )) "|BF")
    )
  )
(progn ;; External
  (with-eval-after-load 'geiser
    ;;(require 'geiser-doc)
    (with-eval-after-load 'geiser-mode
      (setq-default geiser-mode-string "|G")
      (setq geiser-mode-autodoc-p nil
            geiser-mode-start-repl-p t
            geiser-mode-company-p nil
            geiser-mode-smart-tab-p t)

      )
    (with-eval-after-load 'geiser-autodoc
      (setq-default geiser-autodoc-mode-string "_D")
      (setq geiser-autodoc-identifier-format "%s:%s")
      )
    (with-eval-after-load 'geiser-completion
      (setq-default geiser-smart-tab-mode-string "_T")
      )
    (with-eval-after-load 'geiser-impl
      (setq geiser-default-implementation 'chicken
            geiser-active-implementations '(chicken))
      )
    (with-eval-after-load 'geiser-repl
      (setq geiser-repl-skip-version-check-p t
            geiser-repl-history-filename (expand-file-name ".geiser_history" user-path-emacs)))
    (with-eval-after-load 'geiser-edit
      (setq geiser-edit-symbol-method 'window))
    (with-eval-after-load 'geiser-guile
      (setq geiser-guile-init-file "~/.guile_user/init_emacs.scm")
      )
    (with-eval-after-load 'geiser-debug
      (setq geiser-debug-jump-to-debug-p nil
            geiser-debug-auto-display-images-p nil)
      )
    (with-eval-after-load 'geiser-chicken
      (setq user-geiser-chicken-external-url "http://localhost:8080/cdoc?q=%s&query-name=Look+up"
            geiser-chicken--prompt-regexp "\\(#;[0-9]*>[ ]?\\|#[0-9]*(.*)>\\)"
            ;;"#[^;]*;[^:0-9]*:?[0-9]+> "
            ;;geiser-chicken-init-file "~/.bin/user.scm"
            )))
  (with-eval-after-load 'flycheck
    (setcar (cdr (assq 'flycheck-mode minor-mode-alist)) "|Sx")
    (global-flycheck-mode)
    )
  (with-eval-after-load 'elpy
    (setcar (cdr (assq 'elpy-mode minor-mode-alist)) "|Epy")
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    )
  (with-eval-after-load 'smartparens
    (setcar (cdr (assq 'smartparens-mode minor-mode-alist)) "|Sp")
    )
  ;;(with-eval-after-load 'highlight-indentation
    ;;(setcar (cdr (assq 'highlight-indentation-mode minor-mode-alist)) "|HI")
    ;;)
  (with-eval-after-load "openwith"
    (setq application-pdf "xpdf"
          application-image (concat "feh" " --rcfile " user-conf-feh)
          ;;          application-video (concat "mplayer" " -include " (getenv "CONF_MPLAYER"))
          application-audio (concat "mplayer" " -include " user-conf-mplayer)
          )

    (setq openwith-confirm-invocation nil
          ;;openwith-associations
          ;;(list
          ;;(list (regexp-make-extension '("pdf")) application-pdf '(file))
          ;;(list (regexp-make-extension '("png" "jgp")) application-image '(file))
          ;;(list (regexp-make-extension '("mp3")) application-audio '(file))
          ;;(list (regexp-make-extension '("avi" "mov")) application-video '(file))
          )

    (openwith-mode t)
    )
  (with-eval-after-load 'magit
    (magit-auto-revert-mode 0)
    (setq global-magit-file-mode nil
          magit-buffer-name-format "*%M%v: %t*"
          magit-refresh-status-buffer nil
          magit-refresh-verbose t
          ;;magit-refresh-buffer-hook
          ;;magit-post-refresh-hook
          ;;magit-status-refresh-hook
          ;;magit-pre-refresh-hook
          )

    ;;(remove-hook 'server-switch-hook 'magit-commit-diff)
    )
  (with-eval-after-load 'with-editor
    (setcar (cdr (assq 'with-editor-mode minor-mode-alist)) "|E")
    )
  (with-eval-after-load 'skewer-mode
    (setcar (cdr (assq 'skewer-mode minor-mode-alist)) "|Sx")
    )
  (with-eval-after-load 'notmuch
    (setq notmuch-init-file nil
          notmuch-show-logo nil
          notmuch-hello-thousands-separator ""
          notmuch-saved-search-sort-function nil
          notmuch-search-oldest-first nil
          notmuch-fcc-dirs nil
          notmuch-show-mark-read-tags '("-unread")
          notmuch-archive-tags '("-inbox")
          notmuch-search-result-format `(("date" . "%12s ")
                                         ("count" . "%-7s ")
                                         ("authors" . "%-20s ")
                                         ("subject" . "%s ")
                                         ("tags" . "(%s)"))
          notmuch-tree-result-format `(("date" . "%12s")
                                       ("authors" . "%-20s")
                                       ((("tree" . "%s")("subject" . "%s")) ." %-54s ")
                                       ("tags" . "(%s)"))
          notmuch-saved-searches `((:name "inbox" :query "tag:inbox" :key "i")
                                   (:name "unread" :query "tag:unread" :key "u")
                                   (:name "flagged" :query "tag:flagged" :key "f")
                                   (:name "sent" :query "tag:sent" :key "t")
                                   (:name "drafts" :query "tag:draft" :key "d")
                                   (:name "all" :query "*" :key "a"))
          notmuch-hello-sections (list #'user-notmuch-hello-insert-header
                                       #'user-notmuch-hello-insert-saved-searches
                                       #'notmuch-hello-insert-search
                                       #'notmuch-hello-insert-recent-searches
                                       #'notmuch-hello-insert-alltags
                                       ;;#'notmuch-hello-insert-footer
                                       )
          )
    )
  (with-eval-after-load 'simple-httpd
    (setq httpd-port 8090
          httpd-root user-path-tmp))
  (with-eval-after-load 'auto-complete
    (setcar (cdr (assq 'auto-complete-mode minor-mode-alist)) "|Ct")
    (setq ac-delay .1
          ac-use-quick-help 't
          ac-menu-height 30
          ac-quick-help-height 30
          ac-ignore-case 'smart))
  )
(progn ;; Sort
  (with-eval-after-load "android"
    (setq android-mode-sdk-dir "/opt/android-sdk/tools/android")
    )
  (with-eval-after-load "calculator"
    (setq calculator-use-menu nil
          calculator-unary-style 'postfix
          calculator-prompt "%s%% "
          calculator-number-digits 3
          calculator-radix-grouping-mode t
          calculator-radix-grouping-digits 4
          calculator-radix-grouping-separator "'"
          calculator-displayer '(std ?n)
          calculator-user-operators '((\"tf\" cl-to-fr (+ 32 (/ (* X 9) 5)) 1)
                                      (\"tc\" fr-to-cl (/ (* (- X 32) 5) 9) 1)
                                      (\"tp\" kg-to-lb (/ X 0.453592)       1)
                                      (\"tk\" lb-to-kg (* X 0.453592)       1)
                                      (\"tF\" mt-to-ft (/ X 0.3048)         1)
                                      (\"tM\" ft-to-mt (* X 0.3048)         1)
                                      )
          )
    )
  (with-eval-after-load "eldoc"
    (setq eldoc-idle-delay 0.50
          eldoc-minor-mode-string (purecopy "|El")
          eldoc-echo-area-use-multiline-p 'truncate-sym-name-if-fit))
  (with-eval-after-load "imap"
    (setq imap-log t
          imap-debug t)
    )
  (with-eval-after-load "mail-source"
    (setq mail-source-flash nil
          mail-source-ignore-errors nil
          mail-source-delete-incoming 10
          mail-source-incoming-file-prefix "incoming"
          mail-source-report-new-mail-interval 15
          mail-source-idle-time-delay 30
          mail-source-crash-box (concat temporary-file-directory "/emacs_mail_crash"))
    )
  (with-eval-after-load "mime-view"
    (setq mime-view-mailcap-files '("/etc/mailcap" "~/.mime/mailcap")
          mime-view-buttons-visible t
          mime-preview-move-scroll nil
          )
    )
  (with-eval-after-load "minimap"
    (setcar (cdr (assq 'minimap-mode minor-mode-alist)) "|Sw")
    (setq minimap-width-fraction 0.2
          minimap-window-location 'left
          minimap-buffer-name-prefix "*sumwin*"
          minimap-update-delay 0.2
          minimap-always-recenter nil
          minimap-recenter-type 'relative)
    )
  (with-eval-after-load "mm-decode"
    (defun mm-inline-excel (handle)
      "Insert MS Excel attachment described in HANDLE into article bufffer."
      (let ((file (mm-make-temp-file "xls.")))
        (mm-with-unibyte-buffer
          (mm-insert-part handle)
          (write-file file))
        (insert (shell-command-to-string
                 (format "xlhtml -nc -te %s|w3m -dump -T text/html"
                         file))))
      (goto-char (point-max))
      (mm-handle-set-undisplayer
       handle
       `(lambda ()
          (let (buffer-read-only)
            (delete-region ,(point-min-marker)
                           ,(point-max-marker))))))

    (setq mm-discouraged-alternatives '("text/html" "text/richtext"))
    (add-to-list 'mm-inlined-types "application/vnd.ms-excel")
    )
  (with-eval-after-load "mm-url"
    (setq mm-url-retries 2
          mm-url-timeout 10)
    )
  (with-eval-after-load "mml"
    (setcar (cdr (assq 'mml-mode minor-mode-alist)) "|MML")
    )
  (with-eval-after-load "nxml-mode"
    (fset 'xml-mode 'nxml-mode)
    (fset 'html-mode 'nxml-mode)
    (fset 'sgml-mode 'nxml-mode)
    (setq nxml-slash-auto-complete-flag t)
    )
  (with-eval-after-load "pastebox"
    (setq pastebox-url "http://pastebin.ch/new"
          pastebox-user-name user-nick
          pastebox-paste-url-buffer "*Pastebox*"
          pastebox-be-annoying t)

    (defadvice pastebox-region (before input activate)
      "Read variable values."
      (interactive "sSubject:\nsDescription:"
                   (let ((pastebox-subject subj)
                         (pastebox-description desc))))))
  (with-eval-after-load "popup-kill-ring"
    (setq popup-kill-ring-interactive-insert nil
          popup-kill-ring-popup-margin-left 1
          popup-kill-ring-popup-margin-right 1
          popup-kill-ring-popup-width (/ (window-body-width) 2)))
  (with-eval-after-load "pos-tip"
    (setq pos-tip-border-width 1
          pos-tip-internal-border-width 0))
  (with-eval-after-load 'python
    (setq python-mode-modeline-display "Py"
          ;;py-verbose-p nil
          py-outline-minor-mode-p 'outline-minor-mode
          py-hide-show-minor-mode-p 'hs-minor-mode
          ;;py-indent-no-completion-p nil
          ;;py-custom-temp-directory temporary-file-directory
          py-match-paren-mode show-paren-mode
          ;;py-beep-if-tab-change nil
          ;;py-pdbtrack-minor-mode-string "PyDB"
          py-hide-show-hide-docstrings t
          ;;
          ;;py-install-directory (concat user-path-emacs "/python")
          ;;py-python-history (expand-file-name "python_history" temporary-file-directory)
          ;;py-ipython-history py-python-history
          python-shell-interpreter "ipython"
          python-shell-interpreter-args "--simple-prompt -i"
          )
    ;;(elpy-enable)
    )
  (with-eval-after-load 'mailclient
    (setq message-sendmail-f-is-evil 't)
    )
  (with-eval-after-load 'message
    (setq message-kill-buffer-on-exit t
          message-sendmail-extra-arguments '("--read-envelope-from")
          message-send-mail-function 'message-send-mail-with-sendmail
          message-sendmail-envelope-from 'header
          message-directory "${DIR_MAIL}"
          )
    )
  (with-eval-after-load 'sendmail
    (setq mail-from-style 'angles
          mail-specify-envelope-from t
          mail-indentation-spaces 2
          send-mail-function 'sendmail-send-it
          sendmail-program (executable-find "mailsend")
          mail-yank-prefix nil
          mail-signature nil
          mail-signature-file nil
          mail-mailing-lists nil
          mail-interactive t
          mail-alias-file nil
          )
    )
  (with-eval-after-load "shr"
    (setq shr-max-image-proportion 0.3
          shr-table-horizontal-line ?\s
          shr-table-vertical-line ?\s
          shr-table-corner ?\s
          shr-hr-line ?-
          shr-width fill-column)
    )
  (with-eval-after-load "ssl"
    (setq ssl-certificate-verification-policy 1
          ssl-certificate-directory temporary-file-directory
          ssl-program-name "gnutls-cli"
          ssl-program-arguments '("-p" service host)))
  (with-eval-after-load "starttls"
    (setq starttls-use-gnutls t
          starttls-gnutls-program "gnutls-cli"
          starttls-extra-arguments nil))
  )

(provide 'user-conf)
