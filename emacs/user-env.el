;; User Environment
;; Contains variable definitions from system environment.
;;(expand-file-name "emacs" user-path-cache)

(defconst user-name (getenv "USER")
  "Name of user.")
(defconst user-nick (getenv "NICK")
  "Nickname of user.")
(defconst user-path-cache (expand-file-name "/tmp/cache/emacs")
  "Temporary cache.")
(defconst user-path-tmp (expand-file-name "/tmp/data")
  "Temporary data.")
(defvar user-path-emacs (expand-file-name (substitute-in-file-name "$HOME/.emacs"))
  "Emacs data path.")

(defvar user-mark-assocs
  '((home . "~")
    (dot . "~/dot")
    (doc . "~/doc")
    (repo . "~/repo")
    (data . "~/data")
    (task . "~/data/doc/task-software.txt")
    (save . "~/data/doc/offline.txt")
    )
  "Associations of alias to path for mark; expand-file-name is called on path.")
(defvar user-env-path
  '("~/.local/bin"
    "/usr/local/bin"
    "/usr/bin"
    "/bin"
    "/usr/local/sbin"
    "/usr/sbin"
    "/sbin")
  "Executable paths.")
(defconst user-conf-feh (getenv "CONF_FEH")
  "Configuration file for feh.")
(defconst user-conf-mplayer (getenv "CONF_MPLAYER")
  "Configuration file for mplayer.")

(provide 'user-env)
