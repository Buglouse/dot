(deftheme inside_dark
  "Friendly faces for building use, flourecents."
(custom-theme-set-faces
 'inside_dark
    '(default ((() (:foreground "brightwhite" :background "black"))))
 )
(custom-theme-set-faces
 'inside_dark
    '(default ((() (:foreground "black" :background "white"))))
 )
