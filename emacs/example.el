;; List
;; Same level for all elements.
(append
 (list
  '("1" a)
  '("2" b))
(list
  '("3" c)
  '("4" c)
  (append
   '("5" d))))
(let ((a '("A" 1))
      (b '("B" 2))
      (c '("C" 3))
      (d '("D" 4))
      (e '("E" 5)))
`(
   ("1" a)
   ,b
   ("2" b)
   ,@(when d
       `(,d
	 ("6" e)
	 ,e))
   ,@(list 
    '("3" c)
    a
    '("4" d)))))


;; Concat with unique elements per key in alist.
(let ((a '((key1 ("a" 1))
	   (key2 ("A" 1)
		 ("B" 1))))
      (b '((key1 ("a" 1)
		 ("b" 1))
	   (key2 ("A" 1)
		 ("b" 1))
	   (key3 ("A" 1))
	   (key1 ("a" 1) ;; Ignored, assoc returns first result.
		 ("c" 1))))
      ret)
  (defun alist-concat-uniq (sym alist)
    "Concatenat ALIST to SYM alist, removing duplicates."
    ;; Iterate key lists in alist.
    (dolist (alist-list alist ret)
      (let* ((sym-alist (symbol-value sym))
	     (alist-key (car alist-list))
	     ;;Find matching key in sym.
	     (sym-val (assoc alist-key sym-alist))
	     (alist-val (cdr (assoc alist-key alist))))
	(debug)
	(cond (sym-val ;;Found alist key in sym.
	       (setq ret
		     (append
		      (list (delete-dups (append sym-val alist-val)))
		      (delq (assoc alist-key ret) ret))))
	      (t
	       (setq ret (append ret (list alist-list)))))))
	)))
  (alist-concat-uniq 'a b)
  )  


  
