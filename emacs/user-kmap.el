;;; Unset
;; (setcdr (current-global-map) nil)
;; (global-unset-key "\C-h")
;; (global-unset-key "\M-b")
;; (global-unset-key (kbd "A-ESC [46~"))
;; (global-unset-key (kbd "s-ESC [47~"))
;;;Syntax-Character
;; C-g, backspace, tab, newline, vertical, formfeed, space, return, del, escape
;; \a, \b, \t, \n, \v, \f, \s, \r, \d, \e
;;;Modifiers
;; control, meta, alt, super, hyper
;; \C, \M, \A-, \s-, \H-
;; (coerce (kbd "ESC DEL") 'vector)
;; (suppress-keymap local-map)
;; (set-keymap-parent global-map local-map)
;;;Example
;; [?\A-r] point-to-register
;; (setcdr ... ); clear keymap.
;; (set-keymap-parent sub-map global-map); assign default values.
;; (define-keys (sub-map "" mode-map)); assign keymap to mode.
;;; Layout
;; Alt; single press.
;; Meta; maps.
;;; (eval-after-load); quote for before load completes, double-quote for after load?

;; User defined maps; 'major-mode-map->major-mode-user-map'
;; User define prefix maps; 'major-mode-map->major-mode-user-prefix-map'

(progn ;; Functions
  ;; (map (nconc (make-sparse-keymap) -map))
  ;; (substitute-command-keys)
  ;; Keymap
  (defmacro user-set-keymap-parent (keymap parent)
    "Macro for `user-set-keymap-parent-1'."
    `(user-set-keymap-parent-1 (quote ,keymap) ,parent))
  (defun user-set-keymap-parent-1 (keymap parent)
    "Make `define-prefix-command' or `set-keymap-parent'."
    (when (not (boundp keymap))
      (define-prefix-command keymap))
    (set-keymap-parent (symbol-value keymap) parent))
  (defmacro user-define-keys (keymap &rest keys)
    "Macro for `user-define-keys-1'."
    `(user-define-keys-1 (quote ,keymap) (quote ,keys)))
  (defun user-define-keys-1 (keymap keys)
    ;;(gnus-define-keys-1)
    "Set KEYMAP from KEYS list; '(map key function)"
    (cond ((listp keymap)
           (setq keymap (eval keymap))) ;;(current-local-map)
          ((not (boundp keymap))
           (setq keymap (make-sparse-keymap)))
          ((symbolp keymap)
           (setq keymap (symbol-value keymap))))
    (while keys
      (let ((key (pop keys))
            (fun (pop keys)))
        (when (not (or (listp fun)
                       (fboundp fun)))
          (define-prefix-command fun))
        (define-key keymap key fun))))

  (function-put 'user-define-keys 'lisp-indent-function 1)
  (function-put 'user-set-keymap-parent 'lisp-indent-function 1)

  ;; Input-Decode
  (define-key function-key-map (kbd "ESC [47~") 'event-apply-super-modifier)
  (define-key function-key-map (kbd "ESC [46~") 'event-apply-alt-modifier)
  (define-key input-decode-map (kbd "ESC [48~") '[pause])
  )
(progn ;; Initial
  (progn
    ;;(mapc (defvar (makunbound keymap) (make-sparse-keymap)))
    (defvar user-map (make-sparse-keymap))
    (defvar user-query-map (make-sparse-keymap))
    (defvar user-eval-map (make-sparse-keymap))
    (defvar user-file-map (make-sparse-keymap))
    (defvar user-edit-map (make-sparse-keymap))
    (defvar user-special-map (make-sparse-keymap))
    (defvar user-point-map (make-sparse-keymap))
    (defvar user-mode-map (make-sparse-keymap))
    (defvar user-view-map (make-sparse-keymap))
    (defvar user-window-map (make-sparse-keymap))
    (defvar user-kill-map (make-sparse-keymap))
    (defvar user-exit-map (make-sparse-keymap))
    (defvar user-macro-map (make-sparse-keymap))
    (defvar user-buffer-map (make-sparse-keymap))
    (defvar user-complete-map (make-sparse-keymap))
    (defvar user-frame-map (make-sparse-keymap))
    (defvar user-mark-map (make-sparse-keymap)))
  ;; Order
  (suppress-keymap user-point-map)
  (user-define-keys global-map
    ;;"\C-x" undefined
    "\M-x" undefined
    "\M-z" undefined
    "\M-r" undefined
    "\M-d" undefined
    "\M-s" undefined
    ;;"\M-+" user-mode
    ;;"\M-k" user-macro-map
    ;;[?\A-p] user-point-map
    "\M-x" read-only-mode
    "\M-\\" read-only-mode

    "\C-d" delete-backward-char
    "\C-r" redraw-display
    "\M-." repeat
    "\M-\d" backward-kill-word
    "\b" delete-forward-char
    "\d" delete-backward-char
    [27 deletechar] kill-word
    [deletechar] delete-forward-char
    [next] view-mode
    [prior] view-mode

    "\M-'" user-mark-map
    "\M-*" user-special-map
    "\M-/" user-query-map
    "\M-;" user-eval-map
    "\M-i" user-complete-map
    "\M-b" user-buffer-map
    "\M-e" user-edit-map
    "\M-f" user-file-map
    "\M-k" user-macro-map
    "\M-m" user-mode-map
    "\M-p" user-point-map
    "\M-q" user-exit-map
    "\M-t" user-frame-map
    "\M-v" user-view-map
    "\M-w" user-window-map
    "\M-y" user-kill-map
    "\M-M" user-special-map
    "\M-h" help-map
    "\M-:" user-repl-map
    "\M-s" user-syntax-map
    )
  (user-define-keys user-edit-map
    "\M-I" capitalize-region
    "\M-i" downcase-region
    "I" upcase-word
    "i" downcase-word
    ;;"z" case-invert
    "fd" insert-date
    "fD" insert-date-file
    "ft" insert-time
    "." insert-char

    ;;autoload
    "sC" sort-paragraphs
    "sr" reverse-region
    "sl" sort-lines
    "sL" sort-pages
    "s/" sort-regexp-fields
    "sc" sort-columns
    "ss" sort-fields
    "sS" sort-numeric-fields
    "#" rectangle-number-lines
    "z" string-insert-rectangle
    "`" string-rectangle
    "Z" string-rectangle
    "a" align
    "A" align-regexp
    )
  (user-define-keys user-point-map
    ;;[remap self-insert-command] undefined
    "b" end-of-line
    "J" scroll-up
    ;;"\s" undefined
    "K" scroll-down
    "\s" scroll-up
    ;;"t" beginning-of-line
    "h" backward-char
    "l" forward-char
    "L" forward-word

    "/" user-query-map

    ;;autoload
    "." hl-line-mode
    )
  (user-define-keys user-buffer-map
    "n" rename-buffer_file
    "x" user-kill-buffer
    "Z" buffer-exchange-other
    "z" buffer-exchange
    "gr" buffer-doc-log-rolling
    "gt" buffer-doc-log-today
    "f" user-buffer-name
    "b" list-buffers
    )
  (user-define-keys user-kill-map
    ;;autoload
    "z" delete-rectangle
    "w" delete-whitespace-rectangle
    )
  (user-define-keys user-window-map
    "r" (lambda () (interactive) (redisplay))
    "z" user-display-buffer-current
    )
  (user-define-keys user-query-map
    "r" re-builder
    "?" isearch-backward-regexp
    "/" isearch-forward-regexp
    "]" isearch-yank-word-or-char
    "[" isearch-yank-point-symbol

    ;;autoload
    "d" dired
    "." find-file-at-point
    "l" find-library
    "H" highlight-lines-matching-regexp
    "h" highlight-regexp
    )
  (user-define-keys user-view-map
    "\b" scroll-down
    "\s" scroll-up
    "K" scroll-down
    "J" scroll-up
    "n" narrow-to-region
    ;;autoload
    "N" narrow-to-defun
    "p" narrow-to-page
    "w" widen
    "r" redraw-display
    )
  (user-define-keys user-eval-map
    "b" eval-buffer
    "r" eval-region
    ;;autoload
    "v" debug-on-entry
    "V" cancel-debug-on-entry
    "g" run-geiser
    "s" shell
    )
  (user-define-keys user-exit-map
    "q" abort-recursive-edit
    "Q" exit-recursive-edit
    "X" kill-emacs
    )
  (user-define-keys user-frame-map
    "b" user-frame-buffer-list)
  (user-define-keys user-mode-map
    "," toggle-word-wrap
    "." toggle-truncate-lines
    "I" ido-mode
    "d" diff
    "D" diff-backup
    "f" font-lock-mode
    "m" notmuch
    "h" hs-minor-mode
    "c" flyspell-mode
    "w" auto-save-mode
    "v" read-only-mode
    "W" winner-mode
    "\\" auto-fill-mode
    "V" visual-line-mode
    ;;"a" auto-complete-mode
    "i" company-mode
    "g" magit-status
    "N" (lambda () (interactive) (require 'wide-n))
    "?" (lambda () (interactive) (set-auto-mode))

    "f" user-mode-file-map
    "e" user-mode-edit-map
    "o" user-mode-exec-map
    ;;";" user-mode-repl-map
    )
  (user-define-keys user-mode-file-map
    "t" text-mode
    "c" conf-mode
    "m" makefile-mode
    "a" adoc-mode
    "e" emacs-lisp-mode
    "l" lisp-mode
    "s" scheme-mode
    "b" sh-mode
    "r" ruby-mode)
  (user-define-keys user-mode-edit-map
    "o" overwrite-mode
    "a" abbrev-mode
    "c" conf-mode-map)
  (user-define-keys user-mode-exec-map
    "u" browse-url-text-emacs))
(progn ;; User
  (with-eval-after-load 'user
    (user-define-keys user-edit-map
      "\t" user-indent-buffer)
    (user-define-keys user-view-map
      "c" user-css-fontity)
    (user-define-keys user-special-map
      "$" (lambda () (interactive) (shell-command "wget -q http://localhost:8080/r")))
    (with-eval-after-load 'user-mark
      (user-define-keys user-query-map
        "'" user-mark-open)
      (user-define-keys user-mark-map
        "]" user-unpop-to-mark)
      )
    (with-eval-after-load 'user-button
      (user-define-keys user-buffer-map
        "u" buttonize-buffer-url))))
(progn ;; Included
  (with-eval-after-load "startup"
    (user-define-keys user-buffer-map
      "." (lambda () (interactive) (display-buffer "*scratch*"))
      ))
  (with-eval-after-load 'gud
    (setq gud-key-prefix "\C-x")
    )
  (with-eval-after-load 'smerge-mode
    (setcdr smerge-basic-map nil)
    (setcdr smerge-mode-map nil)

    (user-define-keys smerge-mode-map
      "j" smerge-next
      "k" smerge-prev
      "I" smerge-resolve
      "e" smerge-refine
      "id" smerge-keep-current
      "iI" smerge-keep-all
      "ib" smerge-keep-base
      "io" smerge-keep-other
      "ii" smerge-keep-mine
      "ic" smerge-combine-with-next
      "v" smerge-diff-base-mine
      "V" smerge-diff-mine-other
      )
    )
  (with-eval-after-load 'sgml-mode
    (setcdr sgml-mode-map nil)
    (setcdr html-mode-map nil)

    (user-set-keymap-parent html-mode-map user-map)

    (user-define-keys sgml-mode-map
      )
    (user-define-keys html-mode-map
      )

    )
  (with-eval-after-load "eshell"
    (defun eshell-mode-user-map ()
      (interactive)
      (when (eq major-mode 'eshell-mode)
        (setcdr eshell-mode-map nil)
        (setcdr eshell-command-map nil)
        (setcdr eshell-isearch-map nil)

        (user-define-keys eshell-mode-map
          "\r" (lambda ()
                 (interactive) (if current-prefix-arg (eshell-queue-input) (eshell-send-input)))
          "\t" pcomplete

          "\M-;" eshell-mode-user-eval-map
          "\M-p" eshell-mode-user-point-map
          "\M-e" eshell-mode-user-edit-map
          "\M-v" eshell-mode-user-view-map
          "\M-/" eshell-mode-user-query-map
          )
        (user-define-keys eshell-mode-user-eval-map
          "," eshell-command-prefix
          "\r" eshell-toggle-direct-send
          "qQ" eshell-quit-process
          "qb" eshell-send-eof-to-process
          "qq" eshell-interrupt-process
          "qx" eshell-kill-process
          )
        (user-define-keys eshell-mode-user-point-map
          "h" eshell-backward-argument
          "l" eshell-forward-argument
          "t" eshell-bol
          "b" end-of-line
          "T" beginning-of-buffer
          "B" end-of-buffer
          "J" scroll-up
          "K" scroll-down
          )
        (user-define-keys eshell-mode-user-view-map
          "o" eshell-show-output
          "O" eshell-show-maximum-output
          "v" eshell-truncate-buffer
          "r" eshell-smart-redisplay
          )
        (user-define-keys eshell-mode-user-edit-map
          "\r" eshell-queue-input
          "yk" eshell-copy-old-input
          "yK" eshell-kill-old-input
          "h" backward-kill-word
          "." eshell-repeat-argument
          "d" eshell-kill-input
          "q" eshell-quit
          "o" eshell-mark-output
          )
        (user-define-keys eshell-mode-user-query-map
          "t" eshell-find-tag
          )

        (when (featurep 'em-hist)
          (user-define-keys eshell-mode-user-edit-map
            "K" eshell-previous-matching-input
            "J" eshell-next-matching-input
            "k" eshell-previous-input
            "j" eshell-next-input
            )
          (user-define-keys eshell-mode-user-query-map
            "\r" eshell-isearch-return
            "\M-," eshell-isearch-repeat-backward
            "\M-." eshell-isearch-repeat-forward
            "\M-q" eshell-isearch-abort
            "\d" eshell-isearch-delete-char
            "\M-Q" eshell-isearch-cancel
            )
          )
        ))
    (add-hook 'eshell-mode-hook 'eshell-mode-user-map)
    )
  (with-eval-after-load 'cc-mode
    (setcdr c-mode-base-map nil)
    (setcdr c-mode-map nil)
    (setcdr c++-mode-map nil)

    (user-set-keymap-parent user-help-c-mode-base-map help-map)
    (user-define-keys c-mode-base-map
      "\M-h" user-help-c-mode-base-map
      )

    (user-define-keys user-help-c-mode-base-map
      "s" man)

    (set-keymap-parent c-mode-map c-mode-base-map)
    )
  (with-eval-after-load 'font-lock
    (user-define-keys user-view-map
      "f" font-lock-fontify-buffer))
  (with-eval-after-load 'proced
    (setcdr proced-mode-map nil)

    (set-keymap-parent proced-mode-map user-point-map)

    (user-define-keys proced-mode-map
      "m" proced-mark
      "u" proced-unmark
      ;;"U" proced-unmark-backward
      "M" proced-mark-all
      "U" proced-unmark-all
      "t" proced-toggle-marks
      "C" proced-mark-children
      "P" proced-mark-parents
      "f" proced-filter-interactive
      "sc" proced-sort-pcpu
      "sm" proced-sort-pmem
      "sp" proced-sort-pid
      "sn" proced-sort-start
      "ss" proced-sort-interactive
      "st" proced-sort-time
      "su" proced-sort-user
      "sp" proced-sort-header
      "v" proced-toggle-tree
      "o" proced-omit-process
      "x" proced-send-signal
      "!" proced-renice
      "?" proced-help
      "F" proced-refine
      "r" proced
      "q" other-window
      "Q" kill-buffer-and-window
      )
    )
  (with-eval-after-load 'term
    (setcdr term-mode-map nil)
    (setcdr term-raw-escape-map nil)
    (setcdr term-pager-break-map nil)

    (user-set-keymap-parent term-mode-user-help-map help-map)

    (user-define-keys term-mode-map
      "\M-e" term-mode-user-edit-map
      "\M-;" term-mode-user-eval-map
      "\M-v" term-mode-user-view-map
      "\M-h" term-mode-user-help-map
      )
    (user-define-keys term-mode-user-edit-map
      "k" term-previous-input
      "j" term-next-input
      "K" term-previous-matching-input
      "J" term-next-matching-input
      "/" term-next-matching-input-from-input
      "l" term-show-output
      "i" term-send-input
      "d" term-delchar-or-maybe-eof
      "t" term-bol
      "n" term-next-prompt
      "p" term-previous-prompt
      "b" term-send-eof
      "d" term-kill-input
      "H" backward-kill-word
      "y" term-copy-old-input
      "k" term-kill-output
      )
    (user-define-keys term-mode-user-eval-map
      "c" term-char-mode
      "l" term-line-mode
      "x" term-interrupt-subjob
      "z" term-stop-subjob
      "q" term-quit-subjob
      )
    (user-define-keys term-mode-user-view-map
      "m" term-pager-toggle
      "v" term-show-output
      "V" term-show-maximum-output
      )
    (user-define-keys term-mode-user-help-map
      "h" term-dynamic-list-input-ring
      )
    )
  (with-eval-after-load "files"
    (user-define-keys user-buffer-map
      "D" kill-matching-buffers
      "s" save-buffer
      "S" save-some-buffers
      "R" rename-uniquely
      )
    (user-define-keys user-file-map
      "w" write-file
      "n" set-visited-file-name
      "r" recover-this-file
      "R" revert-buffer
      )
    (user-define-keys user-exit-map
      "b" save-buffers-kill-emacs
      )
    (user-define-keys user-query-map
      "q" find-alternate-file
      "f" find-file)
    )
  (with-eval-after-load "isearch"
    ;;(setcdr isearch-mode-map nil)
    (setcdr isearch-help-map nil)
    (setcdr minibuffer-local-isearch-map nil)

    (user-define-keys isearch-mode-map
      "\M-K" scroll-down
      "\M-J" scroll-up
      "\M-." isearch-repeat-forward
      "\M-," isearch-repeat-backward
      "\M-j" isearch-ring-advance
      "\M-k" isearch-ring-retreat
      "\M-t" isearch-toggle-regexp
      "\M-c" isearch-toggle-case-fold
      "\M-e" isearch-edit-string
      "\M-x" isearch-abort
      ;; [?\A-R] isearch-query-replace
      "\d" isearch-del-char)

    (set-keymap-parent minibuffer-local-isearch-map minibuffer-local-map)
    (user-define-keys minibuffer-local-isearch-map
      "\r" isearch-nonincremental-exit-minibuffer
      "\M-\t" isearch-complete-edit
      "\C-s" isearch-forward-exit-minibuffer
      "\C-r" isearch-reverse-exit-minibuffer
      "\C-f" isearch-yank-char-in-minibuffer)
    )
  (with-eval-after-load "replace"
    (setcdr occur-mode-map nil)
    (setcdr query-replace-map nil)
    (setcdr occur-edit-mode-map nil)
    (setcdr multi-query-replace-map nil)

    (user-define-keys query-replace-map
      "." act-and-show
      "\d" act-and-exit
      "\b" delete-and-edit
      "\r" act
      "\s" skip
      "e" edit-replacement
      "E" edit
      "h" help
      "n" skip
      "p" backup
      "q" exit
      "v" recenter
      "x" exit-prefix
      "y" act
      "Y" automatic
      )
    (user-define-keys multi-query-replace-map
      "Y" automatic-all
      "q" exit-current
      )
    (user-define-keys occur-mode-map
      "O" occur-mode-goto-occurrence
      "e" occur-edit-mode
      "o" occur-mode-goto-occurrence-other-window
      ;;"o" occur-mode-display-occurrence
      "j" occur-next
      "k" occur-prev
      "n" occur-rename-buffer
      "c" clone-buffer
      "E" next-error-follow-minor-mode
      "J" scroll-up
      "K" scroll-down
      "q" other-window
      )
    (user-define-keys occur-edit-mode-map
      "q" occur-cease-edit
      "l" occur-mode-display-occurrence
      "E" next-error-follow-minor-mode
      )

    (user-define-keys user-query-map
      "o" occur
      )
    (user-define-keys user-edit-map
      "/" query-replace-regexp
      )
    (user-define-keys user-kill-map
      "?" flush-lines
      )
    )
  (with-eval-after-load "window"
    (user-define-keys user-window-map
      "v" recenter-top-bottom
      )
    (user-define-keys user-buffer-map
      ;;"i" (lambda () (interactive) (display-buffer "*info*"))
      "*" proced
      "\s" (lambda () (interactive) (switch-to-buffer nil))
      "j" switch-to-next-buffer
      "k" switch-to-prev-buffer
      "h" unbury-buffer
      "l" bury-buffer
      "X" kill-buffer-and-window
      )
    (user-define-keys user-query-map
      "g" display-buffer
      "b" switch-to-buffer
      )
    (user-define-keys user-point-map
      "s" move-to-window-line-top-bottom
      )
    (user-define-keys user-window-map
      ;;"q" quit-window
      "$" window-dedicated-toggle
      "=" balance-windows
      "\M-=" balance-windows-area
      "@" user-window-scroll-buffer-toggle
      "_" window-quarter
      "x" delete-window
      "X" delete-other-windows
      "m" maximize-window
      "M" minimize-window
      "p" compare-windows
      "Q" delete-other-windows
      "n" split-window-vertically
      "N" split-window-horizontally
      "J" scroll-other-window
      "K" scroll-other-window-down
      "T" beginning-of-buffer-other-window
      "B" end-of-buffer-other-window
      "+" enlarge-window
      "-" shrink-window
      "\M--" shrink-window-horizontally
      "\M-+" enlarge-window-horizontally
      "o" other-window
      "O" user-other-window-prev
      "k" user-other-window-prev
      "j" other-window)
    )
  (with-eval-after-load "subr"
    (user-define-keys user-point-map
      "\M-\s" forward-whitespace
      ;"\M-L" forward-same-syntax
      ;"\M-l" forward-symbol
      ;"\M-H" backward-same-syntax
      ;"\M-h" (lambda () (interactive) (forward-symbol -1))
      )
    (user-define-keys user-query-map
      "L" locate-library
      )
    (user-define-keys user-kill-map
      "s" just-one-space)
    )
  (with-eval-after-load "simple"
    (setcdr special-mode-map nil)
    (setcdr visual-line-mode-map nil)
    (setcdr completion-list-mode-map nil)
    (setcdr minibuffer-local-shell-command-map nil)
    (setcdr messages-buffer-mode-map nil)

    (set-keymap-parent minibuffer-local-shell-command-map minibuffer-local-map)
    ;;(set-keymap-parent special-mode-map user-map)

    (user-define-keys global-map
      "\M-," universal-argument
      "\M-@" what-cursor-position
      "\r" newline-and-indent
      )
    (user-define-keys special-mode-map_
      "J" scroll-down-command
      "K" scroll-up-command
      "T" beginning-of-buffer
      "B" end-of-buffer
      "r" revert-buffer
      )
    (user-define-keys user-eval-map
      "q" keyboard-quit
      ";" execute-extended-command
      ":" eval-expression
      "\M-;" shell-command
      )
    (user-define-keys user-point-map
      "'" user-mark-map
      "_" what-line
      "j" next-line
      "T" beginning-of-buffer
      "t" move-beginning-of-line-or-indentation
      "B" end-of-buffer
      "}" goto-line
      "k" previous-line
      "H" backward-word
      )
    (user-define-keys user-buffer-map
      "c" clone-indirect-buffer
      ":" (lambda () (interactive) (display-buffer "*Messages*"))
      "?" (lambda () (interactive) (display-buffer "*Help*"))
      "y" (lambda () (interactive)
            (let ((f (buffer-file-name)))
              (kill-new
               (or f (dired-file-name-at-point)))))
      )
    (user-define-keys user-view-map
      "s" selective-overlay-toggle
      "S" set-selective-display
      )
    (user-define-keys user-edit-map
      "'" quoted-insert
      "j" split-line
      "u" undo
      "o" insert-line-indent
      "O" insert-line-indent-above
      "b" insert-buffer
      "B" append-to-buffer
      )
    (user-define-keys user-mark-map
      "?" count-words-region
      "j" mark-line
      "'" set-mark-command
      "y" kill-ring-save
      "c" append-next-kill
      "{" pop-global-mark
      "[" pop-to-mark-command
      "z" exchange-point-and-mark
      )
    (user-define-keys user-kill-map
      "b" kill-line
      "W" delete-trailing-whitespace
      "j" kill-whole-line
      "h" backward-kill-word
      ;;"h" delete-backward-char
      "\t" delete-indentation
      ;;"l" delete-forward-char
      "l" kill-word
      "`" kill-region
      "," user-yank-pop
      "i" user-yank
      "J" kill-line-insert
      "K" kill-line-insert-above
      "I" user-yank-above
      "\s" delete-horizontal-space ;;; fixup-whitespace
      "s" delete-horizontal-space ;;; fixup-whitespace
      "L" delete-blank-lines
      "/" zap-to-char
      "k" join-line
      )
    (user-define-keys completion-list-mode-map
      "K" scroll-down
      "J" scroll-up
      "k" previous-line
      "j" next-line
      "\r" choose-completion
      "\t" next-completion
      "l" next-completion
      [backtab] previous-completion
      "h" previous-completion
      "/" isearch-forward-regexp
      "q" (lambda ()
            (interactive)
            ;;(minibuffer-hide-completions)
            (select-window (minibuffer-window)))
      )

    (user-set-keymap-parent messages-buffer-mode-map user-point-map)
    )
  (with-eval-after-load 'compile
    (setcdr compilation-menu-map nil)
    (setcdr compilation-minor-mode-map nil)
    (setcdr compilation-shell-minor-mode-map nil)
    (setcdr compilation-button-map nil)
    (setcdr compilation-mode-map nil)
    (setcdr compilation-mode-tool-bar-map nil)

    ;;(set-keymap-parent compilation-mode-map special-mode-map)
    (user-set-keymap-parent compilation-mode-map user-point-map)
    (user-define-keys user-eval-map
      "c" compile
      )
    (user-define-keys compilation-mode-map
      "g" compile-goto-error
      "o" compilation-display-error
      "x" kill-compilation
      "j" compilation-next-error
      "k" compilation-previous-error
      "p" compilation-previous-file
      "n" compilation-next-file
      "r" recompile

      "t" beginning-of-buffer
      "b" end-of-buffer
      )
    (set-keymap-parent compilation-minor-mode-map compilation-mode-map)
    )
  (with-eval-after-load 'info
    (setcdr Info-mode-map nil)

    (set-keymap-parent Info-mode-map button-buffer-map)

    (user-define-keys Info-mode-map
      "j" Info-scroll-up
      "\s" Info-scroll-up
      "T" beginning-of-buffer
      "k" Info-scroll-down
      "\d" Info-scroll-down
      "B" end-of-buffer
      "t" Info-toc
      "d" Info-directory
      "h" Info-backward-node
      "l" Info-forward-node
      "K" Info-prev-reference
      "I" Info-index
      "n" Info-history-forward
      "o" Info-follow-nearest-node
      "p" Info-history-back
      "\b" Info-history-back
      "J" Info-next-reference
      "q" other-window
      "Q" Info-exit
      "/" isearch-forward-regexp
      "?" isearch-backward-regexp
      "." isearch-repeat-forward
      "," isearch-repeat-backward
      "y" Info-copy-current-node-name
      ;;  "RET" 'Info-follow-reference
      ;;  "DEL" 'Info-prev-reference
      ;;  "M" 'Info-menu
      )
    )
  (with-eval-after-load 'grep
    (setcdr grep-mode-map nil)
    (setcdr grep-mode-tool-bar-map nil)

    (set-keymap-parent grep-mode-map compilation-minor-mode-map)

    )
  (with-eval-after-load 'prog-mode
    (setcdr prog-mode-map nil)
    )
  (with-eval-after-load 'buff-menu
    (user-define-keys user-buffer-map
      "b" list-buffers)
    )
  (with-eval-after-load 'gnus
    (setcdr gnus-article-mode-map nil)
    (setcdr gnus-summary-mode-map nil)
    (setcdr gnus-group-mode-map nil)

    (with-eval-after-load 'gnus-group
      (setcdr gnus-group-mode-map nil)

      (set-keymap-parent gnus-group-mode-map user-point-map)
      (user-define-keys gnus-group-mode-map
        "@" gnus-group-enter-server-mode
        "J" gnus-group-next-group
        "K" gnus-group-prev-group
        "j" gnus-group-next-unread-group
        "k" gnus-group-prev-unread-group
        "t" gnus-group-first-unread-group
        "g" gnus-group-jump-to-group
        "o" gnus-group-select-group
        "O" gnus-group-visible-select-group
        "Q" gnus-group-exit
        "q" gnus-group-suspend
        "R" gnus-group-get-new-news
        "r" gnus-group-get-new-news-this-group
        "!" gnus-group-restart
        "#" gnus-group-save-newsrc
        "A" gnus-group-make-rss-group
        "v" gnus-group-list-groups
        "V" gnus-group-list-all-groups
        "c" gnus-group-mail

        "e" gnus-group-user-edit-map
        "/" gnus-group-user-query-map
        "m" gnus-group-user-mark-map
        )
      (user-define-keys gnus-group-user-query-map
        "/" gnus-group-apropos
        "," gnus-group-description-apropos)
      (user-define-keys gnus-group-user-edit-map
        "y" gnus-group-kill-group
        "r" gnus-group-kill-region
        "t" gnus-group-transpose-groups
        "l" gnus-group-list-killed
        "i" gnus-group-yank-group
        "s" gnus-group-subscribe-current-group
        "e" gnus-group-edit-group
        "E" gnus-group-edit-group-method
        "p" gnus-group-edit-group-parameters
        "S" gnus-group-unsubscribe-current-group
        )
      (user-define-keys gnus-group-user-mark-map
        "m" gnus-group-mark-group
        "M" gnus-group-unmark-group
        "r" gnus-group-mark-region
        "b" gnus-group-mark-buffer
        "/" gnus-group-mark-regexp
        "." gnus-group-unmark-all-groups
        )
      )
    (with-eval-after-load 'gnus-topic
      (setcdr gnus-topic-mode-map nil)

      (user-define-keys gnus-group-mode-map
        "T" gnus-topic-user-map)
      (user-define-keys gnus-topic-user-map
        "k" gnus-topic-goto-previous-topic
        "e" gnus-topic-edit-parameters
        "j" gnus-topic-goto-next-topic
        "m" gnus-topic-move-group
        "g" gnus-topic-jump-to-topic
        "a" gnus-topic-create-topic
        "\t" gnus-topic-indent
        "t" gnus-topic-mode
        "d" gnus-topic-remove-group
        "D" gnus-topic-delete
        "n" gnus-topic-rename
        "v" gnus-topic-show-topic
        "V" gnus-topic-hide-topic
        "." gnus-topic-toggle-display-empty-topics
        "\r" gnus-topic-select-group
        [backtab] gnus-topic-unindent
        )
      )
    (with-eval-after-load 'gnus-srvr
      (setcdr gnus-server-mode-map nil)
      (setcdr gnus-browse-mode-map nil)

      (user-define-keys gnus-server-mode-map
        "\r" gnus-server-read-server
        "." gnus-server-list-servers
        "r" gnus-server-scan-server
        "R" gnus-server-regenerate-server
        "q" gnus-server-exit
        "i" gnus-server-show-server
        "j" next-line
        "k" previous-line
        "o" gnus-server-open-server
        "x" gnus-server-close-server)
      (user-define-keys gnus-browse-mode-map
        "j" gnus-browse-next-group
        "k" gnus-browse-prev-group
        "l" gnus-browse-read-group
        "." gnus-browse-select-group
        "s" gnus-browse-unsubscribe-current-group
        "H" gnus-browse-describe-group
        "h" gnus-browse-describe-briefly
        "q" gnus-browse-exit)
      )
    (with-eval-after-load 'gnus-sum
      (setcdr gnus-summary-mode-map nil)
      (setcdr gnus-dead-summary-mode-map nil)

      (user-define-keys gnus-summary-mode-map
        "\M-r" gnus-summary-rescan-group
        "R" gnus-summary-reselect-current-group
        "r" gnus-summary-prepare
        "o" gnus-summary-select-article-buffer
        "J" gnus-summary-next-article
        "K" gnus-summary-prev-article
        "j" gnus-summary-next-unread-article
        "k" gnus-summary-prev-unread-article
        "\M-j" gnus-summary-scroll-up
        "\M-k" gnus-summary-scroll-down
        "t" gnus-summary-first-unread-article
        "q" gnus-summary-exit
        "Q" gnus-summary-exit-no-update
        "v" gnus-summary-show-article
        "p" gnus-summary-goto-last-article
        [backtab] gnus-summary-widget-backward

        "f" gnus-summary-user-filter-map
        "/" gnus-summary-user-query-map
        "v" gnus-summary-user-view-map
        "e" gnus-summary-user-edit-map
        "'" gnus-summary-user-mark-map
        "s" gnus-summary-user-sort-map
        )
      (user-define-keys gnus-summary-user-query-map
        "/" gnus-summary-search-article-forward
        "?" gnus-summary-search-article-backward
        "." gnus-summary-repeat-search-article-forward
        "," gnus-summary-repeat-search-article-backward
        )
      (user-define-keys gnus-summary-user-filter-map
        "a" gnus-summary-limit-to-author
        "R" gnus-summary-limit-to-unread
        "p" gnus-summary-pop-limit
        "b" gnus-summary-limit-to-bodies
        )
      (user-define-keys gnus-summary-user-view-map
        "h" gnus-summary-toggle-header
        "o" gnus-summary-expand-window
        "t" gnus-summary-toggle-threads
        )
      (user-define-keys gnus-summary-user-sort-map
        "D" gnus-summary-sort-by-most-recent-date
        "d" gnus-summary-sort-by-date
        "a" gnus-summary-sort-by-author
        "s" gnus-summary-sort-by-original
        )
      (user-define-keys gnus-summary-user-edit-map
        "E" gnus-summary-mail-other-window
        "f" gnus-summary-followup
        ;;"x" gnus-summary-expire-articles
        "X" gnus-summary-delete-article
        )
      (user-define-keys gnus-summary-user-mark-map
        "'" gnus-summary-tick-article-forward
        "r" gnus-summary-mark-as-read-forward
        "u" gnus-summary-clear-mark-forward
        "x" gnus-summary-mark-as-expirable
        "m" gnus-summary-mark-as-processable
        "\"" gnus-summary-mark-region-as-read
        "M" gnus-summary-unmark-as-processable
        )
      )
    (with-eval-after-load 'gnus-art
      (setcdr gnus-article-mode-map nil)
      (setcdr gnus-sticky-article-mode-map nil)
      (setcdr gnus-mime-button-map nil)
      (setcdr gnus-url-button-map nil)
      (setcdr gnus-article-edit-mode-map nil)
      (setcdr gnus-prev-page-map nil)
      (setcdr gnus-next-page-map nil)
      (setcdr gnus-mime-security-button-map nil)

      (set-keymap-parent gnus-article-mode-map widget-keymap)
      (set-keymap-parent gnus-article-mode-map user-point-map)
      (user-define-keys gnus-article-mode-map
        "q" gnus-article-show-summary
        "y" gnus-article-copy-string

        "v" gnus-article-user-view-map)
      (user-define-keys gnus-article-user-view-map
        "v" gnus-mime-view-all-parts
        )
      )
    )
  (with-eval-after-load 'frame
    (user-define-keys user-frame-map
      "n" make-frame-command
      "X" delete-other-frames
      "x" delete-frame
      "j" other-frame
      "\s" select-frame-by-name
      "k" frame-other-neg
      "l" (lambda ()
            (interactive)
            (prin1 (frame-list-name)))
      "L" (lambda ()
            (interactive)
            (prin1 (frame-parameters)))
      "v" frame-change-name
      )
    )
  (with-eval-after-load 'dired
    (setcdr dired-mode-map nil)
    ;; Yank path: (universal-argument) 0 yank

    (user-define-keys user-query-map
      "." find-file
      "P" find-name-dired
      "q" (lambda () (interactive)
            (let ((f (file-name-directory (buffer-file-name))))
              (kill-buffer) (find-file f)))
      )

    (user-define-keys dired-mode-map
      "=" dired-diff
      "[" dired-tree-up
      "\r" dired-find-alternate-file
      "\t" dired-hide-subdir
      "]" dired-tree-down
      "_" dired-backup-diff
      "B" end-of-buffer
      "f" find-file
      "F" find-file-other-frame
      "g" dired-goto-file
      "h" user-dired-up-directory
      "i" dired-info
      "J" scroll-up
      "j" dired-next-line
      "k" dired-previous-line
      "K" scroll-down
      "n" dired-next-dirline
      "N" dired-next-subdir
      "o" dired-display-file
      "p" dired-prev-dirline
      "P" dired-prev-subdir
      "q" user-kill-buffer
      "Q" dired-kill-buffers
      "r" revert-buffer
      "s" dired-maybe-insert-subdir
      "T" beginning-of-buffer
      "u" dired-undo
      "^" magit-status

      "y" dired-copy-filename-as-kill
      ";" user-dired-mode-eval-map
      "/" user-dired-mode-query-map
      "`" user-dired-mode-mark-map
      "v" user-dired-mode-view-map
      "e" user-dired-mode-edit-map)
    (user-define-keys user-dired-mode-eval-map
      "n" dired-do-rename
      "R" dired-toggle-read-only
      "$" epa-dired-do-sign
      ";" dired-do-shell-command
      ":" dired-do-async-shell-command
      "+" epa-dired-do-decrypt
      "-" epa-dired-do-encrypt
      "@" dired-do-byte-compile
      "/" dired-do-isearch-regexp
      "\d" dired-do-kill-lines
      "^" epa-dired-do-verify
      "c" dired-do-compress
      "c" dired-do-copy
      "d" dired-create-directory
      "f" dired-do-touch
      "L" dired-do-hardlink
      "l" dired-do-load
      "l" dired-do-symlink
      "mg" dired-do-chgrp
      "mm" dired-do-chmod
      "mo" dired-do-chown
      "x" dired-do-delete
      "X" dired-do-flagged-delete
      "r" dired-do-redisplay)
    (user-define-keys user-dired-mode-query-map
      "/" dired-isearch-filenames-regexp
      "f" find-name-dired
      "g" find-grep-dired
      "l" locate)
    (user-define-keys user-dired-mode-mark-map
      "'" dired-change-marks
      "." dired-toggle-marks
      "/" dired-mark-files-regexp
      "*" dired-unmark-all-marks
      "d" dired-mark-directories
      "e" dired-mark-executables
      "g" dired-mark-files-containing-regexp
      "j" dired-next-marked-file
      "k" dired-prev-marked-file
      "l" dired-mark-symlinks
      "M" dired-unmark-backward
      "'" dired-mark
      "u" dired-unmark
      "s" dired-mark-subdir-files
      "U" dired-unmark-all-files
      "x" dired-flag-file-deletion
      "X" dired-flag-garbage-files)
    (user-define-keys user-dired-mode-view-map
      "." dired-show-file-type
      "a" dired-hide-all
      "s" dired-sort-toggle-or-edit
      "V" dired-reset-subdir-switches
      "i" user-dired-mode-view-image-map)
    (user-define-keys user-dired-mode-view-image-map
      "." image-dired-dired-toggle-marked-thumbs
      "/" image-dired-mark-tagged-files
      "a" image-dired-display-thumbs-append
      "b" image-dired-jump-thumbnail-buffer
      "c" image-dired-dired-comment-files
      "e" image-dired-dired-edit-comment-and-tags
      "f" image-dired-tag-files
      "i" image-dired-dired-display-image
      "o" image-dired-dired-display-external
      "S" image-dired-display-thumbs
      "s" image-dired-display-thumb
      "x" image-dired-delete-tag)
    (user-define-keys user-dired-mode-edit-map
      "c" dired-downcase
      "/" dired-do-query-replace-regexp)
    )
  (with-eval-after-load 'help
    (user-define-keys help-map
      "a" apropos-command
      "A" apropos-documentation
      "t" finder-by-keyword
      "\M-?" help-for-help
      "e" info-emacs-manual
      "V" info-lookup-symbol
      ":" view-echo-area-messages
      "E" view-emacs-FAQ
      "r" view-lossage
      "/" where-is
      "K" describe-bindings
      "." describe-char
      ;;"C" describe-categories
      "u" describe-coding-system
      "f" describe-face
      "d" describe-function
      "k" describe-key
      "m" describe-mode
      "s" describe-syntax
      "_" describe-text-properties
      "v" (lambda () (interactive) (save-excursion (call-interactively 'describe-variable)))
      "?" display-local-help
      "&" (lambda () (interactive) (describe-variable 'function-key-map))
      "^" (lambda () (interactive) (describe-variable 'key-translation-map))
      "*" (lambda () (interactive) (describe-variable 'global-map))
      ;;"=" (lambda () (interactive) (message "%S" (buffer-file-name)))
      "L" (lambda () (interactive) (find-library "loaddefs"))
      )
    )
  (with-eval-after-load 'help-mode
    (setcdr help-mode-map nil)
    (suppress-keymap help-mode-map)

    (user-define-keys help-mode-map
      ;;"\r" help-follow
      "p" help-go-back
      "n" help-go-forward
      "J" scroll-up
      "K" scroll-down
      "B" end-of-buffer
      "T" beginning-of-buffer
      "l" help-follow-symbol
      "j" View-scroll-line-forward
      "k" View-scroll-line-backward
      "_" describe-text-properties
      "x" kill-this-buffer
      "q" other-window
      )

    (set-keymap-parent help-mode-map button-buffer-map)
    )
  (with-eval-after-load 'hideshow
    ;;set-selective-display
    (setcdr hs-minor-mode-map nil)

    (user-define-keys user-view-map
      "h" hs-hide-level
      "v" hs-toggle-hiding
      "J" hs-show-all
      "K" hs-hide-all
      "k" hs-hide-block
      "j" hs-show-block
      )
    (user-define-keys user-point-map
      "v" user-view-map)
    (user-define-keys isearch-mode-map
      "\t" hs-toggle-hiding)
    )
  (with-eval-after-load 'register
    (user-define-keys user-mark-map
      "i" insert-register
      "P" jump-to-register
      "a" append-to-register
      "b" prepend-to-register
      "K" copy-rectangle-to-register
      "k" copy-to-register
      "p" point-to-register
      "Y" yank-rectangle
      )
    )
  (with-eval-after-load 'minibuffer
    (setcdr minibuffer-local-map nil)
    (setcdr minibuffer-inactive-mode-map nil)
    (setcdr minibuffer-local-completion-map nil)
    (setcdr minibuffer-local-must-match-map nil)
    (setcdr minibuffer-local-filename-completion-map nil)
    (setcdr completion-in-region-mode-map nil)

    (user-define-keys minibuffer-local-map
      "\t" minibuffer-complete
      ;;[backtab] previous-completion
      "\M-e" minibuffer-edit-map
      "\M-x" exit-minibuffer
      "\M-;" exit-minibuffer
      "\r" exit-minibuffer
      )
    (user-define-keys minibuffer-edit-map
      "h" backward-char
      "l" forward-char
      "H" backward-word
      "L" forward-word
      "t" move-beginning-of-line
      "b" move-end-of-line
      "j" next-history-element
      "k" previous-history-element
      "c" switch-to-completions
      )

    (user-define-keys minibuffer-local-completion-map
      "?" minibuffer-completion-help
      "\s" minibuffer-complete-word
      "\t" minibuffer-complete
      "\M-\t" switch-to-completions
      )
    (user-define-keys minibuffer-inactive-mode-map
      "q" switch-to-buffer-other-frame
      )
    (user-define-keys completion-in-region-mode-map
      "\t" completion-at-point
      ;;[backtab] previous-completion
      )
    (user-define-keys minibuffer-local-must-match-map
      "\r" minibuffer-complete-and-exit
      )

    (user-set-keymap-parent minibuffer-local-completion-map minibuffer-local-map)
    (user-set-keymap-parent minibuffer-inactive-mode-map minibuffer-local-map)
    (user-set-keymap-parent minibuffer-local-must-match-map minibuffer-local-completion-map)
    )
  (with-eval-after-load 'ido
    (defun ido-init-completion-maps ()
      'ignore
      )

    (setcdr ido-common-completion-map nil)
    ;;(setcdr ido-completion-map nil)
    (setcdr ido-file-dir-completion-map nil)
    (setcdr ido-file-completion-map nil)
    (setcdr ido-buffer-completion-map nil)

    (set-keymap-parent ido-common-completion-map minibuffer-local-map)
    (user-define-keys ido-common-completion-map
      "\r" ido-exit-minibuffer
      "\t" ido-complete
      "?" ido-completion-help

      "\'" ido-user-edit-map
      ";" ido-user-eval-map
      )
    (user-define-keys ido-user-edit-map
      "e" ido-edit-input
      "l" ido-next-match
      "h" ido-prev-match
      "p" ido-toggle-prefix
      "i" ido-toggle-ignore
      "d" ido-enter-dired
      )
    (user-define-keys ido-user-eval-map
      "q" ido-select-text
      "d" ido-make-directory
      "." ido-fallback-command
      "Q" ido-exit-minibuffer
      )

    (user-set-keymap-parent ido-buffer-completion-map ido-common-completion-map)
    (user-set-keymap-parent ido-file-completion-map ido-common-completion-map)
    (user-set-keymap-parent ido-file-dir-completion-map ido-common-completion-map)

    ;;(add-hook 'ido-setup-hook 'user-ido-map)
    )
  (with-eval-after-load 'shell
    ;;(setcdr shell-mode-map nil)

    ;;(user-define-keys shell-mode-map
    ;;  "\t" completion-at-point
    ;;  )
    )
  (with-eval-after-load 'sh-script
    (setcdr sh-mode-map nil)

    (user-define-keys sh-mode-map
      "\M-p" sh-point-map)
    (user-define-keys sh-point-map
      "N" forward-list
      "P" backward-list
      )
    )
  (with-eval-after-load 'comint
    (setcdr comint-mode-map nil)

    (user-set-keymap-parent user-mark-comint-mode-map user-mark-map)
    (user-set-keymap-parent user-point-comint-mode-map user-point-map)
    (user-define-keys comint-mode-map
      "\r" comint-send-input
      "\M-;" comint-user-eval-map
      "\M-e" comint-edit-map
      "\M-v" view-mode
      "\M-`" user-mark-map
      "\M-m" comint-user-map
      "\M-p" user-point-comint-mode-map
      [menu-bar completion] user-fake
      )
    (user-define-keys user-point-comint-mode-map
      "T" comint-bol-or-process-mark
      )
    (user-define-keys comint-edit-map
      "/" comint-next-matching-input-from-input
      "?" comint-previous-matching-input-from-input
      "\M-/" comint-history-isearch-backward-regexp
      "db" comint-delchar-or-maybe-eof
      "r" comint-delete-output
      "H" backward-kill-word
      "\d" comint-kill-input
      "j" comint-next-input
      "K" comint-insert-previous-argument
      "k" comint-previous-input
      "n" comint-next-prompt
      "p" comint-previous-prompt
      "sx" comint-send-eof
      "T" comint-show-maximum-output
      "t" comint-show-output
      "w" comint-write-output
      "yk" comint-copy-old-input
      "yK" comint-get-next-from-history
      "yr" comint-dynamic-list-input-ring
      "yy" comint-accumulate
      )
    (user-define-keys comint-user-eval-map
      "xc" comint-continue-subjob
      "xk" comint-interrupt-subjob
      "xk" comint-kill-subjob
      "xq" comint-quit-subjob
      "xx" comint-stop-subjob
      ))
  (with-eval-after-load 'find-func
    (user-define-keys user-query-map
      "f" find-function
      "v" find-variable)
    )
  (with-eval-after-load 'view
    (setcdr view-mode-map nil)

    (set-keymap-parent view-mode-map user-point-map)
    ;;(set-keymap-parent view-mode-map user-mark-map)
    ;;(set-keymap-parent view-mode-map user-kill-map)
    (user-define-keys view-mode-map
      "\"" View-back-to-mark
      "\C-j" View-scroll-line-forward
      "\C-k" View-scroll-line-backward
      "Q" View-quit
      "q" View-exit-and-edit
      ;; "\b" View-scroll-half-page-backward
      ;; "\r" View-scroll-half-page-forward
      )
    )
  (with-eval-after-load 'debug
    (setcdr debugger-mode-map nil)

    (set-keymap-parent debugger-mode-map user-point-map)
    (user-define-keys debugger-mode-map
      "\M-l" user-debugger-map)
    (user-define-keys user-debugger-map
      "-" negative-argument
      ":" debugger-record-expression
      ";" debugger-eval-expression
      "l" debugger-step-through
      "h" debugger-continue
      "f" debug-help-follow
      "g" debugger-jump
      "F" debugger-list-functions
      "M" debugger-frame-clear
      "m" debugger-frame
      "q" top-level
      "\r" debugger-return-value
      "\t" forward-button
      )
    )
  (with-eval-after-load 'lisp-mode
    (setcdr emacs-lisp-mode-map nil)
    (setcdr lisp-interaction-mode-map nil)
    (setcdr lisp-mode-map nil)
    (setcdr lisp-mode-shared-map nil)
    (setcdr lisp-mode-shared-map nil)

    (user-set-keymap-parent user-lisp-mode-eval-map user-eval-map)
    (user-set-keymap-parent user-lisp-mode-mark-map user-mark-map)
    (user-set-keymap-parent user-lisp-mode-kill-map user-kill-map)
    (user-set-keymap-parent user-lisp-mode-point-map user-point-map)

    (user-define-keys user-eval-map
      "]" eval-defun
      "[" eval-last-sexp
      "{" eval-print-last-sexp
      )

    (with-eval-after-load "lisp"
      (set-keymap-parent emacs-lisp-mode-map lisp-mode-map)
      ;;(set-keymap-parent emacs-user-lisp-mode-point-map user-point-map)

      (user-define-keys user-mark-map
        "d" mark-defun
        "L" mark-sexp
        )
      (user-define-keys user-kill-map
        "L" kill-sexp
        "H" backward-kill-sexp
        )
      (user-define-keys user-eval-map
        "(" check-parens
        )
      (user-define-keys user-edit-map
        "\"" insert-pair)

      (user-define-keys user-point-map
        "P" backward-up-list
        "n" down-list
        "p" up-list
        "d" beginning-of-defun
        "D" end-of-defun
        "\M-J" forward-sexp
        "\M-K" backward-sexp
        "N" backward-list
        )
      )
    )
  (with-eval-after-load 'scheme
    (setcdr scheme-mode-map nil)

    (set-keymap-parent scheme-mode-map (make-composed-keymap lisp-mode-map emacs-lisp-mode-map))
    (user-set-keymap-parent scheme-mode-user-eval-map user-lisp-mode-eval-map)

    (user-define-keys scheme-mode-map
      "\M-\(" scheme-mode-user-map
      )
    (user-define-keys scheme-mode-user-map
      "B" user-scheme-load-current-file
      "C" user-scheme-compile-current-file
      ";" run-scheme)
    )
  (with-eval-after-load 'python
    (setcdr python-mode-map nil)
    (setcdr inferior-python-mode-map nil)

    (user-define-keys python-mode-map
      "\M-l" user-python-mode-map
      )
    (user-define-keys user-python-mode-map
      "c" python-check
      "d" imenu
      ";" user-python-shell-map
      "t" user-python-skel-map
      )
    (user-define-keys user-python-shell-map
      ";" run-python
      "\"" python-shell-send-string
      "'" python-shell-send-region
      "d" python-shell-send-defun
      "f" python-shell-send-file
      ":" python-shell-switch-to-shell
      "b" python-shell-send-buffer
      )
    (user-define-keys user-python-skel-map
      "c" python-skeleton-class
      "d" python-skeleton-def
      "e" python-skeleton-for
      "c" python-skeleton-if
      "i" python-skeleton-import
      "t" python-skeleton-try
      "w" python-skeleton-while
      )
    )
  (with-eval-after-load 'hl-line
    (user-define-keys user-point-map
      "S" hl-line-reposition)
    (user-define-keys user-view-map
      "." hl-line-mode)
    )
  (with-eval-after-load 'button
    (setcdr button-map nil)
    (setcdr button-buffer-map nil)

    (set-keymap-parent button-buffer-map user-point-map)
    (user-define-keys button-buffer-map
      "\r" push-button
      ;;"\t" (lambda () (interactive) (forward-button 1))
      "\t" forward-button
      [backtab] backward-button
      )
    )
  (with-eval-after-load 'user-button
    (user-define-keys button-buffer-map
      "\M-x" button-buffer-user-map)
    (user-define-keys button-buffer-user-map
      "t" user-button-get-type
      "a" user-button-get-action
      "b" user-button-get-follow
      "f" user-button-get-function
      "p" user-button-get-args
      )
    )
  (with-eval-after-load 'package
    (setcdr package-menu-mode-map nil)

    (set-keymap-parent package-menu-mode-map user-point-map)
    (user-define-keys package-menu-mode-map
      "?" package-menu-describe-package
      "u" package-menu-mark-unmark
      "U" package-menu-backup-unmark
      "d" package-menu-mark-delete
      "i" package-menu-mark-install
      "I" package-menu-mark-upgrades
      "r" package-menu-refresh
      "X" package-menu-mark-obsolete-for-deletion
      "x" package-menu-execute
      "h" package-menu-quick-help
      )
    )
  (with-eval-after-load 'page
    (user-define-keys user-point-map
      "\C-n" forward-page
      "\C-p" backward-page)
    )
  (with-eval-after-load 'reposition
    (user-define-keys user-point-map
      "V" reposition-window
      )
    )
  (with-eval-after-load 'thingatpt
    (user-define-keys user-eval-map
      "}" eval-region-sexp-defun
      )
    )
  (with-eval-after-load 'hi-lock
    (setcdr hi-lock-map nil)

    (user-define-keys user-view-map
      "/" hi-lock-mode)
    (user-define-keys hi-lock-map
      ;;"" hi-lock-find-patterns
      ;;"" hi-lock-find-patterns
      ;;"" highlight-lines-matching-regexp
      ;;"" highlight-phrase
      ;;"" highlight-regexp
      ;;"" unhighlight-regexp
      ;;"" hi-lock-write-interactive-patterns
      "v" unhighlight-regexp
      "q" hi-lock-mode
      )
    )
  (with-eval-after-load 'abbrev
    (setcdr edit-abbrevs-mode-map nil)
    )
  (with-eval-after-load 'vc-dir
    (setcdr vc-dir-mode-map nil)

    (user-define-keys vc-dir-mode-map
      "\r" vc-next-action
      "=" vc-diff
      "a" vc-register
      "r" vc-update
      "l" vc-print-log
      "u" vc-revert
      "A" vc-annotate
      "mm" vc-dir-mark
      "mM" vc-dir-mark-all-files
      "Mm" vc-dir-unmark
      "MM" vc-dir-unmark-all-files
      "\d" vc-dir-unmark-file-up
      "j" vc-dir-next-line
      "J" vc-dir-next-directory
      "k" vc-dir-previous-line
      "K" vc-dir-previous-directory
      "f" vc-dir-find-file
      "F" vc-dir-find-file-other-window
      "!" vc-dir-kill-dir-status-process
      "d" vc-dir-kill-line
      "." vc-dir-hide-up-to-date
      "?" vc-dir-search
      "E" vc-dir-query-replace-regexp
      "/" vc-dir-isearch-regexp)
    )
  (with-eval-after-load 'vc-hooks
    (setcdr vc-prefix-map nil)

    ;; (define-key vc-prefix-map (kbd "c") 'vc-update-change-log)
    ;; (define-key vc-prefix-map (kbd "@") 'vc-switch-backend)
    (user-define-keys user-map
      "\M-^" vc-prefix-map)
    (user-define-keys vc-prefix-map
      "R" vc-rollback
      "m" vc-dir
      "A" vc-annotate
      "H" vc-insert-headers
      "a" vc-register
      "l" vc-print-log
      "L" vc-print-root-log
      "i" vc-log-incoming
      "I" vc-log-outgoing
      "c" vc-merge
      "g" vc-retrieve-tag
      "G" vc-create-tag
      "r" vc-revert
      "n" vc-next-action
      "p" vc-update
      "d" vc-diff
      "D" vc-root-diff
      "o" vc-revision-other-window)
    )
  (with-eval-after-load 'vc-annotate
    (setcdr vc-annotate-mode-map nil)

    (set-keymap-parent vc-annotate-mode-map user-view-map)
    (user-define-keys vc-annotate-mode-map
      "q" kill-buffer-force
      "\M-'" vc-annotate-map)
    (user-define-keys vc-annotate-map
      "." vc-annotate-revision-previous-to-line
      "d" vc-annotate-show-diff-revision-at-line
      "D" vc-annotate-show-changeset-diff-revision-at-line
      "f" vc-annotate-find-revision-at-line
      "l" vc-annotate-show-log-revision-at-line
      "\r" vc-annotate-revision-at-line
      "j" vc-annotate-next-revision
      "k" vc-annotate-prev-revision
      "\s" vc-annotate-working-revision
      "." vc-annotate-toggle-annotation-visibility
      "g" vc-annotate-goto-line)
    )
  (with-eval-after-load 'rcirc
    (setcdr rcirc-mode-map nil)

    (user-define-keys rcirc-mode-map
      "\r" rcirc-send-input
      "\t" rcirc-complete
      "\M-e" user-rcirc-edit-map
      "\M-x" user-rcirc-eval-map
      "\M-/" user-rcirc-query-map
      "\M-v" user-rcirc-view-map
      "\M-p" view-mode
      )
    (user-define-keys user-rcirc-edit-map
      "k" rcirc-insert-prev-input
      "j" rcirc-insert-next-input
      "e" rcirc-edit-multiline
      "i" rcirc-toggle-ignore-buffer-activity
      "b" rcirc-jump-to-first-unread-line
      )
    (user-define-keys user-rcirc-eval-map
      "j" rcirc-cmd-join
      "x" rcirc-cmd-kick
      "-" rcirc-toggle-low-priority
      "M" rcirc-cmd-mode
      "m" rcirc-cmd-msg
      "r" rcirc-cmd-nick
      "q" rcirc-cmd-part
      "p" rcirc-cmd-query
      "t" rcirc-cmd-topic
      "n" rcirc-cmd-names
      "?" rcirc-cmd-whois
      "Q" rcirc-cmd-quit
      ;"r" rcirc-cmd-reconnect
      )
    (user-define-keys user-rcirc-query-map
      "." rcirc-switch-to-server-buffer
      "b" user-rcirc-ido
      )
    (user-define-keys user-rcirc-view-map
      "v" rcirc-omit-mode
      "u" rcirc-browse-url)

    (user-set-keymap-parent user-rcirc-query-map user-query-map)
    )
  (with-eval-after-load 'erc
    (setcdr erc-mode-map nil)

    (user-set-keymap-parent user-erc-buffer-map user-buffer-map)
    (user-set-keymap-parent user-erc-point-map user-point-map)
    (user-set-keymap-parent user-erc-eval-map user-eval-map)
    (user-set-keymap-parent user-erc-kill-map user-kill-map)

    (user-define-keys erc-mode-map
      "\M-i" user-erc-repl-map
      "\r" erc-send-current-line
      "\M-:" user-erc-exec-map
      "\M-b" user-erc-buffer-map
      "\M-p" user-erc-point-map
      "\M-;" user-erc-eval-map
      "\M-\t" ispell-complete-word
      "\t" completion-at-point
      )
    (user-define-keys user-erc-point-map
      "t" erc-bol
      )
    (user-define-keys user-erc-kill-map
      "k" erc-kill-input
      )
    (user-define-keys user-erc-eval-map
      ";" erc-input-action
      "-" erc-toggle-interpret-controls
      "*" erc-toggle-flood-control
      "i" erc-invite-only-mode
      "z" erc-toggle-ctcp-autoresponse
      "@" erc-channel-names
      "#" erc-get-channel-mode-from-keypress
      "x" erc-part-from-channel
      "X" erc-quit-server
      ;;erc-remove-text-properties-region
      "e" erc-set-topic
      )
    (user-define-keys user-erc-exec-map
      )
    (user-define-keys user-erc-buffer-map
      "/" erc-switch-to-buffer
      "f" erc-join-channel
      )
    )
  (with-eval-after-load 'flyspell
    (setcdr flyspell-mouse-map nil)
    (setcdr flyspell-mode-map nil)

    (user-define-keys user-edit-map
      "c" user-flyspell-mode-map)
    ;;(user-define-keys flyspell-mode-map
    ;;"\M-s" user-flyspell-mode-map)
    (user-define-keys user-flyspell-mode-map
      "." flyspell-auto-correct-word
      "," flyspell-auto-correct-previous-word
      "/" flyspell-goto-next-error
      "?" flyspell-correct-word-before-point
      "b" flyspell-buffer
      "r" flyspell-region
      )
    )
  )
(progn ;; External
  (with-eval-after-load 'yasnippet
    (setcdr yas-minor-mode-map nil)
    ;;yas-expand
    ;;yas-expand
    ;;yas-insert-snippet
    ;;yas-new-snippet
    ;;yas-visit-snippet-file
    )
  (with-eval-after-load 'jedi-core
    (setcdr jedi-mode-map nil)

    (user-define-keys jedi-mode-map
      "\M-hd" jedi:show-doc
      "\M-hD" jedi:goto-definition
      "\M-h[" jedi:goto-definition-pop-marker
      )
    )
  (with-eval-after-load 'web-mode
    (setcdr web-mode-map nil)

    )
  (with-eval-after-load 'magit
    (setcdr magit-untracked-section-map nil)
    (setcdr magit-refs-mode-map nil)
    (setcdr magit-branch-section-map nil)
    (setcdr magit-remote-section-map nil)
    (setcdr magit-tag-section-map nil)
    (setcdr magit-worktree-section-map nil)
    (setcdr magit-file-mode-map nil)
    (setcdr magit-blob-mode-map nil)
    (setcdr magit-status-mode-map nil)

    (with-eval-after-load 'magit-mode
      (setcdr magit-mode-map nil)
      (setcdr magit-diff-mode-map nil)
      (setcdr magit-file-section-map nil)
      (setcdr magit-hunk-section-map nil)
      (setcdr magit-unstaged-section-map nil)
      (setcdr magit-staged-section-map nil)
      (setcdr magit-stashes-section-map nil)
      (setcdr magit-stash-section-map nil)
      (setcdr magit-log-read-revs-map nil)
      (setcdr magit-log-mode-map nil)
      (setcdr magit-commit-section-map nil)
      (setcdr magit-module-commit-section-map nil)
      (setcdr magit-log-select-mode-map nil)
      (setcdr magit-cherry-mode-map nil)
      (setcdr magit-reflog-mode-map nil)
      (setcdr magit-unpulled-section-map nil)
      (setcdr magit-unpushed-section-map nil)
      (setcdr magit-process-mode-map nil)
      ;;(setcdr magit-popup-mode-map nil)

      (user-define-keys user-buffer-map
        "\M-g" magit-process-buffer
        )

      ;;(user-set-keymap-parent magit-mode-map user-point-map)
      (user-define-keys global-map
        "\M-g" magit-mode-map
        )
      (user-define-keys magit-mode-map
        "#" magit-gitignore
        "+" magit-diff-more-context
        "-" magit-diff-less-context
        "." magit-log-popup
        ";" magit-dispatch-popup
        "=" magit-diff-default-context
        "B" magit-checkout
        "C" magit-commit-create
        "D" magit-diff-show-or-scroll-down
        "g" magit-status
        "E" magit-rebase-interactive
        "F" magit-pull-from-pushremote
        "I" magit-stage-modified
        "J" scroll-up
        "K" scroll-down
        "M" magit-rebase-onto-pushremote
        "O" (lambda () (interactive) (magit-dired-jump 't))
        "R" magit-refresh-all
        "W" magit-patch-popup
        "S" magit-push-current-to-pushremote
        "U" magit-unstage-all
        "V" magit-section-cycle
        "Z" magit-jump-to-stashes
        "\M-V" magit-section-cycle-global
        "\M-j" magit-section-forward-sibling
        "\M-k" magit-section-backward-sibling
        "\M-o" magit-visit-thing
        "\M-s" magit-jump-to-staged
        "\M-u" magit-jump-to-unstaged
        "\M-v" magit-section-cycle-diffs
        "b" magit-branch-popup
        "c" magit-commit
        "d" magit-diff-popup
        "f" magit-pull-popup
        "h" magit-go-backward
        "i" magit-stage
        "j" next-line
        "k" previous-line
        "l" magit-go-forward
        "m" magit-rebase-popup
        "n" magit-section-forward
        "o" magit-find-file
        "p" magit-section-backward
        "x" magit-mode-bury-buffer
        "r" magit-refresh
        "s" magit-push-popup
        "t" magit-jump-to-tracked
        "u" magit-unstage
        "v" magit-section-toggle
        "yR" magit-copy-buffer-revision
        "yr" magit-copy-section-value
        "z" magit-stash-popup
        ;;"n" magit-jump-to-untracked
        ;;"y" magit-file-untrack
        )
      ;;"J" magit-diff-show-or-scroll-down
      ;;"J" magit-diff-show-or-scroll-up
      ;;"K" magit-section-up
      ;;"x" magit-reset-popup
      (user-define-keys magit-diff-mode-map
        "o" magit-diff-visit-file
        "O" magit-diff-visit-file-worktree
        "g" magit-jump-to-diffstat-or-diff
        "r" magit-diff-refresh-popup
        "t" magit-diff-toggle-refine-hunk
        "Z" magit-diff-switch-range-type
        "z" magit-diff-flip-revs
        "-" magit-diff-less-context
        "+" magit-diff-more-context
        "0" magit-diff-default-context
        "c" magit-diff-while-committing
        )
      (user-define-keys magit-log-mode-map
        "r" magit-log-refresh-popup
        "R" magit-log-refresh
        "q" magit-log-bury-buffer
        "+" magit-log-double-commit-limit
        "=" magit-log-toggle-commit-limit
        "-" magit-log-half-commit-limit
        "g" magit-log-select-pick
        "Q" magit-log-select-quit
        "v" magit-toggle-margin
        )
      (user-define-keys magit-popup-mode-map
        ;;[remap self-insert-command] magit-invoke-popup-action

        "-" magit-invoke-popup-switch
        "+" magit-invoke-popup-option
        ;;"h" magit-popup-help
        "q" magit-popup-quit
        ;;"S" magit-popup-save-default-arguments
        "=" magit-popup-set-default-arguments
        [backtab] backward-button
        [tab] forward-button
        [return] push-button
        )

      (user-define-keys magit-worktree-section-map
        [remap magit-visit-thing] magit-worktree-status)

      (user-set-keymap-parent magit-process-mode-map user-point-map)
      (user-set-keymap-parent magit-process-mode-map magit-mode-map)
      )
    (with-eval-after-load 'git-rebase
      (setcdr git-rebase-mode-map nil)

      (user-set-keymap-parent git-rebase-mode-map user-point-map)
      (user-define-keys git-rebase-mode-map
        [remap undo] git-rebase-undo
        "\M-v" git-rebase-show-commit
        "d" magit-diff-show-or-scroll-up
        ";" git-rebase-exec
        "c" git-rebase-pick
        "n" git-rebase-reword
        "e" git-rebase-edit
        "z" git-rebase-squash
        "Z" git-rebase-fixup
        "i" git-rebase-insert
        "y" git-rebase-kill-line
        "k" git-rebase-backward-line
        "j" forward-line
        "\M-j" git-rebase-move-line-down
        "\M-k" git-rebase-move-line-up
        ))
    )
  (with-eval-after-load 'message
    (setcdr message-mode-map nil)

    (user-define-keys message-mode-map
      "\M-e\r" message-newline-and-reformat
      "\t" message-tab
      "\M-[" user-message-mode-map)
    (user-define-keys user-message-mode-map
      ";" user-message-eval-map
      "e" user-message-edit-map)
    (user-define-keys user-message-eval-map
      "s" message-send-and-exit
      "S" message-send
      "q" message-kill-buffer
      "x" message-dont-send
      )
    (user-define-keys user-message-edit-map
      "a" mml-attach-file
      "t" message-goto-to
      "f" message-goto-from
      "B" message-goto-bcc
      "D" message-goto-fcc
      "c" message-goto-cc
      "s" message-goto-subject
      "S" message-goto-signature
      "r" message-goto-reply-to
      "g" message-goto-newsgroups
      "d" message-goto-distribution
      "R" message-goto-followup-to
      "m" message-goto-mail-followup-to
      "k" message-goto-keywords
      "C" message-goto-summary
      "i" message-insert-to
      "b" message-goto-body))
  (with-eval-after-load 'notmuch
    (setcdr notmuch-search-mode-map nil)
    (setcdr notmuch-common-keymap nil)
    (setcdr notmuch-search-stash-map nil)
    (setcdr notmuch-hello-mode-map nil)
    (setcdr notmuch-tree-mode-map nil)
    (setcdr notmuch-message-mode-map nil)
    (setcdr notmuch-show-stash-map nil)
    (setcdr notmuch-show-part-map nil)
    (setcdr notmuch-show-mode-map nil)

    ;; User
    (defvar user-notmuch-tags-alist '())
    (setq user-notmuch-tags-alist
          '(("x" ("-inbox" "-unread" "+archived"))
            ("r" ("-unread"))))
    (defun user-notmuch-tag-macro (key)
      (interactive "k")
      (message "%s" key)
      (let ((macro (cadr (assoc key user-notmuch-tags-alist))))
        (and macro
             (cond
              ((eq 'notmuch-search-mode major-mode)
               (notmuch-search-tag macro))
              ((eq 'notmuch-tree-mode major-mode)
               (notmuch-tree-set-tags macro))
              ((eq 'notmuch-message-mode major-mode)
               (notmuch-show-tag-message macro))))
        (notmuch-tree-next-message)))
    (user-define-keys notmuch-common-keymap
      "`" user-notmuch-tag-macro)

    (user-define-keys notmuch-hello-mode-map
      "j" widget-forward
      "\t" widget-forward
      "k" widget-backward)
    (user-define-keys notmuch-common-keymap
      "H" notmuch-help
      "x" notmuch-bury-or-kill-this-buffer
      "/" notmuch-search
      "+" notmuch-tree
      "c" notmuch-mua-new-mail
      "r" notmuch-refresh-this-buffer
      "R" notmuch-refresh-all-buffers
      "f" notmuch-poll-and-refresh-this-buffer
      "?" notmuch-jump-search)
    (user-define-keys notmuch-search-mode-map
      "K" notmuch-search-scroll-down
      "J" notmuch-search-scroll-up
      "t" notmuch-search-first-thread
      "b" notmuch-search-last-thread
      "h" notmuch-search-previous-thread
      "l" notmuch-search-next-thread
      "E" notmuch-search-reply-to-thread-sender
      ;;"E" notmuch-search-reply-to-thread
      "s" notmuch-search-toggle-order
      "z" notmuch-search-stash-map
      "\"" notmuch-search-filter-by-tag
      "'" notmuch-search-filter
      "n" notmuch-tag-jump
      "*" notmuch-search-tag-all
      "a" notmuch-search-archive-thread
      "-" notmuch-search-remove-tag
      "+" notmuch-search-add-tag
      "v" notmuch-search-show-thread
      "j" notmuch-tree-next-message
      "k" notmuch-tree-prev-message
      "]" notmuch-tree-next-matching-message
      "[" notmuch-tree-prev-matching-message
      "T" notmuch-tree-from-search-current-query)
    (user-define-keys notmuch-search-stash-map
      "i" notmuch-search-stash-thread-id
      "q" notmuch-stash-query
      "?" notmuch-subkeymap-help)
    (user-define-keys notmuch-tree-mode-map
      [remap notmuch-help] (notmuch-tree-close-message-pane-and #'notmuch-help)
      [remap notmuch-bury-or-kill-this-buffer] notmuch-tree-quit
      [remap notmuch-search] notmuch-tree-to-search
      [remap notmuch-mua-new-mail] (notmuch-tree-close-message-pane-and #'notmuch-mua-new-mail)
      [remap notmuch-jump-search] (notmuch-tree-close-message-pane-and #'notmuch-jump-search)
      "j" notmuch-tree-next-message
      "k" notmuch-tree-prev-message
      "x" notmuch-tree-quit
      "J" notmuch-tree-next-matching-message
      "K" notmuch-tree-prev-matching-message
      "]" notmuch-tree-prev-thread
      "[" notmuch-tree-next-thread
      " " notmuch-tree-scroll-or-next
      "g" notmuch-tag-jump
      "-" notmuch-tree-remove-tag
      "+" notmuch-tree-add-tag
      "t" notmuch-tree-to-tree
      "{" notmuch-tree-archive-thread
      "}" notmuch-tree-archive-thread-then-next
      "\r" notmuch-tree-show-message
      "\t" (notmuch-tree-to-message-pane #'notmuch-show-next-button)
      "<backtab>" (notmuch-tree-to-message-pane #'notmuch-show-next-button)
      ">" (notmuch-tree-to-message-pane #'notmuch-show-next-button)
      "<" (notmuch-tree-to-message-pane #'notmuch-show-prev-button)
      "\\" (notmuch-tree-to-message-pane #'notmuch-show-toggle-process-crypto)
      "/" notmuch-search-from-tree-current-query
      "|" notmuch-show-pipe-message
      "w" notmuch-show-save-attachments
      "z" notmuch-show-view-all-mime-parts
      "c" notmuch-show-stash-map
      "s" notmuch-show-resend-message)
    (user-define-keys notmuch-message-mode-map
      "\M-z" notmuch-mua-send-and-exit
      "\M-Z" notmuch-mua-send
      "\M-[W" notmuch-draft-postpone
      "\M-[w" notmuch-draft-save
      "\M-[x" notmuch-bury-or-kill-this-buffer)
    (user-define-keys notmuch-show-stash-map
      "c" notmuch-show-stash-cc
      "d" notmuch-show-stash-date
      "F" notmuch-show-stash-filename
      "f" notmuch-show-stash-from
      "i" notmuch-show-stash-message-id
      "I" notmuch-show-stash-message-id-stripped
      "s" notmuch-show-stash-subject
      "T" notmuch-show-stash-tags
      "t" notmuch-show-stash-to
      "l" notmuch-show-stash-mlarchive-link
      "L" notmuch-show-stash-mlarchive-link-and-go
      "G" notmuch-show-stash-git-send-email
      "?" notmuch-subkeymap-help)
    (user-define-keys notmuch-show-part-map
      "w" notmuch-show-save-part
      "v" notmuch-show-view-part
      "V" notmuch-show-interactively-view-part
      "|" notmuch-show-pipe-part
      "i" notmuch-show-choose-mime-of-part
      "?" notmuch-subkeymap-help)
    (user-define-keys notmuch-show-mode-map
      "," notmuch-show-stash-map
      "." notmuch-show-part-map

      "#" notmuch-show-print-message
      "*" notmuch-show-open-or-close-all
      "+" notmuch-show-add-tag
      "-" notmuch-show-remove-tag
      "=" notmuch-show-view-raw-message
      "A" notmuch-show-tag-all
      "H" notmuch-show-previous-open-message
      "L" notmuch-show-next-open-message
      "P" notmuch-show-rewind
      "R" notmuch-show-reply
      "U" notmuch-show-toggle-process-crypto
      "V" notmuch-show-toggle-visibility-headers
      "W" notmuch-show-forward-open-messages
      "\\" notmuch-show-save-attachments
      "]" notmuch-show-archive-message-then-next-or-next-thread
      "_" toggle-truncate-lines
      "a" notmuch-tag-jump
      "b" notmuch-show-advance-and-archive
      "e" notmuch-show-resume-message
      "f" notmuch-show-filter-thread
      "h" notmuch-show-previous-message
      "i" notmuch-show-toggle-thread-indentation
      "l" notmuch-show-next-message
      "n" notmuch-show-next-thread-show
      "o" notmuch-show-toggle-elide-non-matching
      "p" notmuch-show-previous-thread-show
      "r" notmuch-show-reply-sender
      "t" notmuch-tree-from-show-current-query
      "v" notmuch-show-toggle-message
      "w" notmuch-show-forward-message
      "z" notmuch-show-resend-message
      "|" notmuch-show-pipe-message
      "}" notmuch-show-archive-thread-then-next
      ;;notmuch-show-archive-message-then-next-or-exit
      "<backtab>" notmuch-show-previous-button
      "<tab>" widget-backward)

    (set-keymap-parent notmuch-common-keymap user-point-map)
    (set-keymap-parent notmuch-hello-mode-map (make-composed-keymap
                                               (list widget-keymap notmuch-common-keymap)))
    (set-keymap-parent notmuch-search-mode-map notmuch-common-keymap)
    (set-keymap-parent notmuch-tree-mode-map notmuch-common-keymap)
    (set-keymap-parent notmuch-show-mode-map notmuch-common-keymap))
  (with-eval-after-load "git-commit"
    (setcdr git-commit-mode-map nil)

    ;;(user-set-keymap-parent git-commit-mode-map user-point-map)
    (user-define-keys git-commit-mode-map
      "\M-x" user-git-map)
    (user-define-keys user-git-map
      "w" git-commit-save-message
      "k" git-commit-prev-message
      "j" git-commit-next-message))
  (with-eval-after-load 'smartparens
    (setcdr sp-smartparens-bindings nil)
    (setcdr smartparens-mode-map nil)
    (setcdr smartparens-strict-mode-map nil)

    (user-define-keys global-map
      "\M-(" smartparens-map)
    (user-define-keys smartparens-map
      "\{" sp-backward-barf-sexp
      "i" sp-backward-down-sexp
      "H" sp-backward-sexp
      "\[" sp-backward-slurp-sexp
      "h" sp-backward-symbol
      "W" sp-backward-unwrap-sexp
      "p" sp-backward-up-sexp
      "t" sp-beginning-of-sexp
      "y" sp-copy-sexp
      "j" sp-down-sexp
      "b" sp-end-of-sexp
      "\}" sp-forward-barf-sexp
      "j" sp-forward-sexp
      "\]" sp-forward-slurp-sexp
      "l" sp-forward-symbol
      "Y" sp-kill-sexp
      "N" sp-next-sexp
      "P" sp-previous-sexp
      "/s" sp-select-next-thing
      "/S" sp-select-next-thing-exchange
      "\t" sp-splice-sexp
      "*" sp-splice-sexp-killing-around
      "\M-[" sp-splice-sexp-killing-backward
      "\M-]" sp-splice-sexp-killing-forward
      "\(" sp-split-sexp
      "W" sp-unwrap-sexp
      "k" sp-up-sexp
      ;"w" sp-wrap-with-pair-ask
      "w" sp-mark-sexp
      )
    ;; sp-next-sexp
    ;; sp-previous-sexp
    )
  (with-eval-after-load 'company
    (setcdr company-active-map nil)
    (setcdr company-mode-map nil)

    (user-define-keys company-active-map
      ;;company-abort
      "\M-q" company-abort
      "\M-j" company-select-next
      "\M-k" company-select-previous
      "\M-J" company-next-page
      "\M-K" company-previous-page
      "\r" company-complete-selection
      "\t" company-complete-common
      "\M-?" company-show-doc-buffer
      ">" company-show-location
      "/" company-search-candidates
      "<" company-filter-candidates
      "1" company-complete-number
      "2" company-complete-number
      "3" company-complete-number
      "4" company-complete-number
      "5" company-complete-number
      "6" company-complete-number
      "7" company-complete-number
      "8" company-complete-number
      "9" company-complete-number)
    )
  (with-eval-after-load 'auto-complete
    (setcdr ac-completing-map nil)

    (user-define-keys global-map
      "\C-\M-i" auto-complete)
    (user-define-keys ac-completing-map
      ;;"\t" ac-expand
      "\r" ac-complete
      "\M-j" ac-next
      "\M-k" ac-previous
      "\M-/" user-ac-completing-map-query)
    (user-define-keys user-ac-completing-map-query
      "a" ac-help
      "A" ac-persist-help
      "j" ac-quick-help-scroll-down
      "k" ac-quick-help-scroll-up
      "/" ac-isearch
      )
    )
  (with-eval-after-load 'popup
    (setcdr popup-menu-keymap nil)

    (user-define-keys popup-menu-keymap
      "\r" popup-select
      "\M-j" popup-next
      "\M-k" popup-previous
      "\M-J" popup-page-next
      "\M-K" popup-page-previous)
    (user-define-keys user-help-map
      "p" popup-help)
    (user-define-keys user-query-map
      "p" popup-isearch)
    (user-define-keys user-view-map
      "A" popup-open
      "a" popup-close)
    )
  (with-eval-after-load 'paredit
    (setcdr paredit-mode-map nil)

    (user-define-keys paredit-mode-map
      "(" paredit-open-round
      ")" paredit-close-round
      "M-)" paredit-close-round-and-newline
      "[" paredit-open-square
      "]" paredit-close-square
      "\"" paredit-doublequote
      "M-\"" paredit-meta-doublequote
      "\\" paredit-backslash
      ";" paredit-semicolon
      "M-#" paredit-comment-dwim
      "C-j" paredit-newline
      "C-d" paredit-forward-delete
      ;;paredit-backward-delete-key paredit-backward-delete
      "C-k" paredit-kill
      "M-d" paredit-forward-kill-word
      ;;,(concat "M-" paredit-backward-delete-key) paredit-backward-kill-word
      "C-M-f" paredit-forward
      "C-M-b" paredit-backward
      "C-M-u" paredit-backward-up
      "C-M-d" paredit-forward-down
      "M-(" paredit-wrap-round
      "M-s" paredit-splice-sexp
      ;;paredit-splice-sexp-killing-forward
      ;;"M-r" paredit-raise-sexp
      ;;"M-?" paredit-convolute-sexp
      "C-}" paredit-forward-barf-sexp
      "C-(" paredit-backward-slurp-sexp
      "C-{" paredit-backward-barf-sexp
      "M-S" paredit-split-sexp
      "M-J" paredit-join-sexps
      "C-c C-M-l" paredit-recenter-on-sexp
      "M-q" paredit-reindent-defun))
  (with-eval-after-load 'geiser
    (with-eval-after-load 'geiser-mode
      (with-eval-after-load 'geiser-repl
        (with-eval-after-load 'geiser-doc
          (with-eval-after-load 'geiser-popup
            (setcdr geiser-mode-map nil)
            (setcdr geiser-doc-mode-map nil)
            (setcdr geiser-repl-mode-map nil)
            (setcdr geiser-debug-mode-map nil)
            (setcdr geiser-popup--overriding-map nil)

            (set-keymap-parent geiser-repl-mode-map comint-mode-map)
            (set-keymap-parent geiser-doc-mode-map button-buffer-map)
            (set-keymap-parent geiser-debug-mode-map button-buffer-map)
            (user-set-keymap-parent geiser-user-help-map help-map)
            (user-set-keymap-parent geiser-user-eval-map user-eval-map)

            (user-define-keys user-eval-map
              "g" switch-to-geiser)
            (user-define-keys user-mode-map
              "\M-A" geiser-autodoc-mode)

            (user-define-keys geiser-mode-map
              "\M-?" geiser-user-help-map
              "\M-/" geiser-user-query-map
              "\M-;" geiser-user-eval-map
              "\M-f" geiser-user-file-map
              )
            (user-define-keys geiser-user-map
              "a" geiser-autodoc-mode
              )
            (user-define-keys geiser-user-eval-map
              "." geiser-mode-switch-to-repl
              "s" geiser-eval-last-sexp
              "b" geiser-eval-buffer
              "'" geiser-eval-definition
              "\"" geiser-eval-definition-and-go
              "`" geiser-eval-region
              "G" geiser-set-scheme
              )
            (user-define-keys geiser-user-help-map
              "d" geiser-doc-symbol-at-point
              ";" geiser-autodoc-show
              "D" geiser-doc-module
              "g" geiser-doc-look-up-manual
              )
            (user-define-keys geiser-user-query-map
              "f" geiser-edit-symbol-at-point
              "d" geiser-edit-module
              )
            (user-define-keys geiser-user-file-map
              "a" geiser-add-to-load-path
              "l" geiser-load-file
              "L" geiser-load-current-buffer
              )
            (user-define-keys geiser-user-buffer-map
              "?" (lambda () (interactive) (display-buffer "*Geiser documentation*"))
              )

            (user-define-keys geiser-repl-mode-map
              "\r" geiser-repl--maybe-send
              ;;"\t" geiser-repl-tab-dwim
              "\M-v" view-mode
              ;;"\M-p" user-point-map
              ";" geiser-repl-user-eval-map
              "\M-Q" switch-to-geiser
              ;;"\M-e" geiser-repl-user-edit-map
              "\M-?" geiser-repl-user-help-map)
            (user-define-keys geiser-repl-user-eval-map
              "d" geiser-repl-import-module
              "D" switch-to-geiser-module
              "x" geiser-repl-interrupt
              "c" geiser-compile-current-buffer
              "q" geiser-repl-exit
              "/" geiser-add-to-load-path)
            (user-define-keys geiser-repl-user-edit-map
              "k" comint-previous-input
              "j" comint-next-input
              "y" comint-kill-input
              "J" comint-next-matching-input-from-input
              "K" comint-previous-matching-input-from-input
              "r" geiser-repl-clear-buffer
              )
            (user-define-keys geiser-repl-user-help-map
              "s" geiser-doc-symbol-at-point
              "d" geiser-doc-module)

            (user-define-keys geiser-doc-mode-map
              "q" other-window)

            (user-define-keys geiser-debug-mode-map
              "q" other-window)

            ;;(user-set-keymap-parent geiser-user-eval-map geiser-repl-user-eval-map)
            ;;(set-keymap-parent geiser-popup--overriding-map Info-mode-map)
            )
          )
        )
      )
    )
  (with-eval-after-load 'with-editor
    (setcdr with-editor-mode-map nil)

    (user-define-keys with-editor-mode-map
      "\M-z" with-editor-finish
      "\M-Q" with-editor-cancel
      )
    )
  (with-eval-after-load 'xah-css-mode
    (setcdr xah-css-key-map nil)

    (user-define-keys xah-css-key-map
      "\t" xah-css-complete-or-indent)
    )
  (with-eval-after-load 'skewer-mode
    (setcdr skewer-mode-map nil)

    (user-define-keys skewer-mode-map
      "\M-zb" skewer-load-buffer
      "\M-z:" skewer-eval-defun
      "\M-z;" skewer-eval-last-expression
      "\M-zx" skewer-repl
      ))
  (with-eval-after-load 'flycheck
    (user-define-keys user-syntax-map
      "f" flycheck-buffer
      )
    )
  )
(progn ;; Sort
  (with-eval-after-load "adoc-mode"
    (setcdr adoc-mode-map nil)

    (user-define-keys (adoc-map "\M-M" adoc-mode-map)
      "d" adoc-denote
      "p" adoc-promote
      "t" adoc-toggle-title-type
      "l" adoc-goto-ref-label
      )
    )
  (with-eval-after-load "bmk"
    (user-define-keys user-buffer-map
      "B" bmk-buffer)
    )
  (with-eval-after-load "browse-url"
    (user-define-keys global-map
      "\M-u" uri-map)
    (user-define-keys uri-map
      "o" browse-url-of-buffer
      "r" browse-url-of-region
      "p" browse-url-at-point
      "E" browse-url-emacs
      )
    )
  (with-eval-after-load "calc"
    ;; (setcdr calc-mode-map nil)
    ;; (setcdr calc-digit-map nil)
    ;;
    ;; (user-define-keys calc-mode-map
    ;;   "+" calc-plus
    ;;   "-" calc-minus
    ;;   "*" calc-times
    ;;   "/" calc-divide
    ;;   "%" calc-mod
    ;;   "&" calc-inv
    ;;   "^" calc-power
    ;;   "\M-." calc-percent
    ;;   "e" calcDigit-start
    ;;   "i" calc-info
    ;;   "n" calc-change-sign
    ;;   "q" calc-quit
    ;;   "Y" nil
    ;;   "Y?" calc-shift-Y-prefix-help
    ;;   " " calc-enter
    ;;   "" calc-algebraic-entry
    ;;   "$" calc-auto-algebraic-entry
    ;;   "\"" calc-auto-algebraic-entry
    ;;   "\t" calc-roll-down
    ;;   "\M-\t" calc-roll-up
    ;;   "\C-x\C-t" calc-transpose-lines
    ;;   "\C-m" calc-enter
    ;;   "\M-\C-m" calc-last-args-stub
    ;;   "\C-j" calc-over
    ;;   "\C-y" calc-yank
    ;;   [remap undo] calc-undo
    ;;   )
    ;; (mapc (lambda (x) (define-key calc-mode-map (char-to-string x) 'calc-missing-key))
    ;;     (concat "ABCDEFGHIJKLMNOPQRSTUVXZabcdfghjkmoprstuvwxyz"
    ;;       ":\\|!()[]<>{},;=~`"))
    ;; (mapc (lambda (x) (define-key calc-mode-map (char-to-string x) 'calcDigit-start))
    ;;     "_0123456789.#@")
    ;;
    ;; (user-define-keys calc-digit-map
    ;;   "'" calcDigit-algebraic
    ;;   "`" calcDigit-edit
    ;;   "\C-g" abort-recursive-edit
    ;;   )
    ;; (map-keymap (lambda (key bind)
    ;;       (define-key calc-digit-map (vector key)
    ;;         (if (eq bind 'undefined)
    ;;       'undefined 'calcDigit-nondigit)))
    ;;     calc-mode-map)
    ;; (mapc (lambda (x) (define-key calc-digit-map (char-to-string x) 'calcDigit-key))
    ;;     "_0123456789.e+-:n#@oh'\"mspM")
    ;; (mapc (lambda (x) (define-key calc-digit-map (char-to-string x) 'calcDigit-letter))
    ;;     "abcdfgijklqrtuvwxyzABCDEFGHIJKLNOPQRSTUVWXYZ")
    )
  (with-eval-after-load "calculator"
    (user-define-keys calculator-mode-map
      "k" calculator-saved-up
      "j" calculator-saved-down
      "q" calculator-save-and-quit
      "h" calculator-help
      "y" calculator-copy
      "d" calculator-clear
      "D" calculator-clear-saved
      "r" calculator-set-register
      "g" calculator-get-register
      )
    )
  (with-eval-after-load "calendar"
    (setcdr calendar-mode-map nil)

    (defvar calendar-help-map (make-sparse-keymap))
    (set-keymap-parent calendar-help-map help-map)

    (user-define-keys calendar-mode-map
      "#" calendar-list-holidays
      "@" calendar-cursor-holidays
      "[" calendar-scroll-left-three-months
      "\C-j" calendar-forward-year
      "\C-k" calendar-backward-year
      "\d" calendar-backward-day
      [?\A-b] calendar-end-of-year
      [?\A-j] calendar-forward-month
      [?\A-k] calendar-backward-month
      [?\A-t] calendar-beginning-of-year
      "]" calendar-scroll-right-three-months
      "b" calendar-end-of-week
      "B" calendar-end-of-month
      "H" calendar-scroll-left
      "j" calendar-forward-day
      "J" calendar-forward-week
      "k" calendar-backward-day
      "K" calendar-backward-week
      "L" calendar-scroll-right
      "o" calendar-other-month
      "q" calendar-exit
      "t" calendar-beginning-of-week
      "T" calendar-beginning-of-month

      "h" calendar-help-map
      "v" calendar-view-map
      "m" calendar-mark-map
      "a" calendar-appt-map
      "d" calendar-diary-map
      "p" calendar-print-map
      "s" calendar-save-map
      )
    (user-define-keys calendar-help-map
      "?" calendar-goto-info-node
      )
    (user-define-keys calendar-mark-map
      "=" calendar-count-days-region
      "m" calendar-set-mark
      "u" calendar-unmark
      "x" calendar-mark-holidays
      "z" calendar-exchange-point-and-mark
      )
    (user-define-keys calendar-view-map
      "." calendar-goto-today
      "d" calendar-goto-date
      "D" calendar-goto-day-of-year
      "M" calendar-lunar-phases
      "r" calendar-redraw
      "S" calendar-sunrise-sunset
      "w" calendar-iso-goto-week
      ;; "j" calendar-julian-goto-date
      ;; "ga" calendar-astro-goto-day-number
      ;; "gh" calendar-hebrew-goto-date
      ;; "gi" calendar-islamic-goto-date
      ;; "gb" calendar-bahai-goto-date
      ;; "gC" calendar-chinese-goto-date
      ;; "gk" calendar-coptic-goto-date
      ;; "ge" calendar-ethiopic-goto-date
      ;; "gp" calendar-persian-goto-date
      ;; "gc" calendar-iso-goto-date
      ;; "gml" calendar-mayan-goto-long-count-date
      ;; "gmpc" calendar-mayan-previous-round-date
      ;; "gmnc" calendar-mayan-next-round-date
      ;; "gmph" calendar-mayan-previous-haab-date
      ;; "gmnh" calendar-mayan-next-haab-date
      ;; "gmpt" calendar-mayan-previous-tzolkin-date
      ;; "gmnt" calendar-mayan-next-tzolkin-date
      ;;"gf" calendar-french-goto-date
      )
    (user-define-keys calendar-appt-map
      "a" appt-add
      "x" appt-delete
      )
    (user-define-keys calendar-diary-map
      "a" diary-show-all-entries
      "ia" diary-insert-anniversary-entry
      "ib" diary-insert-block-entry
      "iBd" diary-bahai-insert-entry
      "iBm" diary-bahai-insert-monthly-entry
      "iBy" diary-bahai-insert-yearly-entry
      "ic" diary-insert-cyclic-entry
      "ihd" diary-hebrew-insert-entry
      "ihm" diary-hebrew-insert-monthly-entry
      "ihy" diary-hebrew-insert-yearly-entry
      "ii" diary-insert-entry
      "im" diary-insert-monthly-entry
      "iw" diary-insert-weekly-entry
      "iy" diary-insert-yearly-entry
      "jid" diary-islamic-insert-entry
      "jim" diary-islamic-insert-monthly-entry
      "jiy" diary-islamic-insert-yearly-entry
      "m" diary-mark-entries
      "v" diary-view-entries
      "V" diary-view-other-diary-entries
      )
    (user-define-keys calendar-print-map
      "a" calendar-astro-print-day-number
      "b" calendar-bahai-print-date
      "C" calendar-chinese-print-date
      "c" calendar-iso-print-date
      "d" calendar-print-day-of-year
      "e" calendar-ethiopic-print-date
      "f" calendar-french-print-date
      "h" calendar-hebrew-print-date
      "i" calendar-islamic-print-date
      "j" calendar-julian-print-date
      "k" calendar-coptic-print-date
      "m" calendar-mayan-print-date
      "p" calendar-persian-print-date
      "P" calendar-print-other-dates
      )
    (user-define-keys calendar-save-map
      "hm" cal-html-cursor-month
      "hy" cal-html-cursor-year
      "td" cal-tex-cursor-day
      "tfd" cal-tex-cursor-filofax-daily
      "tfw" cal-tex-cursor-filofax-2week
      "tfW" cal-tex-cursor-filofax-week
      "tfy" cal-tex-cursor-filofax-year
      "tm" cal-tex-cursor-month
      "tM" cal-tex-cursor-month-landscape
      "tw1" cal-tex-cursor-week
      "tw2" cal-tex-cursor-week2
      "tw3" cal-tex-cursor-week-iso
      "tw4" cal-tex-cursor-week-monday
      "ty" cal-tex-cursor-year
      "tY" cal-tex-cursor-year-landscape
      )
    )
  (with-eval-after-load "conf-mode"
    (setcdr conf-mode-map nil)

    (user-define-keys conf-mode-map
      "\M-m" user-conf-mode-map)
    (user-define-keys user-conf-mode-map
      "\M-m-." conf-unix-mode
      "s" conf-space-keywords
      ";" conf-colon-mode
      "'" conf-quote-normal
      "=" conf-align-assignments
      )
    )
  (with-eval-after-load "dabbrev"
    (user-define-keys user-edit-map
      "e" dabbrev-expand
      )
    )
  (with-eval-after-load "dictem"
    (setcdr dictem-mode-map nil)

    (user-define-keys dictem-mode-map
      "q" previous-multiframe-window
      "Q" user-kill-buffer
      ;;"Q" dictem-kill-all-buffers
      ;; "q" dictem-quit
      "b" dictem-last
      "?" dictem-help
      "s" dictem-run-search
      "m" dictem-run-match
      "d" dictem-run-define
      "\r" dictem-run-define
      "i" dictem-run-show-info
      "I" dictem-run-show-server
      "n" dictem-next-section
      "p" dictem-previous-section
      "j" dictem-next-link
      "k" dictem-previous-link
      "M" dictem-hyperlinks-menu
      "J" scroll-up
      "\s" scroll-up
      "K" scroll-down
      "\d" scroll-down
      "\t" forward-word
      [backtab] backward-word
      )
    )
  (with-eval-after-load "diff-mode"
    (setcdr diff-mode-map nil)
    (setcdr diff-mode-shared-map nil)

    (user-define-keys user-buffer-map
      "=" diff-buffer-with-file
      )

    (user-define-keys user-query-map
      "=" diff-backup
      )

    (user-define-keys diff-mode-map
      "L" diff-ignore-whitespace-hunk
      "d" diff-hunk-kill
      "D" diff-file-kill
      "E" diff-ediff-patch
      "\s" diff-refine-hunk
      "f" diff-goto-source
      "z" diff-split-hunk
      "i" next-error-internal
      "l" diff-file-next
      "J" scroll-up
      "K" scroll-down
      "j" diff-hunk-next
      "h" diff-file-prev
      "k" diff-hunk-prev
      "w" diff-restrict-view
      "n" next-error
      "N" next-error-no-select
      "p" previous-error
      "P" previous-error-no-select
      "q" user-kill-buffer
      "r" diff-reverse-direction
      "s" diff-apply-hunk
      "\r" next-error-follow-minor-mode
      "T" diff-test-hunk
      "t" first-error
      "u" diff-undo
      "v" diff-context->unified
      "V" diff-unified->context
      "W" widen
      )
    )
  (with-eval-after-load "ediff-mult"
    (user-define-keys ediff-dir-diffs-buffer-map
      "b" ediff-bury-dir-diffs-buffer
      "j" next-line
      "k" previous-line
      "c" ediff-dir-diff-copy-file
      )
    )
  (with-eval-after-load "edmacro"
    (user-define-keys user-macro-map
      "e" edit-kbd-macro
      )

    (user-define-keys edmacro-mode-map
      "M-q" edmacro-finish-edit
      "M-'" edmacro-insert-key
      )
    )
  (with-eval-after-load "fill"
    (user-define-keys user-edit-map
      "p" fill-paragraph
      "P" fill-region
      )
    )
  (with-eval-after-load "fold-dwim_"
    (user-define-keys user-view-map
      ;;"\t" fold-dwim-toggle
      ;; "b" (lambda()
      ;;       (interactive)
      ;;       (if current-prefix-arg
      ;;     (fold-dwim-show)
      ;;         (fold-dwim-hide)))
      ;; "a" (lambda()
      ;;       (interactive)
      ;;       (if current-prefix-arg
      ;;     (fold-dwim-show-all)
      ;;         (fold-dwim-hide-all)))
      )
    )
  (with-eval-after-load "hide-comnt"
    (user-define-keys user-view-map
      "c" hide/show-comments-toggle
      )
    )
  (with-eval-after-load "hide-region"
    (user-define-keys user-view-map
      "r" hide-region-hide
      "R" hide-region-unhide
      )
    )
  (with-eval-after-load "indent"
    (user-define-keys global-map
      "\t" indent-for-tab-command
      [backtab] indent-region
      )
    (user-define-keys user-edit-map
      "w" indent-rigidly
      "W" indent-region
      )
    )
  (with-eval-after-load "iso-transl"
    (setcdr key-translation-map nil)

    (user-define-keys global-map
      [?\A-l] iso-transl-ctl-x-8-map
      )
    )
  (with-eval-after-load "kmacro"
    ;; (define-key user-macro-map (kbd "k") 'kmacro-start-macro)
    (user-define-keys user-macro-map
      "k" kmacro-start-macro-or-insert-counter
      "K" kmacro-end-or-call-macro
      "]" kmacro-cycle-ring-previous
      "L" kmacro-name-last-macro
      "l" kmacro-view-macro-repeat
      "b" kmacro-bind-to-key
      "d" kmacro-delete-ring-head
      "c" kmacro-swap-ring
      "\[" kmacro-cycle-ring-next
      "E" kmacro-step-edit-macro
      )
    )
  (with-eval-after-load "ll-debug"
    (user-define-keys debug-map
      "r" ll-debug-toggle-comment-region-or-line
      "u" ll-debug-uncomment-region-or-line
      "p" ll-debug-copy-and-comment-region-or-line
      "d" ll-debug-revert
      "i" ll-debug-insert
      )
    )
  (with-eval-after-load "log-edit"
    (setcdr log-edit-mode-map nil)

    (user-define-keys log-edit-mode-map
      "q" log-edit-done
      "I" log-edit-insert-changelog
      "d" log-edit-show-diff
      "f" log-edit-show-files
      "j" log-edit-next-comment
      "k" log-edit-previous-comment
      "/" log-edit-comment-search-forward
      "?" log-edit-comment-search-backward
      "h" log-edit-mode-help
      )
    )
  (with-eval-after-load "log-view"
    (setcdr log-view-mode-map nil)

    (set-keymap-parent log-view-mode-map user-point-map)

    (user-define-keys log-view-mode-map
      "t" scroll-down
      "b" scroll-up
      "m" log-view-toggle-mark-entry
      "c" log-view-modify-change-comment
      "d" log-view-diff
      "D" log-view-diff-changeset
      "l" log-view-annotate-version
      "/" log-view-find-revision
      "j" log-view-msg-next
      "k" log-view-msg-prev
      "J" log-view-file-next
      "K" log-view-file-prev
      "." log-view-toggle-entry-display
      "q" kill-this-buffer
      )
    )
  (with-eval-after-load "macros"
    (user-define-keys user-macro-map
      "R" apply-macro-to-region-lines
      "i" insert-kbd-macro
      "B" name-last-kbd-macro
      "/" kbd-macro-query
      )
    )
  (with-eval-after-load "mailcap"
    (user-define-keys user-eval-map
      "%" mailcap-parse-mailcaps
      )
    )
  (with-eval-after-load "markdown"
    (setcdr markdown-mode-map nil)

    )
  (with-eval-after-load "make-mode"
    (setcdr makefile-mode-map nil)

    (user-define-keys makefile-mode-map
      "\M-M" makefile-map)
    (user-define-keys makefile-map
      "i" makefile-insert-target-ref
      "f" makefile-pickup-filenames-as-targets
      "b" makefile-switch-to-browser
      "c" comment-region
      "p" makefile-pickup-everything
      "u" makefile-create-up-to-date-overview
      "g" makefile-insert-gmake-function
      "\\" makefile-backslash-region
      "ma" makefile-automake-mode
      "mb" makefile-bsdmake-mode
      "mg" makefile-gmake-mode
      "mi" makefile-imake-mode
      "mm" makefile-mode
      "mp" makefile-makepp-mode
      "P" makefile-previous-dependency
      "N" makefile-next-dependency
      "\t" completion-at-point
      )

    (if makefile-electric-keys
        (progn
          (user-define-keys makefile-map
            "$" makefile-insert-macro-ref
            ":" makefile-electric-colon
            "=" makefile-electric-equal
            "." makefile-electric-dot
            )))

    (user-define-keys makefile-browser-map
      "n" makefile-browser-next-line
      "p" makefile-browser-previous-line
      " " makefile-browser-toggle
      "i" makefile-browser-insert-selection
      "I" makefile-browser-insert-selection-and-quit
      "\\" makefile-browser-insert-continuation
      "q" makefile-browser-quit
      )

    )
  (with-eval-after-load "mingus"
    (setcdr mingus-*-map nil)
    (setcdr mingus-help-map nil)
    ;; (setcdr mingus-sexp-map nil)
    (setcdr mingus-browse-map nil)
    (setcdr mingus-global-map nil)
    (setcdr mingus-playlist-map nil)

    (user-define-keys mingus-global-map
      "j" next-line
      "k" previous-line
      )
    ;;        "!" mingus-update
    ;;        "%" mingus-crossfade
    ;;        "(" mingus-enable-output
    ;;        ")" mingus-disable-output
    ;;        "+" mingus-vol-up
    ;;        "," mingus-consume
    ;;        "-" mingus-vol-down
    ;;        "." mingus-single
    ;;        "?" mingus-show-version
    ;;        "@" mingus-toggle
    ;;        "\r" mingus-play
    ;;        "^" mingus-wake-up-call
    ;;        "A" mingus-load-all
    ;;        "c" mingus-crop
    ;;        "D" mingus-remove-playlist
    ;;        "F" mingus-browse
    ;;        "f" mingus-load-playlist
    ;;        "g" mingus-browse-to-song-at-p
    ;;        "g" mingus-goto-current-song
    ;;        "i" mingus-insert
    ;;        "I" mingus-insert-and-play
    ;;        "m" mingus-unset-insertion-point
    ;;        "O" mingus-add-podcast
    ;;        "o" mingus-add-stream
    ;;        "q" mingus-git-out
    ;;        "R" mingus-random
    ;;        "r" mingus-refresh
    ;;        "r" mingus-repeat
    ;;        "s" mingus-save-playlist
    ;;        "X" mingus-clear
    ;;        "x" mingus-stop
    ;;        "z" mingus-shuffle
    (user-define-keys (mingus-seek-map "v" mingus-global-map)
      "g" mingus-seek-percents
      "l" mingus-dired-file
      "b" mingus-seek-backward
      "v" mingus-seek
      "t" mingus-seek-from-start
      )
    (user-define-keys (mingus-query-map "/" mingus-global-map)
      "/" mingus-query-regexp
      "l" mingus-query-dir
      "?" mingus-last-query-results
      "A" (lambda ()
            (interactive)
            (mingus-query-dir "artist"))
      "B" (lambda ()
            (interactive)
            (mingus-query-dir "album"))
      "F" (lambda ()
            (interactive)
            (mingus-query-dir "filename"))
      "T" (lambda ()
            (interactive)
            (mingus-query-dir "title"))
      )
    (user-define-keys (mingus-mark-map "m" mingus-global-map)
      "m" mingus-mark-regexp
      "s" mingus-mark-sexp
      "r" mingus-mark-region
      "M" mingus-unmark-all
      "p" mingus-set-insertion-point
      "." mingus-toggle-marked
      "!" mingus-update-thing-at-p
      )
    (user-define-keys (mingus-help-map "\M-h" mingus-global-map)
      "f" (lambda ()
            (interactive)
            (dired mingus-mpd-root))
      )
    (user-define-keys (mingus-browse-map "G" mingus-global-map)
      "l" mingus-down-dir-or-play-song
      "s" mingus-browse-sort
      "!" mingus-update-thing-at-p
      "r" mingus-refresh
      )

    (set-keymap-parent mingus-playlist-map mingus-global-map)

    (user-define-keys mingus-playlist-map
      "x" mingus-del-dwim
      "X" mingus-del-dwim2
      "X" mingus-del-other-songs
      "m" mingus-move-all
      ;; "k" mingus-move-up
      ;; "j" mingus-move-down
      "X" mingus-del-marked
      )
    (user-define-keys (mingus-bookmark-map "G" mingus-global-map)
      "f" mingus-bookmark-jump
      "a" mingus-bookmark-set
      "x" mingus-bookmark-delete
      )
    )
  (with-eval-after-load "minimap"
    (setcdr minimap-mode-map nil)

    (user-define-keys user-mode-map
      "\M-w" minimap-toggle
      )
    (user-define-keys minimap-mode-map
      )
    )
  (with-eval-after-load "mule-cmds"
    (setcdr mule-keymap nil)

    (user-define-keys mule-map
      "." toggle-input-method
      "f" set-buffer-file-coding-system
      "r" revert-buffer-with-coding-system
      "F" set-file-name-coding-system
      "t" set-terminal-coding-system
      "k" set-keyboard-coding-system
      "b" set-buffer-process-coding-system
      "s" set-selection-coding-system
      "S" set-next-selection-coding-system
      "i" set-input-method
      "," universal-coding-system-argument
      "l" set-language-environment
      "?" (lambda ()
            (interactive)
            (describe-variable 'default-input-method))
      )
    )
  (with-eval-after-load "multi-region"
    (setcdr multi-region-map nil)

    (user-define-keys user-mark-map
      "d" multi-region-mark-region
      ";" multi-region-execute-command
      "D" multi-region-unmark-region
      "\M-d" multi-region-unmark-regions
      )
    )
  (with-eval-after-load "newcomment"
    (user-define-keys user-edit-map
      "\r" indent-new-comment-line
      ";" comment-dwim
      )
    )
  (with-eval-after-load "pastebox"
    (user-define-keys uri-map
      "P" pastebox-buffer
      "p" pastebox-region
      )
    )
  (with-eval-after-load "re-builder"
    (setcdr reb-mode-map nil)
    (setcdr reb-lisp-mode-map nil)
    (setcdr reb-subexp-mode-map nil)

    ;; (set-keymap-parent reb-subexp-mode-map reb-mode-map)
    ;; (set-keymap-parent reb-lisp-mode-map reb-mode-map)

    (user-define-keys reb-mode-map
      "\r" reb-mode-2-map)
    (user-define-keys reb-mode-2-map
      "i" reb-toggle-case
      "q" reb-quit
      "k" reb-copy
      "j" reb-next-match
      "k" reb-prev-match
      "t" reb-change-syntax
      "s" reb-enter-subexp-mode
      "b" reb-change-target-buffer
      "u" reb-force-update
      )
    (user-define-keys reb-subexp-mode-map
      "q" reb-quit-subexp-mode
      )
    )
  (with-eval-after-load "sendmail"
    (user-define-keys mail-mode-map
      ;; "" mail-
      )
    )
  (with-eval-after-load "shr"
    (setcdr shr-map nil)

    (user-define-keys shr-map
      "." shr-show-alt-text
      "i" shr-browse-image
      "I" shr-insert-image
      "y" shr-copy-url
      "v" shr-browse-url
      "s" shr-save-contents
      )
    )
  (with-eval-after-load "sort"
    ;; (user-define-keys (sort-map "s" user-edit-map)
    )
  (with-eval-after-load "tabulated-list"
    ;; (setcdr tabulated-list-mode-map nil)

    ;; (set-keymap-parent tabulated-list-mode-map button-buffer-map)

    (user-define-keys tabulated-list-mode-map
      "j" next-line
      "k" previous-line
      "q" user-kill-buffer
      "r" list-processes
      )
    )
  (with-eval-after-load "thing-cmds"
    ;; (user-define-keys user-mark-map
    ;; "S" mark-thing
    ;; "t" select-thing-near-point)
    )
  (with-eval-after-load "transpose-frame_"
    (user-define-keys user-window-map
      "t" transpose-frame
      "f" flip-frame
      "F" flop-frame
      ">" rotate-frame-clockwise
      "<" rotate-frame-anticlockwise
      "R" rotate-frame
      )
    )
  (with-eval-after-load "twittering-mode"
    (setcdr twittering-mode-map nil)

    (user-define-keys twittering-mode-map
      "/" twittering-search
      "\r" twittering-enter
      "\t" twittering-goto-next-thing
      "B" twittering-goto-last-status
      "g" twittering-current-timeline
      "h" twittering-switch-to-previous-timeline
      "I" twittering-icon-mode
      "j" twittering-goto-next-status
      "J" twittering-scroll-up
      "k" twittering-goto-previous-status
      "K" twittering-scroll-down
      "l" twittering-switch-to-next-timeline
      "n" twittering-goto-next-status-of-user
      "p" twittering-goto-previous-status-of-user
      "Q" twittering-kill-buffer
      "q" twittering-stop
      "r" twittering-toggle-show-replied-statuses
      "T" twittering-goto-first-status
      "X" twittering-erase-old-statuses
      "Y" twittering-push-uri-onto-kill-ring
      [backtab] twittering-goto-next-thing
      "#" twittering-set-current-hashtag
      )

    (user-define-keys (twittering-exec-map "x" twittering-mode-map)
      "d" twittering-delete-status
      )

    (user-define-keys (twittering-view-map "v" twittering-mode-map)
      "o" twittering-other-user-timeline
      "O" twittering-visit-timeline
      "r" twittering-toggle-show-replied-statuses
      "R" twittering-toggle-or-retrieve-replied-statuses
      )

    )
  (with-eval-after-load "wid-edit"
    (setcdr widget-keymap nil)
    (setcdr widget-text-keymap nil)
    (setcdr widget-field-keymap nil)

    (user-define-keys widget-keymap
      "\t" widget-forward
      [backtab] widget-backward
      "\r" widget-button-press
      "y" widget-kill-line
      )
    (user-define-keys widget-field-keymap
      "`" widget-complete
      "\s" widget-field-activate
      ;; scroll-up
      ;; scroll-down
      ;; beginning-of-line
      ;; widget-end-of-line
      ))
  )

(provide 'user-kmap)
