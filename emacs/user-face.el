;; '((((class color) (background light))
;;    :background "lightblue")
;;   (((class color) (background dark))
;;    :background "steelblue4")
;;   (t
;;    :inverse-video t))

(setq frame-background-mode 'dark)
(setq frame-terminal-default-bg-mode 'nil)
(defvar user-faces-bg 'dark)

(defun user-set-face (l)
  (dolist (e l)
    (apply 'set-face-attribute `(,(car e) nil ,@(cdr e)))))

(progn ;; User
  (with-eval-after-load 'user
    (with-eval-after-load "bindings"
      (cond ((eq 'dark user-faces-bg)
             (user-set-face
              '((user-modeline :foreground "color-144" :background "black" :inherit mode-line)
                (user-modeline-buffer :foreground "color-207" :background nil :inherit user-modeline)
                (user-modeline-byte :foreground "color-144" :background nil)
                (user-modeline-column :foreground "color-34" :background nil)
                (user-modeline-column-total :foreground "color-34" :background nil)
                (user-modeline-dedicated :foreground "color-32" :background nil)
                (user-modeline-frame :foreground "color-27" :background nil)
                (user-modeline-line :foreground "color-34" :background nil)
                (user-modeline-line-total :foreground "color-34" :background nil)
                (user-modeline-mode :foreground "cyan" :background nil)
                (user-modeline-modes :foreground "color-147" :background nil)
                (user-modeline-modified :foreground "color-208" :background nil)
                (user-modeline-directory  :foreground "color-255" :background nil)
                (user-modeline-narrow :foreground "color-208" :background nil)
                (user-modeline-page :foreground "color-34" :background nil)
                (user-modeline-page-total :foreground "color-34" :background nil)
                (user-modeline-readonly :foreground "color-208" :background nil)
                (user-modeline-region :foreground "color-72" :background nil)
                (user-modeline-scroll :foreground "color-32" :background nil)
                (user-modeline-vc :foreground "color-32" :background nil)
                (user-modeline-separator :foreground "brightblack" :background nil)
                (user-modeline-process :foreground "yellow" :background nil)
                (user-modeline-inactive :foreground "color-240" :background "black" :inherit mode-line-inactive)
                )))))
    ))
(progn ;; Internal
  (with-eval-after-load 'faces
    (cond ((eq 'dark user-faces-bg)
           (user-set-face
            '((default :foreground "white" :distant-foreground "white" :background "black" :weight normal)
              (fringe :underline nil :foreground "white" :background nil)
              (link :underline nil :foreground "cyan" :background nil)
              (cursor :inherit default)
              (shadow :inherit default)
              (region :background "color-238" :inherit default)
              (highlight :inherit region)
              (secondary-selection :inherit default)
              (mode-line :foreground "brightblack" :background "black")
              (mode-line-inactive :foreground "color-240" :background "black" :inherit mode-line)
              (mode-line-highlight :foreground "color-255" :background "black" :inherit mode-line)
              (mode-line-emphasis :background "white" :inherit mode-line)
              (show-paren-match :foreground "darkgreen" :background nil :weight bold)
              (show-paren-mismatch :foreground "white" :background "red" :weight normal)
              (header-line :foreground "cyan" :underline nil)
              (variable-pitch :foreground nil :background nil)
              (escape-glyph :inherit default)
              (nobreak-space :inherit escape-glyph :underline nil)
              )))
          ((eq 'light user-faces-bg)
           (user-set-face
            '((default :foreground "black" :background "white")
              )))))
  (with-eval-after-load 'simple
    (user-set-face
     '((next-error :foreground nil :background nil)
       )))
  (with-eval-after-load 'rect
    (user-set-face
     '((rectangle-preview :foreground nil :background nil)
       )))
  (with-eval-after-load 'diff-mode
    (user-set-face
     '((diff-function :foreground nil :background nil)
       (diff-file-header :foreground nil :background nil)
       (diff-header :foreground nil :background nil)
       (diff-hunk-header :foreground nil :background nil)
       (diff-index :foreground nil :background nil))))
  (with-eval-after-load 'sh-script
    (user-set-face
     '((sh-heredoc :foreground "yellow" :background nil :weight normal)
       (sh-escaped-newline :inherit font-lock-string-face)
       (sh-quoted-exec :foreground "salmon" :background nil))))
  (with-eval-after-load 'minibuffer
    (user-set-face
     '((minibuffer-prompt :foreground "gray" :background nil)
       (completions-common-part :foreground "yellow")
       (completions-first-difference :weight normal))))
  (with-eval-after-load 'font-lock
    (user-set-face
     '((font-lock-doc-face :foreground "color-176" :background nil)
       (font-lock-string-face :foreground "color-38" :background nil)
       (font-lock-variable-name-face :foreground "color-208" :background nil)
       (font-lock-keyword-face :foreground "yellow" :background nil :weight normal)
       (font-lock-builtin-face :foreground "color-161" :background nil :weight normal)
       ;;(font-lock-comment-face :foreground "color-244" :background nil :weight normal)
       (font-lock-comment-face :foreground "color-250" :background nil :weight normal)
       (font-lock-comment-delimiter-face :inherit font-lock-comment-face)
       (font-lock-type-face :foreground "color-70" :background nil)
       (font-lock-constant-face :foreground "color-160" :background nil)
       (font-lock-warning-face :foreground "color-196" :background nil)
       (font-lock-negation-char-face :foreground "gray" :background nil)
       (font-lock-preprocessor-face :foreground "color-124" :background nil)
       (font-lock-regexp-grouping-backslash :foreground "color-142" :background nil)
       (font-lock-regexp-grouping-construct :weight normal :foreground "color-130" :background nil)
       (font-lock-function-name-face :foreground "color-34" :background nil :weight normal))))
  (with-eval-after-load 'hideshow
    (user-set-face
     '((hs-overlay-code :foreground "color-245")
       (hs-overlay-comment :inherit font-lock-comment-face))))
  (with-eval-after-load 'smerge-mode
    (user-set-face
     '((smerge-markers :weight bold :background nil :foreground nil)
       (smerge-base :background nil :foreground "brightyellow")
       (smerge-mine :background nil :foreground "brightgreen")
       (smerge-other :background nil :foreground "brightred")
       (smerge-refined-added :background nil :foreground "green")
       (smerge-refined-changed :background nil :foreground "yellow")
       (smerge-refined-removed :background nil :foreground "red"))))
  (with-eval-after-load 'company
    (user-set-face
     '((company-tooltip :foreground "white" :background "color-234")
       (company-tooltip-selection :foreground "yellow" :background nil)
       (company-tooltip-common :foreground "white" :background )
       (company-tooltip-annotation :foreground "red" :background nil)
       (company-scrollbar-bg :foreground "white" :background nil)
       (company-preview :foreground "white" :background nil)
       (company-tooltip-mouse :foreground "white" :background nil)
       (company-preview-common :foreground "white" :background nil)
       (company-preview-search :foreground "white" :background nil)
       (company-tooltip-search :foreground "white" :background nil)
       (company-tooltip-search-selection :foreground "white" :background nil)
       (company-tooltip-common-selection :foreground "yellow" :background nil))))
  (with-eval-after-load 'log-edit
    (user-set-face
     '((log-edit-summary :inherit font-lock-function-name-face)
       (log-edit-header :inherit font-lock-keyword-face)
       (log-edit-unknown-header :inherit font-lock-comment-face)
       )))
  (with-eval-after-load "isearch"
    (user-set-face
     '((isearch :foreground "color-237" :background "color-184")
       (isearch-fail :foreground "red" :background "black")
       (lazy-highlight :foreground "color-184" :background "color-237")
       )))
  (with-eval-after-load 'gnus
    (with-eval-after-load 'gnus-group
      (user-set-face
       '((gnus-group-mail-low :foreground "green" :weight normal)
         (gnus-group-mail-low-empty :foreground "gray" :weight normal)
         (gnus-group-mail-1-empty :foreground nil :weight nil :inherit gnus-group-mail-low-empty)
         (gnus-group-mail-2 :foreground nil :weight nil :inherit gnus-group-mail-low)
         (gnus-group-mail-2-empty :foreground nil :weight nil :inherit gnus-group-mail-low-empty)
         (gnus-group-mail-3 :foreground nil :weight nil :inherit gnus-group-mail-low)
         (gnus-group-mail-3-empty :foreground nil :weight nil :inherit gnus-group-mail-low-empty)
         (gnus-group-news-low :foreground "green" :weight normal)
         (gnus-group-news-low-empty :foreground "gray" :weight normal)
         (gnus-group-news-1-empty :foreground nil :weight nil :inherit gnus-group-news-low-empty)
         (gnus-group-news-2 :foreground nil :weight nil :inherit gnus-group-news-low)
         (gnus-group-news-2-empty :foreground nil :weight nil :inherit gnus-group-news-low-empty)
         (gnus-group-news-3 :foreground nil :weight nil :inherit gnus-group-news-low)
         (gnus-group-news-3-empty :foreground nil :weight nil :inherit gnus-group-news-low-empty)
         ))))
  (with-eval-after-load 'hl-line
    (user-set-face
     '((hl-line :foreground "white" :background "red")
       )))
  (with-eval-after-load "replace"
    (user-set-face
     '((match :foreground "yellow" :background "black")
       )))
  (with-eval-after-load 'rcirc
    (user-set-face
     '((rcirc-timestamp :foreground "brightblack")
       (rcirc-server :foreground "color-166")
       (rcirc-other-nick :foreground "color-217")
       (rcirc-url :foreground "color-38")
       (rcirc-nick-in-message :foreground "green")
       (rcirc-nick-in-message-full-line :weight normal :foreground nil :background nil)
       (rcirc-track-nick :foreground "green")
       (rcirc-keyword :foreground "green" :background nil)
       ;;rcirc-bright-nick
       ;;rcirc-dim-nick
       ;;rcirc-server-prefix
       ;;rcirc-nick-in-message
       ;;rcirc-prompt
       ;;rcirc-my-nick
       ;;rcirc-other-nick
       ;;rcirc-bright-nick
       ;;rcirc-dim-nick
       ;;rcirc-server-prefix
       ;;rcirc-prompt

       ;;rcirc-track-keyword

       )))
  (with-eval-after-load 'erc
    (user-set-face
     '((erc-notice-face :weight normal :foreground "gray" :background nil)
       (erc-prompt-face :weight normal :foreground "white" :background nil)))
    (with-eval-after-load 'erc-match
      (user-set-face
       '((erc-current-nick-face :weight normal :foreground "cyan"))))
    (with-eval-after-load 'erc-stamp
      (user-set-face
       '((erc-timestamp-face :weight normal :foreground "gray" :background nil))))
    )
  (with-eval-after-load 'flyspell
    (user-set-face
     '((flyspell-incorrect :underline nil :foreground "brightred")
       )))
  )
(progn ;; External
  (with-eval-after-load 'notmuch
    (user-set-face
     '((notmuch-search-unread-face :foreground nil :background nil :weight normal)
       (notmuch-hello-logo-background :foreground nil :background nil :weight normal)
       (notmuch-message-summary-face :foreground nil :background nil)
       ))
    )
  (with-eval-after-load 'smartparens
    (user-set-face
     '((sp-pair-overlay-face :foreground "white" :background nil :inherit nil)
       (sp-wrap-overlay-face :foreground "red" :background nil)
       (sp-wrap-tag-overlay-face :foreground "red" :background nil)
       (sp-show-pair-match-face :foreground "red" :background nil)
       (sp-show-pair-mismatch-face :foreground "red" :background nil)
       (sp-show-pair-enclosing :foreground "red" :background nil)
       )))
  (with-eval-after-load 'rainbow-delimiters
    (user-set-face
     '((rainbow-delimiters-unmatched-face :foreground "white" :background "red" :weight bold)
       (rainbow-delimiters-mismatched-face :foreground "white" :background "red" :weight bold)
       (rainbow-delimiters-depth-1-face :foreground "white" :background nil :weight normal)
       (rainbow-delimiters-depth-2-face :foreground "color-42" :background nil :weight normal)
       (rainbow-delimiters-depth-3-face :foreground "color-43" :background nil :weight normal)
       (rainbow-delimiters-depth-4-face :foreground "color-44" :background nil :weight normal)
       (rainbow-delimiters-depth-5-face :foreground "color-45" :background nil :weight normal)
       (rainbow-delimiters-depth-6-face :foreground "color-46" :background nil :weight normal)
       (rainbow-delimiters-depth-7-face :foreground "color-47" :background nil :weight normal)
       (rainbow-delimiters-depth-8-face :foreground "color-48" :background nil :weight normal)
       (rainbow-delimiters-depth-9-face :foreground "color-49" :background nil :weight normal))))
  (with-eval-after-load 'comint
    (user-set-face
     '((comint-highlight-input :weight normal)
       (comint-highlight-prompt :inherit minibuffer-prompt)
       )))
  (with-eval-after-load 'magit
    (user-set-face
     '((magit-hash :weight normal :background nil :foreground nil)
       (magit-branch-current :weight normal :foreground "green")
       (magit-branch-local :weight normal :foreground "color-112")
       (magit-branch-remote :foreground "color-154")
       (magit-diff-base :background nil :foreground nil)
       (magit-diff-base-highlight :weight normal :background nil :foreground nil)
       (magit-diff-conflict-heading :weight normal :background nil :foreground "brightred")
       (magit-diff-context :background nil :foreground "color-250")
       (magit-diff-context-highlight :weight normal :background nil :foreground "color-250")
       (magit-diff-file-heading :weight normal :background nil :foreground nil)
       (magit-diff-file-heading-highlight :weight bold :background nil :foreground nil)
       (magit-diff-file-heading-selection :weight bold :background nil :foreground nil)
       (magit-diff-hunk-heading :background nil :foreground "yellow")
       (magit-diff-hunk-heading-highlight :weight bold :background nil :foreground "yellow")
       (magit-diff-hunk-heading-selection :weight normal :background nil :foreground "yellow")
       (magit-diff-lines-boundary :weight normal :background nil :foreground nil)
       (magit-diff-lines-heading :weight normal :background nil :foreground nil)
       (magit-diff-our :background nil :foreground "green")
       (magit-diff-our-highlight :weight normal :background nil :foreground "green")
       (magit-diff-added :background "brightblack" :foreground "color-40")
       (magit-diff-added-highlight :weight normal :background "brightblack" :foreground nil)
       (magit-diff-removed :background "brightblack" :foreground "color-111")
       (magit-diff-removed-highlight :weight normal :background "brightblack" :foreground "color-111")
       (magit-diff-their :background "brightblack" :foreground "yellow")
       (magit-diff-their-highlight :weight normal :background "brightblack" :foreground "yellow")
       (magit-header-line :weight normal :foreground "white")
       (magit-section-heading :weight normal :background nil :foreground "green")
       (magit-section-heading-selection :weight normal :background nil :foreground "green")
       (magit-section-secondary-heading :weight normal :background nil :foreground "green")
       (magit-section-highlight :weight bold :background nil :foreground "green")
       )))
  (with-eval-after-load 'auto-complete
    (user-set-face
     '((ac-candidate-face :foreground "color-208" :background "black")
       (ac-selection-face :foreground "cyan" :background "black")
       (ac-completion-face :underline t :foreground "color-244")
       )))
  (with-eval-after-load 'popup
    (user-set-face
     '((popup-scroll-bar-background-face :foreground nil :background nil)
       (popup-scroll-bar-foreground-face :foreground nil :background nil)
       (popup-face :foreground "white" :background nil)
       (popup-tip-face :foreground "white" :background "color-233")
       )))
  (with-eval-after-load 'flycheck
    (user-set-face
     '((flycheck-error :weight normal :underline t :background nil :foreground "red")
       (flycheck-warning :weight normal :underline t :background nil :foreground "orange")
       (flycheck-info :weight normal :underline t :background nil :foreground "green")
        ))
    )
  )

(provide 'user-face)
