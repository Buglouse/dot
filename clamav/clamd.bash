clamscan() {
    mkdir -p "${DIR_TMP_DATA}/clamd/infected" 2>/dev/null

    freshclam --verbose --show-progress \
              --log="${DIR_TMP_LOG}/freshclamav.log" --config-file "${DIR_CONF}/.clamav/freshclam.conf"
    printf "Starting ClamAV Scan\n"
    $(type -P clamscan) --recursive=yes --verbose --infected --remove=yes \
                        --database="${DIR_CONF}/.clamav/" --move="${DIR_TMP_DATA}/clamd/infected" "${@}"
}
