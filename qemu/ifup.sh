#!/bin/env sh
set -x
#http://suhu.dlinkddns.com/Presentation/20150203/
#https://wiki.gentoo.org/wiki/QEMU

[[ -n ${1} ]] || { printf 'Specify tap dev'; exit; }
# Forward IP traffic on host.
# Add network device for guest.
# Add bridge for guest to host.
# Route guest through host.
## Filter guest through host masquerade.
# TUN
# IP layer (ip_forward)
# TAP
# MAC layer (bridge, mac broadcast)
bridge="br0"
user="${USER_NAME:-$(id -un)}"
route=( $(ip route list scope global) )
dev="${route[4]}"
host="${route[2]}"

# TAP
# Add Bridge
if [[ ! -e /sys/class/net/${bridge} ]]; then
    sudo sysctl -w net.ipv4.ip_forward=1

    sudo ip link add "${bridge}" type bridge
    # Flush ip of local
    sudo ip addr flush dev "${dev}"
    # Add local to bridge
    [[ ${dev} == ${bridge} ]] || ip link set "${dev}" master "${bridge}"
    sudo ip link set "${bridge}" up
    sudo dhcpcd --rebind "${bridge}"
fi
[[ -e /sys/class/net/${1} ]] && ip link del "${1}"
sudo ip tuntap add "${1}" mode tap #user "${user}"
# TAP to bridge
sudo ip link set "${1}" master "${bridge}"
# Set UP
#ip link set "${1}" up promisc on
sudo ip link set "${1}" up

#nft add table nat
#nft add chain nat prerouting { type nat hook prerouting priority 0 \; }
#nft add chain nat postrouting { type nat hook postrouting priority 100 \; }
#nft add rule nat postrouting oifname "${dev}" masquerade

#ip route add default scope host "${host}" dev "${bridge}"
#ip route add default gw "${host}" "${bridge}"

