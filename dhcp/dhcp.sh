#!/bin/sh
RESOLVE="/etc/resolv.conf"
RESOLVE_HEAD="/etc/resolv.head"
NTP="/etc/ntp.conf"
[[ -n ${broadcast} ]] && BROADCAST="broadcast ${broadcast}"
[[ -n ${subnet} ]] && NETMASK="netmask ${subnet}"
CFG="/var/dhcp.set"
[[ -z ${1} ]] && { printf 'No argument\n'; exit 1; }
case "${1}" in
    defconfig) # dev without ip
        ip addr flush dev ${interface}
        ip link set ${interface} up
        ;;
    leasefail|nak) #
        ;;
    bound|renew)
        set >"${CFG}"
        ip addr flush dev ${interface}
        ip addr add ${ip}/${mask} dev ${interface}
        [[ ${router} ]] && ip route add default via ${router%% *} dev ${interface}

        [[ -f ${RESOLVE} ]] && mv -f "${RESOLVE}" "${RESOLVE}.$(date +%m%d-%H%M)"
        [[ -e ${RESOLVE_HEAD} ]] && cat "${RESOLVE_HEAD}" >"${RESOLVE}"
        [[ ${domain} ]] && printf 'search %s\n' "${domain}" >>"${RESOLVE}"
        for i in ${dns}; do
            printf 'nameserver %s\n' "${i}" >>"${RESOLVE}"
        done

        >"${NTP}"
        for i in ${ntpsrv}; do
            printf 'server %s\n' "${i}" >>"${NTP}"
        done

        ;;
esac
exit 0
