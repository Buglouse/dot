msmtp() {
    export MSMTP_LOG=${DIR_LOG}/msmtp-$(date +${FORMAT_LOG}).log
    $(type -P msmtp) --file="${HOME}/.msmtp/msmtp.conf" "${@}"
}
